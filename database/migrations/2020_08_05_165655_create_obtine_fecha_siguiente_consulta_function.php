<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObtineFechaSiguienteConsultaFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
            CREATE FUNCTION obtine_fecha_proxima_atencion(id_medical_request BIGINT)
            RETURNS DATETIME

            BEGIN
                DECLARE fecha DATETIME;

                SELECT a.next_attention_date INTO fecha
                FROM historical_requests hr
                LEFT JOIN attentions a ON hr.id = a.historical_request_id
                WHERE hr.medical_request_id = id_medical_request
                ORDER BY hr.id DESC
                LIMIT 1;

                RETURN fecha;
            END
        ";
        DB::getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP FUNCTION IF EXISTS obtine_fecha_proxima_atencion;";
        DB::getPdo()->exec($sql);
    }
}
