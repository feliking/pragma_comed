<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTracingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracings', function (Blueprint $table) {
            $table->id();
            $table->text('comment')->nullable();
            $table->date('tracing_date');
            $table->boolean('medical_attention_flag')->default(false);

            $table->foreignId('medical_request_id')->nullable()->constrained();
            $table->foreignId('treatment_stage_id')->nullable()->constrained();
            $table->foreignId('risk_type_id')->nullable()->constrained();
            $table->foreignId('user_id')->constrained();
            $table->unsignedBigInteger('project_id')->nullable();
            $table->foreignId('risk_state_id')->nullable()->constrained();
            $table->foreignId('historical_request_id')->constrained();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracings');
    }
}
