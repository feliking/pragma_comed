<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldToMedicalRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medical_requests', function (Blueprint $table) {
            $table->foreignId('risk_type_id')
                ->nullable()
                ->after('medical_request_status_id')
                ->constrained('risk_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medical_requests', function (Blueprint $table) {
            $table->dropForeign(['risk_type_id']);
            $table->dropColumn(['risk_type_id']);
        });
    }
}
