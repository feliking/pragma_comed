<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObtineFechaProximaConsultaFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
            CREATE FUNCTION obtine_fecha_proxima_consulta(id_medical_request BIGINT)
            RETURNS DATETIME

            BEGIN
                DECLARE fecha DATETIME;

                SELECT a.next_attention_date INTO fecha
                FROM appointments a
                JOIN medical_requests mr ON a.medical_request_id = mr.id
                WHERE a.medical_request_id = id_medical_request
                ORDER BY a.id DESC
                LIMIT 1;

                RETURN fecha;
            END
        ";
        DB::getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP FUNCTION IF EXISTS obtine_fecha_proxima_consulta;";
        DB::getPdo()->exec($sql);
    }
}
