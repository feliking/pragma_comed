<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttentionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attentions', function (Blueprint $table) {
            $table->id();

            $table->date('attention_date');

            $table->string('fc', '50')->nullable();
            $table->string('fr', '50')->nullable();
            $table->string('t', '50')->nullable();
            $table->string('pa', '50')->nullable();
            $table->string('sao2', '50')->nullable();
            $table->string('otros', '100')->nullable();

            $table->text('current_history')->nullable();
            $table->text('physical_exam')->nullable();

            $table->text('diagnosis')->nullable();
            $table->text('treatment')->nullable();

            $table->text('observation')->nullable();
            //Foraneas
            $table->foreignId('historical_request_id')->constrained();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attentions');
    }
}
