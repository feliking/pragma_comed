<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicalRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_requests', function (Blueprint $table) {
            $table->id();

            $table->string('code', 6)->unique();
            $table->string('ci');
            $table->enum('ci_expedition',['LP','CB','SC','CH','OR','PT','TJ','BE','PD']);
            $table->string('enterprise');
            $table->string('email')->nullable();
            $table->string('employee_code')->nullable();
            $table->string('full_name');
            $table->string('phone_number');
            $table->string('reference_full_name')->nullable();
            $table->string('reference_phone_number')->nullable();
            $table->string('short_phone_number')->nullable();
            $table->boolean('whatsapp_flag');
            $table->string('comment', 512)->nullable();

            // Foraneas - Internas
            $table->foreignId('medical_request_status_id')->constrained();
            $table->foreignId('user_id')->nullable()->constrained();

            // Foraneas - Externas
            $table->unsignedBigInteger('employee_id')->nullable();

            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_requests');
    }
}
