<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeightingLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weighting_levels', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->string('description',255)->nullable();
            $table->string('color',255);
            $table->double('minimum',8,2);
            $table->double('maximum',8,2)->nullable();
            $table->integer('evaluation');
            $table->boolean('last_level_flag')->default(false);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weighting_levels');
    }
}
