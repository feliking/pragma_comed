<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplementaryExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complementary_exams', function (Blueprint $table) {
            $table->id();

            $table->string('bill_number',25)->nullable();
            $table->double('cost',10,2)->nullable();
            $table->boolean('enterprise_flag');
            $table->boolean('result_flag')->nullable();

            $table->foreignId('tracing_id')->constrained();
            $table->foreignId('laboratory_id')->constrained();
            $table->foreignId('test_type_id')->nullable()->constrained();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complementary_exams');
    }
}
