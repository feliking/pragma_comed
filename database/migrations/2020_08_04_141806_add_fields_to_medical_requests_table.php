<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToMedicalRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medical_requests', function (Blueprint $table) {
            $table->enum('gender', ['Femenino', 'Masculino'])
                ->default('Masculino')
                ->after('comment');
            $table->date('birthday')
                ->default(null)
                ->nullable()
                ->after('gender');

            //Foraneas - Externas
            $table->unsignedBigInteger('project_id')
                ->nullable()
                ->after('employee_id');
            $table->unsignedBigInteger('health_insurance_id')
                ->default(null)
                ->nullable()
                ->after('project_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medical_requests', function (Blueprint $table) {
            $table->dropColumn(['gender', 'birthday', 'health_insurance_id']);
        });
    }
}
