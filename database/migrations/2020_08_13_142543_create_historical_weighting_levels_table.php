<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoricalWeightingLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historical_weighting_levels', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->string('description',255)->nullable();
            $table->string('color',255);
            $table->double('minimum',8,2);
            $table->double('maximum',8,2)->nullable();
            $table->integer('evaluation');
            $table->double('calc_evaluation');

            $table->foreignId('weighting_level_id')->constrained();
            $table->foreignId('tracing_id')->constrained();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historical_weighting_levels');
    }
}
