<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttentionVitalSignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attention_vital_signs', function (Blueprint $table) {
            $table->id();
            $table->date('attention_date');

            $table->string('fc', '50')->nullable();
            $table->string('fr', '50')->nullable();
            $table->string('t', '50')->nullable();
            $table->string('pa', '50')->nullable();
            $table->string('sao2', '50')->nullable();
            $table->string('otros', '100')->nullable();
            //Foraneas
            $table->foreignId('medical_request_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attention_vital_signs');
    }
}
