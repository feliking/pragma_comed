<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoricalRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historical_requests', function (Blueprint $table) {
            $table->id();

            $table->text('comment')->nullable();

            //Foraneas
            $table->foreignId('medical_request_id')->constrained();
            $table->foreignId('user_id')->nullable()->constrained();
            $table->foreignId('medical_request_status_id')->constrained();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historical_requests');
    }
}
