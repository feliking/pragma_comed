<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddResultFlagToComplementaryExams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('complementary_exams', function (Blueprint $table) {
            $table->boolean('result_flag')
                ->nullable()
                ->after('enterprise_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('complementary_exams', function (Blueprint $table) {
            $table->dropColumn('result_flag');
        });
    }
}
