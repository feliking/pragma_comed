<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\MedicalRequest;
use Faker\Generator as Faker;

$factory->define(MedicalRequest::class, function (Faker $faker) {
    return [
        'code' => $faker->unique()->numberBetween(111111,999999),
        'ci' => $faker->unique()->numberBetween(1111111,9999999),
        'ci_expedition' => $faker->randomElement(['LP','CB','SC','CH','OR','PT','TJ','BE','PD']),
        'enterprise' => $faker->randomElement(['Pragma Invest','Colina','LPL','La Paz Limpia','Trebol']),
        'email' => $faker->unique()->safeEmail,
        'full_name' => $faker->name,
        'phone_number' => $faker->phoneNumber,
        'whatsapp_flag' => $faker->boolean,
        'comment' => $faker->text(255),
        'gender' => $faker->randomElement(['Masculino', 'Femenino']),
        'birthday' => $faker->date('Y-m-d', 'now'),
        'medical_request_status_id' => 1, //SOLICITADO
        'risk_type_id' => $faker->numberBetween(1,3),
        'user_id' => null,
        'employee_id' => null,
        'project_id' => $faker->numberBetween(1, 880),
        'health_insurance_id' => $faker->numberBetween(2,3),
    ];
});
