<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [ //1
                'name'          => 'ROOT',
                'description'   => 'CONTROL SIN RESTRICCIONES',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //2
                'name'          => 'ADMIN',
                'description'   => 'DASHBOARD Y REPORTES',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //3
                'name'          => 'MÉDICO',
                'description'   => 'MÉDICO USUARIO DEL SISTEMA',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //4
                'name'          => 'ENFERMERA',
                'description'   => 'ENFERMERA PARA EL MÉDICO DEL SISTEMA',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //5
                'name'          => 'RRHH',
                'description'   => 'RRHH',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //6
                'name'          => 'RESP. DE SEGUIMIENTO',
                'description'   => 'RRHH',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //7
                'name'          => 'ADM. DE SEGUIMIENTOS',
                'description'   => 'RRHH',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //8
                'name'          => 'CALL CENTER',
                'description'   => 'ROL ESPECIFICO PARA LA ATENCIÓN DE LLAMADAS',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //9
                'name'          => 'REPORTES',
                'description'   => 'ROL QUE PARA LA VISUALIZACIÓN DE REPORTES',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //10
                'name'          => 'ADMINISTRADOR',
                'description'   => 'ROL QUE PUEDE REALIZAR TODO, INCLUSIVE EL CAMBIAR DE ESTADOS A UN CASO',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //11
                'name'          => 'ADM. DE SEGUIMIENTOS Y SEGUIMIENTO',
                'description'   => 'RRHH ADMINISTRA SEGUIMIENTO Y REALIZA SEGUIMIENTO',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
        ]);

    }
}
