<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [//1
                'username'      => 'root',
                'name'          => 'root',
                'email'         => 'soporte@pragmainvest.com.bo',
                'password'      => Hash::make('r00t-pr49mg4'),
                // 'password'      => Hash::make('r00t-c0nm3d&'),
                'role_id'       => 1,
                'employee_id'   => null,
                'rrhh_office_id'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [//2
                'username'      => 'admin',
                'name'          => 'Administrador',
                'email'         => 'soporte@pragmainvest.com.bo',
//                'password'      => Hash::make('4dm1nm4dc0n'),
                'password'      => Hash::make('123456'),
                'role_id'       => 2,
                'employee_id'   => null,
                'rrhh_office_id'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [//3
                'username'      => 'jarze',
                'name'          => 'Dr. Arze',
                'email'         => 'jarze@pragmainvest.com.bo',
                'password'      => Hash::make('123456'),
                'role_id'       => 3,
                'employee_id'   => 134,
                'rrhh_office_id'   => 2,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [//4
                'username'          => 'scapa',
                'name'              => 'Lic. Capa',
                'email'             => 'nurse@pragmainvest.com.bo',
                'password'          => Hash::make('123456'),
                'role_id'           => 4,
                'employee_id'       => 2878,
                'rrhh_office_id'    => 2,
                'created_at'        => now(),
                'updated_at'        => now()
            ],
            [//5
                'username'      => 'djau',
                'name'          => 'Seg. Djau',
                'email'         => 'jarze@pragmainvest.com.bo',
                'password'      => Hash::make('djau'),
                'role_id'       => 6,
                'employee_id'   => 131,
                'rrhh_office_id'   => 2,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [//5
                'username'      => 'djau1',
                'name'          => 'ANDREA ARIZACA',
                'email'         => 'jarze@pragmainvest.com.bo',
                'password'      => Hash::make('djau'),
                'role_id'       => 6,
                'employee_id'   => 131,
                'rrhh_office_id'   => 2,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [//5
                'username'      => 'djau2',
                'name'          => 'CRISTIAN ARCE',
                'email'         => 'jarze@pragmainvest.com.bo',
                'password'      => Hash::make('djau'),
                'role_id'       => 6,
                'employee_id'   => 131,
                'rrhh_office_id'   => 2,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [//5
                'username'      => 'djau3',
                'name'          => 'DR LIMACHI',
                'email'         => 'jarze@pragmainvest.com.bo',
                'password'      => Hash::make('djau'),
                'role_id'       => 6,
                'employee_id'   => 131,
                'rrhh_office_id'   => 2,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [//5
                'username'      => 'djau4',
                'name'          => 'DRA LUCANA',
                'email'         => 'jarze@pragmainvest.com.bo',
                'password'      => Hash::make('djau'),
                'role_id'       => 6,
                'employee_id'   => 131,
                'rrhh_office_id'   => 2,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [//5
                'username'      => 'djau5',
                'name'          => 'DRA MERUVIA',
                'email'         => 'jarze@pragmainvest.com.bo',
                'password'      => Hash::make('djau'),
                'role_id'       => 6,
                'employee_id'   => 131,
                'rrhh_office_id'   => 2,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [//5
                'username'      => 'djau6',
                'name'          => 'GLORIA ZABALETA',
                'email'         => 'jarze@pragmainvest.com.bo',
                'password'      => Hash::make('djau'),
                'role_id'       => 6,
                'employee_id'   => 131,
                'rrhh_office_id'   => 2,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [//5
                'username'      => 'djau7',
                'name'          => 'JUAN JOSE CUSI',
                'email'         => 'jarze@pragmainvest.com.bo',
                'password'      => Hash::make('djau'),
                'role_id'       => 6,
                'employee_id'   => 131,
                'rrhh_office_id'   => 2,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [//5
                'username'      => 'djau8',
                'name'          => 'KAREN LOZA',
                'email'         => 'jarze@pragmainvest.com.bo',
                'password'      => Hash::make('djau'),
                'role_id'       => 6,
                'employee_id'   => 131,
                'rrhh_office_id'   => 2,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [//5
                'username'      => 'djau9',
                'name'          => 'MERY COCHE',
                'email'         => 'jarze@pragmainvest.com.bo',
                'password'      => Hash::make('djau'),
                'role_id'       => 6,
                'employee_id'   => 131,
                'rrhh_office_id'   => 2,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [//5
                'username'      => 'djau10',
                'name'          => 'PATRICIA CAPA',
                'email'         => 'jarze@pragmainvest.com.bo',
                'password'      => Hash::make('djau'),
                'role_id'       => 6,
                'employee_id'   => 131,
                'rrhh_office_id'   => 2,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [//5
                'username'      => 'djau11',
                'name'          => 'RAMBER ACHABAL',
                'email'         => 'jarze@pragmainvest.com.bo',
                'password'      => Hash::make('djau'),
                'role_id'       => 6,
                'employee_id'   => 131,
                'rrhh_office_id'   => 2,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [//5
                'username'      => 'djau12',
                'name'          => 'RUTH CALLISAYA',
                'email'         => 'jarze@pragmainvest.com.bo',
                'password'      => Hash::make('djau'),
                'role_id'       => 6,
                'employee_id'   => 131,
                'rrhh_office_id'   => 2,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [//6
                'username'      => 'ecoronel',
                'name'          => 'Admin Seg.',
                'email'         => 'jarze@pragmainvest.com.bo',
                'password'      => Hash::make('djau'),
                'role_id'       => 7,
                'employee_id'   => 149,
                'rrhh_office_id'   => 2,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [//7
                'username'      => 'call',
                'name'          => 'Call Center.',
                'email'         => 'jarze@pragmainvest.com.bo',
                'password'      => Hash::make('call'),
                'role_id'       => 8,
                'employee_id'   => 139,
                'rrhh_office_id'   => 2,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [//8
                'username'      => 'rrhh',
                'name'          => 'RRHH.',
                'email'         => 'jarze@pragmainvest.com.bo',
                'password'      => Hash::make('rrhh'),
                'role_id'       => 5,
                'employee_id'   => 140,
                'rrhh_office_id'   => 2,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [//8
                'username'      => 'repo',
                'name'          => 'Reportes',
                'email'         => 'jarze@pragmainvest.com.bo',
                'password'      => Hash::make('repo'),
                'role_id'       => 9,
                'employee_id'   => null,
                'rrhh_office_id'=> 2,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
        ]);

        // Relación entre Enfermera y médicos
        DB::table('medical_users')->insert([
            [
                'nurse_id'  => 4,
                'doctor_id' => 3,
            ]
        ]);

        factory(\App\User::class,20)
            ->create();
    }
}
