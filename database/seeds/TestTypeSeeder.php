<?php

use Illuminate\Database\Seeder;

class TestTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('test_types')->insert([
            [
                'name'          => 'PCR',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'PRUEBA RAPIDA',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'RADIOGRAFÍA',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'TOMOGRAFÍA',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'EGS',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'RAYOS X',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'ELISA',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'PRUEBA SEROLOGICA',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
        ]);
    }
}
