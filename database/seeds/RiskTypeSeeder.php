<?php

use Illuminate\Database\Seeder;

class RiskTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('risk_types')->insert([
            [
                'name'          => 'COVID-19',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'ACCIDENTE DE TRABAJO',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'OTROS',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'ENFERMEDAD COMÚN',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
        ]);
    }
}
