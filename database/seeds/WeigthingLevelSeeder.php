<?php

use Illuminate\Database\Seeder;

class WeigthingLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('weighting_levels')->insert([
            [
                'name'              => 'NULO',
                'description'       => null,
                'color'             => '#ffffff',
                'minimum'           => 0,
                'maximum'           => 1,
                'evaluation'        => 0,
                'last_level_flag'   => false,
                'created_at'        => now(),
                'updated_at'        => now()
            ],
            [
                'name'              => 'BAJO',
                'description'       => null,
                'color'             => '#92d050',
                'minimum'           => 2,
                'maximum'           => 5,
                'evaluation'        => 4,
                'last_level_flag'   => false,
                'created_at'        => now(),
                'updated_at'        => now()
            ],
            [
                'name'              => 'MEDIO',
                'description'       => null,
                'color'             => '#ffff00',
                'minimum'           => 6,
                'maximum'           => 10,
                'evaluation'        => 3,
                'last_level_flag'   => false,
                'created_at'        => now(),
                'updated_at'        => now()
            ],
            [
                'name'              => 'ALTO',
                'description'       => null,
                'color'             => '#ffc000',
                'minimum'           => 11,
                'maximum'           => 15,
                'evaluation'        => 2,
                'last_level_flag'   => false,
                'created_at'        => now(),
                'updated_at'        => now()
            ],
            [
                'name'              => 'MUY ALTO',
                'description'       => null,
                'color'             => '#ff0000',
                'minimum'           => 16,
                'maximum'           => null,
                'evaluation'        => 1,
                'last_level_flag'   => true,
                'created_at'        => now(),
                'updated_at'        => now()
            ],
        ]);
    }
}
