<?php

use Illuminate\Database\Seeder;

class TreatmentStageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('treatment_stages')->insert([
            // [
            //     'name'          => 'ALTA MÉDICA',
            //     'description'   => '',
            //     'created_at'    => now(),
            //     'updated_at'    => now()
            // ],
            [
                'name'          => 'EN OBSERVACION',
                'description'   => '',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'INTERNACION PRIVADA',
                'description'   => '',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'INTERNACION EGS',
                'description'   => '',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'TRATAMIENTO EGS',
                'description'   => '',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'TRATAMIENTO INTERNO',
                'description'   => '',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'TRATAMIENTO PRIVADO',
                'description'   => '',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'FALLECIDO',
                'description'   => '',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
        ]);
    }
}
