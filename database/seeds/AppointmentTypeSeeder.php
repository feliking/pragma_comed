<?php

use Illuminate\Database\Seeder;

class AppointmentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('appointment_types')->insert([
            [ //1
                'name'          => 'PRESENCIAL',
                'description'   => 'TIPO DE ATENCIÓN FISICA',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //2
                'name'          => 'ZOOM',
                'description'   => 'TIPO DE ATENCIÓN POR VIDEO CONFERÉNCIA (ZOOM)',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //3
                'name'          => 'WHATSAPP',
                'description'   => 'TIPO DE ATENCIÓN POR VIDEO LLAMADA (WHATSAPP)',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //4
                'name'          => 'LLAMADA TELEFÓNICA',
                'description'   => 'TIPO DE ATENCIÓN POR LLAMADA TELEFÓNICA',
                'created_at'    => now(),
                'updated_at'    => now()
            ]
        ]);
    }
}
