<?php

use Illuminate\Database\Seeder;

class RiskStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('risk_states')->insert([
            [
                'name'          => 'CONFIRMADO',
                'description'   => null,
                'weighting'     => 10,
                'risk_type_id'  => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'DESCARTADO',
                'description'   => null,
                'weighting'     => 0,
                'risk_type_id'  => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'FALLECIDO',
                'description'   => null,
                'weighting'     => 0,
                'risk_type_id'  => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'RECUPERADO',
                'description'   => null,
                'weighting'     => 0,
                'risk_type_id'  => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'SOSPECHOSO',
                'description'   => null,
                'weighting'     => 2,
                'risk_type_id'  => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ],

        ]);
    }
}
