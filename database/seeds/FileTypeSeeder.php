<?php

use Illuminate\Database\Seeder;

class FileTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('file_types')->insert([
            [ //
                'name'          => 'PRUEBAS - EXAMENES COMPLEMENTARIOS',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
        ]);
    }
}
