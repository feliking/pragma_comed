<?php

use Illuminate\Database\Seeder;

class AntecedentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('antecendent_types')->insert([
            'name'        => 'ANTECEDENTES PATOLÓGICOS',
            'description' => 'ANTECEDENTES PATOLÓGICOS',
        ]);
        DB::table('antecendent_types')->insert([
            'name'        => 'ANTECEDENTES PATOLÓGICOS ',
            'description' => 'ANTECEDENTES PATOLÓGICOS',
        ]);
        DB::table('antecendent_types')->insert([
            'name'        => 'ANTECEDENTES NO PATOLÓGICOS',
            'description' => 'ANTECEDENTES PATOLÓGICOS',
        ]);
        DB::table('antecendent_types')->insert([
            'name'        => 'OTROS ANTECESDENTES',
            'description' => 'OTROS ANTECESDENTES',
        ]);
    }
}
