<?php

use Illuminate\Database\Seeder;

class KitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kits')->insert([
            [ //
                'name'          => 'KIT DE MEDICAMENTOS',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
        ]);
        DB::table('kits')->insert([
            [ //
                'name'          => 'KIT DE PRUEBA RÁPIDA',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
        ]);
    }
}
