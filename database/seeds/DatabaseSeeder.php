<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MedicalRequestStatusSeeder::class);
        $this->call(ParameterSeeder::class);

        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);

        $this->call(AntecedentTypeSeeder::class);
        $this->call(AppointmentTypeSeeder::class);
        $this->call(TreatmentStageSeeder::class);
        $this->call(RiskTypeSeeder::class);
        $this->call(RiskStateSeeder::class);
        $this->call(DisaeseCriteriaSeeder::class);
        $this->call(LaboratorySeeder::class);
        $this->call(TestTypeSeeder::class);
        $this->call(FileTypeSeeder::class);
        $this->call(WeigthingLevelSeeder::class);

        $this->call(MedicalRequestSeeder::class);

        $this->call(KitSeeder::class);
    }
}
