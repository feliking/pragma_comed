<?php

use Illuminate\Database\Seeder;

class DisaeseCriteriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('disease_criterias')->insert([
            [
                'syntom'          => 'TOS',
                'weighing'   => 5,
                'risk_type_id' => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'syntom'          => 'TEMPERATURA MAYOR A 38º',
                'weighing'   => 4,
                'risk_type_id' => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'syntom'          => 'DOLOR CORPORAL',
                'weighing'   => 3,
                'risk_type_id' => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'syntom'          => 'DIFICULTAD PARA RESPIRAR',
                'weighing'   => 5,
                'risk_type_id' => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'syntom'          => 'DOLOR DE CABEZA',
                'weighing'   => 2,
                'risk_type_id' => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'syntom'          => 'FATIGA',
                'weighing'   => 2,
                'risk_type_id' => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'syntom'          => 'DOLOR DE GARGANTA',
                'weighing'   => 2,
                'risk_type_id' => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'syntom'          => 'PERDIDA DEL GUSTO O DEL OLFATO',
                'weighing'   => 2,
                'risk_type_id' => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'syntom'          => 'DIARREA',
                'weighing'   => 2,
                'risk_type_id' => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'syntom'          => 'DOLOR O PRESIÓN EN EL PECHO',
                'weighing'   => 3,
                'risk_type_id' => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'syntom'          => 'SECRECIÓN NASAL',
                'weighing'   => 1,
                'risk_type_id' => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'syntom'          => 'ESTORNUDOS',
                'weighing'   => 1,
                'risk_type_id' => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'syntom'          => 'PROBLEMAS EN LOS OJOS',
                'weighing'   => 1,
                'risk_type_id' => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'syntom'       => 'CONTACTO DIRECTO  CON CASO COVID',
                'weighing'     => 6,
                'risk_type_id' => 1,
                'created_at'   => now(),
                'updated_at'   => now()
            ],
            [
                'syntom'       => 'NINGUNO',
                'weighing'     => 0,
                'risk_type_id' => 1,
                'created_at'   => now(),
                'updated_at'   => now()

            ],
            [
                'syntom'       => 'ENFERMEDAD BASE',
                'weighing'     => 6,
                'risk_type_id' => 1,
                'created_at'   => now(),
                'updated_at'   => now()
            ],
        ]);
    }
}
