<?php

use Illuminate\Database\Seeder;

class ParameterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('parameters')->insert([
            [ //1
                'name'          => 'TIEMPO_SOLICITUD__MINUTOS_ABIERTA',
                'description'   => 'TIEMPO EN QUE UNA SOLICITUD PUEDA ESTAR ABIERTA ANTES DE QUE OTRO PUEDA DAR USO DE ESTA, DEBE SER MENOR A 60 MINUTOS',
                'value'         => '2',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //2
                'name'          => 'TIEMPO_REPROGRAMACION_DIAS',
                'description'   => 'TIEMPO EN QUE SE REPROGRAMARA UN SEGUIMIENTO EN DIAS',
                'value'         => '1',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //3
                'name'          => 'INTERVALO_TIEMPO_SEGUIMIENTO',
                'description'   => 'INTERVALO DE TIEMPO DE SEGUIMIENTO EN MINUTOS',
                'value'         => '10',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //4
                'name'          => 'INTERVALO_TIEMPO_CITA',
                'description'   => 'INTERVALO DE TIEMPO DE CITA MEDICA EN MINUTOS',
                'value'         => '20',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //5
                'name'          => 'HORA_ENTRADA',
                'description'   => 'HORARIO DE ENTRADA EN HORAS 24 HORAAS',
                'value'         => '8:00',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //6
                'name'          => 'HORA_SALIDA',
                'description'   => 'INTERVALO DE TIEMPO DE CITA MEDICA EN MINUTOS',
                'value'         => '18:00',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //7
                'name'          => 'EDAD_INSEGURA',
                'description'   => 'EDAD PARA LA CUAL LA PONDERACIÓN CALIFICARA EN "1" UNA UNIDAD AL QUE CUENTE CON ESTE',
                'value'         => '50',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [ //8
                'name'          => 'VALOR_EDAD_INSEGURA',
                'description'   => 'EDAD PARA LA CUAL LA PONDERACIÓN CALIFICARA EN "1" UNA UNIDAD AL QUE CUENTE CON ESTE',
                'value'         => '1',
                'created_at'    => now(),
                'updated_at'    => now()
            ],
        ]);
    }
}
