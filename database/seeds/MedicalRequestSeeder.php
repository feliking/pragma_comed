<?php

use Illuminate\Database\Seeder;

class MedicalRequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //  DB::table('medical_requests')->insert([
        //      [ //1
        //          'code' => 123456,
        //          'ci' => 213213123,
        //          'ci_expedition' => 'LP',
        //          'enterprise' => 'Pragma Invest',
        //          'email' => null,
        //          'full_name' => 'NOMBRE',
        //          'phone_number' => '7862138',
        //          'whatsapp_flag' => true,
        //          'comment' => 'COMENTARIO',
        //          'medical_request_status_id' => 13,
        //          'risk_type_id' => 1,
        //              'user_id' => 5,
        //          'employee_id' => 135
        //      ],
        //      [ //2
        //          'code' => 123457,
        //          'ci' => 213213123,
        //          'ci_expedition' => 'LP',
        //          'enterprise' => 'Pragma Invest',
        //          'email' => null,
        //          'full_name' => 'NOMBRE',
        //          'phone_number' => '7862138',
        //          'whatsapp_flag' => true,
        //          'comment' => 'COMENTARIO',
        //          'medical_request_status_id' => 13,
        //          'risk_type_id' => 1,
        //          'user_id' => 5,
        //          'employee_id' => 135
        //      ],
        //      [ //3
        //          'code' => 123458,
        //          'ci' => 213213123,
        //          'ci_expedition' => 'LP',
        //          'enterprise' => 'Pragma Invest',
        //          'email' => null,
        //          'full_name' => 'NOMBRE',
        //          'phone_number' => '7862138',
        //          'whatsapp_flag' => true,
        //          'comment' => 'COMENTARIO',
        //          'medical_request_status_id' => 13,
        //          'risk_type_id' => 1,
        //          'user_id' => 5,
        //          'employee_id' => 135
        //      ],
        //  ]);

        factory(\App\MedicalRequest::class, 100)
            ->create()
            ->each(function ($medical_request){
                $medical_request->medical_requests_status()
                    ->attach(1, [ //ASIGNADO EL ESTADO DE 'SOLICITADO'
                        'user_id' => null,
                        'comment' => 'ASIGNACIÓN DE SOLICITUD - AUTOMÁTICO'
                    ]);
            });
    }
}
