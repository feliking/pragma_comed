<?php

use Illuminate\Database\Seeder;

class MedicalRequestStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medical_request_statuses')->insert([ //1
            'name' => 'SOLICITADO',
            'description' => null,
            'system_flag' => true
        ]);
        DB::table('medical_request_statuses')->insert([ //2
            'name' => 'ASIGNADO A MÉDICO',
            'description' => null,
            'system_flag' => true
        ]);
        DB::table('medical_request_statuses')->insert([ //3
            'name' => 'PENDIENTE',
            'description' => null,
            'system_flag' => true
        ]);
        DB::table('medical_request_statuses')->insert([ //4
            'name' => 'CERRADO',
            'description' => null,
            'system_flag' => true
        ]);
        DB::table('medical_request_statuses')->insert([ //5
            'name' => 'VALIDADO',
            'description' => null,
            'system_flag' => true
        ]);
        DB::table('medical_request_statuses')->insert([ //6
            'name' => 'PRE REVISIÓN',
            'description' => null,
            'system_flag' => true
        ]);
        DB::table('medical_request_statuses')->insert([ //7
            'name' => 'RECHAZADO',
            'description' => null,
            'system_flag' => true
        ]);
        DB::table('medical_request_statuses')->insert([ //8
            'name' => 'EN TRATAMIENTO',
            'description' => null,
            'system_flag' => false
        ]);
        DB::table('medical_request_statuses')->insert([ //9
            'name' => 'EN CONSULTA MÉDICA (PRESENCIAL)',
            'description' => null,
            'system_flag' => false
        ]);
        DB::table('medical_request_statuses')->insert([ //10
            'name' => 'EN CONSULTA MÉDICA (VIA WHATSAPP)',
            'description' => null,
            'system_flag' => false
        ]);
        DB::table('medical_request_statuses')->insert([ //11
            'name' => 'EN CONSULTA MÉDICA (VIA ZOOM)',
            'description' => null,
            'system_flag' => false
        ]);
        DB::table('medical_request_statuses')->insert([ //12
            'name' => 'POR ASIGNAR',
            'description' => 'UNA SOLICITUD QUE SE TIENE QUE ASIGNAR A SEGUIMIENTO',
            'system_flag' => true
        ]);
        DB::table('medical_request_statuses')->insert([ //13
            'name' => 'ASIGNADO A SEGUIMIENTO',
            'description' => 'SE ASIGNO A ALGUIEN PARA QUE LE HAGA SEGUIMIENTO',
            'system_flag' => true
        ]);
        DB::table('medical_request_statuses')->insert([ //14
            'name' => 'REPROGRAMADO',
            'description' => 'SE RE ASIGNO DEBIDO A QUE NO SE LE PUDO HACER SEGUIMIENTO',
            'system_flag' => true
        ]);
        DB::table('medical_request_statuses')->insert([ //15
            'name' => 'PRE GRABADO',
            'description' => 'REGISTRO DE ATENCIÓN PRE GRABADO PARA SER REGULARIZADO',
            'system_flag' => true
        ]);
        DB::table('medical_request_statuses')->insert([ //16
            'name' => 'REMITIDO-CONSULTA',
            'description' => 'ESTADO QUE PERMITE REMITIR UN CASO AL ADM. DE SEGUIMIENTOS POR PARTE DE UN MÉDICO',
            'system_flag' => true
        ]);
        DB::table('medical_request_statuses')->insert([ //17
            'name' => 'REMITIDO-SEGUIMIENTO',
            'description' => 'ESTADO QUE PERMITE REMITIR UN CASO AL ADM. DE SEGUIMIENTOS POR PARTE DE UN RESPONSABLE DE SEGUIMIENTO',
            'system_flag' => true
        ]);
    }
}
