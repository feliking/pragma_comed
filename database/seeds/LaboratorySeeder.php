<?php

use Illuminate\Database\Seeder;

class LaboratorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('laboratories')->insert([
            [
                'name'          => 'AGRAMONT',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'ALFALAB',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'BIOSLAB',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'CENELIP',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'CLINY GEN',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'CNS',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],[
                'name'          => 'INLASA',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],[
                'name'          => 'INTERNO',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],[
                'name'          => 'LABOGEN',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],[
                'name'          => 'MEDILAB',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],[
                'name'          => 'PLEXUS',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],[
                'name'          => 'SERRANO',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'HC MEDIC',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'HOPSITAL AGRAMONT',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'I-MED',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'ARCOIRIS',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'DR. NAVA',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'DESCONOCIDO',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
            [
                'name'          => 'HOSP LA PAZ',
                'description'   => null,
                'created_at'    => now(),
                'updated_at'    => now()
            ],
        ]);
    }
}
