<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Antecendent extends Model
{
    protected $table = 'antecendents';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    protected $fillable = [
        'description'
    ];

    public function medical_request(){
        return $this->belongsTo(MedicalRequest::class,'medical_request_id','id');
    }

    public function antecedent_type(){
        return $this->belongsTo(AntecendentType::class,'antecendent_type_id','id');
    }

    public function employee(){
        return $this->hasOne(EmployeeRRHH::class,'id','employee_id');
    }
}
