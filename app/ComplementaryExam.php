<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ComplementaryExam extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'complementary_exams';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bill_number', 'cost', 'enterprise_flag', 'tracing_id', 'laboratory_id', 'test_type_id'
    ];
    /*
     * Mutators
     */
    // SET Numero de Factura
    public function setBillNumberAttribute($value){
        $this->attributes['bill_number'] = mb_strtoupper($value, 'UTF-8');
    }

    public function files(){
        return $this->belongsToMany(File::class,'file_complementary_exam','complementary_exam_id','file_id')
            ->withTimestamps();
    }

    public function tracing()
    {
        return $this->belongsTo('App\Tracing');
    }

    public function laboratory()
    {
        return $this->belongsTo('App\Laboratory');
    }

    public function testType()
    {
        return $this->belongsTo('App\TestType');
    }
}
