<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username','name', 'email', 'password', 'role_id','employee_id', 'rrhh_office_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @description = Obtiene el rol al que pertenece
     */
    public function role(){
        return $this->belongsTo(Role::class);
    }
    public function getUserameAttribute($value){
        return mb_strtoupper($value, 'UTF-8');
    }
    // SET Name
    public function setUsernameAttribute($value){
        $this->attributes['username'] = mb_strtoupper($value, 'UTF-8');
    }
    public function getFullNameAttribute(){
        if ($this->office!=null) {
            return $this->name.' ('.$this->office->direccion.')';
        } else {
            return $this->name.' (SIN ESPECIFICAR)';
        }
        
    }
    public function getFullNameRolAttribute(){
        if ($this->office!=null) {
            return $this->name.' ('.$this->role->name.' - '.$this->office->direccion.')';
        } else {
            return $this->name.' ('.$this->role->name.' - '.'SIN ESPECIFICAR'.')';
        }
        
    }

    public function employee(){
        return $this->belongsTo('App\EmployeeRRHH', 'employee_id');
    }
    public function office(){
        return $this->belongsTo('App\Office', 'rrhh_office_id');
    }
    public function medical_users(){
        return $this->belongsToMany('App\User', 'medical_users', 'nurse_id', 'doctor_id');
    }
    public function user_medical(){
        return $this->belongsToMany('App\User', 'medical_users', 'doctor_id', 'nurse_id');
    }
}
