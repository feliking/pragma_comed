<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttentionVitalSign extends Model
{
    protected $table = 'attention_vital_signs';

    public $timestamps = true;

    protected $fillable = [
        'attention_date','fc','fr','t','pa','sao2','otros','medical_request_id'
    ];

    public function medical_request(){
        return $this->belongsTo(MedicalRequest::class);
    }
}
