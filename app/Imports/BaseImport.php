<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class BaseImport implements ToCollection
{
    private $mje_error;
    private $errors;
    private $completo;
    function __construct()
    {

        $this->mje_error = '';
        $this->errors = [];
        $this->completo = [];
    }

    public function startRow(): int
    {
        // TODO: Implement startRow() method.
        return 2;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $errados = [];
        foreach ($rows as $key=>$row) {
            $dCi = $row[0];
            $dNombre = $row[1];
            $dEmpresa = $row[2];
            $fini = $row[5];
            $dTelefono = $row[8];
            $dReferencia = $row[10];
            $dReferenciaTel = $row[11];

            $new_array = (object) array('ci' => $row[0], 'nombre' =>  $dNombre, 'fecha' => $fini,
                'empresa' => $dEmpresa, 'telefono' => $dTelefono, 'referencia' => $dReferencia, 'referencia_telefono' => $dReferenciaTel);
            array_push($errados,$new_array);
        }
        $this->completo = $errados;
    }

    public function getCompleto()
    {
        return $this->completo;
    }
}
