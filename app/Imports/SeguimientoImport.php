<?php

namespace App\Imports;

use App\EmployeeRRHH;
use App\MedicalRequest;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class SeguimientoImport implements ToCollection, WithStartRow
{
    private $mje_error;
    private $errors;
    private $extras;
    private $datos;
    function __construct($datos)
    {
        $this->mje_error = '';
        $this->errors = [];
        $this->extras = [];
        $this->datos = $datos;
    }

    public function startRow(): int
    {
        // TODO: Implement startRow() method.
        return 2;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $errados = [];
        $datos_recibidos = $this->datos;

        /* foreach ($rows as $key=>$row) {
            $dLaboratorio = $row[17];
            $dCostoLab = $row[19];
            $dFacLab = $row[20];
        } */
        foreach ($rows as $key=>$row) {
            $dCi = $row[0];
            $dNombre = $row[1];
            $dTelefono = $row[2];
            $dTelefonoContacto = $row[3];
            $dEtapa = $row[6];
            $dRiesgo = $row[8];

            //$fseg = $this->transformDateTime($row[10],'d/m/Y');
            $dEmpresa = $row[13];
            $dComment = $row[21];
            $test_type = $row[16];
            $dResultado = $row[18];
            $dLaboratorio = $row[17];
            $dCostoLab = $row[19];
            $dFacLab = $row[20];

            //BUSCAR DATOS DE BASE
            $key = array_search($dCi, array_column($datos_recibidos, 'ci'));
            $rNombre = $datos_recibidos[$key]->nombre;
            $rEmpresa = $datos_recibidos[$key]->empresa;
            $rTelefono = $datos_recibidos[$key]->telefono;
            $rRef = $datos_recibidos[$key]->referencia;
            //dd($datos_recibidos[$key]->fecha, $this->transformDateTime($datos_recibidos[$key]->fecha,'d/m/Y'));
            $finicio = $this->transformDateTime($datos_recibidos[$key]->fecha,'d/m/Y');
            $rRefTel = $datos_recibidos[$key]->referencia_telefono;

            if($rTelefono == null)
                $rTelefono = 0;

            $request_status = 1;
            $request_comment = 'ASIGNACIÓN DE SOLICITUD - AUTOMÁTICO';
            $request_user_id = null;

            //CODIGO
            do {
                $code = strtoupper(substr(uniqid(), -6));

                $verification_code = MedicalRequest::where('code', $code)
                    ->first();
            } while ($verification_code != null);

            //VERIFICAR EMPLEADO POR CI Y LLENAR SUS DATOS
            $employee = EmployeeRRHH::where([
                ['ci_numero', 'LIKE', $dCi],
                ['estado', '!=', 'Inactivo']
            ])->first();

            $eCiExp = "LP";
            $eEmpId =   null;
            $eEmplCode = null;
            $eProyecto = null;
            $eGender = "Masculino";
            $eBirthday = null;
            if($employee != null){
                $eCiExp = $employee->ci_expedido;
                $eEmpId =   $employee->id;
                $eEmplCode = $employee->cubo_id;
                // $eProyecto = $employee->sol_proyecto_id;
                $eProyecto = $employee->proyecto_id;
                $eGender = $employee->sexo;
                $eBirthday = $employee->fecha_nacimiento;
            }


            //OBTENER ID DE RIESGO
            $eRiesgo = 3;
            if($dRiesgo == "COVID 19" || $dRiesgo == "ENFERMEDAD COMUN")
                $eRiesgo = 1;
            elseif ($dRiesgo == "ACCIDENTE DE TRABAJO")
                $eRiesgo = 2;

            //REVISAR ETAPA DE TRATAMIENTO
            $eEstado = 12;
            if($dEtapa == "ALTA MEDICA" || $dEtapa == "FALLECIDO")
                $eEstado = 4;


            $array_medical_request = [
                'ci' => $dCi,
                'code' => $code,
                'ci_expedition' => $eCiExp,
                'enterprise' => $rEmpresa,
                'email' => null,
                'employee_code' => $eEmplCode,
                'full_name' => $rNombre,
                'phone_number' => $rTelefono,
                'reference_full_name' => $rRef,
                'reference_phone_number' => $rRefTel,
                'short_phone_number' => null,
                'whatsapp_flag' => 1,
                'comment' => $dComment,
                'gender' => $eGender,
                'birthday' => $eBirthday,
                'medical_request_status_id' => $eEstado,
                'risk_type_id' => $eRiesgo,
                'user_id' => null,
                'employee_id' => $eEmpId,
                'project_id' => $eProyecto,
                'health_insurance_id' => null,
                //'created_at' => $finicio

            ];
            $medical_request = new MedicalRequest($array_medical_request);
            $medical_request->created_at = $finicio;
            $medical_request->save();

            $medical_request->medical_requests_status()
                ->attach($request_status, [ //ASIGNADO EL ESTADO DE 'SOLICITADO'
                    'user_id' => null,
                    'comment' => $request_comment,
                    'created_at' => $finicio,
                ]);


            $new_array = (object) array('ci' => $row[0], 'etapa' =>  $dEtapa,
                'laboratorio' => $dLaboratorio, 'costo' => $dCostoLab, 'factura' => $dFacLab, 'prueba' => $test_type, 'resultado' => $dResultado);
            array_push($errados,$new_array);
        }
        $this->extras = $errados;
    }

    public function getExtras()
    {
        return $this->extras;
    }
    private function transformDateTime($value, string $format = 'Y-m-d')
    {
        switch (gettype($value)){
            case "integer":
                return Carbon::instance(Date::excelToDateTimeObject($value))
                    ->format('Y-m-d');
                break;
            case "string":
                return Carbon::createFromFormat($format, $value);
                break;
            default:
                //throw new RuntimeException("Error en formato fecha");
                break;
        }
    }
}
