<?php

namespace App\Imports;

use App\EmployeeRRHH;
use App\MedicalRequest;
use App\RiskType;
//use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;

class MedicalRequestImport implements ToCollection
{
    private $mje_error;
    private $nombre_archivo;

    function __construct($nombre_archivo)
    {
        $this->nombre_archivo = $nombre_archivo;
        $this->mje_error = '';
    }

    public function collection(Collection $rows){
        DB::beginTransaction();
        try {
            foreach ($rows as $key=>$field) {
                //dd($field);
                if ($key>0) {
                    do {
                        $code = strtoupper(substr(uniqid(), -6));

                        $verification_code = MedicalRequest::where('code', $code)
                            ->first();
                    } while ($verification_code != null);
                    //echo $field;
                    $employee = EmployeeRRHH::where([
                        ['ci_numero', 'LIKE', $field[0]],
                        ['estado', '!=', 'Inactivo']
                    ])->get()->first();
                    //dd($employee);
                    $medical_request = new MedicalRequest();
                    $medical_request->code = $code;
                    $medical_request->ci = $field[0];
                    $medical_request->ci_expedition = $employee->ci_expedido;//rrhh
                    $medical_request->enterprise = $field[0];//de la hoja de base
                    //$medical_request->email = null;
                    $medical_request->employee_code = $employee->cubo_id;//rrhh
                    $medical_request->full_name = $field[1];
                    $medical_request->phone_number = strlen($field[2])!=4?$field[2]:0;
                    $medical_request->reference_full_name = null;//Hoja base
                    $medical_request->reference_phone_number = null;//Hoja base
                    $medical_request->short_phone_number = strlen($field[2])==4?$field[2]:'';
                    $medical_request->whatsapp_flag = 1;
                    $medical_request->comment = $field[21]!=''?$field[21]:null;
                    $medical_request->gender = $employee->sexo=='MASCULINO'?'Masculino':'Femenino';
                    $medical_request->birthday = $employee->fecha_nacimiento;
                    $medical_request->medical_request_status_id = 12; //de acuerdo a la hoja de fichas verificar
                    $risk_type = RiskType::where('name',$field[8])->get();
                    $medical_request->risk_type_id = $risk_type->id;
                    $medical_request->user_id = null;//con el id del usuario de creacion en hoja de fichas verificar
                    $medical_request->employee_id = $employee->id;
                    $medical_request->project_id = null;
                    $medical_request->health_insurance_id = 2; // rrhh


                }
                /* if ($key==15) {
                    dd($field);
                } */

            }
        } catch (\Throwable $ex) {
            DB::rollBack();
            //$this->eliminarArchivo();
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema. '.$ex)
                ->error()
                ->important();
        }
        DB::commit();
        flash('Se importo correctamente las consultas')
            ->success()
            ->important();

    }
    public function eliminarArchivo()
    {
        unlink(storage_path('tmp/uploads/'.$this->nombre_archivo));
    }
}
