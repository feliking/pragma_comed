<?php

namespace App\Imports;

use App\ComplementaryExam;
use App\EmployeeRRHH;
use App\HistoricalRequest;
use App\HistoricalWeightingLevel;
use App\kitTracing;
use App\Laboratory;
use App\MedicalRequest;
use App\RiskState;
use App\TestType;
use App\Tracing;
use App\TreatmentStage;
use App\User;
use App\WeightingLevel;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use PhpOffice\PhpSpreadsheet\Calculation\DateTime;

class FichasImport implements ToCollection, WithStartRow, WithCalculatedFormulas
{
    private $mje_error;
    private $errors;
    private $extras;
    function __construct($extras)
    {
        $this->extras = $extras;
        $this->mje_error = '';
        $this->errors = [];
    }

    public function startRow(): int
    {
        // TODO: Implement startRow() method.
        return 2;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {

        $datos_recibidos = $this->extras;
        foreach ($rows as $keyy=>$row) {
            $dCi = $row[0];
            $dCovid = $row[5];
            $dResponsable = $row[6];
            //dd($row[3], $row[4], $row[5], $row[6], $row[7], date("d/m/y", $row[7]), $this->transformDateTime($row[7],'d/m/Y')/* , DateTime::createFromFormat('d/m/y', $row[7]) */);
            $dFecha = $this->transformDateTime($row[7],'d/m/Y')/* $row[7] *//* Carbon::createFromFormat('d/m/Y', $row[7])->toDateTimeString() */;
            $dBase = $row[9];
            $dTos = $row[10];
            $dTempratura = $row[11];
            $dDolorCorporal = $row[12];
            $dDificultadrespirar = $row[13];
            $dDolorCabeza = $row[14];
            $dFatiga= $row[15];
            $dDolorGarganta = $row[16];
            $dPerdidaGusto = $row[17];
            $dDiarrea = $row[18];
            $dPresionPecho = $row[19];
            $dSecrecion = $row[20];
            $dEstornudos = $row[21];
            $dOjos = $row[22];
            $dContacto = $row[23];
            $dPonderacion = $row[24];
            $dObservaciones = $row[26];
            $dKit = $row[28];

            //OBTENER MEDICAL REQUEST DE ESE CI
            $medical_request = MedicalRequest::where('ci','LIKE',$dCi)->first();

            //OBTENER USER (EL NOMBRE TIENE Q ESTAR BIEN)
            $user = User::where('name','LIKE',$dResponsable)
                ->first();
            if($user != null)
                $eUser = $user->id;
            else
                $eUser = 1;

            //CREAR HISTORICAL
            $historical_request = new HistoricalRequest();
            $historical_request->user_id = $eUser;
            $historical_request->comment = 'SEGUIMIENTO REALIZADO';
            $historical_request->medical_request_id = $medical_request->id;
            $historical_request->medical_request_status_id = 12;
            $historical_request->created_at = $dFecha;
            $historical_request->save();

            //ESTADO DE RIESGO
            $risk_state = RiskState::where('name','LIKE',$dCovid)->first();

            //OBTENER EMPLEADO Y PROYECTO EN BASE AL MEDICAL REQUEST
            $project = null;
            if($medical_request->employee_id!=null){
                $employee = EmployeeRRHH::find($medical_request->employee_id);
                // $project = $employee->sol_proyecto_id;
                $project = $employee->proyecto_id;
            }

            //OBTTNER ESTADO TRATAMIENTO (FUCKING GG)
            $key = array_search($dCi, array_column($datos_recibidos, 'ci'));
            $etapa_recibida = $datos_recibidos[$key]->etapa;

            if($etapa_recibida != "ALTA MEDICA")
                $etapa = TreatmentStage::where('name','LIKE',$etapa_recibida)->first();
            else
                $etapa = TreatmentStage::where('name','LIKE','EN OBSERVACION')->first();

            $array_tracing = [
                'comment' => $dObservaciones,
                'tracing_date' => $dFecha/* Date::excelToDateTimeObject($dFecha) *//* ->format('Y-m-d') */,
                'medical_attention_flag' => 0,
                'medical_request_id' => $medical_request->id,
                'treatment_stage_id' => $etapa->id,
                'risk_type_id' => 1,
                'user_id' => $eUser,
                'project_id' => $project,
                'risk_state_id' => $risk_state->id,
                'historical_request_id' => $historical_request->id,

            ];
            $tracing = new Tracing($array_tracing);
            $tracing->created_at=$dFecha;
            $tracing->save();
            $tracings = Tracing::where('medical_request_id',$medical_request->id)->get();
            if(count($tracings)==1){
                $billnumber = null;
                $cost = null;
                $enterprise_flag = false;
                //$tracing_id = $datos_recibidos[$key]->referencia;
                $laboratory_id = null;
                $test_type_id = null;//cambiar nulo en la bd?
                $keys = array_search($dCi, array_column($datos_recibidos, 'ci'));
                $cii = $datos_recibidos[$keys]->ci;

                $medical_request = MedicalRequest::where('ci','LIKE',$datos_recibidos[$keys]->ci)->first();

                if($cii==$dCi){
                    //Busca Labo
                    if ($datos_recibidos[$keys]->laboratorio!=null&&$datos_recibidos[$keys]->laboratorio!=='X' &&$datos_recibidos[$keys]->laboratorio!=='x' &&$datos_recibidos[$keys]->laboratorio!=''&&$datos_recibidos[$keys]->laboratorio!=='0'&&$datos_recibidos[$keys]->laboratorio!=='O'&&$datos_recibidos[$keys]->laboratorio!==0) {
                        $laboratory_id = Laboratory::where('name', $datos_recibidos[$keys]->laboratorio)->get()->first();
                        if ($datos_recibidos[$keys]->laboratorio=='HOSP.AGRAMONT') {
                            $laboratory_id = Laboratory::find(14);
                        }
                        if ($datos_recibidos[$keys]->laboratorio=='DESCONOCEMOS') {
                            $laboratory_id = Laboratory::find(18);
                        }
                    }
                    //Busca Test
                    if ($datos_recibidos[$keys]->prueba!=null&&$datos_recibidos[$keys]->prueba!='X' && $datos_recibidos[$keys]->prueba!='x' && $datos_recibidos[$keys]->prueba!='') {
                        $test_type_id = TestType::where('name','like', $datos_recibidos[$keys]->prueba)->get()->first();
                        if ($datos_recibidos[$keys]->laboratorio=='RAPIDA') {
                            $test_type_id = TestType::find(2);
                        }
                    }else{
                        if ($datos_recibidos[$keys]->laboratorio=='EGS' &&$datos_recibidos[$keys]->laboratorio!='0'&&$datos_recibidos[$keys]->laboratorio!='O'&&$datos_recibidos[$keys]->laboratorio!==0) {
                            $test_type_id = TestType::find(5);
                        }
                        if ($datos_recibidos[$keys]->laboratorio=='RAYOS X'&&$datos_recibidos[$keys]->laboratorio!='0'&&$datos_recibidos[$keys]->laboratorio!='O'&&$datos_recibidos[$keys]->laboratorio!==0) {
                            $test_type_id = TestType::find(6);
                        }
                    }

                    if($medical_request!=null && $laboratory_id!=null){

                        $billnumber = ($datos_recibidos[$keys]->factura!=''&&$datos_recibidos[$keys]->factura!='x'&&$datos_recibidos[$keys]->factura!='X'&&$datos_recibidos[$keys]->factura!=0&&$datos_recibidos[$keys]->factura!='0')?$datos_recibidos[$keys]->factura:null;
                        $cost = ($datos_recibidos[$keys]->costo!=''&&$datos_recibidos[$keys]->costo!='x'&&$datos_recibidos[$keys]->costo!='X'&&$datos_recibidos[$keys]->costo!=0&&$datos_recibidos[$keys]->costo!='0')?$datos_recibidos[$keys]->costo:null;
                        $enterprise_flag = ($datos_recibidos[$keys]->costo!=''&&$datos_recibidos[$keys]->costo!='x'&&$datos_recibidos[$keys]->costo!='X'&&$datos_recibidos[$keys]->costo!=0&&$datos_recibidos[$keys]->costo!='0')?true:false;
                        $tracing_id = $tracing->id;
                        $laboratory_id = $laboratory_id->id;
                        $test_type_id = ($test_type_id!=null)?$test_type_id->id:null;

                        $exams = new ComplementaryExam();
                        $exams->bill_number = $billnumber;
                        $exams->cost = $cost;
                        $exams->enterprise_flag = $enterprise_flag;
                        $exams->tracing_id = $tracing_id;
                        $exams->laboratory_id = $laboratory_id;
                        $exams->test_type_id = $test_type_id;
                        $exams->save();
                    }else{
                        if($medical_request!=null && ($datos_recibidos[$keys]->laboratorio==null||$datos_recibidos[$keys]->laboratorio=='X'||$datos_recibidos[$keys]->laboratorio=='x'||$datos_recibidos[$keys]->laboratorio==''||$datos_recibidos[$keys]->laboratorio===0)&&$test_type_id!=null){
                            $billnumber = ($datos_recibidos[$keys]->factura!=''&&$datos_recibidos[$keys]->factura!='x'&&$datos_recibidos[$keys]->factura!='X'&&$datos_recibidos[$keys]->factura!=0&&$datos_recibidos[$keys]->factura!='0')?$datos_recibidos[$keys]->factura:null;
                            $cost = ($datos_recibidos[$keys]->costo!=''&&$datos_recibidos[$keys]->costo!='x'&&$datos_recibidos[$keys]->costo!='X'&&$datos_recibidos[$keys]->costo!=0&&$datos_recibidos[$keys]->costo!='0')?$datos_recibidos[$keys]->costo:null;
                            $enterprise_flag = ($datos_recibidos[$keys]->costo!=''&&$datos_recibidos[$keys]->costo!='x'&&$datos_recibidos[$keys]->costo!='X'&&$datos_recibidos[$keys]->costo!=0&&$datos_recibidos[$keys]->costo!='0')?true:false;
                            $tracing_id = $tracing->id;
                            //$laboratory_id = $laboratory_id->id;
                            $test_type_id = ($test_type_id!=null)?$test_type_id->id:null;

                            $exams = new ComplementaryExam();
                            $exams->bill_number = $billnumber;
                            $exams->cost = $cost;
                            $exams->enterprise_flag = $enterprise_flag;
                            $exams->tracing_id = $tracing_id;
                            $exams->laboratory_id = 18;//DESCONOCIDO
                            $exams->test_type_id = $test_type_id;
                            $exams->save();
                        }
                    }
                }
            }
            if($dBase == "X")
                $tracing->disease_criterias()->attach(15);
            if($dTos == "X")
                $tracing->disease_criterias()->attach(1);
            if($dTempratura == "X")
                $tracing->disease_criterias()->attach(2);
            if($dDolorCorporal == "X")
                $tracing->disease_criterias()->attach(3);
            if($dDificultadrespirar == "X")
                $tracing->disease_criterias()->attach(4);
            if($dDolorCabeza == "X")
                $tracing->disease_criterias()->attach(5);
            if($dFatiga == "X")
                $tracing->disease_criterias()->attach(6);
            if($dDolorGarganta == "X")
                $tracing->disease_criterias()->attach(7);
            if($dPerdidaGusto == "X")
                $tracing->disease_criterias()->attach(8);
            if($dDiarrea == "X")
                $tracing->disease_criterias()->attach(9);
            if($dPresionPecho == "X")
                $tracing->disease_criterias()->attach(10);
            if($dSecrecion == "X")
                $tracing->disease_criterias()->attach(11);
            if($dEstornudos == "X")
                $tracing->disease_criterias()->attach(12);
            if($dOjos == "X")
                 $tracing->disease_criterias()->attach(13);
            if($dContacto == "X")
                $tracing->disease_criterias()->attach(14);

        //GUARDANDO LA ADVERTENCIA Y EL RANGO
            $weighting_level = WeightingLevel::where('minimum','<=',$dPonderacion)
                ->where('maximum','>=',$dPonderacion)
                ->where('last_level_flag','=',false)
                ->first();

            $historical_weigthing_level = new HistoricalWeightingLevel;
            $historical_weigthing_level->calc_evaluation = $dPonderacion;
            if($weighting_level==null){
                //VERIFICA SI PERTENCE AL MAXIMO
                $weighting_level_max = WeightingLevel::where('minimum','<=',$dPonderacion)
                    ->where('last_level_flag','=',true)
                    ->first();
                if($weighting_level_max!=null){
                    $historical_weigthing_level->name = $weighting_level_max->name;
                    $historical_weigthing_level->description = $weighting_level_max->description;
                    $historical_weigthing_level->color = $weighting_level_max->color;
                    $historical_weigthing_level->minimum = $weighting_level_max->minimum;
                    $historical_weigthing_level->maximum = $weighting_level_max->maximum;
                    $historical_weigthing_level->evaluation = $weighting_level_max->evaluation;

                    $historical_weigthing_level->weighting_level_id = $weighting_level_max->id;
                    $historical_weigthing_level->tracing_id = $tracing->id;
                }
            }else{
                $historical_weigthing_level->name = $weighting_level->name;
                $historical_weigthing_level->description = $weighting_level->description;
                $historical_weigthing_level->color = $weighting_level->color;
                $historical_weigthing_level->minimum = $weighting_level->minimum;
                $historical_weigthing_level->maximum = $weighting_level->maximum;
                $historical_weigthing_level->evaluation = $weighting_level->evaluation;

                $historical_weigthing_level->weighting_level_id = $weighting_level->id;
                $historical_weigthing_level->tracing_id = $tracing->id;
            }
            $historical_weigthing_level->created_at =$dFecha;
            $historical_weigthing_level->save();

            //REGISTRAR KITS
            if($dKit != null && $dKit != "NO" && $dKit != "X"){
                $array_kit = [
                    'quantity' => 1,
                    'kit_id' => 1,
                    'tracing_id' => $tracing->id,
                ];
                $tracingkit = new kitTracing($array_kit);
                $tracingkit->save();
            }

        }
//        dd("FICHAAAAAS",$this->extras);
    }
    private function transformDateTime($value, string $format = 'Y-m-d')
    {
        /*try {
            return Carbon::instance(Date::excelToDateTimeObject($value))->format($format);
        } catch (\ErrorException $e) {
            return Carbon::createFromFormat($format, $value);
        }*/

        switch (gettype($value)){
            case "integer":
                return Carbon::instance(Date::excelToDateTimeObject($value))
                    ->format('Y-m-d');
                break;
            case "string":
                return Carbon::createFromFormat($format, $value);
                break;
            default:
                throw new RuntimeException("Error en formato fecha");
                break;
        }
    }

}
