<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithConditionalSheets;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class DatosBaseImport implements WithMultipleSheets
{
    private $mje_error;
    private $errors;
    public $seguimiento;

    function __construct($file)
    {
        $this->mje_error = '';
        $this->errors = [];
        $this->seguimiento = new BaseImport();
    }

    use WithConditionalSheets;

    public function conditionalSheets(): array
    {
        return [
            'BASE' => $this->seguimiento,
        ];
    }

    public function getSeguimientos()
    {
        return $this->seguimiento->getCompleto();
    }
}
