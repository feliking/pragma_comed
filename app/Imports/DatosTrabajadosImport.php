<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithConditionalSheets;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class DatosTrabajadosImport implements WithMultipleSheets
{
    private $mje_error;
    private $errors;
    public $seguimiento;
    private $datos;
    function __construct($file, $datos)
    {
        $this->mje_error = '';
        $this->errors = [];
        $this->datos = $datos;
    }

    use WithConditionalSheets;

    public function conditionalSheets(): array
    {
        return [
            'FICHAS' => new FichasImport($this->datos),
        ];
    }

}
