<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithConditionalSheets;

class DatosImport implements WithMultipleSheets
{
    private $mje_error;
    private $errors;
    public $seguimiento;

    function __construct($file, $datos)
    {
        $this->mje_error = '';
        $this->errors = [];
        $this->seguimiento = new SeguimientoImport($datos);
    }

    use WithConditionalSheets;

    public function conditionalSheets(): array
    {
        return [
            'SEGUIMIENTO' => $this->seguimiento,
        ];
    }

    public function getSeguimientos()
    {
        return $this->seguimiento->getExtras();
    }
}
