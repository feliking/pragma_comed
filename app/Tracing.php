<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tracing extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at','tracing_date'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tracings';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'comment', 'tracing_date', 'medical_request_id',
        'treatment_stage_id', 'risk_type_id', 'user_id', 'project_id', 'risk_state_id',
        'historical_request_id', 'medical_attention_flag'
    ];
    /*
     * Mutators
     */

    /*
     * Scope
     */
    public function scopeLastPerGroup(Builder $query, ?array $fields = null)
    {
        return $query->whereIn('id', function (QueryBuilder $query) use ($fields) {
            return $query->from(static::getTable())
                ->selectRaw('max(`id`)')
                ->groupBy($fields ?? static::$groupedLastScopeFields);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     *
     * @description = Muchos a muchos
     */
    public function disease_criterias(){
        return $this->belongsToMany(DiseaseCriteria::class,'tracing_disaese_criteria','tracing_id','disease_criteria_id')
            ->withTimestamps();
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function treatmentStage()
    {
        return $this->belongsTo('App\TreatmentStage');
    }

    public function riskType()
    {
        return $this->belongsTo('App\RiskType');
    }

    public function riskState()
    {
        return $this->belongsTo('App\RiskState');
    }

    public function medicalRequest()
    {
        return $this->belongsTo('App\MedicalRequest');
    }

    public function diseaseCriterias()
    {
        return $this->belongsToMany('App\DiseaseCriteria', 'tracing_disaese_criteria', 'tracing_id', 'disease_criteria_id');
    }

    public function complementaryExams()
    {
        return $this->hasMany('App\ComplementaryExam');
    }

    public function historical_weighting_level(){
        return $this->hasOne(HistoricalWeightingLevel::class);
    }

    public function kits()
    {
        return $this->belongsToMany('App\Kit', 'kit_tracing');
    }
}
