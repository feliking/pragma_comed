<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Support\Facades\Mail;
use App\Mail\AppointmentMail;

use App\Appointment;
use App\MedicalRequest;

class AppointmentJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 120;

    protected $medical_request;
    protected $appointment;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(MedicalRequest $medical_request, Appointment $appointment)
    {
        $this->medical_request = $medical_request;
        $this->appointment = $appointment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->medical_request->email)
            ->send(new AppointmentMail($this->medical_request, $this->appointment));
    }
}
