<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Support\Facades\Mail;
use App\Mail\MedicalConsultationMail;

use App\Attention;
use App\MedicalRequest;
use App\User;

class MedicalConsultatioJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 120;

    protected $medical_request;
    protected $attention;
    protected $doctor;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(MedicalRequest $medical_request, Attention $attention, User $doctor)
    {
        $this->medical_request = $medical_request;
        $this->attention = $attention;
        $this->doctor = $doctor;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->medical_request->email)
            ->send(new MedicalConsultationMail($this->medical_request, $this->attention, $this->doctor));
    }
}
