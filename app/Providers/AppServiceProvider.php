<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Index Lengths & MySQL / MariaDB
        Schema::defaultStringLength(191);

        $this->publishes([
            __DIR__.'/../../vendor/almasaeed2010/adminlte/dist' => public_path('adminlte/dist'),
            __DIR__.'/../../vendor/almasaeed2010/adminlte/plugins' => public_path('adminlte/plugins'),
        ], 'public');
    }
}
