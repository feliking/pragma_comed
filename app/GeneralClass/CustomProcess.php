<?php
namespace App\GeneralClass;
use App\Tracing;
/**
 *
 */

class CustomProcess
{

    public function getMessageTreatmentStages($medical_request_id)
    {
        $mensaje = null;
        $tracing_internacion_privada = Tracing::where('medical_request_id' , $medical_request_id)
            ->where('treatment_stage_id',2)
            ->count();
        $tracing_internacion_egs = Tracing::where('medical_request_id' , $medical_request_id)
            ->where('treatment_stage_id',3)
            ->count();
        $tracing_tratamiento_egs = Tracing::where('medical_request_id' , $medical_request_id)
            ->where('treatment_stage_id',4)
            ->count();
        $tracing_tratamiento_privado = Tracing::where('medical_request_id' , $medical_request_id)
            ->where('treatment_stage_id',6)
            ->count();
        // dd($tracing_internacion_privada, $tracing_internacion_egs, $tracing_tratamiento_egs, $tracing_tratamiento_privado);
        if ($tracing_internacion_privada > 0) {
            $mensaje = 'Internacion privada';
        }
        if ($tracing_internacion_egs > 0) {
            $mensaje = $mensaje.' Internacion EGS';
        }
        if ($tracing_tratamiento_egs > 0) {
            $mensaje = $mensaje.' Tratamiento EGS';
        }
        if ($tracing_tratamiento_privado > 0) {
            $mensaje = $mensaje.' Tratamiento privado';
        }

        return $mensaje;
    }
}
