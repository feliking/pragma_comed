<?php

namespace App\Http\Controllers;

use App\Http\Requests\TreatmentStageRequest;
use App\TreatmentStage;
use Illuminate\Http\Request;

class TreatmentStageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $treatment_stages = TreatmentStage::withTrashed()->get();
        return view('treatment_stage.index')
            ->with([
                'treatment_stages' => $treatment_stages
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('treatment_stage.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(TreatmentStageRequest $request)
    {
        DB::beginTransaction();
        try{

            $treatment_stage = new TreatmentStage($request->all());

            $treatment_stage->save();
            DB::commit();

            if(!$request->ajax()){
                flash('Se registró la etapa: '.$treatment_stage->name)
                    ->success()
                    ->important();
            }
        }catch(\Exception $ex){
            DB::rollBack();
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        if($request->ajax()){
            return response()->json([
                'id' => $treatment_stage->id,
                'treatment_stage' => $treatment_stage,
            ]);
        }else{
            return redirect()
                ->route('tratamiento_etapas.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $treatment_stage = TreatmentStage::withTrashed()->find($id);

        if($treatment_stage != null){
            return view('treatment_stage.show')
                ->with([
                    'treatment_stage' => $treatment_stage,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('tratamiento_etapas.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $treatment_stage = TreatmentStage::withTrashed()->find($id);

        if($treatment_stage != null){
            return view('treatment_stage.edit')
                ->with([
                    'cvd_state' => $treatment_stage,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('tratamiento_etapas.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(TreatmentStageRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $treatment_stage = TreatmentStage::withTrashed()->find($id);

            if($treatment_stage != null){
                $treatment_stage->fill($request->all());

                $treatment_stage->update();
                DB::commit();

                flash('Se modificó la etapa: '.$treatment_stage->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            DB::rollBack();
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        return redirect()
            ->route('tratamiento_etapas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $treatment_stage = TreatmentStage::withTrashed()->find($id);
            if($treatment_stage != null){
                if($treatment_stage->deleted_at == null){
                    $treatment_stage->delete();
                }else{
                    $treatment_stage->restore();
                }
                DB::commit();

                flash('Se modificó la etapa: '.$treatment_stage->name)
                    ->warning()
                    ->important();
            }else{
                DB::rollBack();
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            DB::rollBack();
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }
        return redirect()->route('tratamiento_etapas.index');
    }
}
