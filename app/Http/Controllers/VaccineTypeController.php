<?php

namespace App\Http\Controllers;

use App\VaccineType;
use Illuminate\Http\Request;

use DB;

class VaccineTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vaccine_types = VaccineType::withTrashed()->get();
        return view('vaccine_type.index')
            ->with([
                'vaccine_types' => $vaccine_types
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vaccine_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{

            $vaccine_type = new VaccineType($request->all());

            $vaccine_type->save();
            if(!$request->ajax()){
                flash('Se registró el tipo de vacuna: '.$vaccine_type->name)
                    ->success()
                    ->important();
            }
            DB::commit();
        }catch(\Exception $ex){
            DB::rollBack();
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        if($request->ajax()){
            return response()->json([
                'id' => $vaccine_type->id,
                'vaccine_type' => $vaccine_type,
            ]);
        }else{
            return redirect()
                ->route('tipos_vacuna.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vaccine_type = VaccineType::withTrashed()->find($id);

        if($vaccine_type != null){
            return view('vaccine_type.show')
                ->with([
                    'vaccine_type' => $vaccine_type,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('tipos_vacuna.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vaccine_type = VaccineType::withTrashed()->find($id);

        if($vaccine_type != null){
            return view('vaccine_type.edit')
                ->with([
                    'vaccine_type' => $vaccine_type,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('tipos_vacuna.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $vaccine_type = VaccineType::withTrashed()->find($id);

            if($vaccine_type != null){
                $vaccine_type->fill($request->all());

                $vaccine_type->update();
                DB::commit();

                flash('Se modificó el tipo de vacuna: '.$vaccine_type->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            DB::rollBack();
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        return redirect()
            ->route('tipos_vacuna.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $vaccine_type = VaccineType::withTrashed()->find($id);
            if($vaccine_type != null){
                if($vaccine_type->deleted_at == null){
                    $vaccine_type->delete();
                }else{
                    $vaccine_type->restore();
                }
                DB::commit();

                flash('Se eliminó el tipo de prueba: '.$vaccine_type->name)
                    ->warning()
                    ->important();
            }else{
                DB::rollBack();
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            DB::rollBack();
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }
        return redirect()->route('tipos_vacuna.index');
    }
}
