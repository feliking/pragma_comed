<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\NurseMedicalRequestValidation;
use App\Http\Requests\AttentionVitalSignRequest;

use App\MedicalRequest;
use App\HealthInsuranceRRHH;
use App\HealthInsuranceEnterpriseRRHH;
use App\EmployeeRRHH;
use App\RiskType;
use App\Appointment;
use App\User;
use App\AttentionVitalSign;
use App\EnterpriseSOL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
// Jobs
use App\Jobs\AssignToMedicJob;

// Whatsapp
use App\GeneralClass\WhatsApp;
use App\HistoricalRequest;
use App\MedicalRequestStatus;
use App\ProjectEnterprise;
use App\ProjectSOL;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class NurseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $related_doctors = Auth::user()
            ->medical_users
            ->pluck('id')
            ->toArray();

        /* $medical_requests = MedicalRequest::select('*', DB::raw('obtine_fecha_proxima_consulta(id) as fecha_proxima_consulta'))
            ->whereIn('user_id', $related_doctors)
            ->whereIn('medical_request_status_id', [2,15])
            ->orderByRaw('fecha_proxima_consulta is null desc')
            ->orderBy('fecha_proxima_consulta', 'asc')
            ->get(); */
        $enterprises = EnterpriseSOL::get();
        $medical_request_status = MedicalRequestStatus::where('system_flag', false)
            ->get();
        $enterprises_search = EnterpriseSOL::pluck('nombre', 'id')
            ->toArray();
        $enterprises_search = Arr::prepend($enterprises_search, 'TODOS', 0);

        $projects_search = array();
        $projects_search = Arr::prepend($projects_search, 'TODOS', 0);
        $medical_request_statuses = MedicalRequestStatus::whereIn('id', [2,15])
                ->pluck('name', 'id')
                ->toArray();
        $medical_request_statuses = Arr::prepend($medical_request_statuses, 'TODOS', 0);
        //dd($medical_requests, $request->all());
        //dd($request->all());
        /* if(isset($request->_) and count($request->all()) == 1){
            $medical_requests = MedicalRequest::select('*', DB::raw('obtine_fecha_proxima_consulta(id) as fecha_proxima_consulta'))
                ->where('user_id', $related_doctors)
                ->whereIn('medical_request_status_id', [1, 3, 4, 5, 6, 7])
                ->orderByRaw('fecha_proxima_consulta is null desc')
                ->orderBy('fecha_proxima_consulta', 'asc')
                ->get();
            //dd($medical_requests);
        }else 
            if(count($request->all()) > 1){ */
                
                if($request->ajax()){
                    //dd($request->all());
                    if(isset($request->medical_request_id_search)){
                        if($request->medical_request_id_search != '0'){
                            //$medical_requests = MedicalRequest::where('medical_request_status_id', $request->medical_request_id_search);
                            $medical_requests = MedicalRequest::select('*', DB::raw('obtine_fecha_proxima_consulta(id) as fecha_proxima_consulta'))
                            ->where('user_id', $related_doctors)
                            ->where('medical_request_status_id', $request->medical_request_id_search)
                            ->orderByRaw('fecha_proxima_consulta is null desc')
                            ->orderBy('fecha_proxima_consulta', 'asc');
                        }else{
                            //dd('fvf', $related_doctors);
                            $medical_requests = MedicalRequest::select('*', DB::raw('obtine_fecha_proxima_consulta(id) as fecha_proxima_consulta'))
                                ->whereIn('user_id', $related_doctors)
                                ->whereIn('medical_request_status_id', [2,15])
                                ->orderByRaw('fecha_proxima_consulta is null desc')
                                ->orderBy('fecha_proxima_consulta', 'asc')
                            /* ->get() */;
                            //dd('fvf', $medical_requests->get());
                        }
                    }
                    //dd($medical_requests->get());
                    if(isset($request->enterprise_id_search)){
                        if($request->enterprise_id_search != '0'){
                            $medical_requests = $medical_requests->where('employee_id','<>',null)
                                ->whereHas('employee', function (Builder $query) use ($request){
                                    $query->where('sol_empresa_id','=', $request->enterprise_id_search);
                                });
    
                            $projects_ids = ProjectEnterprise::where('empresa_id','=',$request->enterprise_id_search)
                                ->pluck('proyecto_id')
                                ->toArray();
    
                            $projects_search = ProjectSOL::whereIn('id',$projects_ids)
                                ->pluck('nombre','id')
                                ->toArray();
                            $projects_search = Arr::prepend($projects_search, 'TODOS', 0);
                        }
                    }
                    //dd($medical_requests->get());
                    if(isset($request->project_id_search)){
                        if($request->project_id_search != '0'){
                            $medical_requests = $medical_requests->where('employee_id','<>',null)
                                ->whereHas('employee', function (Builder $query) use ($request){
                                    $query->where('proyecto_id','=', $request->project_id_search);
                                });
                        }
                    }
                    $medical_request_statuses = MedicalRequestStatus::whereIn('id', [2,15])
                                ->pluck('name', 'id')
                                ->toArray();
                    $medical_request_statuses = Arr::prepend($medical_request_statuses, 'TODOS', 0);
                    $medical_requests = $medical_requests->get();
                    //dd($medical_requests);
                    $table = view('medical_request_nurse.partials.table')
                        ->with([
                            'medical_requests'         => $medical_requests,
                        ])
                        ->render();
                    $buscador = view('layouts.components.cards.search_nurse_pacients')
                        ->with([
                            'medical_request_statuses'  => $medical_request_statuses,
                            'enterprises_search'        => $enterprises_search,
                            'projects_search'           => $projects_search
                        ])
                        ->render();
                    return response()->json([
                            'table' => $table,
                            'buscador' => $buscador,
                        ]
                    );
                }else {
                    return view('medical_request_nurse.index')
                        ->with([
                            //'medical_requests' => $medical_requests,
                            'enterprises_search' => $enterprises_search,
                            'projects_search' => $projects_search,
                            'medical_request_statuses' => $medical_request_statuses,
                        ]);
                }
            /* } */
        //dd($medical_requests);
        /* if($request->ajax()){
            //dd($medical_requests);
            $table = view('medical_request_nurse.partials.table')
                    ->with([
                        'medical_requests'         => $medical_requests,
                    ])
                    ->render();
            $buscador = view('layouts.components.cards.search_nurse_pacients')
                    ->with([
                        'medical_request_statuses'  => $medical_request_statuses,
                        'enterprises_search'        => $enterprises_search,
                        'projects_search'           => $projects_search
                    ])
                    ->render();
            return response()->json([
                    'table' => $table,
                    'buscador' => $buscador,
                ]
            );

        }else{
            //dd($medical_requests);
            return view('medical_request_nurse.index')
                ->with([
                    //'medical_requests'       => $medical_requests,
                    'medical_request_status' => $medical_request_status,
                    'medical_request_statuses'       => $medical_request_statuses,
                    'enterprises_search'       => $enterprises_search,
                    'projects_search'       => $projects_search,

                ]);
        } */
        /* return view('medical_request_nurse.index')
            ->with([
                'medical_requests' => $medical_requests,
                'enterprises_search' => $enterprises_search,
                'projects_search' => $projects_search,
                'medical_request_statuses' => $medical_request_statuses,
            ]); */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        $health_insurances = HealthInsuranceRRHH::where('estado', 'Activo')
            ->where('id', '!=', 1)
            ->orderBy('id')
            ->get();

        $risk_types = RiskType::get();

        $related_doctors = Auth::user()
            ->medical_users
            ->pluck('id')
            ->toArray();

        $medical_users = User::whereIn('id', $related_doctors)->orderBy('name')
            ->get();

        return view('medical_request_nurse.create')
            ->with([
                'health_insurances' => $health_insurances,
                'risk_types'        => $risk_types,
                'medical_users'     => $medical_users,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(NurseMedicalRequestValidation $request)
    {
        DB::beginTransaction();
        try {
            do {
                $code = strtoupper(substr(uniqid(), -6));

                $verification_code = MedicalRequest::where('code', $code)
                    ->first();
            } while ($verification_code != null);

            $request_status = 2;
            $request_comment = 'ASIGNACIÓN DE MÉDICO - ENFERMERA';
            $request_user_id = $request->user_id;

            $request->merge([
                'code'                      => $code,
                'medical_request_status_id' => $request_status,
                'user_id'                   => $request_user_id
            ]);

            //VALIDA QUE NO TENGA NINGUN REGISTRO PENDIENTE
            $medical_request_validate = MedicalRequest::where('ci','=',$request->ci)
                ->where('medical_request_status_id', '<>', 4) //QUE NO ESTE CERRADO
                ->first();

            if($medical_request_validate == null){
                $medical_request = MedicalRequest::create($request->all());

                $employee = EmployeeRRHH::where([
                    ['ci_numero', 'LIKE', $request->ci],
                    ['ci_expedido', '=', $request->ci_expedition],
                    ['estado', '!=', 'Inactivo']
                ]);

                if ($employee->get() != null && $employee->count() == 1)
                {
                    $employee = $employee->first();

                    $medical_request->employee_id = $employee->id;
                    // $medical_request->project_id = $employee->sol_proyecto_id;
                    $medical_request->project_id = $employee->proyecto_id;
                    $medical_request->update();
                }

                $medical_request->medical_requests_status()
                    ->attach($request_status, [ //ASIGNADO EL ESTADO DE 'SOLICITADO'
                        'user_id' => null,
                        'comment' => $request_comment
                    ]);

                // Registro de la cita
                $appointment = new Appointment();
                $appointment->next_attention_date = $request->attention_date.' '.$request->attention_time;
                $appointment->variable_text = null;
                $appointment->comment = 'CITA GENERADA POR ENFERMERA';
                $appointment->appointment_type_id = 1; //PRESENCIAL
                $appointment->medical_request_id = $medical_request->id;
                $appointment->user_id = $request->user_id;

                $appointment->save();

                // Envío de Correo electrónico
                if ($medical_request->email != null && $medical_request->email != '')
                {
                    AssignToMedicJob::dispatch($medical_request);
                }

                if($medical_request->whatsapp_flag){
                    $whats = new WhatsApp();

                    $whats->wassengerSend($medical_request->phone_number, '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                            'Se realizó la solicitud médica de manera correcta, el código de seguimiento es el siguiente: *' . $medical_request->code . '* \n\n' .
                            '*Paciente:* ' . $medical_request->full_name . '\n' .
                            '*Cédula:* ' . $medical_request->full_ci . '\n' .
                            '*Empresa:* ' . $medical_request->enterprise . '\n' .
                            '*Médico:* ' . $medical_request->medic->employee->nombres . ' ' . $medical_request->medic->employee->apellido_1 . ' ' . $medical_request->medic->employee->apellido_2);
                }
            }else{
                $medical_request_validate->medical_requests_status()
                    ->attach(2, [ //ASIGNADO EL ESTADO DE 'SOLICITADO'
                        'user_id' => null,
                        'comment' => 'ASIGNACIÓN DE MÉDICO - ENFERMERA'
                    ]);

                $medical_request_validate->medical_request_status_id = 2;
                $medical_request_validate->user_id = $request->user_id;

                $medical_request_validate->update();

                // Registro de la cita
                $appointment = new Appointment();
                $appointment->next_attention_date = $request->attention_date.' '.$request->attention_time;
                $appointment->variable_text = null;
                $appointment->comment = 'CITA GENERADA POR ENFERMERA';
                $appointment->appointment_type_id = 1;
                $appointment->medical_request_id = $medical_request_validate->id;
                $appointment->user_id = Auth::user()->id;

                $appointment->save();

                // Envío de Correo electrónico
                if ($medical_request_validate->email != null && $medical_request_validate->email != '')
                {
                    AssignToMedicJob::dispatch($medical_request_validate);
                }

                if($medical_request_validate->whatsapp_flag){
                    $whats = new WhatsApp();

                    $whats->wassengerSend($medical_request_validate->phone_number, '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                            'Se realizó la solicitud médica de manera correcta, el código de seguimiento es el siguiente: *' . $medical_request_validate->code . '* \n\n' .
                            '*Paciente:* ' . $medical_request_validate->full_name . '\n' .
                            '*Cédula:* ' . $medical_request_validate->full_ci . '\n' .
                            '*Empresa:* ' . $medical_request_validate->enterprise . '\n' .
                            '*Médico:* ' . $medical_request_validate->medic->employee->nombres . ' ' . $medical_request_validate->medic->employee->apellido_1 . ' ' . $medical_request_validate->medic->employee->apellido_2);
                }
            }

            flash('Se registró la cíta de manera correcta')
                ->success()
                ->important();

            DB::commit();

            return redirect()->route('enfermera.index');
        }catch (\Throwable $e){
            DB::rollBack();

            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();

            return redirect()
                ->route('enfermera.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search_by_nurse (Request $request)
    {
        if ($request->ci != '' && $request->ci != null)
        {
            $employee = EmployeeRRHH::where([
                ['ci_numero', 'LIKE', $request->ci],
                ['ci_expedido', '=', $request->ci_expedition],
                ['estado', '!=', 'Inactivo']
            ])
                ->first();

            if ($employee != null)
            {
                $employee->enterprise;

                // Sugerimos el seguro social segun la empresa
                $health_insurance = HealthInsuranceRRHH::find(HealthInsuranceEnterpriseRRHH::where('empresa_id', $employee->sol_empresa_id)
                    ->first()
                    ->caja_id);

                $medical_request = MedicalRequest::where([
                    ['ci', 'LIKE', $request->ci],
                    ['ci_expedition', 'LIKE', $request->ci_expedition]
                ])
                    ->orderBy('created_at', 'DESC')
                    ->first();

                if ($medical_request != null)
                {
                    $medical_request->medical_request_status;
                    $medical_request->healthInsurance;

                    if ($medical_request->medic != null)
                    {
                        $medical_request->medic->role;
                        $medical_request->medic;
                    }

                    $msg = 'Último registro de consulta encontrado. Se copiaron los datos.';

                    $medical_request->lastAppointment;
                }
                else
                {
                    $msg = 'Información de empleado encontrado. Se copiaron los datos.';
                }

                return response()->json([
                    'employee'          => $employee,
                    'medical_request'   => $medical_request,
                    'health_insurance'  => $health_insurance,
                    'msg'               => $msg,
                    'msg_type'          => 'success'
                ]);
            }
            else
            {
                return response()->json([
                    'msg'           => 'No se encontró al empleado, favor ingrese todos los datos del formulario.',
                    'msg_type'      => 'danger',
                    'ci'            => $request->ci,
                    'ci_expedition' => $request->ci_expedition
                ]);
            }
        }
        else
        {
            return response()->json([
                'msg'       => 'Debe ingresar el C.I. del empleado para buscar.',
                'msg_type'  => 'danger'
            ]);
        }
    }
    public function view_cases(){
        $related_doctors = Auth::user()
            ->medical_users
            ->pluck('id')
            ->toArray();
        $historical_request = HistoricalRequest::whereIn('user_id', $related_doctors)->get()->pluck('medical_request_id')->toArray();
        $historical_request_w = array_unique($historical_request);
        $medical_requests = MedicalRequest::whereIn('id', $historical_request_w)->get();
        return view('nurse_cases_doctor.index')
            ->with([
                'related_doctors' => $related_doctors,
                'medical_requests' => $medical_requests
            ]);
    }

    public function getSignosVitales($medical_request_id)
    {
        $medical_request = MedicalRequest::find($medical_request_id);

        $attention_vital_sign = AttentionVitalSign::where('medical_request_id',$medical_request_id)
            ->orderBy('id','desc')
            ->first();

        $html = view('medical_request_nurse.modals.content_modal')
            ->with([
                'attention_vital_sign' => $attention_vital_sign,
                'medical_request' => $medical_request,
            ])
            ->render();

        return response()->json([
            'content' => $html,
        ]);

//        $html = view('medical_request_nurse.modals.content_modal')
//                    ->with([
//                        'medical_request' => $medical_request,
//                    ])
//                    ->render();
//
//
//        return response()->json([
//            'content' => $html,
//        ]);
    }

    public function storeVitalSigns(AttentionVitalSignRequest $request)
    {
        // dd('paso validacion', $request->all());
        DB::beginTransaction();
        try {
//            $medical_request = MedicalRequest::find($request->medical_request_id);

            $medical_request = MedicalRequest::find($request->medical_request_id);
            $attention_vital_sign = AttentionVitalSign::where('medical_request_id',$medical_request->id)
                ->orderBy('id','desc')
                ->first();
            if($attention_vital_sign != null){
                $attention_vital_sign->attention_date = Carbon::now();
                $attention_vital_sign->fc = $request->fc;
                $attention_vital_sign->fr = $request->fr;
                $attention_vital_sign->t = $request->t;
                $attention_vital_sign->pa = $request->pa;
                $attention_vital_sign->sao2 = $request->sao2;
                $attention_vital_sign->otros = $request->other;
                $attention_vital_sign->medical_request_id = $medical_request->id;
                $attention_vital_sign->update();
            }else{
                // dd($medical_request, $request->medical_request_id);
                $attention_vital_sign = new AttentionVitalSign;
                $attention_vital_sign->attention_date = Carbon::now();
                $attention_vital_sign->fc = $request->fc;
                $attention_vital_sign->fr = $request->fr;
                $attention_vital_sign->t = $request->t;
                $attention_vital_sign->pa = $request->pa;
                $attention_vital_sign->sao2 = $request->sao2;
                $attention_vital_sign->otros = $request->other;
                $attention_vital_sign->medical_request_id = $medical_request->id;
                $attention_vital_sign->save();
            }


            DB::commit();
            $filas[$medical_request->id] = view('medical_request_nurse.partials.tr_content')
                    ->with([
                        'medical_request' => $medical_request
                    ])->render();

            return response()->json([
                'type'  => 'correct',
                'msj'   => 'Se registraron los signos vitales',
                'filas' => $filas
            ]);


        } catch (\Throwable $e) {
            DB::rollBack();
            flash('No se pudo registrar la atencion')
                ->error()
                ->important();
        }
    }

    /**
     * obtien el ultimo registro de los signos vitales auxiliares
     * @param  [type] $medical_request_id [description]
     * @return [type]                     [description]
     */
    public function editSignosVitales($medical_request_id)
    {
        $medical_request = MedicalRequest::find($medical_request_id);

        $attention_vital_sign = AttentionVitalSign::where('medical_request_id',$medical_request_id)
            ->orderBy('id','desc')
            ->first();

        $html = view('medical_request_nurse.modals.content_modal')
                    ->with([
                        'attention_vital_sign' => $attention_vital_sign,
                        'medical_request' => $medical_request,
                    ])
                    ->render();

        return response()->json([
            'content' => $html,
        ]);
    }

    public function updateVitalSigns(AttentionVitalSignRequest $request)
    {
        // dd('paso validacion', $request->all());
        DB::beginTransaction();
        // try {
            $medical_request = MedicalRequest::find($request->medical_request_id);
            $attention_vital_sign = AttentionVitalSign::where('medical_request_id',$medical_request->id)
            ->orderBy('id','desc')
            ->first();

            $attention_vital_sign->attention_date = Carbon::now();
            $attention_vital_sign->fc = $request->fc;
            $attention_vital_sign->fr = $request->fr;
            $attention_vital_sign->t = $request->t;
            $attention_vital_sign->pa = $request->pa;
            $attention_vital_sign->sao2 = $request->sao2;
            $attention_vital_sign->otros = $request->other;
            $attention_vital_sign->medical_request_id = $medical_request->id;
            $attention_vital_sign->update();

            DB::commit();
            $filas[$medical_request->id] = view('medical_request_nurse.partials.tr_content')
                    ->with([
                        'medical_request' => $medical_request
                    ])->render();

            return response()->json([
                'type'  => 'correct',
                'msj'   => 'Se registraron los signos vitales',
                'filas' => $filas
            ]);


        // } catch (\Throwable $e) {
        //     DB::rollBack();
        //     flash('No se pudo registrar la atencion')
        //         ->error()
        //         ->important();
        // }
    }
}
