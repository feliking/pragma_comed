<?php

namespace App\Http\Controllers;

use App\EmployeeRRHH;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Http\Requests\PendingRequestValidation;

use App\MedicalRequest;
use App\EnterpriseSOL;
use App\HistoricalRequest;
use App\User;
use DB;
use Carbon\Carbon;

class PendingRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $medical_requests = MedicalRequest::where('medical_request_status_id', 3)
            ->orderBy('created_at', 'ASC')
            ->get();

        $enterprises = EnterpriseSOL::get();

        return view('pending_request.index')
            ->with([
                'medical_requests' => $medical_requests,
                'enterprises'      => $enterprises,
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function assignToEmployee(PendingRequestValidation $request){
        DB::beginTransaction();

        try {
            $filas = [];

            $medical_request = MedicalRequest::find($request->medical_request_id);

            $employee = EmployeeRRHH::find($request->employee_id);

            $medical_request->medical_requests_status()
                ->attach(5, [ //VALIDADO
                    'user_id' => Auth::id(),
                    'comment' => 'VALIDADO POR RRHH'
                ]);

            $medical_request->employee_id = $request->employee_id;
            $medical_request->full_name = $request->full_name;
            $medical_request->medical_request_status_id = 5; //VALIDADO

            $medical_request->ci = $employee->ci_numero;
            $medical_request->ci_expedition = $employee->ci_expedido;

            $medical_request->update();

            $filas[$medical_request->id] = view('pending_request.partials.tr_content')
                ->with([
                    'medical_request' => $medical_request
                ])->render();
            DB::commit();

            return response()->json([
                'type' => 'correct',
                'msj' => 'Se validó la solicitud ('.$medical_request->code.') por RRHH',
                'filas' => $filas
            ]);
        }catch (\Throwable $e){
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj' => 'Ocurrio un error'
            ]);
        }
    }

    public function assign(PendingRequestValidation $request){
        DB::beginTransaction();

        try {
            $filas = [];

            $medical_request = MedicalRequest::withTrashed()->find($request->medical_request_id);

            $medical_request->medical_requests_status()
                ->attach(5, [ //VALIDADO
                    'user_id' => Auth::id(),
                    'comment' => 'VALIDADO POR RRHH'
                ]);

            $medical_request->full_name = $request->full_name;
            $medical_request->ci = $request->ci;
            $medical_request->ci_expedition = $request->ci_expedition;
            $medical_request->enterprise = $request->enterprise;
            $medical_request->medical_request_status_id = 5; //VALIDADO
            $medical_request->update();

            $filas[$medical_request->id] = view('pending_request.partials.tr_content')
                ->with([
                    'medical_request' => $medical_request
                ])->render();
            DB::commit();

            return response()->json([
                'type' => 'correct',
                'msj' => 'Se validó la solicitud ('.$medical_request->code.') por RRHH',
                'filas' => $filas
            ]);
        }catch (\Throwable $e){
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj' => 'Ocurrio un error'
            ]);
        }
    }

    public function deleteRequest(PendingRequestValidation $request){
        DB::beginTransaction();

        try {
            $filas = [];

            $medical_request = MedicalRequest::find($request->medical_request_id);

            $medical_request->medical_requests_status()
                ->attach(7, [ //RECHAZADO
                    'user_id' => Auth::id(),
                    'comment' => 'RECHAZADO POR RRHH'
                ]);

            $medical_request->medical_request_status_id = 7; //RECHAZADO

            // $medical_request->historial_requests()
            //     ->delete();
            $medical_request->update();
            $medical_request->delete();

            $filas[$medical_request->id] = view('pending_request.partials.tr_content')
                ->with([
                    'medical_request' => $medical_request
                ])->render();
            DB::commit();

            return response()->json([
                'type' => 'correct',
                'msj' => 'Se eliminó la solicitud ('.$medical_request->code.')',
                'filas' => $filas
            ]);
        }catch (\Throwable $e){
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj' => 'Ocurrio un error'
            ]);
        }
    }
    public function reject_request(){

        $medical_requests = MedicalRequest::withTrashed()->where('deleted_at', '!=', null)
            ->orderBy('created_at', 'ASC')
            ->get();
        //dd($medical_requests);
        $enterprises = EnterpriseSOL::get();

        return view('medical_request_rejected.index')
            ->with([
                'medical_requests' => $medical_requests,
                'enterprises'      => $enterprises,
            ]);
    }

    public function reassignment_request(){
        $medical_requests = MedicalRequest::whereIn('medical_request_status_id', [2,8,9,10,11,12,13])
            ->orderBy('created_at', 'ASC')
            ->get();
        //dd($medical_requests);
        $enterprises = EnterpriseSOL::get();
        $medical_users = User::where('role_id', 3)
            ->orderBy('name')
            ->get();
        return view('medical_request_reassignment.index')
            ->with([
                'medical_requests'  => $medical_requests,
                'enterprises'       => $enterprises,
                'medical_users'     => $medical_users->pluck('name','id'),
            ]);
    }

    public function reassignment_request_post(Request $request){

        if ($request!=null) {
            $medical_request = MedicalRequest::find($request->id);
            if($medical_request != null){
                $medical_request->user_id = $request->select_doc;
                $medical_request->medical_request_status_id = 2;
                $medical_request->update();
                $medical_request->medical_requests_status()
                    ->attach(2, [
                        'user_id' => $request->select_doc,
                        'comment' => 'ASIGNACIÓN DE SOLICITUD - REASIGNACIÓN'
                    ]);
                return response()->json([
                    'message' => 'Se reasignó la solicitud médica Nro: ' . $medical_request->code,
                ]);
            }else{
                return response()->json([
                    'error' => 'La splicitud médica es incorrecta',
                ]);
            }
        } else {
            return response()->json([
                'error' => 'No escogió al médico para la reasignación',
            ]);
        }
    }
}
