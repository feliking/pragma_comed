<?php

namespace App\Http\Controllers;

use App\ComplementaryExam;
use App\Documentos;
use App\EmployeeRRHH;
use App\HealthInsuranceRRHH;
use App\GeneralClass\WhatsApp;
use App\HistoricalRequest;
use App\Http\Requests\MedicalRequestValidation;
use App\Imports\DatosBaseImport;
use App\Imports\DatosImport;
use App\Imports\DatosTrabajadosImport;
use App\Imports\RecursosHumanos\EmployeesParametersImport;
use App\MedicalRequest;
use App\MedicalRequestStatus;
use App\RiskType;
use App\Appointment;
use App\DiseaseCriteria;
use App\HistoricalWeightingLevel;
use App\Tracing;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;

use File;
use Excel;
use Response;

// Jobs
use App\Jobs\MedicalRequestJob;
use App\Jobs\AssignToMedicJob;
use App\Kit;
use App\kitTracing;
use App\Laboratory;
use App\Parameter;
use App\RiskState;
use App\TestType;
use App\TreatmentStage;
use App\User;
use App\WeightingLevel;
use Illuminate\Support\Facades\Storage;
use PDF;

class MedicalRequestController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('check_data_per_rol')
            ->only(['view_edit_data_request','update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('layouts.app_menu');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $risk_types = RiskType::get();

        if(Auth::user()!=null){
            switch (Auth::user()->role_id) {
                case Auth::user()->role_id==3:
                    $medical_users = User::where('role_id', 3)
                        ->orderBy('name')
                        ->get();

                    break;
                case Auth::user()->role_id==4:
                    $medical_users1 = User::find(Auth::user()->id);
                    $array = [];
                    foreach ($medical_users1->medical_users as $key => $doc) {
                        array_push($array, $doc->id);
                    }
                    $medical_users = User::where('role_id', 3)
                        ->whereIn('id',$array)
                        ->orderBy('name')
                        ->get();
                    //dd($medical_users1, $medical_users);
                    break;
                default:
                    $medical_users = User::where('role_id', 3)
                        ->orderBy('name')
                        ->get();
                    break;
            }
        }else{
            $medical_users = User::where('role_id', 3)
                ->orderBy('name')
                ->get();
        }

        $health_insurances = HealthInsuranceRRHH::where('estado', 'Activo')
            ->where('id', '!=', 1)
            ->orderBy('id')
            ->get();
        $treatment_stages = TreatmentStage::get()
            ->pluck('name','id');
        $kit_types = Kit::orderBy('id', 'asc')->get()->pluck('name','id');
        $parameter_age = Parameter::find(7)->value; //EDAD_INSEGURA
        $parameter_value = Parameter::find(8)->value; //VALOR_EDAD_INSEGURA
        $laboratories = Laboratory::get()
            ->pluck('name','id');
        $test_types = TestType::get()
            ->pluck('name','id');
        return view('medical_request.create')
            ->with([
                'risk_types'            => $risk_types,
                'medical_users'         => $medical_users,
                'health_insurances'     => $health_insurances,
                'treatment_stages'      => $treatment_stages,
                'risk_types'            => $risk_types,
                'kit_types'            => $kit_types,
                'parameter_age'            => $parameter_age,
                'parameter_value'            => $parameter_value,
                'laboratories'            => $laboratories,
                'test_types'            => $test_types,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(MedicalRequestValidation $request)
    {
        DB::beginTransaction();
        try {
            do {
                $code = strtoupper(substr(uniqid(), -6));

                $verification_code = MedicalRequest::where('code', $code)
                    ->first();
            } while ($verification_code != null);

            if ($request->user_id)
            {
                $request_status = 2;
                $request_comment = 'ASIGNACIÓN DE MÉDICO - AUTOMÁTICO';
                $request_user_id = $request->user_id;
            }
            else
            {
                $request_status = 1;
                $request_comment = 'ASIGNACIÓN DE SOLICITUD - AUTOMÁTICO';
                $request_user_id = null;
            }

            $request->merge([
                'code'                      => $code,
                'medical_request_status_id' => $request_status,
                'user_id'                   => $request_user_id
            ]);

            //VALIDA QUE NO TENGA NINGUN REGISTRO PENDIENTE
            $medical_request_validate = MedicalRequest::where('ci','=',$request->ci)
                ->where('medical_request_status_id', '<>', 4) //QUE NO ESTE CERRADO
                ->first();
            if($medical_request_validate == null){
                $medical_request = MedicalRequest::create($request->all());

                if (Auth::user() != null)
                {
                    $employee = EmployeeRRHH::where([
                        ['ci_numero', 'LIKE', $request->ci],
                        ['ci_expedido', '=', $request->ci_expedition],
                        ['estado', '!=', 'Inactivo']
                    ]);

                    if ($employee->get() != null && $employee->count() == 1)
                    {
                        $employee = $employee->first();

                        $medical_request->employee_id = $employee->id;
                        // $medical_request->project_id = $employee->sol_proyecto_id;
                        $medical_request->project_id = $employee->proyecto_id;
                        $medical_request->update();
                    }
                }

                $medical_request->medical_requests_status()
                    ->attach($request_status, [ //ASIGNADO EL ESTADO DE 'SOLICITADO'
                        'user_id' => null,
                        'comment' => $request_comment
                    ]);

                // Envío de Correo electrónico
                if ($medical_request->email != null && $medical_request->email != '' && Auth::user() != null)
                {
                    if ($request_status == 1)
                    {
                        MedicalRequestJob::dispatch($medical_request);
                    }
                    else
                    {
                        AssignToMedicJob::dispatch($medical_request);
                    }
                }
                //Form Seg
                if($request->treatment_stage_id!=null && $request->risk_state_id!=null){
                    $historical_request = new HistoricalRequest();
                    $historical_request->user_id = Auth::id();
                    $historical_request->comment = 'SEGUIMIENTO REALIZADO';
                    $historical_request->medical_request_id = $medical_request->id;
                    $medical_request->medical_request_status_id = 12; //ASIGNADO EL ESTADO DE 'EN SEGUIMIENTO'
                    $historical_request->medical_request_status_id = 12; //ASIGNADO EL ESTADO DE 'EN SEGUIMIENTO'
                    $medical_request->update();

                    $historical_request->save();

                    $project = null;
                    if($medical_request->employee_id!=null){
                        $employee = EmployeeRRHH::find($medical_request->employee_id);
                        $project = $employee->sol_proyecto_id;
                    }

                    $request->merge([
                        'tracing_date' => Carbon::now(),
                        'medical_request_id' => $medical_request->id,
                        'user_id' => Auth::id(),
                        'historical_request_id' => $historical_request->id,
                        'project_id' => $project
                    ]);
                    $tracing = new Tracing($request->all());
                    $tracing->comment = $request->comment_tracing;
                    $tracing->save();

                    //************************************INICIO:CALCULO DE LA PONDERACION
                    $sum = 0;

                    //INICIO:PREGUNTA LA EDAD
                    $parameter_age = Parameter::find(7)->value; //EDAD_INSEGURA
                    $edad = 0;
                    if($medical_request->employee!=null){
                        $edad = $medical_request->employee->edad;
                    }else{
                        $edad = $medical_request->edad;
                    }

                    if($edad >= (int) $parameter_age){
                        $sum++;
                    }
                    //FIN:PREGUNTA LA EDAD

                    //INICIO:PREGUNTA ESTADO DE RIESGO
                    $risk_state = RiskState::find($request->risk_state_id);
                    if($risk_state->weighting > 0){
                        $sum = $sum + $risk_state->weighting;
                    }
                    //FIN:PREGUNTA ESTADO DE RIESGO

                    //INICIO:PREGUNTA CAMPOS MARCADOS
                    if($request->disease_criteria!=null){
                        foreach ($request->disease_criteria as $disease_criteria){
                            $tracing->disease_criterias()->attach($disease_criteria);
                            $disease = DiseaseCriteria::find($disease_criteria);
                            $sum += $disease->weighing;
                        }
                    }
                    //FIN:PREGUNTA CAMPOS MARCADOS

                    //GUARDANDO LA ADVERTENCIA Y EL RANGO
                    $weighting_level = WeightingLevel::where('minimum','<=',$sum)
                        ->where('maximum','>=',$sum)
                        ->where('last_level_flag','=',false)
                        ->first();

                    $historical_weigthing_level = new HistoricalWeightingLevel();
                    $historical_weigthing_level->calc_evaluation = $sum;

                    if($weighting_level==null){
                        //VERIFICA SI PERTENCE AL MAXIMO
                        $weighting_level_max = WeightingLevel::where('minimum','<=',$sum)
                            ->where('last_level_flag','=',true)
                            ->first();
                        if($weighting_level_max!=null){
                            $historical_weigthing_level->name = $weighting_level_max->name;
                            $historical_weigthing_level->description = $weighting_level_max->description;
                            $historical_weigthing_level->color = $weighting_level_max->color;
                            $historical_weigthing_level->minimum = $weighting_level_max->minimum;
                            $historical_weigthing_level->maximum = $weighting_level_max->maximum;
                            $historical_weigthing_level->evaluation = $weighting_level_max->evaluation;

                            $historical_weigthing_level->weighting_level_id = $weighting_level_max->id;
                            $historical_weigthing_level->tracing_id = $tracing->id;
                        }
                    }else{
                        $historical_weigthing_level->name = $weighting_level->name;
                        $historical_weigthing_level->description = $weighting_level->description;
                        $historical_weigthing_level->color = $weighting_level->color;
                        $historical_weigthing_level->minimum = $weighting_level->minimum;
                        $historical_weigthing_level->maximum = $weighting_level->maximum;
                        $historical_weigthing_level->evaluation = ($request->risk_type_id==2 ||$request->risk_type_id==4)?7:$weighting_level->evaluation;

                        $historical_weigthing_level->weighting_level_id = $weighting_level->id;
                        $historical_weigthing_level->tracing_id = $tracing->id;
                    }
                    $historical_weigthing_level->save();
                    //************************************FIN:CALCULO DE LA PONDERACION

                    //GUARDANDO EXAMENES COMPLEMENTARIOS
                    $files = $request->file('files');
                    // dd($files);
                    if($request->laboratories!=null){
                        for ($i=0 ; $i<count($request->laboratories) ;$i++){
                            $complementary_exam = new ComplementaryExam;
                            $complementary_exam->tracing_id = $tracing->id;

                            $complementary_exam->laboratory_id = $request->laboratories[$i];
                            $complementary_exam->test_type_id = $request->test_types[$i];
                            $complementary_exam->cost = $request->costs[$i];
                            $complementary_exam->bill_number = $request->bill_numbers[$i];
                            $complementary_exam->enterprise_flag = ($request->enterprise_pay_flags[$i]=="true")?true:false;
                            $complementary_exam->result_flag = ($request->result_flags[$i]=="true")?true:false;
                            $complementary_exam->save();

                            if ($files != null)
                            {
                                if(array_key_exists($i,$files)){
                                    $path = $files[$i]->store('complementary_exams');

                                    $complementary_exam->files()->create([
                                        'name' => $files[$i]->getClientOriginalName(),
                                        'file_type_id' => 1,
                                        'location' => $path,
                                        'mime' => $files[$i]->getClientMimeType()
                                    ]);
                                }
                            }
                        }

                    }
                    //GUARDANDO KITS
                    if($request->kit!=null && $request->kit==1){
                        $kit_tracing = new kitTracing();
                        $kit_tracing->quantity = $request->quantity;
                        $kit_tracing->kit_id = $request->kit_id;
                        $kit_tracing->tracing_id = $tracing->id;
                        $kit_tracing->save();
                    }
                    $request_status = 12;
                }
                DB::commit();

                if($medical_request->whatsapp_flag){
                    $whats = new WhatsApp();

                    if ($request_status == 1)
                    {
                        $whats->wassengerSend($medical_request->phone_number, '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                            'Se realizó la solicitud médica de manera correcta, el código de seguimiento es el siguiente: *' . $medical_request->code . '* \n\n' .
                            '*Paciente:* ' . $medical_request->full_name . '\n' .
                            '*Cédula:* ' . $medical_request->full_ci . '\n' .
                            '*Empresa:* ' . $medical_request->enterprise);
                    }
                    else
                    {
                        if(Auth::user() != null){
                            if(Auth::user()->role_id!=8&&Auth::user()->role_id!=7){
                            $whats->wassengerSend($medical_request->phone_number, '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                                'Se realizó la solicitud médica de manera correcta, el código de seguimiento es el siguiente: *' . $medical_request->code . '* \n\n' .
                                '*Paciente:* ' . $medical_request->full_name . '\n' .
                                '*Cédula:* ' . $medical_request->full_ci . '\n' .
                                '*Empresa:* ' . $medical_request->enterprise . '\n' .
                                '*Médico:* ' . $medical_request->medic->employee->nombres . ' ' . $medical_request->medic->employee->apellido_1 . ' ' . $medical_request->medic->employee->apellido_2);
                            }
                        }else{
                            $whats->wassengerSend($medical_request->phone_number, '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                                'Se realizó la solicitud médica de manera correcta, el código de seguimiento es el siguiente: *' . $medical_request->code . '* \n\n' .
                                '*Paciente:* ' . $medical_request->full_name . '\n' .
                                '*Cédula:* ' . $medical_request->full_ci . '\n' .
                                '*Empresa:* ' . $medical_request->enterprise . '\n' .
                                '*Médico:* ' . $medical_request->medic->employee->nombres . ' ' . $medical_request->medic->employee->apellido_1 . ' ' . $medical_request->medic->employee->apellido_2);
                        }
                    }
                }
                if($request_status == 12){
                    /* flash('Se registró la solicitud con código: '.$medical_request->code.' y el seguimiento correctamente')
                        ->success()
                        ->important();
                    return redirect()->route('administracion_seguimientos.index'); */
                    return redirect()->route('solicitudes_medicas.show', $medical_request->code);
                }
                if ($request_status == 1)
                {
                    return redirect()->route('solicitudes_medicas.show', $medical_request->code);
                }
                else
                {
                    return redirect()->route('administracion_solicitudes.index');
                }
            }else{
                DB::rollBack();
                flash('Este paciente ya cuenta con una solicitud de atención en curso con el código: ' . $medical_request_validate->code . ' favor tener paciencia, nuestro personal se contactará a la brevedad posible.')
                    ->error()
                    ->important();
                return redirect()->back();
            }


        }catch (\Throwable $e){
            DB::rollBack();
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
            return redirect()
                ->route('solicitudes_medicas.create');
                //return redirect()->route('administracion_seguimientos.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($code)
    {
        $medical_request = MedicalRequest::firstWhere('code','=',$code);
        if($medical_request!=null){
            //EXISTE EL REGISTRO

            return view('consult.index')
                ->with([
                    'medical_request' => $medical_request
                ]);
        }else{
            //NO SE ENCONTRO EL REGISTRO CON ESE CODIGO
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MedicalRequestValidation $request, $id)
    {
        //dd($request->all());
        try{
            $medical_request = MedicalRequest::find($id);

            if($medical_request != null){
                $medical_request->fill($request->all());

                $medical_request->update();

                flash('Se modificó la información del paciente '.$medical_request->full_name.' con solicitud Nro: '.$medical_request->code)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }
        switch (Auth::user()->role_id) {
            case Auth::user()->role_id==4:
                return redirect()->route('enfermera.index');
                break;
            case Auth::user()->role_id==7:
                return redirect()->route('administracion_seguimientos.index');
                break;
            case Auth::user()->role_id==10:
                return redirect()->route('administracion_seguimientos.index');
            break;
            default:
                return redirect()->back();
                break;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function search_code_view(){
        return view('medical_request.search_code');
    }

    public function showFiles($id){
        $exams = ComplementaryExam::find($id);

//        $files = Files::where('medical_request_id','=',$id)
//            ->orderBy('id', 'DESC')
//            ->get();

        $view = view('layouts.components.modals.partials.table_modal_files')
            ->with([
                'files' => $exams->files
            ])->render();

        return response()->json([
            'type'=>'correct',
            'view' => $view
        ]);
    }

    public function search_code(Request $request){

        if ($request->code != '' && $request->code != "null") {
            $code = MedicalRequest::select('*', DB::raw('obtine_fecha_proxima_consulta(id) as fecha_proxima_consulta'))
                ->where('code', $request->code)
                ->get()
                ->first();
            if($code!=null){
                $fecha_proxima_atencion = Appointment::where('medical_request_id', $code->id)
                    ->orderBy('id','desc')
                    ->first();
                $history = HistoricalRequest::where('medical_request_id', $code->id)
                    ->orderBy('created_at', 'DESC')
                    ->get();
                return response()->json(
                    view('medical_request.partials.time_line')
                    ->with([
                        'history'                => $history,
                        'code'                   => $code,
                        'fecha_proxima_atencion' => $fecha_proxima_atencion,
                    ])->render()
                );
            }else{
                return response()->json([
                    'error' => 'No se encontró el código ingresado: ' . $request->code,
                ]);
            }
        }else{
            return response()->json([
                'error' => 'No ingresó ningún código',
            ]);
        }
    }

    public function mostrar_timeline(Request $request){

        if ($request->code != '' && $request->code != "null") {
            $code = MedicalRequest::select('*', DB::raw('obtine_fecha_proxima_consulta(id) as fecha_proxima_consulta'))
                ->where('code', $request->code)
                ->get()
                ->first();
            if($code!=null){
                $fecha_proxima_atencion = Appointment::where('medical_request_id', $code->id)
                    ->orderBy('id','desc')
                    ->first();
                $history = HistoricalRequest::where('medical_request_id', $code->id)
                    ->orderBy('created_at', 'DESC')
                    ->get();
                return response()->json(
                    view('medical_consultations.partials.time_line')
                        ->with([
                            'history'                => $history,
                            'code'                   => $code,
                            'fecha_proxima_atencion' => $fecha_proxima_atencion,
                        ])->render()
                );
            }else{
                return response()->json([
                    'error' => 'No se encontró el código ingresado: ' . $request->code,
                ]);
            }
        }else{
            return response()->json([
                'error' => 'No ingresó ningún código',
            ]);
        }
    }


    public function search_ci(Request $request){
        if ($request->ci != '' && $request->ci != "null") {
            $employee = EmployeeRRHH::where('ci_numero',$request->ci)->where('ci_expedido',$request->ci_expedition)->Where('estado', '=', 'Activo')
                ->get()->first();
            if($employee!=null){
                $medical_request = MedicalRequest::select('*', DB::raw('obtine_fecha_proxima_consulta(id) as fecha_proxima_consulta'))
                    ->where('employee_id', $employee->id)
                    ->get();
                //dd($request->all(), $employee, $medical_request);//489791
                return response()->json(
                    view('medical_request.partials.time_line_employee')
                    ->with([
                        'employee' => $employee,
                        'medical_request' => $medical_request,
                    ])->render()
                );
            }else{
                return response()->json([
                    'error' => 'No se ha encontrado el historial del C.I. ingresado',
                ]);
            }
        }else{
            return response()->json([
                'error' => 'No ingresó ningún ci',
            ]);
        }
    }

    public function search_medical_request (Request $request)
    {
        $medical_request = MedicalRequest::where([
            ['ci', '=', $request->ci],
            ['ci_expedition', '=', $request->ci_expedition]
        ])
            ->orderBy('created_at', 'DESC')
            ->first();
        //dd($medical_request);
        //$medical_request->medical_requests_type;
        //$medical_request->healthInsurance;


        if ($medical_request != null)
        {
            $medical_request->medical_request_status;
            $medical_request->healthInsurance;
            $medical_request->risk_type;
            $appointment = $medical_request->appointments()
                ->orderBy('next_attention_date', 'DESC')
                ->first();

            return response()->json([
                'medical_request' => $medical_request,
                'appointment' => $appointment
            ]);
        }
        else
        {
            return response()->json([
                'msj' => 'No se encuentra a la persona, debe generar una nueva solicitud.'
            ]);
        }
    }
    public function search_medical_request_employee(Request $request){

        if ($request->ci != '' && $request->ci != "null") {
            $employee = EmployeeRRHH::where('ci_numero',$request->ci)->where('ci_expedido',$request->ci_expedition)->Where('estado', '=', 'Activo')
                ->get()->first();
            if($employee!=null){
                $employee->enterprise;
                $medical_request = MedicalRequest::where([
                    ['ci', '=', $request->ci],
                    ['ci_expedition', '=', $request->ci_expedition]
                ])
                    ->orderBy('created_at', 'DESC')
                    ->first();
                $appointment = null;
                if ($medical_request != null){
                    $medical_request->medical_request_status;
                    $medical_request->healthInsurance;
                    $medical_request->risk_type;
                    $appointment = $medical_request->appointments()
                        ->orderBy('next_attention_date', 'DESC')
                        ->first();
                }
                //dd($appointment);
                return response()->json([
                    'medical_request' => $medical_request,
                    'appointment' => $appointment,
                    'employee' => $employee
                ]);
            }else{
                return response()->json([
                    'error' => 'No existe un empleado registrado con ese ci',
                ]);
            }
        }else{
            return response()->json([
                'error' => 'No ingresó ningún ci',
            ]);
        }

    }

    public function print($code)
    {
        $medical_request = MedicalRequest::where('code', $code)
            ->first();

        if ($medical_request != null)
        {
            $pdf = PDF::loadView('medical_request.pdfs.submit', [
                'medical_request' => $medical_request
            ]);

            return $pdf->download($medical_request->code . '.pdf');

            // return view('medical_request.pdfs.submit')
            //     ->with('medical_request', $medical_request);
        }
    }

    public function view_call_center(){
        $medical_users = User::where('role_id', 3)
                    ->orderBy('name')
                    ->get();

        $health_insurances = HealthInsuranceRRHH::where('estado', 'Activo')
            ->where('id', '!=', 1)
            ->orderBy('id')
            ->get();

        $risk_types = RiskType::get();

        $treatment_stages = TreatmentStage::get()
            ->pluck('name','id');
        //Datos para los kits
        $kit_types = Kit::orderBy('id', 'asc')->get()->pluck('name','id');
        $parameter_age = Parameter::find(7)->value; //EDAD_INSEGURA
        $parameter_value = Parameter::find(8)->value; //VALOR_EDAD_INSEGURA
        $laboratories = Laboratory::get()
            ->pluck('name','id');
        $test_types = TestType::get()
            ->pluck('name','id');
        return view('medical_request.create_call_center')
            ->with([
                'medical_users'     => $medical_users,
                'health_insurances' => $health_insurances,
                'risk_types'        => $risk_types,
                'treatment_stages'  => $treatment_stages,
                'kit_types'  => $kit_types,
                'parameter_age'  => $parameter_age,
                'parameter_value'  => $parameter_value,
                'laboratories'  => $laboratories,
                'test_types'  => $test_types,
            ]);
    }
    public function view_edit_data_request($id){
        $medical_request = MedicalRequest::find($id);

        if($medical_request!=null){
            $health_insurances = HealthInsuranceRRHH::where('estado', 'Activo')
                ->where('id', '!=', 1)
                ->orderBy('id')
                ->get();

            $risk_types = RiskType::get();

            return view('medical_request.edit_medical_request')
                ->with([
                    'medical_request'     => $medical_request,
                    'health_insurances' => $health_insurances,
                    'risk_types'        => $risk_types,
                    
                ]);
        }else{
            /* return response()->json([
                'error' => 'No se encontró la solicitud con código: ' . $medical_request->code,
            ]); */
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            return redirect()->back();
            //return redirect()->route('usuario.index');
        }
        
    }
    public function displayImage($filename)
    {
        $path = storage_path('app/complementary_exams/' . $filename);
        if (!File::exists($path)) {
            abort(404);
        }

        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;

    }

    public function downloadFile($id){
        $exam = ComplementaryExam::find($id);
        $filename = $exam->files->first()->location;

        $path = storage_path('app/' . $filename);
        $extension = File::extension($path);
        $name = 'EC'.$id.'_'.$exam->tracing->medicalRequest->code.'_'.$exam->tracing->tracing_date;

        return response()->download($path);
    }

    public function getTracingsFromMedicalRequest($id){
        $medical_request = MedicalRequest::find($id);
        $tracings = $medical_request->tracings()->orderBy('tracing_date','desc')->get();

        if($medical_request!=null){
            return response()->json([
                'type' => 'correct',
                'view' => view('layouts.components.modals.partials.form_modal_history_tracing')
                    ->with([
                        'tracings' => $tracings,
                        'medical_request' => $medical_request
                    ])->render()
            ]);
        }else{
            return response()->json([
                'type' => 'error',
                'msj' => 'No se encontro al seguimiento indicado'
            ]);
        }
    }

    public function edit_import(){
        return view('medical_request.import');
    }

    public function update_import(Request $request){

        $import0 = new DatosBaseImport($request->file);
        $import0->onlySheets('BASE', $request->file);

        Excel::import($import0,$request->file);

        $import = new DatosImport($request->file, $import0->getSeguimientos());
        $import->onlySheets('SEGUIMIENTO', $request->file);


        Excel::import($import,$request->file);

//        dd($import->getSeguimientos());
        $import2 = new DatosTrabajadosImport($request->file, $import->getSeguimientos());
        $import2->onlySheets('FICHAS', $request->file);

        Excel::import($import2,$request->file);
        $medical_requests = MedicalRequest::where('medical_request_status_id',4)->get();
        foreach ($medical_requests as $key => $medical_request) {
            $historical_request = HistoricalRequest::where('medical_request_id',$medical_request->id)->get()->last();
            //dd($historical_request);
            $historical_request->comment = 'SOLICITUD CERRADA';
            $historical_request->medical_request_status_id = 4;
            $historical_request->update();
        }
        $complemntary_exams =ComplementaryExam::all();
        foreach ($complemntary_exams as $key => $complemntary_exam) {
            $tracing = Tracing::find($complemntary_exam->tracing_id);
            $all_tracings = Tracing::where('medical_request_id',$tracing->medical_request_id)->get()->last();
            $complemntary_exam->tracing_id=$all_tracings->id;
            $complemntary_exam->update();
        }

        flash('Se importó correctamente')
            ->success()
            ->important();

        return view('medical_request.import');
    }

    public function getExtras()
    {
        return $this->extras;
    }
}
