<?php

namespace App\Http\Controllers;

use App\DiseaseCriteria;
use App\Http\Requests\RiskTypeRequest;
use App\RiskState;
use App\RiskType;
use App\WeightingLevel;
use Illuminate\Http\Request;

class RiskTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $risk_types = RiskType::withTrashed()->get();
        return view('risk_type.index')
            ->with([
                'risk_types' => $risk_types
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        return view('risk_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(RiskTypeRequest $request)
    {
        try{

            $risk_type = new RiskType($request->all());

            $risk_type->save();

            if(!$request->ajax()){
                flash('Se registró el tipo de riesgo: '.$risk_type->name)
                    ->success()
                    ->important();
            }
        }catch(\Exception $ex){

            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        if($request->ajax()){
            return response()->json([
                'id' => $risk_type->id,
                'risk_type' => $risk_type,
            ]);
        }else{
            return redirect()
                ->route('tipos_riesgo.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show($id)
    {
        $risk_type = RiskType::withTrashed()->find($id);

        if($risk_type != null){
            return view('risk_type.show')
                ->with([
                    'risk_type' => $risk_type,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('tipos_riesgo.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $risk_type = RiskType::withTrashed()->find($id);

        if($risk_type != null){
            return view('risk_type.edit')
                ->with([
                    'risk_type' => $risk_type,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('tipos_riesgo.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RiskTypeRequest $request, $id)
    {
        try{
            $risk_type = RiskType::withTrashed()->find($id);

            if($risk_type != null){
                $risk_type->fill($request->all());

                $risk_type->update();

                flash('Se modificó el tipo de riesgo: '.$risk_type->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        return redirect()
            ->route('tipos_riesgo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $risk_type = RiskType::withTrashed()->find($id);
            if($risk_type != null){
                if($risk_type->deleted_at == null){
                    $risk_type->delete();
                }else{
                    $risk_type->restore();
                }

                flash('Se modificó el tipo de riesgo: '.$risk_type->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }
        return redirect()->route('tipos_riesgo.index');
    }

    public function getDisaeseCriteriaFromRiskType($id){
        try{
            $disaeses = DiseaseCriteria::where('risk_type_id','=',$id)
                ->orderBy('weighing','desc')
                ->get();
            $weighting_levels = WeightingLevel::get();

            return response()->json([
                'type' => 'correct',
                'view' => view('disease_criteria.partials.check_boxes')
                    ->with([
                        'disaeses' => $disaeses,
                        'weighting_levels' => $weighting_levels
                    ])
                    ->render()
            ]);
        }catch (\Exception $e){
            return response()->json([
                'type' => 'error',
                'msj' => 'Ocurrio un error al obtener los criterios de ponderación'
            ]);
        }
    }

    public function getRiskStatesFromRiskType($id){
        try{
            $risk_states = RiskState::where('risk_type_id','=',$id)
                ->get();

            return response()->json([
                'type' => 'correct',
                'risk_states' => $risk_states
            ]);
        }catch (\Exception $e){
            return response()->json([
                'type' => 'error',
                'msj' => 'Ocurrio un error al obtener los criterios de ponderación'
            ]);
        }
    }
}
