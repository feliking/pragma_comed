<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\AppointmentType;
use App\GeneralClass\WhatsApp;
use App\Http\Requests\AppointmentValidation;
use App\MedicalRequest;
use App\User;
use Illuminate\Http\Request;

// Jobs
use App\Jobs\AppointmentJob;

use DB;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function store(AppointmentValidation $request)
    {
        DB::beginTransaction();
        try {

            $medical_request = MedicalRequest::find($request->medical_request_id);

            $appointment = new Appointment;

            $appointment->next_attention_date = $request->next_attention_date.' '.$request->next_attention_time;
            $appointment->comment = $request->comment;
            $appointment->appointment_type_id = $request->appointment_type_id;
            $appointment->medical_request_id = $request->medical_request_id;
            $appointment->user_id = $medical_request->user_id;

            $whatsapp_msj = "";
            switch ((int)$request->appointment_type_id){
                case 1: //PRESENCIAL
                    $appointment->variable_text = null;
                    $whatsapp_msj = '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                        'Se registró una cita para atención médica de manera *PRESENCIAL* en fecha *'. $appointment->next_attention_date->format('d/m/Y') . '* y hora *' . $appointment->next_attention_date->format('H:i') . '* \n';
                    break;
                case 2: //ZOOM
                    $appointment->variable_text = $request->variable_zoom;
                    $whatsapp_msj = '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                        'Se registró una cita para atención médica via *ZOOM* en fecha *'. $appointment->next_attention_date->format('d/m/Y') . '* y hora *' . $appointment->next_attention_date->format('H:i') . '* \n' .
                        'El link de acceso es el siguiente: ' . $request->variable_zoom . ' \n';
                    break;
                case 3: //WHATSAPP
                    $appointment->variable_text = $request->variable_whatsapp;
                    $whatsapp_msj = '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                        'Se registró una cita para atención médica via *WHATSAPP* en fecha *'. $appointment->next_attention_date->format('d/m/Y') . '* y hora *' . $appointment->next_attention_date->format('H:i') . '* \n' .
                        'Se comunicarán a partir del siguiente número: '.$request->variable_whatsapp . ' \n';
                    break;
            }
            $whatsapp_msj .= '*Nota* \n' . $appointment->comment;

            $appointment->save();

            $filas[$medical_request->id] = view('medical_request_nurse.partials.tr_content')
                ->with([
                    'medical_request' => $medical_request,
                    'asignar'         => true
                ])->render();

            DB::commit();

            // Envío de Correo electrónico
            if ($medical_request->email != null && $medical_request->email != '')
            {
                AppointmentJob::dispatch($medical_request, $appointment);
            }

            if($medical_request->whatsapp_flag){
                $whats = new WhatsApp();
                $whats->wassengerSend($medical_request->phone_number, $whatsapp_msj);
            }

            return response()->json([
                'type'      => 'correct',
                'msj'       => 'Se registro la cita via '.$appointment->appointment_type->name. ' en fecha '.$appointment->next_attention_date,
                'filas'     => $filas,
            ]);

        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj'  => 'No se pudo registrar la cita'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function update(AppointmentValidation $request, $id)
    {
        DB::beginTransaction();
        try {

            $medical_request = MedicalRequest::find($request->medical_request_id);
            $appointment = Appointment::find($id);

            if($appointment!=null){
                $appointment->next_attention_date = $request->next_attention_date.' '.$request->next_attention_time;
                $appointment->comment = $request->comment;
                $appointment->appointment_type_id = $request->appointment_type_id;
                $appointment->medical_request_id = $request->medical_request_id;
                $appointment->user_id = $medical_request->user_id;

                $whatsapp_msj = "";
                switch ((int)$request->appointment_type_id){
                    case 1: //PRESENCIAL
                        $appointment->variable_text = null;
                        $whatsapp_msj = '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                        'Se actualizó su cita para atención médica de manera *PRESENCIAL* en fecha *'. $appointment->next_attention_date->format('d/m/Y') . '* y hora *' . $appointment->next_attention_date->format('H:i') . '* \n';
                        break;
                    case 2: //ZOOM
                        $appointment->variable_text = $request->variable_zoom;
                        $whatsapp_msj = '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                        'Se actualizó su cita para atención médica via *ZOOM* en fecha *'. $appointment->next_attention_date->format('d/m/Y') . '* y hora *' . $appointment->next_attention_date->format('H:i') . '* \n' .
                        'El link de acceso es el siguiente: ' . $request->variable_zoom . ' \n';
                        break;
                    case 3: //WHATSAPP
                        $appointment->variable_text = $request->variable_whatsapp;
                        $whatsapp_msj = '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                        'Se actualizó su cita para atención médica via *WHATSAPP* en fecha *'. $appointment->next_attention_date->format('d/m/Y') . '* y hora *' . $appointment->next_attention_date->format('H:i') . '* \n' .
                        'Se comunicarán a partir del siguiente número: '.$request->variable_whatsapp . ' \n';
                        break;
                }
                $whatsapp_msj .= '*Nota* \n' . $appointment->comment;

                $appointment->update();

                $filas[$medical_request->id] = view('medical_request_nurse.partials.tr_content')
                    ->with([
                        'medical_request' => $medical_request,
                        'asignar'         => true,
                    ])->render();

                DB::commit();
                // Envío de Correo electrónico
                if ($medical_request->email != null && $medical_request->email != '')
                {
                    AppointmentJob::dispatch($medical_request, $appointment);
                }

                if($medical_request->whatsapp_flag){
                    $whats = new WhatsApp();
                    $whats->wassengerSend($medical_request->phone_number, $whatsapp_msj);
                }

                return response()->json([
                    'type'      => 'correct',
                    'msj'       => 'Se actualizó la cita, esta será via '.$appointment->appointment_type->name. ' en fecha '.$appointment->next_attention_date,
                    'filas'     => $filas,
                ]);
            }else{
                DB::rollBack();
                return response()->json([
                    'type'      => 'error',
                    'msj'       => 'No se encontro la cita indicada',
                ]);
            }

        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj'  => 'No se pudo registrar la cita'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getFormAppointment(Request $request){
        $medical_request = MedicalRequest::find($request->medical_request_id);
        $appointment_types = AppointmentType::get();
        $form = view('layouts.components.modals.partials.form_create')
            ->with([
                'appointment_types' => $appointment_types,
                'medical_request'    => $medical_request
            ])
            ->render();

        return response()->json([
            'form'           => $form,
        ]);
    }

    function getEditFormAppointment(Request $request, $id){
        $appointment = Appointment::find($id);
        $medical_request = MedicalRequest::find($request->medical_request_id);
        if($appointment!=null){

            $appointment_types = AppointmentType::get();
            $form = view('layouts.components.modals.partials.form_create')
                ->with([
                    'appointment_types' => $appointment_types,
                    'appointment'       => $appointment,
                    'medical_request'   => $medical_request
                ])
                ->render();

            return response()->json([
                'form' => $form,
            ]);
        }else{
            return response()->json([
                'typer' => 'error',
                'content' => 'No se pudo cargar el formulario'
            ]);
        }
    }

    function getAppointmentsFromUserDate(Request $request){
        try{
            $appointments = Appointment::where(DB::raw('date(next_attention_date)'),'=',$request->next_attention_date)
                ->where('user_id','=',$request->user_id)
                ->orderBy('next_attention_date', 'asc')
                ->get();
            $array_time = [];
            foreach ($appointments as $appointment) {
                array_push($array_time, $appointment->next_attention_date->format('H:i'));
            }

            return response()->json([
                'type' => 'correct',
                'view' => view('doctor_agenda.table')
                    ->with([
                        'appointments'   => $appointments,
                        'attention_date' => $request->next_attention_date,
                        'user'           => User::find($request->user_id)
                    ])->render(),
                'array_time' => $array_time
            ]);
        } catch (\Throwable $e) {
            return response()->json([
                'type' => 'error',
                'msj'  => 'No se pudo realizar la busqueda indicada'
            ]);
        }

    }
}
