<?php

namespace App\Http\Controllers;

use App\PatientStatus;
use Illuminate\Http\Request;

class PatientStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PatientStatus  $patientStatus
     * @return \Illuminate\Http\Response
     */
    public function show(PatientStatus $patientStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PatientStatus  $patientStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(PatientStatus $patientStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PatientStatus  $patientStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PatientStatus $patientStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PatientStatus  $patientStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(PatientStatus $patientStatus)
    {
        //
    }
}
