<?php

namespace App\Http\Controllers;

use App\AppointmentType;
use App\EmployeeRRHH;
use App\Parameter;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AsigAtentionRequest;
// modelos
use App\MedicalRequest;
use App\File;
use App\ComplementaryExam;
use App\MedicalRequestStatus;
use App\Attention;
use App\HistoricalRequest;
use App\Appointment;
use App\Antecendent;
use App\AntecendentType;
use App\PatientStatus;
use App\MedicalUser;
use App\Laboratory;
use App\TestType;
use App\EnterpriseSOL;
use App\RiskState;
use App\RiskType;
use App\TreatmentStage;
use App\Tracing;
use App\DiseaseCriteria;
use App\WeightingLevel;
use App\HistoricalWeightingLevel;
use App\Kit;
use App\AttentionVitalSign;
use App\GeneralClass\CustomProcess;
use PDF;

use Illuminate\Database\Eloquent\Builder;

// Whatsapp
use App\GeneralClass\WhatsApp;

// Jobs
use App\Jobs\MedicalConsultatioJob;
use App\ProjectEnterprise;
use App\ProjectSOL;
use Illuminate\Support\Arr;

use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use const http\Client\Curl\Versions\IDN;

class MedicalConsultationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        //dd($request->all());
        $medical_requests = MedicalRequest::select('*', DB::raw('obtine_fecha_proxima_consulta(id) as fecha_proxima_consulta'))
            ->where('user_id', Auth::id())
            ->whereIn('medical_request_status_id', [2, 15])
            ->orderByRaw('fecha_proxima_consulta is null desc')
            ->orderBy('fecha_proxima_consulta', 'asc')
            ->get();

        $medical_request_status = MedicalRequestStatus::where('system_flag', false)
            ->get();
        $enterprises = EnterpriseSOL::get();

        $enterprises_search = EnterpriseSOL::pluck('nombre', 'id')
            ->toArray();
        $enterprises_search = Arr::prepend($enterprises_search, 'TODOS', 0);

        $projects_search = array();
        $projects_search = Arr::prepend($projects_search, 'TODOS', 0);
        $medical_request_statuses = MedicalRequestStatus::whereIn('id', [2, 15])
                ->pluck('name', 'id')
                ->toArray();
        $medical_request_statuses = Arr::prepend($medical_request_statuses, 'TODOS', 0);
        //dd(isset($request->medical_request_id_search),$request->all(), $medical_requests);
        if(isset($request->_) and count($request->all()) == 1){
            $medical_requests = MedicalRequest::select('*', DB::raw('obtine_fecha_proxima_consulta(id) as fecha_proxima_consulta'))
                ->where('user_id', Auth::id())
                ->whereIn('medical_request_status_id', [2, 15])
                ->orderByRaw('fecha_proxima_consulta is null desc')
                ->orderBy('fecha_proxima_consulta', 'asc')
                ->get();
        }else if(count($request->all()) > 1){
            if(isset($request->medical_request_id_search)){
                if($request->medical_request_id_search != '0'){
                    //$medical_requests = MedicalRequest::where('medical_request_status_id', $request->medical_request_id_search);
                    $medical_requests = MedicalRequest::select('*', DB::raw('obtine_fecha_proxima_consulta(id) as fecha_proxima_consulta'))
                    ->where('user_id', Auth::id())
                    ->where('medical_request_status_id', $request->medical_request_id_search)
                    ->orderByRaw('fecha_proxima_consulta is null desc')
                    ->orderBy('fecha_proxima_consulta', 'asc');
                }else{
                    $medical_requests = MedicalRequest::select('*', DB::raw('obtine_fecha_proxima_consulta(id) as fecha_proxima_consulta'))
                    ->where('user_id', Auth::id())
                    ->whereIn('medical_request_status_id', [2, 15])
                    ->orderByRaw('fecha_proxima_consulta is null desc')
                    ->orderBy('fecha_proxima_consulta', 'asc')
                    /* ->get() */;
                }
            }else{
                $medical_requests = MedicalRequest::select('*', DB::raw('obtine_fecha_proxima_consulta(id) as fecha_proxima_consulta'))
                ->where('user_id', Auth::id())
                ->whereIn('medical_request_status_id', [2, 15])
                ->orderByRaw('fecha_proxima_consulta is null desc')
                ->orderBy('fecha_proxima_consulta', 'asc')
                /* ->get() */;
            }
            //dd($medical_requests->get());
            if(isset($request->enterprise_id_search)){
                if($request->enterprise_id_search != '0'){
                    $medical_requests = $medical_requests->where('employee_id','<>',null)
                        ->whereHas('employee', function (Builder $query) use ($request){
                            $query->where('sol_empresa_id','=', $request->enterprise_id_search);
                        });

                    $projects_ids = ProjectEnterprise::where('empresa_id','=',$request->enterprise_id_search)
                        ->pluck('proyecto_id')
                        ->toArray();

                    $projects_search = ProjectSOL::whereIn('id',$projects_ids)
                        ->pluck('nombre','id')
                        ->toArray();
                    $projects_search = Arr::prepend($projects_search, 'TODOS', 0);
                }
            }
            //dd($medical_requests->get());
            if(isset($request->project_id_search)){
                if($request->project_id_search != '0'){
                    $medical_requests = $medical_requests->where('employee_id','<>',null)
                        ->whereHas('employee', function (Builder $query) use ($request){
                            $query->where('proyecto_id','=', $request->project_id_search);
                        });
                }
            }
            $medical_request_statuses = MedicalRequestStatus::whereIn('id', [2, 15])
                        ->pluck('name', 'id')
                        ->toArray();
            $medical_request_statuses = Arr::prepend($medical_request_statuses, 'TODOS', 0);
            $medical_requests = $medical_requests->get();
            if($request->ajax()){
                $table = view('medical_consultations.partials.table')
                    ->with([
                        'medical_requests'         => $medical_requests,
                    ])
                    ->render();
                $buscador = view('layouts.components.cards.search_my_consultation')
                    ->with([
                        'medical_request_statuses'  => $medical_request_statuses,
                        'enterprises_search'        => $enterprises_search,
                        'projects_search'           => $projects_search
                    ])
                    ->render();
                return response()->json([
                        'table' => $table,
                        'buscador' => $buscador,
                    ]
                );
            }
        }
        if($request->ajax()){
            $table = view('medical_consultations.partials.table')
                    ->with([
                        'medical_requests'         => $medical_requests,
                    ])
                    ->render();
            $buscador = view('layouts.components.cards.search_my_consultation')
                    ->with([
                        'medical_request_statuses'  => $medical_request_statuses,
                        'enterprises_search'        => $enterprises_search,
                        'projects_search'           => $projects_search
                    ])
                    ->render();
            return response()->json([
                    'table' => $table,
                    'buscador' => $buscador,
                ]
            );

        }else{
            return view('medical_consultations.index')
                ->with([
                    'medical_requests'       => $medical_requests,
                    'medical_request_status' => $medical_request_status,
                    'fecha_actual'           => Carbon::now(),
                    'medical_request_statuses'       => $medical_request_statuses,
                    'enterprises_search'       => $enterprises_search,
                    'projects_search'       => $projects_search,

                ]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AsigAtentionRequest $request)
    {
        DB::beginTransaction();
        try {
            $medical_request = MedicalRequest::find($request->medical_request_id);
            $historical_request = new HistoricalRequest;
            // $historical_request->comment = 'ASIGNACIÓN DE SOLICITUD';
            $historical_request->user_id = Auth::id();
            $historical_request->medical_request_id = $medical_request->id;
            $historical_request->medical_request_status_id = $request->medical_status_id;
            $historical_request->save();

            $attention = new Attention;
            $attention->attention_date = $request->fecha_consulta;
            $attention->next_attention_date = $request->fecha_proxima_consulta;
            $attention->fc = $request->fc;
            $attention->fr = $request->fr;
            $attention->fr = $request->diagnostico;
            $attention->t = $request->t;
            $attention->pa = $request->pa;
            $attention->sao2 = $request->sao2;
            $attention->other = $request->other;
            $attention->current_history = $request->current_history;
            $attention->physical_exam = $request->physical_exam;
            $attention->observacion = $request->observacion;
            $attention->diagnosis = $request->diagnostico;
            $attention->treatment = $request->tratamiento;
            $attention->historical_request_id = $historical_request->id;
            $attention->save();

            $medical_request->medical_request_status_id = $request->medical_status_id;
            $medical_request->update();

            $filas[$medical_request->id] = view('medical_consultations.partials.tr_content')
                ->with([
                    'medical_request' => $medical_request
                ])->render();

            DB::commit();

            // Envío de Correo electrónico
            if ($medical_request->email != null && $medical_request->email != '')
            {
                MedicalConsultatioJob::dispatch($medical_request, $attention);
            }

            if($medical_request->whatsapp_flag){
                $whats = new WhatsApp();
                $whats->wassengerSend($medical_request->phone_number, '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                'Se registró una atención médica en fecha *'. $attention->attention_date->format('d/m/Y') . '* y hora *' . $attention->attention_date->format('H:i') . '* \n' .
                '*Tratamiento:* ' . $attention->treatment . ' \n' .
                '*Observación:* ' . $attention->observation);
            }

            return response()->json([
                'type'   => 'correct',
                'msj'    => 'Se registraron la atencion ',
                'filas'  => $filas,
                'status' => $historical_request->medical_request_status_id,
            ]);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj'  => 'No se pudo registrar el tratamiento'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        try {
            $medical_request = MedicalRequest::find($id);
            $historial_requests = HistoricalRequest::where('medical_request_id', $medical_request->id)
                // ->whereNotIn('medical_request_status_id',[1,2,3,5,6])
                ->orderBy('id', 'desc')
                ->get();
            $medical_request_status = MedicalRequestStatus::where('system_flag', false)
                    ->orderBy('id', 'desc')
                    ->get();

            $attention_vital_sign = AttentionVitalSign::where('medical_request_id', $id)
                ->orderBy('id','desc')
                ->first();

            $antescedent_type = AntecendentType::get();

            $appointment_types = AppointmentType::get();
            $laboratories = Laboratory::get()
                ->pluck('name','id');
            $test_types = TestType::get()
                ->pluck('name','id');
            $enterprises = EnterpriseSOL::get()
                ->pluck('nombre','id');
            $risk_types = RiskType::get()
                ->pluck('name','id');
            $treatment_stages = TreatmentStage::get()
                ->pluck('name','id');
            //Datos para los kits
            $kit_types = Kit::orderBy('id', 'asc')->get()->pluck('name','id');

            // tracing ultimo
            $tracing_last_data = Tracing::where('medical_request_id', $id)
                ->orderBy('id', 'desc')
                ->first();

            $custom_process = new CustomProcess;
            $mi_mje = $custom_process->getMessageTreatmentStages($id);

            if($medical_request->consulta_proxima->format('d/m/Y') == Carbon::now()->format('d/m/Y')){
                $diferencia_fecha = false;
            }else{
                $diferencia_fecha = true;
            }

            $parameter_age = Parameter::find(7)->value; //EDAD_INSEGURA
            $parameter_value = Parameter::find(8)->value; //VALOR_EDAD_INSEGURA

            // Pre grabado
            if ($medical_request->medical_request_status_id == 15)
            {
                $historical_request = HistoricalRequest::where([
                    ['medical_request_id', '=', $medical_request->id],
                    ['comment', 'LIKE', 'PRE GRABADO']
                ])
                    ->orderBy('id', 'DESC')
                    ->first();
                $attention = Attention::where('historical_request_id', $historical_request->id)
                    ->orderBy('id', 'DESC')
                    ->first();
                $tracing = Tracing::where('historical_request_id', $historical_request->id)
                    ->orderBy('id', 'DESC')
                    ->first();
            }
            else
            {
                $historical_request = null;
                $attention = null;
                $tracing = null;
            }

            if ($medical_request->employee_id != null) {
                $antecendent =  Antecendent::where('employee_id', $medical_request->employee_id)
                    ->get();

                $medical_requests = MedicalRequest::select('*', DB::raw('obtine_fecha_proxima_consulta(id) as fecha_proxima_consulta'))
                    ->where('employee_id', $medical_request->employee_id)
                    ->orderBy('id','desc')
                    ->get();

                return view('medical_consultations.edit')
                    ->with([
                        'medical_requests'       => $medical_requests,
                        'medical_request'        => $medical_request,
                        'historial_requests'     => $historial_requests,
                        'cantidad'               => count($medical_requests),
                        'medical_request_status' => $medical_request_status,
                        'fecha_actual'           => Carbon::now(),
                        'historial_total'        => true,
                        'antecendents'           => $antecendent,
                        'antescedent_types'      => $antescedent_type,
                        'appointment_types'      => $appointment_types,
                        'laboratories'           => $laboratories,
                        'test_types'             => $test_types,
                        'enterprises'            => $enterprises,
                        'risk_types'             => $risk_types,
                        'treatment_stages'       => $treatment_stages,
                        //kits
                        'kit_types'              => $kit_types,
                        // mje de tratamiento
                        'mi_mje'                 => $mi_mje,
                        'diferencia_fecha'       => $diferencia_fecha,
                        'attention_vital_sign'   => $attention_vital_sign,
                        //PARAMETROS
                        'parameter_age'          => $parameter_age,
                        'parameter_value'        => $parameter_value,
                        // PRE GRABADO
                        'historical_request'     => $historical_request,
                        'attention'              => $attention,
                        'tracing'                => $tracing,
                        // ultimo registro
                        'tracing_last_data'      => $tracing_last_data,
                        'tracing_last'           => true
                    ]);
            }else{
                $antecendent =  Antecendent::where('medical_request_id', $medical_request->id)
                    ->get();
                    // dd($antecendent);
                $fecha_proxima_atencion = Appointment::where('medical_request_id', $id)
                    ->orderBy('id','desc')
                    ->first();

                return view('medical_consultations.edit')
                    ->with([
                        'medical_request'        => $medical_request,
                        'historial_requests'     => $historial_requests,
                        'cantidad'               => count($historial_requests),
                        'medical_request_status' => $medical_request_status,
                        'fecha_actual'           => Carbon::now(),
                        'historial_total'        => false,
                        'fecha_proxima_atencion' => $fecha_proxima_atencion,
                        'antecendents'           => $antecendent,
                        'antescedent_types'      => $antescedent_type,
                        'appointment_types'      => $appointment_types,
                        'laboratories'           => $laboratories,
                        'test_types'             => $test_types,
                        'enterprises'            => $enterprises,
                        'risk_types'             => $risk_types,
                        'treatment_stages'       => $treatment_stages,
                        //kits
                        'kit_types'              => $kit_types,
                        // mje de tratamiento
                        'attention_vital_sign'   => $attention_vital_sign,
                        'mi_mje'                 => $mi_mje,
                        'diferencia_fecha'       => $diferencia_fecha,
                        //PARAMETROS
                        'parameter_age'          => $parameter_age,
                        'parameter_value'        => $parameter_value,
                        // PRE GRABADO
                        'historical_request'     => $historical_request,
                        'attention'              => $attention,
                        'tracing'                => $tracing,
                        // ultimo registro
                        'tracing_last_data'      => $tracing_last_data,
                        'tracing_last'           => true
                    ]);
            }

        } catch (\Exception $ex) {
            flash('No se pudo mostrar la atención')
                    ->error()
                    ->important();

            return redirect()
                    ->route('consultas_medicas.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AsigAtentionRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            // Solicitud de Atención
            $medical_request = MedicalRequest::find($id);

            // Historial
            if ($medical_request->medical_request_status_id != 15) // No se realizó el pre grabado
            {
                $historical_request = new HistoricalRequest();
                $historical_request->user_id = Auth::id();
                $historical_request->medical_request_id = $medical_request->id;

                if ($request->presave == "1") // pre grabar
                {
                    // $historical_request->medical_request_status_id = $request->medical_status_id;
                    $historical_request->medical_request_status_id = 15;
                    $historical_request->comment = 'PRE GRABADO';
                }
                else // no se debe pre grabar
                {
                    $historical_request->medical_request_status_id = $request->medical_status_id;
                    $historical_request->comment = 'ATENCIÓN Y SEGUIMIENTO REALIZADO';


                }

                $historical_request->save();
            }
            else // se ha realizado un pre grabado
            {
                if ($request->presave == "0") // no se debe pre grabar
                {
                    $historical_request = new HistoricalRequest();
                    $historical_request->user_id = Auth::id();
                    $historical_request->medical_request_id = $medical_request->id;
                    $historical_request->medical_request_status_id = $request->medical_status_id;
                    $historical_request->comment = 'ATENCIÓN Y SEGUIMIENTO REALIZADO';
                    $historical_request->save();
                }
                else
                {
                    $historical_request = HistoricalRequest::where('medical_request_id', $medical_request->id)
                        ->orderBy('id', 'DESC')
                        ->first();
                }
            }

            // Registro de Atención
            if ($medical_request->medical_request_status_id != 15) // No se realizó el pre grabado
            {
                $attention = new Attention();
                $attention->attention_date = $request->fecha_consulta;
            }
            else
            {
                $histo_request = HistoricalRequest::where([
                    ['medical_request_id', '=', $medical_request->id],
                    ['comment', 'LIKE', 'PRE GRABADO']
                ])
                    ->orderBy('id', 'DESC')
                    ->first();

                $attention = Attention::where('historical_request_id', $histo_request->id)
                    ->first();
            }

            $attention->fc = $request->fc;
            $attention->fr = $request->fr;
            $attention->t = $request->t;
            $attention->pa = $request->pa;
            $attention->sao2 = $request->sao2;
            $attention->otros = $request->other;
            $attention->current_history = $request->current_history;
            $attention->physical_exam = $request->physical_exam;
            $attention->diagnosis = $request->diagnostico;
            $attention->treatment = $request->tratamiento;
            $attention->historical_request_id = $historical_request->id;

            if ($request->presave == "0"){
                // eliminacion de la tabla auxilar
                AttentionVitalSign::where('medical_request_id', $id)
                    ->delete();
            }
            else{
                $attention_vital_sign = AttentionVitalSign::where('medical_request_id', $id)
                    ->first();
                if($attention_vital_sign!=null){
                    $attention_vital_sign->fc = $request->fc;
                    $attention_vital_sign->fr = $request->fr;
                    $attention_vital_sign->t = $request->t;
                    $attention_vital_sign->pa = $request->pa;
                    $attention_vital_sign->sao2 = $request->sao2;
                    $attention_vital_sign->otros = $request->other;
                    $attention_vital_sign->update();
                }else{
                    $attention_vital_sign = new AttentionVitalSign;
                    $attention_vital_sign->attention_date = Carbon::now();

                    $attention_vital_sign->fc = $request->fc;
                    $attention_vital_sign->fr = $request->fr;
                    $attention_vital_sign->t = $request->t;
                    $attention_vital_sign->pa = $request->pa;
                    $attention_vital_sign->sao2 = $request->sao2;
                    $attention_vital_sign->otros = $request->other;

                    $attention_vital_sign->medical_request_id = $medical_request->id;

                    $attention_vital_sign->save();
                }
            }

            if ($medical_request->medical_request_status_id != 15) // No se realizó el pre grabado
            {
                $attention->save();
            }
            else
            {
                $attention->update();
            }

            // Seguimiento
            $project = null;
            if($medical_request->employee_id != null)
            {
                $employee = EmployeeRRHH::find($medical_request->employee_id);
                // $project = $employee->sol_proyecto_id;
                $project = $employee->proyecto_id;
            }

            $request->merge([
                'tracing_date'          => Carbon::now(),
                'medical_request_id'    => $id,
                'user_id'               => Auth::id(),
                'historical_request_id' => $historical_request->id,
                'project_id'            => $project
            ]);

            if ($medical_request->medical_request_status_id != 15) // No se realizó el pre grabado
            {
                $tracing = new Tracing($request->all());
                $tracing->comment = $request->comment_tracing;
                $tracing->risk_type_id = $request->risk_type_id_hidden;
                $tracing->risk_state_id = $request->risk_state_id_hidden;
                $tracing->treatment_stage_id = $request->treatment_stage_id_hidden;
                $tracing->save();
            }
            else
            {
                $histo_request = HistoricalRequest::where([
                    ['medical_request_id', '=', $medical_request->id],
                    ['comment', 'LIKE', 'PRE GRABADO']
                ])
                    ->orderBy('id', 'DESC')
                    ->first();

                $tracing = Tracing::where([
                    ['medical_request_id', '=', $medical_request->id],
                    ['historical_request_id', '=', $histo_request->id]
                ])
                    ->first();
                $tracing->fill($request->all());
                $tracing->comment = $request->comment_tracing;
                $tracing->risk_type_id = $request->risk_type_id_hidden;
                $tracing->risk_state_id = $request->risk_state_id_hidden;
                $tracing->treatment_stage_id = $request->treatment_stage_id_hidden;
                $tracing->update();
            }

            //************************************INICIO:CALCULO DE LA PONDERACION
            $sum = 0;

            //INICIO:PREGUNTA LA EDAD
            $parameter_age = Parameter::find(7)->value; //EDAD_INSEGURA
            $edad = 0;
            if($medical_request->employee!=null){
                $edad = $medical_request->employee->edad;
            }else{
                $edad = $medical_request->edad;
            }

            if($edad >= (int) $parameter_age){
                $sum++;
            }
            //FIN:PREGUNTA LA EDAD

            //INICIO:PREGUNTA ESTADO DE RIESGO
            $risk_state = RiskState::find($request->risk_state_id_hidden);
            if($risk_state != null && $risk_state->weighting > 0){
                $sum = $sum + $risk_state->weighting;
            }
            //FIN:PREGUNTA ESTADO DE RIESGO

            // Verificamos si esten datos anteriores
            if (count($tracing->disease_criterias) > 0)
            {
                $tracing->disease_criterias()->detach();
            }

            //INICIO:PREGUNTA CAMPOS MARCADOS
            if($request->disease_criteria!=null){
                foreach ($request->disease_criteria as $disease_criteria){
                    $tracing->disease_criterias()->attach($disease_criteria);
                    $disease = DiseaseCriteria::find($disease_criteria);
                    $sum += $disease->weighing;
                }
            }
            //FIN:PREGUNTA CAMPOS MARCADOS

            //GUARDANDO LA ADVERTENCIA Y EL RANGO
            $weighting_level = WeightingLevel::where('minimum','<=',$sum)
                ->where('maximum','>=',$sum)
                ->where('last_level_flag','=',false)
                ->first();

            if ($tracing->historical_weighting_level == null)
            {
                $historical_weigthing_level = new HistoricalWeightingLevel();
            }
            else
            {
                $historical_weigthing_level = $tracing->historical_weighting_level;
            }

            $historical_weigthing_level->calc_evaluation = $sum;

            if($weighting_level==null){
                //VERIFICA SI PERTENCE AL MAXIMO
                $weighting_level_max = WeightingLevel::where('minimum','<=',$sum)
                    ->where('last_level_flag','=',true)
                    ->first();

                if($weighting_level_max!=null){
                    $historical_weigthing_level->name = $weighting_level_max->name;
                    $historical_weigthing_level->description = $weighting_level_max->description;
                    $historical_weigthing_level->color = $weighting_level_max->color;
                    $historical_weigthing_level->minimum = $weighting_level_max->minimum;
                    $historical_weigthing_level->maximum = $weighting_level_max->maximum;
                    $historical_weigthing_level->evaluation = $weighting_level_max->evaluation;

                    $historical_weigthing_level->weighting_level_id = $weighting_level_max->id;
                    $historical_weigthing_level->tracing_id = $tracing->id;
                }
            }else{
                $historical_weigthing_level->name = $weighting_level->name;
                $historical_weigthing_level->description = $weighting_level->description;
                $historical_weigthing_level->color = $weighting_level->color;
                $historical_weigthing_level->minimum = $weighting_level->minimum;
                $historical_weigthing_level->maximum = $weighting_level->maximum;
                $historical_weigthing_level->evaluation = $weighting_level->evaluation;

                $historical_weigthing_level->weighting_level_id = $weighting_level->id;
                $historical_weigthing_level->tracing_id = $tracing->id;
            }

            if ($tracing->historical_weighting_level == null)
            {
                $historical_weigthing_level->save();
            }
            else
            {
                $historical_weigthing_level->update();
            }
            //************************************FIN:CALCULO DE LA PONDERACION

            //GUARDANDO EXAMENES COMPLEMENTARIOS
            $files = $request->file('files');

            if($request->laboratories!=null){
                for ($i=0 ; $i<count($request->laboratories) ;$i++){
                    $complementary_exam = new ComplementaryExam;
                    $complementary_exam->tracing_id = $tracing->id;

                    $complementary_exam->laboratory_id = $request->laboratories[$i];
                    $complementary_exam->test_type_id = $request->test_types[$i];
                    $complementary_exam->cost = $request->costs[$i];
                    $complementary_exam->bill_number = $request->bill_numbers[$i];
                    $complementary_exam->enterprise_flag = ($request->enterprise_pay_flags[$i]=="true")?true:false;
                    $complementary_exam->result_flag = ($request->result_flags[$i]=="true")?true:false;
                    $complementary_exam->save();

                    if ($files != null){
                        if(array_key_exists($i,$files)){
                            $path = $files[$i]->store('complementary_exams');

                            $complementary_exam->files()->create([
                                'name' => $files[$i]->getClientOriginalName(),
                                'file_type_id' => 1,
                                'location' => $path,
                                'mime' => $files[$i]->getClientMimeType()
                            ]);

                        }
                    }

                }
            }

            // Actualización de estado de la Solicitud
            if ($request->presave == "1")
            {
                $medical_request->medical_request_status_id = 15;
            }
            else
            {
                $medical_request->medical_request_status_id = 12;
                $medical_request->user_id = null;
            }

            $medical_request->update();


            if(!$request->ajax()){
                flash('Se registró el seguimiento correctamente')
                    ->success()
                    ->important();
            }

            DB::commit();

            if ($request->presave == "0")
            {
                // Envío de Correo electrónico
                if ($medical_request->email != null && $medical_request->email != '')
                {
                    $doctor = Auth::user();
                    MedicalConsultatioJob::dispatch($medical_request, $attention, $doctor);
                }

                if($medical_request->whatsapp_flag){
                    $whats = new WhatsApp();
                    $whats->wassengerSend($medical_request->phone_number, '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                    'Se registró una atención médica en fecha *'. $attention->attention_date->format('d/m/Y') . '* y hora *' . $attention->attention_date->format('H:i') . '* \n' .
                    '*Tratamiento:* ' . $attention->treatment . ' \n' .
                    '*Observación:* ' . $attention->observation);
                }
            }

            if(!$request->ajax()){
                flash('Se Registro la atención de la solicitud: '.$medical_request->code)
                        ->success()
                        ->important();
                return redirect()
                    ->route('consultas_medicas.index');
            }else{
                return response()->json([
                    'type'      => 'correct',
                    'msj'       => 'Se registraron la atencion ',
                    'route_pdf' => route('consultas_medicas.imprimir',['attention_id' => $attention->id]),
                    'route'     => route('consultas_medicas.index'),
                ]);
            }
        } catch (\Throwable $e) {
            DB::rollBack();
            flash('No se pudo registrar la atencion')
                ->error()
                ->important();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAttentionHistorial($medical_request_id)
    {
        $medical_request = MedicalRequest::find($medical_request_id);
        $historial_requests = HistoricalRequest::where('medical_request_id', $medical_request->id)
            // ->whereNotIn('medical_request_status_id',[1,2,3,5,6])
            ->orderBy('id', 'desc')
            ->get();
        $medical_request_status = MedicalRequestStatus::where('system_flag', false)
                ->orWhere('id', 4)
                ->orderBy('id', 'desc')
                ->get();
        // dd($medical_request->employee_id != null);
        if ($medical_request->employee_id != null) {
            $medical_requests = MedicalRequest::where('employee_id', $medical_request->employee_id)
                ->orderBy('id','desc')
                ->get();
            $html = view('medical_consultations.modals.content_modal')
                    ->with([
                        'medical_requests'       => $medical_requests,
                        'medical_request'        => $medical_request,
                        'historial_requests'     => $historial_requests,
                        'cantidad'               => count($medical_requests),
                        'medical_request_status' => $medical_request_status,
                        'fecha_actual'           => Carbon::now(),
                        'historial_total'        => true,
                    ])
                    ->render();
        }else{
            $html = view('medical_consultations.modals.content_modal')
                    ->with([
                        'medical_request'        => $medical_request,
                        'historial_requests'     => $historial_requests,
                        'cantidad'               => count($historial_requests),
                        'medical_request_status' => $medical_request_status,
                        'fecha_actual'           => Carbon::now(),
                        'historial_total'        => false,
                    ])
                    ->render();
        }
        return response()->json([
                    'content' => $html,
                ]);
    }

    public function indexMmedicalRequestClose()
    {
        $medical_requests = MedicalRequest::leftJoin('historical_requests','medical_requests.id','=','historical_requests.medical_request_id')
            ->where('historical_requests.user_id','=',Auth::id())
            ->where('medical_requests.medical_request_status_id','<>',2) //QUE NO SEA ASIGNACION MEDICA
            ->distinct()
            ->select('medical_requests.*')
            ->get();

        return view('medical_consultations.index_cerradas')
            ->with([
                'medical_requests' => $medical_requests,
                'fecha_actual'     => Carbon::now()
            ]);
    }

    public function indexMedicalRequestAgend()
    {
        $medical_user_ids = MedicalUser::select('doctor_id')
            ->where('nurse_id',Auth::id())
            ->get()
            ->toArray();
        // dd($medical_user_ids);
        $medical_requests = MedicalRequest::whereIn('user_id', $medical_user_ids)
            ->where('medical_request_status_id', '!=',4)
            ->get();

        return view('medical_consultations.index_agendar')
            ->with([
                'medical_requests' => $medical_requests,
                'fecha_actual'     => Carbon::now(),
                'asignar'          =>true
            ]);
    }

    public function printAttention(Request $request, $attention_id)
    {

        $attention = Attention::find($attention_id);
        $historical_request = HistoricalRequest::find($attention->historical_request_id);
        $medical_request = $historical_request->medical_request;
        // dd($request->all(), $attention, $attention_id);
//        dd($request->all(),$attention, $historical_request->tracing->complementaryExams);
        if ($attention != null)
        {
             $pdf = PDF::loadView('medical_consultations.pdfs.attention', [
                 'attention'       => $attention,
                 'medical_request' => $medical_request,
                 'historical_request' => $historical_request,
                  'request'   => $request
             ]);

//            $pdf = PDF::loadView('medical_consultations.pdfs.prescription', [
//                'attention'       => $attention,
//                'medical_request' => $medical_request,
//                'historical_request' => $historical_request,
//                'request'   => $request
//            ]);

//            return view('medical_consultations.pdfs.attention')
//                ->with([
//                    'attention'       => $attention,
//                    'medical_request' => $medical_request,
//                    'historical_request' => $historical_request,
//                    'request'   => $request
//                ]);

            $customPaper = array(0,0,396.85,612.283);
             return $pdf->setPaper('letter')
                 ->stream('atencion.pdf');

        }
    }

    public function printPrescription(Request $request)
    {
        $medical_request = MedicalRequest::find($request->medical);
        // dd($request->all(), $attention, $attention_id);
//        dd($request->all(),$attention, $historical_request->tracing->complementaryExams);
        if ($medical_request != null)
        {
            $pdf = PDF::loadView('medical_consultations.pdfs.prescription', [
                'request'   => $request,
                'medical_request'   => $medical_request
            ]);

//            return view('medical_consultations.pdfs.prescription')
//                ->with([
//                    'request'   => $request,
//                    'medical_request'   => $medical_request
//                ]);


            $customPaper = array(0,0,396.85,612.283);
            return $pdf->setPaper($customPaper)
                ->stream('receta.pdf');
        }
    }

    public function getModalResumen(Request $request)
    {
        // dd($request->all());
        $medical_request_status = MedicalRequestStatus::where('system_flag', false)
            ->orderBy('id', 'desc')
            ->get();
        $laboratories = Laboratory::get()
            ->pluck('name','id');
        $test_types = TestType::get()
            ->pluck('name','id');
        $aux_test_types = TestType::get();
        $enterprises = EnterpriseSOL::get()
            ->pluck('nombre','id');
        $risk_types = RiskType::get()
            ->pluck('name','id');
        $treatment_stages = TreatmentStage::get()
            ->pluck('name','id');
        $estados_riesgos = RiskState::where('risk_type_id',$request->risk_type_id)
            ->get()
            ->pluck('name','id');
        $disaeses = DiseaseCriteria::where('risk_type_id','=',$request->risk_type_id)
            ->get();
        $weighting_levels = WeightingLevel::get();

        if (isset($request->disease_criteria)) {
            $disease_points = DiseaseCriteria::whereIn('id',$request->disease_criteria)
                ->get()->sum('weighing');
        }else{
            $disease_points = 0;
        }

        $laboratorios = Laboratory::get();
        // dd($request->all());
        $resumen =  view('medical_consultations.partials.form_resumen_ajax')
            ->with([
                'datos'                  => $request,
                'medical_request_status' => $medical_request_status,
                'laboratories'           => $laboratories,
                'test_types'             => $test_types,
                'enterprises'            => $enterprises,
                'risk_types'             => $risk_types,
                'treatment_stages'       => $treatment_stages,
                'estados_riesgos'        => $estados_riesgos,
                'disaeses'               => $disaeses,
                'weighting_levels'       => $weighting_levels,
                'disease_points'         => $disease_points,
                'laboratorios'         => $laboratorios,
                'aux_test_types'         => $aux_test_types,
            ])
             ->render();
        return response()->json([
                    'resumen' => $resumen,
                ]);
    }

    public function getSignosVitales($medical_request_id)
    {
        $attention_vital_sign = AttentionVitalSign::where('medical_request_id', $medical_request_id)
            ->orderBy('id','desc')
            ->first();
        $frm_signos =  view('medical_consultations.partials.form_signos_vitales')
            ->with([
                'attention_vital_sign' => $attention_vital_sign,
            ])
             ->render();
        return response()->json([
                    'frm_signos' => $frm_signos,
                ]);
    }

}
