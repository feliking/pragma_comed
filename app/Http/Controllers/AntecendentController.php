<?php

namespace App\Http\Controllers;

use App\Antecendent;
use App\MedicalRequest;
use App\AntecendentType;
use Illuminate\Http\Request;
use App\Http\Requests\AtecedentRequest;

use DB;

class AntecendentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AtecedentRequest $request)
    {
        DB::beginTransaction();
        try {
            $medical_request = MedicalRequest::find($request->modal_medical_request_id);
            $antecedent = new Antecendent;
            $antecedent->description = $request->descripcion_antecedente;
            $antecedent->medical_request_id = $request->modal_medical_request_id;
            $antecedent->antecendent_type_id = $request->modal_antecedent_type_id;
            $antecedent->employee_id = $request->modal_employee_id;
            $antecedent->save();
            DB::commit();

            if ($medical_request->employee_id != null) {
                $antecendents =  Antecendent::where('employee_id', $request->modal_employee_id)
                    ->where('antecendent_type_id', $request->modal_antecedent_type_id)
                    ->get();
            }else{
                $antecendents =  Antecendent::where('medical_request_id', $request->modal_medical_request_id)
                    ->where('antecendent_type_id', $request->modal_antecedent_type_id)
                    ->get();
            }

            $lista = view('medical_consultations.partials.antecedent_list')
                        ->with([
                            'antecendents' => $antecendents,
                        ])->render();

            return response()->json([
                'type'             => 'correct',
                'msj'              => 'Se registraron el antecedente',
                'lista'            => $lista,
                'cantidad'         => count($antecendents),
                'tipo_antecedente' => $request->modal_antecedent_type_id,
            ]);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj'  => 'No se pudo registrar el tratamiento'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Antecendent  $antecendent
     * @return \Illuminate\Http\Response
     */
    public function show(Antecendent $antecendent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Antecendent  $antecendent
     * @return \Illuminate\Http\Response
     */
    public function edit(Antecendent $antecendent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Antecendent  $antecendent
     * @return \Illuminate\Http\Response
     */
    public function update(AtecedentRequest $request, Antecendent $antecendent)
    {
        DB::beginTransaction();
        try {
            $medical_request = MedicalRequest::find($request->modal_medical_request_id);
            $antecedent = Antecendent::find($request->modal_antecedent_id);

            if($antecedent!=null){
                $antecedent->description = $request->descripcion_antecedente;
                $antecedent->employee_id = $request->modal_employee_id;
                $antecedent->update();
                DB::commit();

                if ($medical_request->employee_id != null) {
                    $antecendents =  Antecendent::where('employee_id', $medical_request->employee_id)
                        ->where('antecendent_type_id', $antecedent->antecendent_type_id)
                        ->get();
                }else{
                    $antecendents =  Antecendent::where('medical_request_id', $medical_request->id)
                        ->where('antecendent_type_id', $antecedent->antecendent_type_id)
                        ->get();
                }

                $lista = view('medical_consultations.partials.antecedent_list')
                            ->with([
                                'antecendents' => $antecendents,
                            ])->render();


                return response()->json([
                    'type'             => 'correct',
                    'msj'              => 'Se actualizo el antecedente',
                    'lista'            => $lista,
                    'cantidad'         => count($antecendents),
                    'tipo_antecedente' => $request->modal_antecedent_type_id,
                ]);
            }else{
                DB::rollBack();
                return response()->json([
                    'type'      => 'error',
                    'msj'       => 'No se encontro la cita indicada',
                ]);
            }

        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj'  => 'No se pudo registrar la cita'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Antecendent  $antecendent
     * @return \Illuminate\Http\Response
     */
    public function destroy($antecendent)
    {
        DB::beginTransaction();
        try {
            $my_antecedent = Antecendent::find($antecendent);

            $medical_request = MedicalRequest::find($my_antecedent->medical_request_id);
            $antecedent_type = AntecendentType::find($my_antecedent->antecendent_type_id);
            if($my_antecedent!=null){
                $my_antecedent->delete();

                DB::commit();
                if ($medical_request->employee_id != null) {
                    $antecendents =  Antecendent::where('employee_id', $medical_request->employee_id)
                        ->where('antecendent_type_id', $antecedent_type->id)
                        ->get();
                }else{
                    $antecendents =  Antecendent::where('medical_request_id', $medical_request->id)
                        ->where('antecendent_type_id', $antecedent_type->id)
                        ->get();
                }
                $lista = view('medical_consultations.partials.antecedent_list')
                            ->with([
                                'antecendents' => $antecendents,
                            ])->render();

                return response()->json([
                    'type'             => 'correct',
                    'msj'              => 'Se Elimino el antecedente',
                    'lista'            => $lista,
                    'cantidad'         => count($antecendents),
                    'tipo_antecedente' => $antecedent_type->id,
                ]);
            }else{
                DB::rollBack();
                return response()->json([
                    'type'      => 'error',
                    'msj'       => 'No se pudo eliminar el antecedente',
                ]);
            }

        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj'  => 'No se pudo eliminar el antecedente'
            ]);
        }
    }
}
