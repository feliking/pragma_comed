<?php

namespace App\Http\Controllers;

use App\MedicalRequestPriority;
use Illuminate\Http\Request;
use App\Http\Requests\MedicalRequestPriorityR;

use Illuminate\Support\Facades\Auth;

class MedicalRequestPrioritiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd('khbkb ');
        $medical_request_priorities = MedicalRequestPriority::withTrashed()->get();
        //dd($medical_request_priorities);
        return view('medical_request_priority.index')
            ->with([
                'medical_request_priorities' => $medical_request_priorities
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('medical_request_priority.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MedicalRequestPriorityR $request)
    {
        try{

            $medical_request_priority = new MedicalRequestPriority($request->all());

            $medical_request_priority->save();

            if(!$request->ajax()){
                flash('Se registró la prioridad: '.$medical_request_priority->name)
                    ->success()
                    ->important();
            }
        }catch(\Exception $ex){

            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        if($request->ajax()){
            return response()->json([
                'id' => $medical_request_priority->id,
                'medical_request_priority' => $medical_request_priority,
            ]);
        }else{
            return redirect()
                ->route('prioridades.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $medical_request_priority = MedicalRequestPriority::withTrashed()->find($id);

        if($medical_request_priority != null){
            return view('medical_request_priority.show')
                ->with([
                    'medical_request_priority' => $medical_request_priority,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('prioridades.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $medical_request_priority = MedicalRequestPriority::withTrashed()->find($id);

        if($medical_request_priority != null){
            return view('medical_request_priority.edit')
                ->with([
                    'medical_request_priority' => $medical_request_priority,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('prioridades.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MedicalRequestPriorityR $request, $id)
    {
        try{
            $medical_request_priority = MedicalRequestPriority::withTrashed()->find($id);

            if($medical_request_priority != null){
                $medical_request_priority->fill($request->all());

                $medical_request_priority->update();

                flash('Se modificó la prioridad: '.$medical_request_priority->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        return redirect()
            ->route('prioridades.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $medical_request_priority = MedicalRequestPriority::withTrashed()->find($id);
            if($medical_request_priority != null){
                if($medical_request_priority->deleted_at == null){
                    $medical_request_priority->delete();
                }else{
                    $medical_request_priority->restore();
                }

                flash('Se modificó el estado de la prioridad: '.$medical_request_priority->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }
        return redirect()->route('prioridades.index');
    }
}
