<?php

namespace App\Http\Controllers;

use App\Http\Requests\TestTypeRequest;
use App\TestType;
use Illuminate\Http\Request;

use DB;

class TestTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $test_types = TestType::withTrashed()->get();
        return view('test_type.index')
            ->with([
                'test_types' => $test_types
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        return view('test_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TestTypeRequest $request)
    {
        DB::beginTransaction();
        try{

            $test_type = new TestType($request->all());

            $test_type->save();
            if(!$request->ajax()){
                flash('Se registró el tipo de prueba: '.$test_type->name)
                    ->success()
                    ->important();
            }
            DB::commit();
        }catch(\Exception $ex){
            DB::rollBack();
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        if($request->ajax()){
            return response()->json([
                'id' => $test_type->id,
                'laboratory' => $test_type,
            ]);
        }else{
            return redirect()
                ->route('tipos_prueba.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $test_type = TestType::withTrashed()->find($id);

        if($test_type != null){
            return view('test_type.show')
                ->with([
                    'test_type' => $test_type,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('tipos_prueba.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $test_type = TestType::withTrashed()->find($id);

        if($test_type != null){
            return view('test_type.edit')
                ->with([
                    'test_type' => $test_type,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('tipos_prueba.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(TestTypeRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $test_type = TestType::withTrashed()->find($id);

            if($test_type != null){
                $test_type->fill($request->all());

                $test_type->update();
                DB::commit();

                flash('Se modificó el tipo de prueba: '.$test_type->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            DB::rollBack();
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        return redirect()
            ->route('tipos_prueba.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $test_type = TestType::withTrashed()->find($id);
            if($test_type != null){
                if($test_type->deleted_at == null){
                    $test_type->delete();
                }else{
                    $test_type->restore();
                }
                DB::commit();

                flash('Se modificó el tipo de prueba: '.$test_type->name)
                    ->warning()
                    ->important();
            }else{
                DB::rollBack();
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            DB::rollBack();
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }
        return redirect()->route('tipos_prueba.index');
    }
}
