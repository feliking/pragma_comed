<?php

namespace App\Http\Controllers;

use App\Http\Requests\RiskStatusesRequest;
use App\RiskState;
use App\RiskType;
use Illuminate\Http\Request;

use DB;

class RiskStatusesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $risk_statuses = RiskState::withTrashed()->get();
        return view('risk_status.index')
            ->with([
                'risk_statuses' => $risk_statuses
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        $risk_types = RiskType::orderBy('id', 'ASC')->get();
        return view('risk_status.create')
            ->with('risk_types', $risk_types->pluck('name','id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(RiskStatusesRequest $request)
    {
        DB::beginTransaction();
        try{

            $risk_status = new RiskState($request->all());

            $risk_status->save();

            if(!$request->ajax()){
                flash('Se registró el estado de riesgo: '.$risk_status->name)
                    ->success()
                    ->important();
            }
            DB::commit();
        }catch(\Exception $ex){
            DB::rollBack();
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        if($request->ajax()){
            return response()->json([
                'id' => $risk_status->id,
                'risk_status' => $risk_status,
            ]);
        }else{
            return redirect()
                ->route('estados_riesgo.index');
        }
    }

   /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show($id)
    {
        $risk_status = RiskState::withTrashed()->find($id);

        if($risk_status != null){
            return view('risk_status.show')
                ->with([
                    'risk_status' => $risk_status,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('estados_riesgo.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $risk_status = RiskState::withTrashed()->find($id);
        $risk_types = RiskType::orderBy('id', 'ASC')->get();
        if($risk_status != null){
            return view('risk_status.edit')
                ->with([
                    'risk_status' => $risk_status,
                    'risk_types' => $risk_types->pluck('name','id'),
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('estados_riesgo.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(RiskStatusesRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $risk_status = RiskState::withTrashed()->find($id);

            if($risk_status != null){
                $risk_status->fill($request->all());
                $risk_status->update();

                DB::commit();

                flash('Se modificó el estado de riesgo: '.$risk_status->name)
                    ->warning()
                    ->important();
            }else{
                DB::rollBack();

                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        return redirect()
            ->route('estados_riesgo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $risk_status = RiskState::withTrashed()->find($id);
            if($risk_status != null){
                if($risk_status->deleted_at == null){
                    $risk_status->delete();
                }else{
                    $risk_status->restore();
                }

                flash('Se modificó el estado de riesgo: '.$risk_status->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }
        return redirect()->route('estados_riesgo.index');
    }

    public function getRiskStatesFromRiskType($id){
        $risk_type = RiskType::find($id);
    }
}
