<?php

namespace App\Http\Controllers;

use App\Http\Requests\LaboratoryRequest;
use App\Laboratory;
use Illuminate\Http\Request;

use DB;

class LaboratoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $laboratories = Laboratory::withTrashed()->get();
        return view('laboratory.index')
            ->with([
                'laboratories' => $laboratories
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        return view('laboratory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(LaboratoryRequest $request)
    {
        DB::beginTransaction();
        try{

            $laboratory = new Laboratory($request->all());

            $laboratory->save();
            if(!$request->ajax()){
                flash('Se registró el laboratorio: '.$laboratory->name)
                    ->success()
                    ->important();
            }
            DB::commit();
        }catch(\Exception $ex){
            DB::rollBack();
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        if($request->ajax()){
            return response()->json([
                'id' => $laboratory->id,
                'laboratory' => $laboratory,
            ]);
        }else{
            return redirect()
                ->route('laboratorios.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show($id)
    {
        $laboratory = Laboratory::withTrashed()->find($id);

        if($laboratory != null){
            return view('laboratory.show')
                ->with([
                    'laboratory' => $laboratory,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('laboratorios.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $laboratory = Laboratory::withTrashed()->find($id);

        if($laboratory != null){
            return view('laboratory.edit')
                ->with([
                    'laboratory' => $laboratory,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('laboratorios.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(LaboratoryRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $laboratory = Laboratory::withTrashed()->find($id);

            if($laboratory != null){
                $laboratory->fill($request->all());

                $laboratory->update();
                DB::commit();

                flash('Se modificó el laboratorio: '.$laboratory->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            DB::rollBack();
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        return redirect()
            ->route('laboratorios.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $laboratory = Laboratory::withTrashed()->find($id);
            if($laboratory != null){
                if($laboratory->deleted_at == null){
                    $laboratory->delete();
                }else{
                    $laboratory->restore();
                }
                DB::commit();

                flash('Se modificó el laboratorio: '.$laboratory->name)
                    ->warning()
                    ->important();
            }else{
                DB::rollBack();
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            DB::rollBack();
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }
        return redirect()->route('laboratorios.index');
    }
}
