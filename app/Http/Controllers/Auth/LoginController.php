<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
     protected $redirectTo = RouteServiceProvider::HOME;
//    protected $redirectTo = '/administracion_solicitudes';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Permite el inicio de sesión por nombre de usuario o correo electrónico
     */
    public function username()
    {
        $loginData = request()->input('login');
        $fieldName = filter_var($loginData, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        request()->merge([$fieldName => $loginData]);
        return $fieldName;
    }

    public function authenticated()
    {
        switch (Auth::user()->role_id){
            case 1: //ROOT
                return redirect()->route('administracion_solicitudes.index');
                break;
            case 2: //ADMIN
                return redirect()->route('usuario.index');
                break;
            case 3: //MÉDICO
                return redirect()->route('consultas_medicas.index');
                break;
            case 4: //ENFERMERA
                return redirect()->route('enfermera.index');
//                return redirect()->route('consultas_medicas.agendar');
                break;
            case 5: //RRHH
                return redirect()->route('consultas_medicas_pendientes.index');
                break;
            case 6: //RES. DE SEGUIMIENTOS
                return redirect()->route('seguimientos.index');
                break;
            case 7: //ADM. DE SEGUIMIENTOS
            case 8: //CALL CENTER
            case 10: //ADMIN TOTAL
                return redirect()->route('administracion_seguimientos.index');
                break;
            case 9: //REPORTES
                return redirect()->route('report.global_risk_state');
                break;
        }
    }
}
