<?php

namespace App\Http\Controllers;

use App\MedicalRequestStatus;
use App\WeightingLevel;
use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\Builder;

use App\MedicalRequest;
use App\EnterpriseSOL;
use App\RiskState;
use App\ProjectEnterprise;
use App\ProjectSOL;
use App\Tracing;
use Illuminate\Support\Arr;

use Carbon\Carbon;

class ReportController extends Controller
{
    public function index_casos(Request $request){
        $enterprises = EnterpriseSOL::get();

        $enterprises_search = EnterpriseSOL::pluck('nombre', 'id')
            ->toArray();
        $enterprises_search = Arr::prepend($enterprises_search, 'TODOS', 0);

        $projects_search = array();
        $projects_search = Arr::prepend($projects_search, 'TODOS', 0);

        $medical_request_statuses = MedicalRequestStatus::whereIn('id',[1,2,3,4,5,6,7,12,13,14,15,16,17])
            ->orderBy('name', 'desc')
            ->pluck('name', 'id')
            ->toArray();
        $medical_request_statuses = Arr::prepend($medical_request_statuses, 'TODOS', 0);

        $weighting_levels = WeightingLevel::get()
            ->pluck('name', 'id')
            ->toArray();
        $weighting_levels = Arr::prepend($weighting_levels, 'TODOS', 0);

        $risk_states = RiskState::get()
            ->pluck('name', 'id')
            ->toArray();
//        $risk_states = Arr::prepend($risk_states, '', '');

        if($request->ajax()) {
            //BUSQUEDA ESTADO
            if($request->medical_request_id_search != '0')
                $medical_requests = MedicalRequest::where('medical_request_status_id', $request->medical_request_id_search);
            else
                $medical_requests = MedicalRequest::whereIn('medical_request_status_id',[1,2,3,4,5,6,7,12,13,14,15,16,17]);


            //BUSQUEDA EMPRESA
            if(isset($request->enterprise_id_search)){
                if($request->enterprise_id_search != '0'){
                    $medical_requests = $medical_requests->where('employee_id','<>',null)
                        ->whereHas('employee', function (Builder $query) use ($request){
                            $query->where('sol_empresa_id','=', $request->enterprise_id_search);
                        });

                    $projects_ids = ProjectEnterprise::where('empresa_id','=',$request->enterprise_id_search)
                        ->pluck('proyecto_id')
                        ->toArray();

                    $projects_search = ProjectSOL::whereIn('id',$projects_ids)
                        ->pluck('nombre','id')
                        ->toArray();
                    $projects_search = Arr::prepend($projects_search, 'TODOS', 0);
                }
            }

            //BUSQUEDA PROYECTO
            if(isset($request->project_id_search)){
                if($request->project_id_search != '0'){
                    $medical_requests = $medical_requests->where('employee_id','<>',null)
                        ->whereHas('employee', function (Builder $query) use ($request){
                            $query->where('proyecto_id','=', $request->project_id_search);
                        });
                }
            }

            //BUSQUEDA DE FECHAS
            /*if($request->start_tracing_date_search != null or $request->end_tracing_date_search != null){
                if($request->start_tracing_date_search != null){
                    $medical_requests = $medical_requests->whereHas('tracings', function (Builder $query) use ($request){
                        $query->where('tracing_date','>=',$request->start_tracing_date_search);
                    });
                }

                if($request->end_tracing_date_search != null){
                    $medical_requests = $medical_requests->whereHas('tracings', function (Builder $query) use ($request){
                        $query->where('tracing_date','>=',$request->end_tracing_date_search);
                    });
                }
            }*/

            //BUSQUEDA ETAPA TRATAMIENTO
            if(isset($request->risk_state_id_search) && count($request->risk_state_id_search)>0){
                $medical_requests = $medical_requests->whereHas('tracings', function (Builder $query) use ($request){
                        $query->lastPerGroup(['medical_request_id'])
                            ->whereIn('risk_state_id', $request->risk_state_id_search);
                    });
            }

            //BUSQUEDA RIESGO
            if(isset($request->weighting_level_id_search)){
                if($request->weighting_level_id_search != '0'){
                    $medical_requests = $medical_requests->whereHas('tracings', function (Builder $first_query) use ($request){
                        $first_query->lastPerGroup(['medical_request_id'])
                            ->whereHas('historical_weighting_level',function (Builder $second_query) use ($request){
                                $second_query->where('weighting_level_id','=',$request->weighting_level_id_search);
                            });
                    });
                }
            }

            $medical_requests = $medical_requests->get();

            $table = view('report.partials.table_cases')
                ->with([
                    'medical_requests'         => $medical_requests,
                ])
                ->render();

            $buscador = view('layouts.components.cards.search_report_cases')
                ->with([
                    'medical_request_statuses'  => $medical_request_statuses,
                    //OPCIONES DE BUSCADOR
                    'enterprises_search'        => $enterprises_search,
                    'projects_search'           => $projects_search,
                    'weighting_levels'          => $weighting_levels,
                    'risk_states'               => $risk_states
                ])
                ->render();

            return response()->json([
                    'table' => $table,
                    'buscador' => $buscador,
                ]
            );
        }else{
            return view('report.index_case')
                ->with([
                    'enterprises'               => $enterprises,
                    'medical_request_statuses'  => $medical_request_statuses,
                    //OPCIONES DE BUSCADOR
                    'enterprises_search'        => $enterprises_search,
                    'projects_search'           => $projects_search,
                    'weighting_levels'          => $weighting_levels,
                    'risk_states'               => $risk_states
                ]);
        }

    }

    public function globalMedicalRequest ()
    {
        // Obtenemos los totales por tipo de riesgo (COVID-19)
        $confirmed = $this->getByRiskState(1);
        $discarted = $this->getByRiskState(2);
        $deceased = $this->getByRiskState(3);
        $recovered = $this->getByRiskState(4);
        $suspect = $this->getByRiskState(5);

        // Obtenemos los totales por niveles de riesgo (COVID-19)
        $level_1 = $this->getByLevel(1);
        $level_2 = $this->getByLevel(2);
        $level_3 = $this->getByLevel(3);
        $level_4 = $this->getByLevel(4);
        $level_5 = $this->getByLevel(5);

        // Obtenemos los totales por empresa (COVID-19)
        $enterprises = EnterpriseSOL::orderBy('id', 'ASC')
            ->get();

        $percentages_enterprises = array();

        foreach($enterprises as $enterprise)
        {
            $project_ids = ProjectEnterprise::where('empresa_id', $enterprise->id)
                ->select('proyecto_id')
                ->pluck('proyecto_id')
                ->toArray();

            $percentages_enterprises[$enterprise->id] = $this->getByEnterprise($enterprise->id, $project_ids);
        }

        // Obtenemos los totales por proyecto (COVID-19)
        $projects = ProjectSOL::orderBy('id', 'ASC')
            ->get();

        $percentages_projects = array();

        foreach ($projects as $project) {
            $percentages_projects[$project->id] = $this->getByProject($project->id);
        }

        // Total de Solicitudes
        $total_medical_requests = MedicalRequest::count();

        // Array de empresas
        $array_enterprises = array();

        foreach ($enterprises as $enterprise)
        {
            array_push($array_enterprises, array($enterprise->id, $enterprise->nombre));
        }

        return view ('report.global')
            ->with([
                'confirmed'                 => $confirmed,
                'discarted'                 => $discarted,
                'deceased'                  => $deceased,
                'recovered'                 => $recovered,
                'suspect'                   => $suspect,
                'level_1'                   => $level_1,
                'level_2'                   => $level_2,
                'level_3'                   => $level_3,
                'level_4'                   => $level_4,
                'level_5'                   => $level_5,
                'percentages_enterprises'   => $percentages_enterprises,
                'percentages_projects'      => $percentages_projects,
                'total_medical_requests'    => $total_medical_requests,
                'enterprises'               => $array_enterprises,
            ]);
    }

    public function globalRiskState ()
    {
        // Obtenemos los totales por empresa (COVID-19)
        $enterprises = EnterpriseSOL::orderBy('id', 'ASC')
            ->get();

        $total_cases_per_enterprise = array();
        $array_enterprise_cases = array();

        foreach($enterprises as $enterprise)
        {
            $project_ids = ProjectEnterprise::where('empresa_id', $enterprise->id)
                ->select('proyecto_id')
                ->pluck('proyecto_id')
                ->toArray();

            $medical_requests = $this->getByEnterprise($enterprise->id, $project_ids);

            $total_cases_per_enterprise[$enterprise->id] = $medical_requests;

            $array_enterprise_cases[$enterprise->id] = count($medical_requests);
        }

        arsort($array_enterprise_cases);

        // Obtenemos los totales por proyecto (COVID-19)
        $projects = ProjectSOL::orderBy('id', 'ASC')
            ->get();

        // Total de Solicitudes
        $total_medical_requests = MedicalRequest::count();

        // Fechas
        // $init_date = $date_cases_reported[0];

        return view ('report.risk_state')
            ->with([
                // Filtros
                'enterprises'                   => $enterprises,
                'projects'                      => $projects,
                // Totales
                'total_medical_requests'        => $total_medical_requests,
                'array_enterprise_cases'        => $array_enterprise_cases,
                // Estado de riesgo
                'risk_state_id'                 => 0,
            ]);
    }

    public function globalReportCases ()
    {
        // Obtenemos los totales por empresa (COVID-19)
        $enterprises = EnterpriseSOL::orderBy('id', 'ASC')
            ->get();

        $total_cases_per_enterprise = array();
        $array_enterprise_cases = array();

        foreach($enterprises as $enterprise)
        {
            $project_ids = ProjectEnterprise::where('empresa_id', $enterprise->id)
                ->select('proyecto_id')
                ->pluck('proyecto_id')
                ->toArray();

            $medical_requests = $this->getByEnterprise($enterprise->id, $project_ids);

            $total_cases_per_enterprise[$enterprise->id] = $medical_requests;

            $array_enterprise_cases[$enterprise->id] = count($medical_requests);
        }

        arsort($array_enterprise_cases);

        // Obtenemos los totales por proyecto (COVID-19)
        $projects = ProjectSOL::orderBy('id', 'ASC')
            ->get();

        // Total de Solicitudes
        $total_medical_requests = MedicalRequest::count();

        // Fechas
        // $init_date = $date_cases_reported[0];

        return view ('report.report_cases')
            ->with([
                // Filtros
                'enterprises'                   => $enterprises,
                'projects'                      => $projects,
                // Totales
                'total_medical_requests'        => $total_medical_requests,
                'array_enterprise_cases'        => $array_enterprise_cases,
                // Estado de riesgo
                'risk_state_id'                 => 0,
            ]);
    }

    public function ajaxEnterpriseRiskState (Request $request)
    {
        $init_date = null;
        $end_date = null;

        if (isset($request->init_date))
        {
            $init_date = Carbon::createFromFormat('d/m/Y', $request->init_date);
        }
        else
        {
            $init_date = Tracing::orderBy('tracing_date', 'ASC')
                ->first();
            if ($init_date==null) {
                $init_date = Carbon::now();
            }else{
                $init_date = $init_date->tracing_date;
            }
        }

        if (isset($request->end_date))
        {
            $end_date = Carbon::createFromFormat('d/m/Y', $request->end_date);
        }
        else
        {
            $end_date = Tracing::orderBy('tracing_date', 'DESC')
                ->first();
            if ($end_date==null) {
                $end_date = Carbon::now();
            }else{
                $end_date = $end_date->tracing_date;
            }
        }

        // Fechas en las que se ha reportado casos
        if ($init_date != null and $end_date != null)
        {
            $date_cases_reported = Tracing::orderBy('tracing_date', 'ASC')
                ->where([
                    ['tracing_date', '>=', $init_date],
                    ['tracing_date', '<=', $end_date]
                ])
                ->select('tracing_date')
                ->distinct()
                ->pluck('tracing_date');
        }
        else if ($init_date != null)
        {
            $date_cases_reported = Tracing::orderBy('tracing_date', 'ASC')
                ->where([
                    ['tracing_date', '>=', $init_date],
                ])
                ->select('tracing_date')
                ->distinct()
                ->pluck('tracing_date');
        }
        else if ($end_date != null)
        {
            $date_cases_reported = Tracing::orderBy('tracing_date', 'ASC')
                ->where([
                    ['tracing_date', '<=', $end_date],
                ])
                ->select('tracing_date')
                ->distinct()
                ->pluck('tracing_date');
        }
        else{
            $date_cases_reported = Tracing::orderBy('tracing_date', 'ASC')
                ->select('tracing_date')
                ->distinct()
                ->pluck('tracing_date');
        }

        $array_dates = array();

        foreach ($date_cases_reported as $date)
        {
            array_push($array_dates, $date->format('d-m'));
        }

        if (isset($request->enterprises) and isset($request->projects))
        {
            $medical_requests = MedicalRequest::join('tracings', 'tracings.medical_request_id', '=', 'medical_requests.id')
                ->select('medical_requests.*')
                ->distinct()
                ->where([
                    ['tracings.tracing_date', '>=', $init_date],
                    ['tracings.tracing_date', '<=', $end_date]
                ])
                ->whereIn('medical_requests.project_id', $request->projects)
                ->whereHas('employee', function (Builder $query) use ($request) {
                    $query->whereIn('sol_empresa_id', $request->enterprises);
                })
                ->orderBy('medical_requests.id', 'ASC')
                ->get();
        }
        else
        {
            $medical_requests = MedicalRequest::join('tracings', 'tracings.medical_request_id', '=', 'medical_requests.id')
                ->select('medical_requests.*')
                ->distinct()
                ->where([
                    ['tracings.tracing_date', '>=', $init_date],
                    ['tracings.tracing_date', '<=', $end_date]
                ])
                ->orderBy('id', 'ASC')
                ->get();
        }

        // dd($medical_requests);

        $matrix_risk_state = array();

        foreach ($medical_requests as $medical_request)
        {
            $array_tracings = $this->getArrayTracings($medical_request->id, $date_cases_reported);
            $array_risk_state = array();

            $last_risk_state = 0;
            foreach ($array_tracings as $case_per_date)
            {
                if ($case_per_date != null)
                {
                    array_push($array_risk_state, $case_per_date->risk_state_id);
                    $last_risk_state = $case_per_date->risk_state_id;
                }
                else
                {
                    array_push($array_risk_state, $last_risk_state);
                }
            }

            $matrix_risk_state[$medical_request->id] = $array_risk_state;
        }

        $array_confirmed_per_date = array();
        $array_discarted_per_date = array();
        $array_deceased_per_date = array();
        $array_recovered_per_date = array();
        $array_suspect_per_date = array();

        foreach ($matrix_risk_state as $risk_state)
        {
            // dd($risk_state, count($risk_state), $risk_state[20]);
            for ($i = 0; $i < count($risk_state); $i++)
            {
                if (!array_key_exists($i, $array_confirmed_per_date))
                {
                    $array_confirmed_per_date[$i] = 0;
                }
                if (!array_key_exists($i, $array_discarted_per_date))
                {
                    $array_discarted_per_date[$i] = 0;
                }
                if (!array_key_exists($i, $array_deceased_per_date))
                {
                    $array_deceased_per_date[$i] = 0;
                }
                if (!array_key_exists($i, $array_recovered_per_date))
                {
                    $array_recovered_per_date[$i] = 0;
                }
                if (!array_key_exists($i, $array_suspect_per_date))
                {
                    $array_suspect_per_date[$i] = 0;
                }

                switch ($risk_state[$i])
                {
                    case 1: // Confirmado
                        $array_confirmed_per_date[$i] += 1;
                    break;
                    case 2: // Descartado
                        $array_discarted_per_date[$i] += 1;
                    break;
                    case 3: // Fallecido
                        $array_deceased_per_date[$i] += 1;
                    break;
                    case 4: // Recuperadp
                        $array_recovered_per_date[$i] += 1;
                    break;
                    case 5: // Sospechoso
                        $array_suspect_per_date[$i] += 1;
                    break;
                }
            }
        }

        return response()->json([
            'array_dates'                   => $array_dates,
            'array_confirmed_per_date'      => $array_confirmed_per_date,
            'array_discarted_per_date'      => $array_discarted_per_date,
            'array_deceased_per_date'       => $array_deceased_per_date,
            'array_recovered_per_date'      => $array_recovered_per_date,
            'array_suspect_per_date'        => $array_suspect_per_date,
            // Fechas
            'init_date'                     => $init_date->format('d/m/Y'),
            'end_date'                      => $end_date->format('d/m/Y'),
            // Tabla de datos
            'view_data'                     => view ('report.partials.table_risk_state')->with([
                'array_dates'       => $array_dates,
                'matrix_risk_state' => $matrix_risk_state
            ])
                ->render()
        ]);
    }

    public function ajaxReportCases (Request $request)
    {
        $init_date = null;
        $end_date = null;

        if (isset($request->init_date))
        {
            $init_date = Carbon::createFromFormat('d/m/Y', $request->init_date);
        }
        else
        {
            $init_date = Tracing::orderBy('tracing_date', 'ASC')
                ->first()
                ->tracing_date;
        }

        if (isset($request->end_date))
        {
            $end_date = Carbon::createFromFormat('d/m/Y', $request->end_date);
        }
        else
        {
            $end_date = Tracing::orderBy('tracing_date', 'DESC')
                ->first()
                ->tracing_date;
        }

        // Fechas en las que se ha reportado casos
        if ($init_date != null and $end_date != null)
        {
            $date_cases_reported = Tracing::orderBy('tracing_date', 'ASC')
                ->where([
                    ['tracing_date', '>=', $init_date],
                    ['tracing_date', '<=', $end_date]
                ])
                ->select('tracing_date')
                ->distinct()
                ->pluck('tracing_date');
        }
        else if ($init_date != null)
        {
            $date_cases_reported = Tracing::orderBy('tracing_date', 'ASC')
                ->where([
                    ['tracing_date', '>=', $init_date],
                ])
                ->select('tracing_date')
                ->distinct()
                ->pluck('tracing_date');
        }
        else if ($end_date != null)
        {
            $date_cases_reported = Tracing::orderBy('tracing_date', 'ASC')
                ->where([
                    ['tracing_date', '<=', $end_date],
                ])
                ->select('tracing_date')
                ->distinct()
                ->pluck('tracing_date');
        }
        else{
            $date_cases_reported = Tracing::orderBy('tracing_date', 'ASC')
                ->select('tracing_date')
                ->distinct()
                ->pluck('tracing_date');
        }

        $array_dates = array();

        $array_confirmed_per_date = array();
        $array_discarted_per_date = array();
        $array_deceased_per_date = array();
        $array_recovered_per_date = array();
        $array_suspect_per_date = array();

        $i = 0;
        foreach ($date_cases_reported as $date)
        {
            array_push($array_dates, $date->format('d-m'));

            if (isset($request->enterprises) and isset($request->projects))
            {
                $tracings_date = MedicalRequest::join('tracings', 'tracings.medical_request_id', '=', 'medical_requests.id')
                    ->select('tracings.*')
                    ->distinct()
                    ->where([
                        ['tracings.tracing_date', '=', $date],
                    ])
                    ->whereIn('medical_requests.project_id', $request->projects)
                    ->whereHas('employee', function (Builder $query) use ($request) {
                        $query->whereIn('sol_empresa_id', $request->enterprises);
                    })
                    ->orderBy('tracings.id', 'ASC')
                    ->get();
            }
            else
            {
                $tracings_date = Tracing::where([
                    ['tracing_date', '=', $date],
                ])
                    ->get();
            }

            $array_confirmed_per_date[$i] = 0;
            $array_discarted_per_date[$i] = 0;
            $array_deceased_per_date[$i] = 0;
            $array_recovered_per_date[$i] = 0;
            $array_suspect_per_date[$i] = 0;

            foreach ($tracings_date as $tracing)
            {
                switch ($tracing->risk_state_id)
                {
                    case 1:
                        $array_confirmed_per_date[$i] += 1;
                    break;
                    case 2:
                        $array_discarted_per_date[$i] += 1;
                    break;
                    case 3:
                        $array_deceased_per_date[$i] += 1;
                    break;
                    case 4:
                        $array_recovered_per_date[$i] += 1;
                    break;
                    case 5:
                        $array_suspect_per_date[$i] += 1;
                    break;
                }
            }
            $i +=1;
        }

        return response()->json([
            'array_dates'                   => $array_dates,
            'array_confirmed_per_date'      => $array_confirmed_per_date,
            'array_discarted_per_date'      => $array_discarted_per_date,
            'array_deceased_per_date'       => $array_deceased_per_date,
            'array_recovered_per_date'      => $array_recovered_per_date,
            'array_suspect_per_date'        => $array_suspect_per_date,
            // Fechas
            'init_date'                     => $init_date->format('d/m/Y'),
            'end_date'                      => $end_date->format('d/m/Y'),
            // Tabla de datos
            'view_data'                     => view ('report.partials.table_report_cases')->with([
                'array_dates'                   => $array_dates,
                'array_confirmed_per_date'      => $array_confirmed_per_date,
                'array_discarted_per_date'      => $array_discarted_per_date,
                'array_deceased_per_date'       => $array_deceased_per_date,
                'array_recovered_per_date'      => $array_recovered_per_date,
                'array_suspect_per_date'        => $array_suspect_per_date,
            ])
                ->render()
        ]);
    }

    public function ajaxEnterpriseCases (Request $request)
    {
        // Total de Solicitudes
        if ($request->risk_state_id == 0)
        {
            $total_medical_requests = MedicalRequest::count();
        }
        else
        {
            $total_medical_requests = MedicalRequest::join('tracings', 'tracings.medical_request_id', '=', 'medical_requests.id')
                ->whereIn('tracings.id', function($query) {
                    $query->selectRaw('MAX(tracings.id)')
                        ->from('tracings')
                        ->groupBy('tracings.medical_request_id')
                        ->get();
                })
                ->where('tracings.risk_state_id', $request->risk_state_id)
                ->count();
        }

        // Obtenemos los totales por empresa (COVID-19)
        $enterprises = EnterpriseSOL::orderBy('id', 'ASC')
            ->get();

        $total_cases_per_enterprise = array();
        $array_enterprise_cases = array();

        foreach($enterprises as $enterprise)
        {
            $project_ids = ProjectEnterprise::where('empresa_id', $enterprise->id)
                ->select('proyecto_id')
                ->pluck('proyecto_id')
                ->toArray();

            if ($request->risk_state_id == 0)
            {
                $medical_requests = $this->getByEnterprise($enterprise->id, $project_ids);
            }
            else
            {
                $medical_requests = $this->getByEnterpriseRiskState($enterprise->id, $project_ids, $request->risk_state_id);
            }

            $total_cases_per_enterprise[$enterprise->id] = $medical_requests;

            $array_enterprise_cases[$enterprise->id] = count($medical_requests);
        }

        arsort($array_enterprise_cases);

        return response()->json([
            'view' => view('report.partials.ajax_enterprise_risk_state')
                ->with([
                    'total_medical_requests'    => $total_medical_requests,
                    'array_enterprise_cases'    => $array_enterprise_cases,
                    'risk_state_id'             => $request->risk_state_id
                ])
                ->render()
        ]);
    }

    private function getArrayTracings ($medical_request_id, $array_dates)
    {
        $array_risk_state = array();

        foreach ($array_dates as $date)
        {
            $risk_state = MedicalRequest::join('tracings', 'tracings.medical_request_id', '=', 'medical_requests.id')
                ->where([
                    ['tracings.medical_request_id', '=', $medical_request_id],
                    ['tracings.tracing_date', '=', $date],
                ])
                ->orderBy('tracings.id', 'DESC')
                ->select('tracings.risk_state_id')
                ->first();

            array_push($array_risk_state, $risk_state);
        }

        return $array_risk_state;
    }

    private function getByRiskState (int $risk_state_id)
    {
        return MedicalRequest::join('tracings', 'tracings.medical_request_id', '=', 'medical_requests.id')
            ->select('medical_request_id')
            ->distinct()
            ->where('tracings.risk_state_id', $risk_state_id)
            ->get();
    }

    private function getByLevel (int $weighting_level_id)
    {
        return MedicalRequest::join('tracings', 'tracings.medical_request_id', '=', 'medical_requests.id')
            ->join('historical_weighting_levels', 'historical_weighting_levels.tracing_id', '=', 'tracings.id')
            ->where('historical_weighting_levels.weighting_level_id', $weighting_level_id)
            ->whereIn('tracings.id', function($query) {
                $query->selectRaw('MAX(tracings.id)')
                    ->from('tracings')
                    ->groupBy('tracings.medical_request_id')
                    ->get();
            })
            ->get();
    }

    private function getByEnterprise ($enterprise_id, $project_ids)
    {
        return MedicalRequest::whereIn('project_id', $project_ids)
            ->whereHas('employee', function (Builder $query) use ($enterprise_id) {
                $query->where('sol_empresa_id', '=', $enterprise_id);
            })
            ->get();
    }

    private function getByEnterpriseRiskState ($enterprise_id, $project_ids, $risk_state_id)
    {
        return MedicalRequest::join('tracings', 'tracings.medical_request_id', '=', 'medical_requests.id')
            ->whereIn('medical_requests.project_id', $project_ids)
            ->whereHas('employee', function (Builder $query) use ($enterprise_id) {
                $query->where('sol_empresa_id', '=', $enterprise_id);
            })
            ->whereIn('tracings.id', function($query) {
                $query->selectRaw('MAX(tracings.id)')
                    ->from('tracings')
                    ->groupBy('tracings.medical_request_id')
                    ->get();
            })
            ->where('tracings.risk_state_id', $risk_state_id)
            ->get();
    }

    private function getByProject ($project_id)
    {
        return MedicalRequest::where('project_id', $project_id)
            ->get();
    }
}
