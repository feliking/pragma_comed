<?php

namespace App\Http\Controllers;

use App\EmployeeRRHH;
use App\Http\Requests\UserRequest;
use App\Office;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use function GuzzleHttp\json_encode;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::withTrashed()->get();
        return view('user.index')
            ->with([
                'users' => $users
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empleados = EmployeeRRHH::where([
            ['estado', '=', 1],
//            ['sol_empresa_id', '=', 1],
        ])
            ->orderBy('apellido_1', 'ASC')
            ->orderBy('apellido_2', 'ASC')
            ->orderBy('nombres', 'ASC')
            ->get()
            ->pluck('full_name', 'id');

        $roles = Role::withTrashed()->where([
            ['deleted_at', '=', null],
            ['id', '!=', 1],
        ])
            ->orderBy('id', 'ASC')
            ->pluck('name', 'id');
        $offices = Office::where('estado', 'Activo')
            ->orderBy('direccion', 'ASC')
            ->pluck('direccion', 'id');
        $medical_users = User::where('role_id', 3)
            ->orderBy('name')
            ->get();
        $nurse_users = User::where('role_id', 4)
            ->orderBy('name')
            ->get();
        return view('user.create')
            ->with([
                'empleados' => $empleados,
                'roles' => $roles,
                'offices' => $offices,
                'medical_staff' => $medical_users->pluck('name', 'id'),
                'nurse_staff' => $nurse_users->pluck('name', 'id'),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        //dd($request->all(), $request->doctor_staff_id, $request->doctor_staff_id!=null, $request->nurse_staff_id);
        /* $this->validate($request, [
            'employee_id' => 'required',
            'username' => 'required|string|min:3|max:255|unique:users,name',
            'role_id' => 'required|exists:roles,id',
            'rrhh_office_id' => 'required_if:role_id,3,4',
            'password' => 'required|min:5|confirmed',
        ]); */
        try{
            $empleado = EmployeeRRHH::find($request->employee_id);
            //dd($request->all(), $empleado, $empleado->nombres);
            if($empleado != null){
                $usuario = new User($request->all());
                $usuario->name = $empleado->nombres;
                $usuario->password = bcrypt($request->password);

                $usuario->save();
                if($request->doctor_staff_id!=null){
                    $usuario->medical_users()->attach($request->doctor_staff_id);
                }
                if($request->nurse_staff_id!=null){
                    $usuario->user_medical()->attach($request->nurse_staff_id);
                }
                flash('Se registró al usuario: '.$empleado->apellido_1.' '.$empleado->apellido_2.' '.$empleado->nombres.' ')
                    ->success()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();

                return redirect()
                    ->route('usuario.index');
            }
        }catch(\Exception $ex){

            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }
        return redirect()->route('usuario.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $usuario = User::withTrashed()->find($id);
        $value = $request->session()->get('key');
        if($usuario != null){
            return view('user.show')
                ->with([
                    'usuario' => $usuario,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();

            return redirect()
                ->route('user.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::withTrashed()->find($id);
        //dd($user);
        $roles = Role::withTrashed()->where('deleted_at', null)
            ->where('id','!=',1)
            ->orderBy('name', 'ASC')
            ->pluck('name', 'id');

        $empleados = EmployeeRRHH::where([
            ['estado', '=', 1],
//            ['sol_empresa_id', '=', 1],
        ])
            ->orderBy('apellido_1', 'ASC')
            ->orderBy('apellido_2', 'ASC')
            ->orderBy('nombres', 'ASC')
            ->get()
            ->pluck('full_name', 'id');
        $offices = Office::where('estado', 'Activo')
            ->orderBy('direccion', 'ASC')
            ->pluck('direccion', 'id');
        $medical_users = User::where('role_id', 3)
            ->orderBy('name')
            ->get();
        $nurse_users = User::where('role_id', 4)
            ->orderBy('name')
            ->get();
        //dd($user->medical_users()->pluck('doctor_id')->toArray(), $user->user_medical->first(), $user->user_medical()->pluck("nurse_id")->toArray());
        $a=[];
        if($user->role_id==3){
            $a=$user->user_medical()->pluck("nurse_id")->toArray();
        }
        if($user->role_id==4){
            $a=$user->medical_users()->pluck("doctor_id")->toArray();
        }
        //dd(json_encode($a));
        if($user != null){
            return view('user.edit')
                ->with([
                    'user' => $user,
                    'empleados' => $empleados,
                    'roles' => $roles,
                    'offices' => $offices,
                    'a' => json_encode($a),
                    'medical_staff' => $medical_users->pluck('name', 'id'),
                    'nurse_staff' => $nurse_users->pluck('name', 'id'),
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();

            return redirect()
                ->route('usuario.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario = User::withTrashed()->find($id);

        $this->validate($request, [
            'employee_id' => 'required',
            'username' => 'required|unique:users,name,'.$usuario->id.',id|string|min:5',
            'role_id' => 'required|exists:roles,id',
            'rrhh_office_id' => 'required_if:role_id,3,4',
            'password' => 'required|min:5|confirmed',
        ]);

        try{
            //$empleado = EmployeeRRHH::find($request->employee_id);
            if($usuario != null){
                $usuario->fill($request->all());
                if($request->password != ''){
                    $usuario->password = bcrypt($request->password);
                }else{
                    $usuario->password = User::withTrashed()->find($id)->password;
                }

                $usuario->update();
                if($request->doctor_staff_id!=null){
                    $usuario->medical_users()->detach();
                    $usuario->medical_users()->sync($request->doctor_staff_id);
                }
                if($request->nurse_staff_id!=null){
                    $usuario->user_medical()->detach();
                    $usuario->user_medical()->sync($request->nurse_staff_id);
                }
                flash('Se modificó al usuario: '.$usuario->employee->apellido_1.' '.$usuario->employee->apellido_2.' '.$usuario->employee->nombres)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            //Log::info('NEW updatee user: '.$ex);
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                    ->error()
                    ->important();
        }

        return redirect()
            ->route('usuario.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $usuario = User::withTrashed()->find($id);
            if($usuario != null){
                if($usuario->deleted_at == null){
                    $usuario->delete();
                }else{
                    $usuario->restore();
                }

                flash('Se modificó el estado del usuario: '.$usuario->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }
        return redirect()->route('usuario.index');
    }
    /**
     * Muestra el formulario para editar la contraseña.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editContrasena($id)
    {
        $usuario = User::find($id);
        //dd(Auth::user()->id);
        if (Auth::user()->id==$id) {
            return view('user.edit_contrasena')
            ->with('usuario', $usuario);
        }else{
            flash('No tiene permisos para ingresar a esta sección')
                ->error()
                ->important();
            return redirect()
                ->route('administracion_solicitudes.index');
        }

    }
    /**
     * Modifica la contraseña del usuario
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateContrasena(Request $request, $id)
    {
        $this->validate($request, [
            'password_actual' => 'required',
            'password' => 'required|min:5|confirmed',
        ]);

        try{
            $usuario = User::find($id);

            //$empleado = Employee::find($request->rrhh_empleado_id);

            if($usuario != null){
                if(\Hash::check($request->password_actual, $usuario->password)){
                    $usuario->password = bcrypt($request->password);

                    $usuario->update();

                    if ($id==1) {
                        flash('Se modificó la contraseña del usuario')
                        ->warning()
                        ->important();
                    }else{
                        flash('Se modificó la contraseña del usuario: '.$usuario->employee->apellido_1.' '.$usuario->employee->apellido_2.' '.$usuario->employee->nombres)
                        ->warning()
                        ->important();
                    }

                }else{
                    flash('La contraseña actual no corresponde con la cual ha iniciado sesión, vuelva a intentar. Si necesita ayuda puede comunicarse con el Departamento de TIC\'s.')
                        ->error()
                        ->important();
                }
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            //dispatch(new JobExceptionMail('UsuarioController - updateContrasena: '.$ex->getMessage(), Auth::user(), Carbon::now()));

            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                    ->error()
                    ->important();
        }

        return redirect()
            ->route('administracion_solicitudes.index');
    }

    public function getpersonalmedical(Request $request){
        //dd($request->all());
        if($request->role == 3){
            $nurse_users = User::where('role_id', 4)
                ->where('rrhh_office_id', $request->select_office)
                ->orderBy('name')
                ->get();
            if($nurse_users != null ){

                return response()->json([
                    'nurses' => $nurse_users/* ->pluck('name', 'id')->toArray() */,
                ]);
            }else{
                return response()->json([
                    'nurses' => null,
                ]);
            }
        }
        if($request->role == 4){
            $medical_users = User::where('role_id', 3)
                ->where('rrhh_office_id', $request->select_office)
                ->orderBy('name')
                ->get();
            //dd($medical_users);
            if($medical_users != null ){

                return response()->json([
                    'doctors' => $medical_users/* ->pluck('name', 'id')->toArray() */,
                ]);
            }else{
                return response()->json([
                    'doctors' => null,
                ]);
            }
        }
    }
}
