<?php

namespace App\Http\Controllers;

use App\Http\Requests\MedicalRequestStatusR;
use App\MedicalRequestStatus;
use Illuminate\Http\Request;

class MedicalRequestStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $medical_request_statuses = MedicalRequestStatus::withTrashed()->get();

        return view('medical_request_status.index')
            ->with([
                'medical_request_statuses' => $medical_request_statuses
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('medical_request_status.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MedicalRequestStatusR $request)
    {
        try{

            $medical_request_status = new MedicalRequestStatus($request->all());

            $medical_request_status->save();

            if(!$request->ajax()){
                flash('Se registró el estado: '.$medical_request_status->name)
                    ->success()
                    ->important();
            }
        }catch(\Exception $ex){

            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        if($request->ajax()){
            return response()->json([
                'id' => $medical_request_status->id,
                'medical_request_status' => $medical_request_status,
            ]);
        }else{
            return redirect()
                ->route('estado.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $medical_request_status = MedicalRequestStatus::withTrashed()->find($id);

        if($medical_request_status != null){
            return view('medical_request_status.show')
                ->with([
                    'medical_request_status' => $medical_request_status,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('estado.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $medical_request_status = MedicalRequestStatus::withTrashed()->find($id);
        if($medical_request_status != null){
            return view('medical_request_status.edit')
                ->with([
                    'medical_request_status' => $medical_request_status,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('estado.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MedicalRequestStatusR $request, $id)
    {
        try{
            $medical_request_status = MedicalRequestStatus::withTrashed()->find($id);

            if($medical_request_status != null){
                $medical_request_status->fill($request->all());

                $medical_request_status->update();

                flash('Se modificó el estado: '.$medical_request_status->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        return redirect()
            ->route('estado.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $medical_request_status = MedicalRequestStatus::withTrashed()->find($id);
            if($medical_request_status != null){
                if($medical_request_status->deleted_at == null){
                    $medical_request_status->delete();
                }else{
                    $medical_request_status->restore();
                }

                flash('Se modificó el estado: '.$medical_request_status->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }
        return redirect()->route('estado.index');
    }
}
