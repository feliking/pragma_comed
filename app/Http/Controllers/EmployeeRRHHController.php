<?php

namespace App\Http\Controllers;

use App\EmployeeRRHH;
use Illuminate\Http\Request;

class EmployeeRRHHController extends Controller
{

    public function getEmployeeFromCI($ci, $ci_expedition){
        $employees = EmployeeRRHH::orWhere(function ($query) {
                $query->orWhere('estado', '=', 'Activo')
                    ->orWhere('estado', '=', 'Externo');
            })
            ->where('ci_numero','=',$ci)
            ->where('ci_expedido','=',$ci_expedition);

        $data = null;
        $type = null;
        $msj = null;

        switch ($employees->count()){
            case 0: //NINGUN EMPLEADO ENCONTRADO
                $type = "warning";
                $msj = "Ningun empleado encontrado con el criterio buscado: ".$ci." ".$ci_expedition;
                break;
            case 1: //1 EMPLEADO ENCONTRADO
                $type = "correct_uno";
                $data = [
                    'employee' => $employees->first()
                ];
                break;
            default: //MAS DE UN EMPLEADO ENCONTRADO
                $type = "correct_mas_de_uno";
                $data = view('employee.partials.table_modal_search_result')
                    ->with([
                        'employees' => $employees->get(),
                        'ci' => $ci,
                        'ci_expedition' => $ci_expedition
                    ])->render();
                break;
        }

        return response()->json([
           'type' => $type,
           'msj' => $msj,
            'data' => $data
        ]);

    }

    public function getEmployeeFromEnterprise($enterprise_id){
        $employees = EmployeeRRHH::where('sol_empresa_id','=',$enterprise_id)
            ->where('estado','=','Activo')
            ->orderBy('nombres')
            ->get()
            ->pluck('full_name','id');

        return response()->json([
            'employees' => $employees
        ]);
    }
}
