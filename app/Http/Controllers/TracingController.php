<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\AppointmentType;
use App\ComplementaryExam;
use App\DiseaseCriteria;
use App\EmployeeRRHH;
use App\EnterpriseSOL;
use App\File;
use App\GeneralClass\WhatsApp;
use App\HistoricalRequest;
use App\HistoricalWeightingLevel;
use App\Http\Requests\RemitRequest;
use App\Http\Requests\ReprogrammingRequest;
use App\Http\Requests\TracingRequest;
use App\Jobs\AppointmentJob;
use App\Kit;
use App\kitTracing;
use App\Laboratory;
use App\MedicalRequest;
use App\MedicalRequestStatus;
use App\Parameter;
use App\ProjectSOL;
use App\RiskState;
use App\RiskType;
use App\TestType;
use App\Tracing;
use App\TreatmentStage;
use App\User;
use App\WeightingLevel;
use App\GeneralClass\CustomProcess;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

use DB;
use Illuminate\Support\Facades\Storage;
use const http\Client\Curl\Versions\IDN;

class TracingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $medical_requests = MedicalRequest::select('*', DB::raw('obtine_fecha_proxima_consulta(id) as fecha_proxima_consulta'))
            ->whereIn('medical_request_status_id',[13,14])
        // ->where(function ($query){
        //     $query->orWhereIn('medical_request_status_id','=',[13); //ASIGNADO A SEGUIMIENTO
        // })
            ->where('user_id', Auth::id()) //ASIGNADOS A MI PERSONA
            ->get();

        return view('tracing.index')
            ->with([
                'medical_requests'       => $medical_requests,
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(TracingRequest $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $medical_request = MedicalRequest::find($id);

        //DATOS PARA EL FORMULARIO
        $treatment_stages = TreatmentStage::get()
            ->pluck('name','id');
        $risk_types = RiskType::get()
            ->pluck('name','id');
        $enterprises = EnterpriseSOL::pluck('nombre','id')
            ->toArray();
        $enterprises = Arr::prepend($enterprises, '', '');

        //DATOS PARA EL MODAL DE EXAMENES COMPLEMENTARIOS
        $laboratories = Laboratory::get()
            ->pluck('name','id');
        $test_types = TestType::get()
            ->pluck('name','id');
        $medical_users = User::where('role_id', 3)
            ->orderBy('name')
            ->get();
        $appointment_types = AppointmentType::orderBy('id', 'asc')->get();
        //Datos para los kits
        $kit_types = Kit::orderBy('id', 'asc')->get()->pluck('name','id');

        $custom_process = new CustomProcess;
        $mi_mje = $custom_process->getMessageTreatmentStages($id);

        $parameter_age = Parameter::find(7)->value; //EDAD_INSEGURA
        $parameter_value = Parameter::find(8)->value; //VALOR_EDAD_INSEGURA

        // tracing ultimo
        $tracing_last_data = Tracing::where('medical_request_id', $id)
            ->orderBy('id', 'desc')
            ->first();
        // dd($tracing);

        if ($medical_request->employee_id != null) {
            $medical_requests = MedicalRequest::select('*', DB::raw('obtine_fecha_proxima_consulta(id) as fecha_proxima_consulta'))
                ->where('employee_id', $medical_request->employee_id)
                ->orderBy('id','desc')
                ->get();

            return view('tracing.edit')
                ->with([
                    'medical_request'   => $medical_request,
                    'historial_total'   => true,
                    'medical_requests'  => $medical_requests,
                    'cantidad'          => count($medical_requests),
                    //DATOS PARA EL FORMULARIO
                    'treatment_stages'  => $treatment_stages,
                    'risk_types'        => $risk_types,
                    'enterprises'       => $enterprises,
                    'medical_users'     => $medical_users->pluck('name','id'),
                    'appointment_types' => $appointment_types,
                    //DATOS PARA EL MODAL DE EXAMENES COMPLEMENTARIOS
                    'laboratories'      => $laboratories,
                    'test_types'        => $test_types,
                    //kits
                    'kit_types'         => $kit_types,
                    // mje de tratamiento
                    'mi_mje'            => $mi_mje,
                    //PARAMETROS
                    'parameter_age'     => $parameter_age,
                    'parameter_value'   => $parameter_value,
                    // ultimo registro
                    'tracing_last_data' => $tracing_last_data,
                    'tracing_last'      => true
                ]);
        }else{
            $historial_requests = HistoricalRequest::where('medical_request_id', $medical_request->id)
                ->orderBy('id', 'desc')
                ->get();
            $fecha_proxima_atencion = Appointment::where('medical_request_id', $id)
                ->orderBy('id','desc')
                ->first();

            return view('tracing.edit')
                ->with([
                    'medical_request'        => $medical_request,
                    'historial_requests'     => $historial_requests,
                    'historial_total'        => false,
                    'fecha_proxima_atencion' => $fecha_proxima_atencion,
                    'cantidad'               => count($historial_requests),
                    //DATOS PARA EL FORMULARIO
                    'treatment_stages'       => $treatment_stages,
                    'risk_types'             => $risk_types,
                    'enterprises'            => $enterprises,
                    'medical_users'          => $medical_users->pluck('name','id'),
                    'appointment_types'      => $appointment_types,
                    //DATOS PARA EL MODAL DE EXAMENES COMPLEMENTARIOS
                    'laboratories'           => $laboratories,
                    'test_types'             => $test_types,
                    //kits
                    'kit_types'              => $kit_types,
                    // mje de tratamiento
                    'mi_mje'                 => $mi_mje,
                    //PARAMETROS
                    'parameter_age'          => $parameter_age,
                    'parameter_value'        => $parameter_value,
                    // ultimo registro
                    'tracing_last_data'      => $tracing_last_data,
                    'tracing_last'           => true
                ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(TracingRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            //ACTUALIZACION DE SEGUIMIENTO
            $medical_request = MedicalRequest::find($id);

            $historical_request = new HistoricalRequest();
            $historical_request->user_id = Auth::id();
            $historical_request->comment = 'SEGUIMIENTO REALIZADO';
            $historical_request->medical_request_id = $medical_request->id;

            //REALIZACION DE CITAS
            /* if($request->asigna!=null || $request->asigna==1){

                // $medical_request = MedicalRequest::find($medical_request->id);

                $appointment = new Appointment();

                $appointment->next_attention_date = $request->next_attention_date;
                $appointment->comment = $request->comment;
                $appointment->appointment_type_id = $request->appointment_type_id;
                $appointment->medical_request_id = $medical_request->id;
                $appointment->user_id = $medical_request->user_id;

                $whatsapp_msj = "";
                switch ((int)$request->appointment_type_id){
                    case 1: //PRESENCIAL
                        $appointment->variable_text = null;
                        $whatsapp_msj = '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                            'Se registró una cita para atención médica de manera *PRESENCIAL* en fecha *'. $appointment->next_attention_date->format('d/m/Y') . '* y hora *' . $appointment->next_attention_date->format('H:i') . '* \n';
                        break;
                    case 2: //ZOOM
                        $appointment->variable_text = $request->variable_zoom;
                        $whatsapp_msj = '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                            'Se registró una cita para atención médica via *ZOOM* en fecha *'. $appointment->next_attention_date->format('d/m/Y') . '* y hora *' . $appointment->next_attention_date->format('H:i') . '* \n' .
                            'El link de acceso es el siguiente: ' . $request->variable_zoom . ' \n';
                        break;
                    case 3: //WHATSAPP
                        $appointment->variable_text = $request->variable_whatsapp;
                        $whatsapp_msj = '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                            'Se registró una cita para atención médica via *WHATSAPP* en fecha *'. $appointment->next_attention_date->format('d/m/Y') . '* y hora *' . $appointment->next_attention_date->format('H:i') . '* \n' .
                            'Se comunicarán a partir del siguiente número: '.$request->variable_whatsapp . ' \n';
                        break;
                }
                $appointment->save();

                $medical_request->medical_request_status_id = 2; //ASIGNADO A MÉDICO
                $medical_request->user_id = $request->user_id;

                $historical_request->medical_request_status_id = 2; //ASIGNADO A MÉDICO

                $whatsapp_msj .= '*Nota* \n' . $appointment->comment;
                if($medical_request->whatsapp_flag){
                    $whats = new WhatsApp();
                    $whats->wassengerSend($medical_request->phone_number, $whatsapp_msj);
                }

            } */
            /* else{
                $medical_request->medical_request_status_id = 12; //ASIGNADO EL ESTADO DE 'EN SEGUIMIENTO'
                $historical_request->medical_request_status_id = 12; //ASIGNADO EL ESTADO DE 'EN SEGUIMIENTO'
            } */
            $medical_request->medical_request_status_id = 12; //ASIGNADO EL ESTADO DE 'EN SEGUIMIENTO'
            $historical_request->medical_request_status_id = 12; //ASIGNADO EL ESTADO DE 'EN SEGUIMIENTO'
            $medical_request->update();

            $historical_request->save();

            $project = null;
            if($medical_request->employee_id!=null){
                $employee = EmployeeRRHH::find($medical_request->employee_id);
                // $project = $employee->sol_proyecto_id;
                $project = $employee->proyecto_id;
            }

            $request->merge([
                'tracing_date' => Carbon::now(),
                'medical_request_id' => $id,
                'user_id' => Auth::id(),
                'historical_request_id' => $historical_request->id,
                'project_id' => $project,
            ]);
            $tracing = new Tracing($request->all());
            $tracing->comment = $request->comment_tracing;
            $tracing->risk_type_id = $request->risk_type_id_hidden;
            $tracing->risk_state_id = $request->risk_state_id_hidden;
            $tracing->treatment_stage_id = $request->treatment_stage_id_hidden;
            $tracing->save();

            //************************************INICIO:CALCULO DE LA PONDERACION
            $sum = 0;

            //INICIO:PREGUNTA LA EDAD
            $parameter_age = Parameter::find(7)->value; //EDAD_INSEGURA
            $edad = 0;
            if($medical_request->employee!=null){
                $edad = $medical_request->employee->edad;
            }else{
                $edad = $medical_request->edad;
            }

            if($edad >= (int) $parameter_age){
                $sum++;
            }
            //FIN:PREGUNTA LA EDAD

            //INICIO:PREGUNTA ESTADO DE RIESGO
            $risk_state = RiskState::find($request->risk_state_id_hidden);
            if($risk_state->weighting > 0){
                $sum = $sum + $risk_state->weighting;
            }
            //FIN:PREGUNTA ESTADO DE RIESGO

            //INICIO:PREGUNTA CAMPOS MARCADOS
            if($request->disease_criteria!=null){
                foreach ($request->disease_criteria as $disease_criteria){
                    $tracing->disease_criterias()->attach($disease_criteria);
                    $disease = DiseaseCriteria::find($disease_criteria);
                    $sum += $disease->weighing;
                }
            }
            //FIN:PREGUNTA CAMPOS MARCADOS

            //GUARDANDO LA ADVERTENCIA Y EL RANGO
            $weighting_level = WeightingLevel::where('minimum','<=',$sum)
                ->where('maximum','>=',$sum)
                ->where('last_level_flag','=',false)
                ->first();

            $historical_weigthing_level = new HistoricalWeightingLevel;
            $historical_weigthing_level->calc_evaluation = $sum;

            if($weighting_level==null){
                //VERIFICA SI PERTENCE AL MAXIMO
                $weighting_level_max = WeightingLevel::where('minimum','<=',$sum)
                    ->where('last_level_flag','=',true)
                    ->first();
                if($weighting_level_max!=null){
                    $historical_weigthing_level->name = $weighting_level_max->name;
                    $historical_weigthing_level->description = $weighting_level_max->description;
                    $historical_weigthing_level->color = $weighting_level_max->color;
                    $historical_weigthing_level->minimum = $weighting_level_max->minimum;
                    $historical_weigthing_level->maximum = $weighting_level_max->maximum;
                    $historical_weigthing_level->evaluation = $weighting_level_max->evaluation;

                    $historical_weigthing_level->weighting_level_id = $weighting_level_max->id;
                    $historical_weigthing_level->tracing_id = $tracing->id;
                }
            }else{
                $historical_weigthing_level->name = $weighting_level->name;
                $historical_weigthing_level->description = $weighting_level->description;
                $historical_weigthing_level->color = $weighting_level->color;
                $historical_weigthing_level->minimum = $weighting_level->minimum;
                $historical_weigthing_level->maximum = $weighting_level->maximum;
                $historical_weigthing_level->evaluation = $weighting_level->evaluation;

                $historical_weigthing_level->weighting_level_id = $weighting_level->id;
                $historical_weigthing_level->tracing_id = $tracing->id;
            }
            $historical_weigthing_level->save();
            //************************************FIN:CALCULO DE LA PONDERACION

            //GUARDANDO EXAMENES COMPLEMENTARIOS
            $files = $request->file('files');
            // dd($files);
            if($request->laboratories!=null){
                for ($i=0 ; $i<count($request->laboratories) ;$i++){
                    $complementary_exam = new ComplementaryExam;
                    $complementary_exam->tracing_id = $tracing->id;

                    $complementary_exam->laboratory_id = $request->laboratories[$i];
                    $complementary_exam->test_type_id = $request->test_types[$i];
                    $complementary_exam->cost = $request->costs[$i];
                    $complementary_exam->bill_number = $request->bill_numbers[$i];
                    $complementary_exam->enterprise_flag = ($request->enterprise_pay_flags[$i]=="true")?true:false;
                    $complementary_exam->result_flag = ($request->result_flags[$i]=="true")?true:false;
                    $complementary_exam->save();

                    if ($files != null)
                    {
                        if(array_key_exists($i,$files)){
                            $path = $files[$i]->store('complementary_exams');

                            $complementary_exam->files()->create([
                                'name' => $files[$i]->getClientOriginalName(),
                                'file_type_id' => 1,
                                'location' => $path,
                                'mime' => $files[$i]->getClientMimeType()
                            ]);
                        }
                    }
                }

            }
            //GUARDANDO KITS
            if($request->kit!=null && $request->kit==1){
                $kit_tracing = new kitTracing();
                $kit_tracing->quantity = $request->quantity;
                $kit_tracing->kit_id = $request->kit_id;
                $kit_tracing->tracing_id = $tracing->id;
                $kit_tracing->save();
            }
            if(!$request->ajax()){
                flash('Se registró el seguimiento correctamente')
                    ->success()
                    ->important();
            }
            DB::commit();
        }catch(\Exception $ex){
            DB::rollBack();
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        if($request->ajax()){
            return response()->json([
                'id' => $tracing->id,
                'tracing' => $tracing,
            ]);
        }else{
            switch (Auth::user()->role_id){
                case 10: //ADMIN TOTAL
                    return redirect()->route('administracion_seguimientos.index');
                    break;
                default:
                    return redirect()
                        ->route('seguimientos.index');
                    break;
            }


        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     *
     * @description = Funcion que carga al modal para la vista de la ficha covid
     */
    public function getDiseaseCriteriaFromTracing($id){
        $tracing = Tracing::find($id);

        if($tracing!=null){
            $medical_request = $tracing->medicalRequest;
            $parameter_age = Parameter::find(7)->value; //EDAD_INSEGURA
            $parameter_value = Parameter::find(8)->value; //VALOR_EDAD_INSEGURA

            $edad = 0;
            if($medical_request->employee!=null)
                $edad = $medical_request->employee->edad;
            else
                $edad = $medical_request->edad;
            $laboratories = $tracing->complementaryExams;
            return response()->json([
                'type' => 'correct',
                'view' => view('layouts.components.modals.partials.list_covid_file')
                    ->with([
                        'disease_criterias' => $tracing->diseaseCriterias,
                        'historical_weigthing_level' => $tracing->historical_weighting_level,
                        'medical_request' => $tracing->medicalRequest,
                        'edad' => $edad,
                        'parameter_age' => $parameter_age,
                        'parameter_value' => $parameter_value,
                        'tracing' => $tracing,
                        'laboratories' => $laboratories,
                    ])->render()
            ]);
        }else{
            return response()->json([
                'type' => 'error',
                'msj' => 'No se encontro al seguimiento indicado'
            ]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getReprogrammingFormFromMedicalRequest($id){
        $medical_request = MedicalRequest::find($id);

        if($medical_request!=null){
            $parameter = Parameter::find(2); //TIEMPO_REPROGRAMACION_DIAS

            return response()->json([
                'type' => 'correct',
                'view' => view('layouts.components.modals.partials.form_reprogramming')
                    ->with([
                        'medical_request' => $medical_request,
                        'days' => $parameter->value
                    ])->render()
            ]);
        }else{
            return response()->json([
                'type' => 'error',
                'msj' => 'No se encontro al seguimiento indicado'
            ]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function updateReprogramming(ReprogrammingRequest $request, $id){
        $medical_request = MedicalRequest::find($id);

        if($medical_request!=null){
            DB::beginTransaction();
            try{
                $parameter = Parameter::find(2); //TIEMPO_REPROGRAMACION_DIAS
                $days = $parameter->value;

                $fecha_hora = $medical_request->last_appointment->next_attention_date->addDays($days)->format('Y-m-d').' '.$request->next_reprogramming_time;

                $last_appointment = $medical_request->last_appointment;
                $last_appointment->next_attention_date = $fecha_hora;
                $last_appointment->update();

                $medical_request->medical_requests_status()
                    ->attach(14, [ //ASIGNADO EL ESTADO DE 'RE ASIGNADO A SEGUIMIENTO'
                        'user_id' => Auth::id(),
                        'comment' => $request->reprogramming_comment
                    ]);
                $medical_request->medical_request_status_id = 14; //ASIGNADO EL ESTADO DE 'RE ASIGNADO A SEGUIMIENTO'
                $medical_request->update();

                $filas = [];
                $filas[$medical_request->id] = view('tracing.partials.tr_content')
                    ->with([
                        'medical_request' => $medical_request
                    ])->render();

                DB::commit();
                return response()->json([
                    'type' => 'correct',
                    'msj' => 'Registro realizado',
                    'filas' => $filas
                ]);
            }catch(\Exception $ex){
                DB::rollBack();
                return response()->json([
                    'type' => 'error',
                    'msj' => 'Error al realizar el registro'
                ]);
            }


        }else{
            return response()->json([
                'type' => 'error',
                'msj' => 'No se encontro al seguimiento indicado'
            ]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getRemitFormFromMedicalRequest($id){
        $medical_request = MedicalRequest::find($id);

        if($medical_request!=null){
            return response()->json([
                'type' => 'correct',
                'view' => view('layouts.components.modals.partials.form_remit')
                    ->with([
                        'medical_request' => $medical_request,
                    ])->render()
            ]);
        }else{
            return response()->json([
                'type' => 'error',
                'msj' => 'No se encontro al seguimiento indicado'
            ]);
        }
    }

    public function updateRemit(RemitRequest $request, $id){
        $medical_request = MedicalRequest::find($id);

        if($medical_request!=null){
            DB::beginTransaction();
            try{
                $estado = null;

                switch ($medical_request->actual_user->role_id){
                    case 3: //EL QUE SE ENCUENTRA ACTUALMENTE ASIGNADO ES UN 'MEDICO'
                        $estado = 16;
                        break;
                    case 4: //EL QUE SE ENCUENTRA ACTUALMENTE ASIGNADO ES UNA 'ENFERMERA'
                    case 6: //EL QUE SE ENCUENTRA ACTUALMENTE ASIGNADO ES UN 'RESP. SE SEGUIMIENTO'
                        $estado = 17;
                        break;
                    default: //NO DEBERIA EXISTIR OTRO CASO
                        DB::rollBack();
                        return response()->json([
                            'type'  => 'warning',
                            'msj'   => 'Usted no cuenta con los permisos para remitir un caso',
                        ]);
                        break;
                }

                $medical_request->medical_requests_status()
                    ->attach($estado, [ //ASIGNADO EL ESTADO DE 'RE EMISION'
                        'user_id' => Auth::id(),
                        'comment' => $request->comment_remit
                    ]);
                $medical_request->medical_request_status_id = $estado; //ASIGNADO EL ESTADO DE 'RE EMISION'
                $medical_request->update();

                $filas = [];
                $filas[$medical_request->id] = view('tracing.partials.tr_content')
                    ->with([
                        'medical_request' => $medical_request
                    ])->render();

                DB::commit();
                return response()->json([
                    'type' => 'correct',
                    'msj' => 'Re-emisión realizado',
                    'filas' => $filas
                ]);
            }catch(\Exception $ex){
                DB::rollBack();
                return response()->json([
                    'type' => 'error',
                    'msj' => 'Error al realizar el registro'
                ]);
            }


        }else{
            return response()->json([
                'type' => 'error',
                'msj' => 'No se encontro al seguimiento indicado'
            ]);
        }
    }
}
