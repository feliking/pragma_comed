<?php

namespace App\Http\Controllers;

use App\Http\Requests\WeightingLevelRequest;
use App\WeightingLevel;
use Illuminate\Http\Request;

class WeightingLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $weighting_levels = WeightingLevel::withTrashed()->orderBy('id', 'ASC')->get();
        return view('weighting_level.index')
            ->with([
                'weighting_levels' => $weighting_levels
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        $weighting_levels = WeightingLevel::orderBy('id', 'ASC')->get();
        return view('weighting_level.create')
            ->with('weighting_levels', $weighting_levels);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(WeightingLevelRequest $request)
    {
        try{

            $weighting_level = new WeightingLevel($request->all());
            if($request->radio_c=="true"){
                $weighting_level->last_level_flag = 1;
            }else{
                $weighting_level->last_level_flag = 0;
            }
            $weighting_level->save();

            if(!$request->ajax()){
                flash('Se registró el nivel de ponderación: '.$weighting_level->name)
                    ->success()
                    ->important();
            }
        }catch(\Exception $ex){

            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        if($request->ajax()){
            return response()->json([
                'id' => $weighting_level->id,
                'weighting_level' => $weighting_level,
            ]);
        }else{
            return redirect()
                ->route('niveles_ponderacion.index');
        }
    }

   /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show($id)
    {
        $weighting_level = WeightingLevel::withTrashed()->find($id);

        if($weighting_level != null){
            return view('weighting_level.show')
                ->with([
                    'weighting_level' => $weighting_level,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('niveles_ponderacion.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $weighting_level = WeightingLevel::withTrashed()->find($id);
        $weighting_levels = WeightingLevel::orderBy('id', 'ASC')->get();
        $cant = count($weighting_levels);
        $plastel = 0;
        $pweighting_levels = null;
        if ($cant>1) {
            $plastel = $cant-2;
            $pweighting_levels = $weighting_levels[$plastel];
        }
        //dd($plastel, $pweighting_levels);
        if($weighting_level != null){
            return view('weighting_level.edit')
                ->with([
                    'weighting_level' => $weighting_level,
                    'weighting_levels' => $weighting_levels,
                    'pweighting_levels' => $pweighting_levels,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('niveles_ponderacion.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(WeightingLevelRequest $request, $id)
    {
        try{
            $weighting_level = WeightingLevel::withTrashed()->find($id);

            if($weighting_level != null){
                $weighting_level->fill($request->all());
                if($request->radio_c=="true"){
                    $weighting_level->last_level_flag = 1;
                }else{
                    $weighting_level->last_level_flag = 0;
                }
                $weighting_level->update();

                flash('Se modificó el nivel de ponderación: '.$weighting_level->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        return redirect()
            ->route('niveles_ponderacion.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $weighting_level = WeightingLevel::withTrashed()->find($id);
            if($weighting_level != null){
                if($weighting_level->deleted_at == null){
                    $weighting_level->delete();
                }else{
                    $weighting_level->restore();
                }

                flash('Se modificó el nivel de ponderación: '.$weighting_level->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }
        return redirect()->route('niveles_ponderacion.index');
    }
}
