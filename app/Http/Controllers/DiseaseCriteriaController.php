<?php

namespace App\Http\Controllers;

use App\DiseaseCriteria;
use App\Http\Requests\DiseaseCriteriaRequest;
use App\RiskType;
use Illuminate\Http\Request;

class DiseaseCriteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $disease_criterias = DiseaseCriteria::withTrashed()->get();
        return view('disease_criteria.index')
            ->with([
                'disease_criterias' => $disease_criterias
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $risk_types = RiskType::orderBy('id', 'ASC')->get();
        //dd($risk_types, count($risk_types));
        return view('disease_criteria.create')
            ->with('risk_types', $risk_types->pluck('name','id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DiseaseCriteriaRequest $request)
    {
        //dd($request->all());
        try{

            $disease_criteria = new DiseaseCriteria($request->all());
            
            $disease_criteria->save();

            if(!$request->ajax()){
                flash('Se registró el nivel de ponderación: '.$disease_criteria->name)
                    ->success()
                    ->important();
            }
        }catch(\Exception $ex){

            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        if($request->ajax()){
            return response()->json([
                'id' => $disease_criteria->id,
                'disease_criteria' => $disease_criteria,
            ]);
        }else{
            return redirect()
                ->route('criterios_enfermedad.index');
        }
    }

   /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $disease_criteria = DiseaseCriteria::withTrashed()->find($id);

        if($disease_criteria != null){
            return view('disease_criteria.show')
                ->with([
                    'disease_criteria' => $disease_criteria,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('criterios_enfermedad.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $disease_criteria = DiseaseCriteria::withTrashed()->find($id);
        $risk_types = RiskType::orderBy('id', 'ASC')->get();
        if($disease_criteria != null){
            return view('disease_criteria.edit')
                ->with([
                    'disease_criteria' => $disease_criteria,
                    'risk_types' => $risk_types->pluck('name','id'),
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('criterios_enfermedad.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DiseaseCriteriaRequest $request, $id)
    {
        try{
            $disease_criteria = DiseaseCriteria::withTrashed()->find($id);

            if($disease_criteria != null){
                $disease_criteria->fill($request->all());
                $disease_criteria->update();

                flash('Se modificó el nivel de ponderación: '.$disease_criteria->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        return redirect()
            ->route('criterios_enfermedad.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $disease_criteria = DiseaseCriteria::withTrashed()->find($id);
            if($disease_criteria != null){
                if($disease_criteria->deleted_at == null){
                    $disease_criteria->delete();
                }else{
                    $disease_criteria->restore();
                }

                flash('Se modificó el nivel de ponderación: '.$disease_criteria->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }
        return redirect()->route('criterios_enfermedad.index');
    }
}
