<?php

namespace App\Http\Controllers;

use App\ProjectEnterprise;
use App\ProjectSOL;
use App\Tracing;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\MedicalRequest;
use App\MedicalRequestStatus;
use App\User;
use App\AppointmentType;
use App\Appointment;
use App\EnterpriseSOL;
use App\Parameter;
use App\HistoricalRequest;
use App\TreatmentStage;
use App\RiskState;
use App\RiskType;
use App\TestType;
use App\Kit;
use App\kitTracing;
use App\Laboratory;
use App\DiseaseCriteria;
use App\WeightingLevel;
use App\HistoricalWeightingLevel;
use App\ComplementaryExam;

use App\File;

use Carbon\Carbon;
use App\Http\Requests\TracingAdministrationRequest;
use App\Http\Requests\TracingAdministrationValidation;
use App\Http\Requests\GetMedicalRequestValidation;
use App\EmployeeRRHH;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests\TracingRequest;
use App\Http\Requests\TracingRequestAdmin;

use DB;

// Jobs
use App\Jobs\AssignToMedicJob;

// Whatsapp
use App\GeneralClass\WhatsApp;
use Illuminate\Database\Eloquent\Builder;
use function GuzzleHttp\Promise\all;

// Helpers
use Illuminate\Support\Arr;

class TracingAdministrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // dd($request->all(), 'aqui');
        $tab = 'seguimiento';
        $enterprises = EnterpriseSOL::get();

        $enterprises_search = EnterpriseSOL::pluck('nombre', 'id')
            ->toArray();
        $enterprises_search = Arr::prepend($enterprises_search, 'TODOS', 0);

        $projects_search = array();
        $projects_search = Arr::prepend($projects_search, 'TODOS', 0);

        $medical_request_statuses = MedicalRequestStatus::whereIn('id', [2,8,9,10,11,12,13,14,16,17])
            ->pluck('name', 'id')
            ->toArray();
        /*$search_dates_options = [
            0=>'NINGUNO',
            2=>'',
            3=>'',
        ];*/
        $cant_tabs = [];
        $option_date_search = [
            0=>'NINGUNO',
            1=>'ULTIMA CONSULTA',
            2=>'FECHA SUGERIDA PARA PRÓXIMA CONSULTA',
        ];

        if(isset($request->_) and count($request->all()) == 1){
            // Primera Carga
            $medical_requests = MedicalRequest::whereIn('medical_request_status_id', [2,8,9,10,11,12,13,14,16,17])
                ->get();
        }else if(count($request->all()) > 1){

            switch ($request->tab_value) {
                case 'solicitado':
                    if(isset($request->medical_request_id_search)){
                        if($request->medical_request_id_search != '0'){
                            $medical_requests = MedicalRequest::where('medical_request_status_id', $request->medical_request_id_search);
                        }else{
                            $medical_requests = MedicalRequest::whereIn('medical_request_status_id', [1,5,6]);
                        }
                    }else{
                        $medical_requests = MedicalRequest::whereIn('medical_request_status_id', [1,5,6]);
                    }

                    if(isset($request->enterprise_id_search)){
                        if($request->enterprise_id_search != '0'){
                            $medical_requests = $medical_requests->where('employee_id','<>',null)
                                ->whereHas('employee', function (Builder $query) use ($request){
                                    $query->where('sol_empresa_id','=', $request->enterprise_id_search);
                                });

                            $projects_ids = ProjectEnterprise::where('empresa_id','=',$request->enterprise_id_search)
                                ->pluck('proyecto_id')
                                ->toArray();

                            $projects_search = ProjectSOL::whereIn('id',$projects_ids)
                                ->pluck('nombre','id')
                                ->toArray();
                            $projects_search = Arr::prepend($projects_search, 'TODOS', 0);
                        }
                    }

                    if(isset($request->project_id_search)){
                        if($request->project_id_search != '0'){
                            $medical_requests = $medical_requests->where('employee_id','<>',null)
                                ->whereHas('employee', function (Builder $query) use ($request){
                                    $query->where('proyecto_id','=', $request->project_id_search);
                                });
                        }
                    }

                    $cant_tabs[0] = $medical_requests->count();
                    $medical_requests = $medical_requests->get();

                    $tab = 'solicitado';

                    $medical_request_statuses = MedicalRequestStatus::whereIn('id', [1,5,6])
                        ->pluck('name', 'id')
                        ->toArray();
                    $medical_request_statuses = Arr::prepend($medical_request_statuses, 'TODOS', 0);

                    if($request->ajax()){
                        $table = view('tracing_administrations.partials.table')
                            ->with([
                                'medical_requests'         => $medical_requests,
                                'tab'                      => $tab,
                            ])
                            ->render();
                        $buscador = view('layouts.components.cards.search_tracing_administration')
                            ->with([
                                'medical_request_statuses'  => $medical_request_statuses,
                                //OPCIONES DE BUSCADOR
                                'enterprises_search'        => $enterprises_search,
                                'projects_search'           => $projects_search,
                                'option_date_search'        => $option_date_search,
                            ])
                            ->render();
                        return response()->json([
                                'table' => $table,
                                'buscador' => $buscador,
                            ]
                        );
                    }
                    break;
                case 'seguimiento':
                    if(isset($request->medical_request_id_search)){
                        if($request->medical_request_id_search != '0'){
                            $medical_requests = MedicalRequest::where('medical_request_status_id', $request->medical_request_id_search);
                        }else{
                            $medical_requests = MedicalRequest::whereIn('medical_request_status_id', [2,8,9,10,11,12,13,14,16,17]);
                        }
                    }else{
                        $medical_requests = MedicalRequest::whereIn('medical_request_status_id', [2,8,9,10,11,12,13,14,16,17]);
                    }

                    if(isset($request->enterprise_id_search)){
                        if($request->enterprise_id_search != '0'){
                            $medical_requests = $medical_requests->where('employee_id','<>',null)
                            ->whereHas('employee', function (Builder $query) use ($request){
                                $query->where('sol_empresa_id','=', $request->enterprise_id_search);
                            });

                            $projects_ids = ProjectEnterprise::where('empresa_id','=',$request->enterprise_id_search)
                                ->pluck('proyecto_id')
                                ->toArray();

                            $projects_search = ProjectSOL::whereIn('id',$projects_ids)
                                ->pluck('nombre','id')
                                ->toArray();
                            $projects_search = Arr::prepend($projects_search, 'TODOS', 0);
                        }
                    }

                    if(isset($request->project_id_search)){
                        if($request->project_id_search != '0'){
                            $medical_requests = $medical_requests->where('employee_id','<>',null)
                                ->whereHas('employee', function (Builder $query) use ($request){
                                    $query->where('proyecto_id','=', $request->project_id_search);
                                });
                        }
                    }

                    //BUSCADOR DE OPCIONES DE LAS FECHAS
                    switch ($request->option_date_search){
                        case 1: //ULTIMA CONSULTA
                            $medical_requests = $medical_requests->get()->filter(function($model){
                                if($model->last_appointment!=null){
                                    return $model->last_appointment->next_attention_date->format('d/m/Y') == '05/09/2020';
                                }
                            });
                            break;
                        case 2: //FECHA SUGERIDA PARA PRÓXIMA CONSULTA
                            $medical_requests = $medical_requests->get()->filter(function($model){
                                if($model->last_appointment!=null){
                                    return $model->last_appointment->next_suggest_date->format('d/m/Y') == '05/09/2020';
                                }
                            });
                            break;
                        default: //NINGUNO O NINGUNA OPCION CODIFICADA
                            $cant_tabs[1] = $medical_requests->count();
                            $medical_requests = $medical_requests->get();
                            break;
                    }

                    $tab = 'seguimiento';
                    $medical_request_statuses = MedicalRequestStatus::whereIn('id', [2,8,9,10,11,12,13,14,16,17])
                        ->pluck('name', 'id')
                        ->toArray();
                    $medical_request_statuses = Arr::prepend($medical_request_statuses, 'TODOS', 0);
                    if($request->ajax()){
                        $table = view('tracing_administrations.partials.table_tracing')
                                ->with([
                                    'medical_requests'         => $medical_requests,
                                    'tab'                      => $tab,
                                ])
                                ->render();
                        $buscador = view('layouts.components.cards.search_tracing_administration')
                                ->with([
                                    'medical_request_statuses'  => $medical_request_statuses,
                                    //OPCIONES DE BUSCADOR
                                    'enterprises_search'        => $enterprises_search,
                                    'projects_search'           => $projects_search,
                                    'option_date_search'        => $option_date_search,
                                ])
                                ->render();
                        return response()->json([
                                'table' => $table,
                                'buscador' => $buscador,
                            ]
                        );
                    }

                    break;
                case 'pendiente':
                    if(isset($request->medical_request_id_search)){
                        if($request->medical_request_id_search != '0'){
                            $medical_requests = MedicalRequest::where('medical_request_status_id', $request->medical_request_id_search);
                        }else{
                            $medical_requests = MedicalRequest::whereIn('medical_request_status_id', [3]);
                        }
                    }else{
                        $medical_requests = MedicalRequest::whereIn('medical_request_status_id', [3]);
                    }

                    if(isset($request->enterprise_id_search)){
                        if($request->enterprise_id_search != '0'){
                            $medical_requests = $medical_requests->where('employee_id','<>',null)
                                ->whereHas('employee', function (Builder $query) use ($request){
                                    $query->where('sol_empresa_id','=', $request->enterprise_id_search);
                                });

                            $projects_ids = ProjectEnterprise::where('empresa_id','=',$request->enterprise_id_search)
                                ->pluck('proyecto_id')
                                ->toArray();

                            $projects_search = ProjectSOL::whereIn('id',$projects_ids)
                                ->pluck('nombre','id')
                                ->toArray();
                            $projects_search = Arr::prepend($projects_search, 'TODOS', 0);
                        }
                    }

                    if(isset($request->project_id_search)){
                        if($request->project_id_search != '0'){
                            $medical_requests = $medical_requests->where('employee_id','<>',null)
                                ->whereHas('employee', function (Builder $query) use ($request){
                                    $query->where('proyecto_id','=', $request->project_id_search);
                                });
                        }
                    }

                    $cant_tabs[2] = $medical_requests->count();
                    $medical_requests = $medical_requests->get();

                    $tab = 'pendiente';

                    $medical_request_statuses = MedicalRequestStatus::whereIn('id', [1,5,6])
                        ->pluck('name', 'id')
                        ->toArray();
                    $medical_request_statuses = Arr::prepend($medical_request_statuses, 'TODOS', 0);

                    if($request->ajax()){
                        $table = view('tracing_administrations.partials.table_tracing')
                            ->with([
                                'medical_requests'         => $medical_requests,
                                'tab'                      => $tab,
                            ])
                            ->render();
                        $buscador = view('layouts.components.cards.search_tracing_administration')
                            ->with([
                                'medical_request_statuses'  => $medical_request_statuses,
                                //OPCIONES DE BUSCADOR
                                'enterprises_search'        => $enterprises_search,
                                'projects_search'           => $projects_search,
                                'option_date_search'        => $option_date_search,
                            ])
                            ->render();
                        return response()->json([
                                'table' => $table,
                                'buscador' => $buscador,
                            ]
                        );
                    }
                    break;
                case 'cerrados':
                    if(isset($request->medical_request_id_search)){
                        if($request->medical_request_id_search != '0'){
                            $medical_requests = MedicalRequest::where('medical_request_status_id', $request->medical_request_id_search);
                        }else{
                            $medical_requests = MedicalRequest::whereIn('medical_request_status_id', [4]);
                        }
                    }else{
                        $medical_requests = MedicalRequest::whereIn('medical_request_status_id', [4]);
                    }

                    if(isset($request->enterprise_id_search)){
                        if($request->enterprise_id_search != '0'){
                            $medical_requests = $medical_requests->where('employee_id','<>',null)
                                ->whereHas('employee', function (Builder $query) use ($request){
                                    $query->where('sol_empresa_id','=', $request->enterprise_id_search);
                                });

                            $projects_ids = ProjectEnterprise::where('empresa_id','=',$request->enterprise_id_search)
                                ->pluck('proyecto_id')
                                ->toArray();

                            $projects_search = ProjectSOL::whereIn('id',$projects_ids)
                                ->pluck('nombre','id')
                                ->toArray();
                            $projects_search = Arr::prepend($projects_search, 'TODOS', 0);
                        }
                    }

                    if(isset($request->project_id_search)){
                        if($request->project_id_search != '0'){
                            $medical_requests = $medical_requests->where('employee_id','<>',null)
                                ->whereHas('employee', function (Builder $query) use ($request){
                                    $query->where('proyecto_id','=', $request->project_id_search);
                                });
                        }
                    }

                    $cant_tabs[3] = $medical_requests->count();
                    $medical_requests = $medical_requests->get();

                    $tab = 'cerrados';
                    $medical_request_statuses = MedicalRequestStatus::whereIn('id', [4])
                        ->pluck('name', 'id')
                        ->toArray();
                    $medical_request_statuses = Arr::prepend($medical_request_statuses, 'TODOS', 0);
                    if($request->ajax()){
                        $table = view('tracing_administrations.partials.table_tracing')
                            ->with([
                                'medical_requests'         => $medical_requests,
                                'tab'                      => $tab,
                            ])
                            ->render();
                        $buscador = view('layouts.components.cards.search_tracing_administration')
                            ->with([
                                'medical_request_statuses'  => $medical_request_statuses,
                                //OPCIONES DE BUSCADOR
                                'enterprises_search'        => $enterprises_search,
                                'projects_search'           => $projects_search,
                                'option_date_search'        => $option_date_search,
                            ])
                            ->render();
                        return response()->json([
                                'table' => $table,
                                'buscador' => $buscador,
                            ]
                        );
                    }
                    break;
                case 'rechazados':
                    if(isset($request->medical_request_id_search)){
                        if($request->medical_request_id_search != '0'){
                            $medical_requests = MedicalRequest::withTrashed()
                                ->where('medical_request_status_id', $request->medical_request_id_search);
                        }else{
                            $medical_requests = MedicalRequest::withTrashed()
                                ->whereIn('medical_request_status_id', [7]);
                        }
                    }else{
                        $medical_requests = MedicalRequest::withTrashed()
                            ->whereIn('medical_request_status_id', [7]);
                    }

                    if(isset($request->enterprise_id_search)){
                        if($request->enterprise_id_search != '0'){
                            $medical_requests = $medical_requests->where('employee_id','<>',null)
                                ->whereHas('employee', function (Builder $query) use ($request){
                                    $query->where('sol_empresa_id','=', $request->enterprise_id_search);
                                });

                            $projects_ids = ProjectEnterprise::where('empresa_id','=',$request->enterprise_id_search)
                                ->pluck('proyecto_id')
                                ->toArray();

                            $projects_search = ProjectSOL::whereIn('id',$projects_ids)
                                ->pluck('nombre','id')
                                ->toArray();
                            $projects_search = Arr::prepend($projects_search, 'TODOS', 0);
                        }
                    }

                    if(isset($request->project_id_search)){
                        if($request->project_id_search != '0'){
                            $medical_requests = $medical_requests->where('employee_id','<>',null)
                                ->whereHas('employee', function (Builder $query) use ($request){
                                    $query->where('proyecto_id','=', $request->project_id_search);
                                });
                        }
                    }

                    $cant_tabs[4] = $medical_requests->count();
                    $medical_requests = $medical_requests->get();

                    $tab = 'rechazados';
                    $medical_request_statuses = MedicalRequestStatus::whereIn('id', [7])
                        ->pluck('name', 'id')
                        ->toArray();
                    $medical_request_statuses = Arr::prepend($medical_request_statuses, 'TODOS', 0);
                    if($request->ajax()){
                        $table = view('tracing_administrations.partials.table_tracing')
                            ->with([
                                'medical_requests'         => $medical_requests,
                                'tab'                      => $tab,
                            ])
                            ->render();
                        $buscador = view('layouts.components.cards.search_tracing_administration')
                            ->with([
                                'medical_request_statuses'  => $medical_request_statuses,
                                //OPCIONES DE BUSCADOR
                                'enterprises_search'        => $enterprises_search,
                                'projects_search'           => $projects_search,
                                'option_date_search'        => $option_date_search,
                            ])
                            ->render();
                        return response()->json([
                                'table' => $table,
                                'buscador' => $buscador,
                            ]
                        );
                    }
                    break;
            }

        }

        if($request->ajax()){
            /*$table = view('tracing_administrations.partials.table')
                    ->with([
                        'medical_requests'         => $medical_requests,
                        'tab'                      => $tab,
                    ])
                    ->render();*/
            $table = view('tracing_administrations.partials.table_tracing')
                    ->with([
                        'medical_requests'         => $medical_requests,
                        'tab'                      => $tab,
                    ])
                    ->render();
            $buscador = view('layouts.components.cards.search_tracing_administration')
                    ->with([
                        'medical_request_statuses'  => $medical_request_statuses,
                        //OPCIONES DE BUSCADOR
                        'enterprises_search'        => $enterprises_search,
                        'projects_search'           => $projects_search,
                        'option_date_search'        => $option_date_search,
                    ])
                    ->render();
            return response()->json([
                    'table' => $table,
                    'buscador' => $buscador,
                ]
            );

        }else{
            //DATOS PARA EL MODAL DE EXAMENES COMPLEMENTARIOS
            $laboratories = Laboratory::get()
                ->pluck('name','id');
            $test_types = TestType::get()
                ->pluck('name','id');
            $medical_request_statuses = Arr::prepend($medical_request_statuses, 'TODOS', 0);

            $cant_tabs[0] = MedicalRequest::whereIn('medical_request_status_id', [1,5,6])->count();
            $cant_tabs[1] = MedicalRequest::whereIn('medical_request_status_id', [2,8,9,10,11,12,13,14,16,17])->count();
            $cant_tabs[2] =  MedicalRequest::whereIn('medical_request_status_id', [3])->count();
            $cant_tabs[3] = MedicalRequest::whereIn('medical_request_status_id', [4])->count();
            $cant_tabs[4] = MedicalRequest::whereIn('medical_request_status_id', [7])->count();
            return view('tracing_administrations.index')
                ->with([
                    'enterprises'               => $enterprises,
                    'medical_request_statuses'  => $medical_request_statuses,
                    //OPCIONES DE BUSCADOR
                    'enterprises_search'        => $enterprises_search,
                    'projects_search'           => $projects_search,
                    'option_date_search'        => $option_date_search,
                    //OPCIONES DE PESTAÑAS
                    'cant_tabs'                 => $cant_tabs,
                    'tab'                       => $tab,
                    //DATOS PARA EL MODAL DE EXAMENES COMPLEMENTARIOS
                    'laboratories'              => $laboratories,
                    'test_types'                => $test_types,
                ]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TracingAdministrationRequest $request)
    {
        if (isset($request->parametro_asignacion)) {
            DB::beginTransaction();
            try {
                $user =User::find($request->personal_id);
                for ($i=0; $i <count($request->medical_request_id) ; $i++) {
                    $medical_request = MedicalRequest::find($request->medical_request_id[$i]);
                    $medical_request->medical_request_status_id = 13;
                    $medical_request->user_id = $request->personal_id;
                    $medical_request->update();

                    // Historial
                    $medical_request->medical_requests_status()
                    ->attach(13, [ //ASIGNADO A SEGUIMIENTO
                        'user_id' => $request->personal_id,
                        'comment' => ($request->comment_tracing!='')?'ASIGNACIÓN DE SOLICITUD - ' .$request->comment_tracing:'ASIGNACIÓN DE SOLICITUD'
                    ]);

                    $appointment = new Appointment;
                    $appointment->next_attention_date = $request->next_attention_date_tracing.' '.$request->hora_asignada[$i];
                    $appointment->comment = $request->comment_tracing;
                    $appointment->appointment_type_id = $request->appointment_type_tracing_id;
                    $appointment->medical_request_id = $medical_request->id;
                    $appointment->user_id = $request->personal_id;

                    switch ((int)$request->appointment_type_tracing_id){
                        case 1: //PRESENCIAL
                            $appointment->variable_text = null;
                            break;
                        case 2: //ZOOM
                            $appointment->variable_text = $request->variable_tracing_zoom;
                            break;
                        case 3: //WHATSAPP
                            $appointment->variable_text = $request->variable_tracing_whatsapp;
                            break;
                    }
                    $appointment->save();
                }

                DB::commit();

                // Envío de Correo electrónico
                /*if ($medical_request->email != null && $medical_request->email != '')
                {
                    AssignToMedicJob::dispatch($medical_request);
                }
                // en vio de mensaje whatsapp
                if($medical_request->whatsapp_flag)
                {
                    $whats = new WhatsApp();

                    $whats->wassengerSend($medical_request->phone_number, '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                        'Su solicitud médica con código de seguimiento : *' . $medical_request->code . '* \n\n' .
                        'Fue asignada a *seguimiento:* ' . $medical_request->medic->employee->nombres . ' ' . $medical_request->medic->employee->apellido_1 . ' ' . $medical_request->medic->employee->apellido_2);
                }*/

                return response()->json([
                    'type'  => 'correct',
                    'msj'   => 'Se asigno la solicitud a seguimiento a: '.$user->full_name,
                    'filas' => [],
                    'reload_table' => true
                ]);
            }catch (\Throwable $e){
                DB::rollBack();
                return response()->json([
                    'type' => 'error',
                    'msj' => 'Ocurrio un error'
                ]);
            }
        }else{
            DB::beginTransaction();
            try {
                $user =User::find($request->personal_id);
                $filas = [];
                $medical_request = MedicalRequest::find($request->medical_request_id);
                $medical_request->medical_request_status_id = 13;
                $medical_request->user_id = $request->personal_id;
                $medical_request->update();

                // Historial
                $medical_request->medical_requests_status()
                ->attach(13, [ //ASIGNADO A SEGUIMIENTO
                    'user_id' => $request->personal_id,
                    'comment' =>($request->comment_tracing!='')?'ASIGNACIÓN DE SOLICITUD - ' .$request->comment_tracing:'ASIGNACIÓN DE SOLICITUD'
                ]);

                $filas[$medical_request->id] = view('tracing_administrations.partials.tr_tracing_content')
                    ->with([
                        'medical_request' => $medical_request
                    ])->render();

                $appointment = new Appointment;
                $appointment->next_attention_date = $request->next_attention_date_tracing.' '.$request->next_attention_time_tracing;
                $appointment->comment = $request->comment_tracing;
                $appointment->appointment_type_id = $request->appointment_type_tracing_id;
                $appointment->medical_request_id = $medical_request->id;
                $appointment->user_id = $request->personal_id;

                switch ((int)$request->appointment_type_tracing_id){
                    case 1: //PRESENCIAL
                        $appointment->variable_text = null;
                        break;
                    case 2: //ZOOM
                        $appointment->variable_text = $request->variable_tracing_zoom;
                        break;
                    case 3: //WHATSAPP
                        $appointment->variable_text = $request->variable_tracing_whatsapp;
                        break;
                }
                $appointment->save();

                DB::commit();

                // Envío de Correo electrónico
                //
                if ($medical_request->email != null && $medical_request->email != '')
                {
                    AssignToMedicJob::dispatch($medical_request);
                }
                // en vio de mensaje whatsapp
                if($medical_request->whatsapp_flag)
                {
                    $whats = new WhatsApp();

                    $whats->wassengerSend($medical_request->phone_number, '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                        'Su solicitud médica con código de seguimiento : *' . $medical_request->code . '* \n\n' .
                        'Fue asignada a *seguimiento:* ' . $medical_request->medic->employee->nombres . ' ' . $medical_request->medic->employee->apellido_1 . ' ' . $medical_request->medic->employee->apellido_2);
                }

                return response()->json([
                    'type'  => 'correct',
                    'msj'   => 'Se asigno la solicitud a seguimiento a: '.$user->full_name,
                    'filas' => $filas,
                    'reload_table' => false
                ]);
            }catch (\Throwable $e){
                DB::rollBack();
                return response()->json([
                    'type' => 'error',
                    'msj' => 'Ocurrio un error'
                ]);
            }
        }
    }

    public function storeMedic(TracingAdministrationRequest $request)
    {
        if (isset($request->parametro_asignacion)) {
            DB::beginTransaction();
            try {
                $user = User::find($request->medico_id);
                for ($i=0; $i <count($request->medical_request_id) ; $i++) {
                    $medical_request = MedicalRequest::find($request->medical_request_id[$i]);
                    $medical_request->medical_request_status_id = 2;
                    $medical_request->user_id = $request->medico_id;
                    $medical_request->update();

                    // Historial
                    $medical_request->medical_requests_status()
                    ->attach(2, [ //ASIGNADO A MEDICO
                        'user_id' => $request->medico_id,
                        'comment' =>  ($request->comment_tracing!='')?'ASIGNACIÓN DE SOLICITUD - ' .$request->comment_tracing:'ASIGNACIÓN DE SOLICITUD'
                    ]);

                    $appointment = new Appointment;
                    $appointment->next_attention_date = $request->next_attention_date.' '.$request->hora_asignada[$i];
                    $appointment->comment = $request->comment;
                    $appointment->appointment_type_id = $request->appointment_type_id;
                    $appointment->medical_request_id = $medical_request->id;
                    $appointment->user_id = $request->medico_id;

                    switch ((int)$request->appointment_type_id){
                        case 1: //PRESENCIAL
                            $appointment->variable_text = null;
                            break;
                        case 2: //ZOOM
                            $appointment->variable_text = $request->variable_zoom;
                            break;
                        case 3: //WHATSAPP
                            $appointment->variable_text = $request->variable_whatsapp;
                            break;
                    }
                    $appointment->save();
                }

                DB::commit();

                // Envío de Correo electrónico
                /*if ($medical_request->email != null && $medical_request->email != '')
                {
                    AssignToMedicJob::dispatch($medical_request);
                }
                // en vio de mensaje whatsapp
                if($medical_request->whatsapp_flag)
                {
                    $whats = new WhatsApp();

                    $whats->wassengerSend($medical_request->phone_number, '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                        'Su solicitud médica con código de seguimiento : *' . $medical_request->code . '* \n\n' .
                        'Fue asignada al *Médico:* ' . $medical_request->medic->employee->nombres . ' ' . $medical_request->medic->employee->apellido_1 . ' ' . $medical_request->medic->employee->apellido_2);
                }*/

                return response()->json([
                    'type'  => 'correct',
                    'msj'   => 'Se asigno la solicitud al m-edico'.$user->full_name,
                    'filas' => [],
                    'reload_table' => true
                ]);
            }catch (\Throwable $e){
                DB::rollBack();
                return response()->json([
                    'type' => 'error',
                    'msj' => 'Ocurrio un error'
                ]);
            }
        }else{
            DB::beginTransaction();
            try {
                $user = User::find($request->medico_id);
                $filas = [];
                $medical_request = MedicalRequest::find($request->medical_request_id);
                $medical_request->medical_request_status_id = 2;
                $medical_request->user_id = $request->medico_id;
                $medical_request->update();

                // Historial
                $medical_request->medical_requests_status()
                ->attach(2, [ //ASIGNADO A MEDICO
                    'user_id' => $request->medico_id,
                    'comment' =>($request->comment_tracing!='')?'ASIGNACIÓN DE SOLICITUD - ' .$request->comment_tracing:'ASIGNACIÓN DE SOLICITUD'
                ]);

                $filas[$medical_request->id] = view('tracing_administrations.partials.tr_tracing_content')
                    ->with([
                        'medical_request' => $medical_request
                    ])->render();

                $appointment = new Appointment;
                $appointment->next_attention_date = $request->next_attention_date.' '.$request->next_attention_time;
                $appointment->comment = $request->comment;
                $appointment->appointment_type_id = $request->appointment_type_id;
                $appointment->medical_request_id = $medical_request->id;
                $appointment->user_id = $request->medico_id;

                switch ((int)$request->appointment_type_id){
                    case 1: //PRESENCIAL
                        $appointment->variable_text = null;
                        break;
                    case 2: //ZOOM
                        $appointment->variable_text = $request->variable_zoom;
                        break;
                    case 3: //WHATSAPP
                        $appointment->variable_text = $request->variable_whatsapp;
                        break;
                }
                $appointment->save();

                DB::commit();

                // Envío de Correo electrónico
                if ($medical_request->email != null && $medical_request->email != '')
                {
                    AssignToMedicJob::dispatch($medical_request);
                }
                // en vio de mensaje whatsapp
                if($medical_request->whatsapp_flag)
                {
                    $whats = new WhatsApp();

                    $whats->wassengerSend($medical_request->phone_number, '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                        'Su solicitud médica con código de seguimiento : *' . $medical_request->code . '* \n\n' .
                        'Fue asignada al *Médico:* ' . $medical_request->medic->employee->nombres . ' ' . $medical_request->medic->employee->apellido_1 . ' ' . $medical_request->medic->employee->apellido_2);
                }

                return response()->json([
                    'type'         => 'correct',
                    'msj'           => 'Se asigno la solicitud al médico '.$user->full_name,
                    'filas'        => $filas,
                    'reload_table' => false
                ]);
            }catch (\Throwable $e){
                DB::rollBack();
                return response()->json([
                    'type' => 'error',
                    'msj' => 'Ocurrio un error'
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_patient($medical_request_id)
    {
        $medical_request = MedicalRequest::find($medical_request_id);

        $users_seguimiento = User::whereIn('role_id', [6,4,10,11])->orderBy('name')
            ->get();
        $users_medico = User::where('role_id', 3)->orderBy('name')
            ->get();

        $appointment_types = AppointmentType::get();
        $tracings = $medical_request->last_tracing;
        if($tracings!=null){
            $tracings = new Collection([$tracings]);
        }else{
            $tracings = new Collection();
        }

        $html = view('tracing_administrations.modals.content_modal')
                    ->with([
                        'medical_request'       => $medical_request,
                        'users_seguimiento'     => $users_seguimiento,
                        'users_medico'          => $users_medico,
                        'appointment_types'     => $appointment_types,
                        'tracings'              => $tracings,
                    ])
                    ->render();

        return response()->json([
            'content' => $html,
        ]);
    }

     function verifyStatePreSol(TracingAdministrationValidation $request){
        DB::beginTransaction();
        try {
            $filas = [];

            $medical_request = MedicalRequest::find($request->medical_request_id);
            $parameter = Parameter::find(1); //BUSCA EL PARAMETRO QUE TIENE EL TIEMPO

            $datos_empleado = view('medical_consultations.partials.datos_paciente')
                ->with([
                    'medical_request' => $medical_request
                ])->render();

            switch ($medical_request->medical_request_status_id){
                case 1: //SOLICITADO
                case 5: //VALIDADO
                    $historical_request = new HistoricalRequest();
                    $historical_request->user_id = Auth::id();
                    $historical_request->comment = 'PRE ASIGNACIÓN';
                    $historical_request->medical_request_id = $medical_request->id;
                    $historical_request->medical_request_status_id = 6; //ASIGNADO EL ESTADO DE 'PRE REVISIÓN'
                    $historical_request->save();

                    $medical_request->medical_request_status_id = 6; //ASIGNADO EL ESTADO DE 'PRE REVISIÓN'
                    $medical_request->update();

                    $filas[$medical_request->id] = view('tracing_administrations.partials.tr_content')
                        ->with([
                            'medical_request' => $medical_request
                        ])->render();
                    DB::commit();

                    return response()->json([
                        'type'                => 'correct_nuevo',
                        'msj'                 => 'Se cambio el estado del registro ('.$medical_request->code.')',
                        'tiempo'              => $parameter->value,
                        'fecha_hora_registro' => $historical_request->updated_at,
                        'updated_at'          => Carbon::now()->timestamp,
                        'filas'               => $filas,
                        'filas_actualizadas'  => $this->revisarPreRevisados(),
                        'medical_request'     => $medical_request,
                        'motivo'              => $medical_request->risk_type->name,
                        'datos_empleado'      => $datos_empleado,
                    ]);
                break;
                case 6: //PRE ASIGNADO
                    $historical_request = HistoricalRequest::where('medical_request_id','=',$medical_request->id)
                        ->latest()
                        ->first();

                    //VERIFICA SI EL TIEMPO YA SE PASO O NO
                    $diff = $historical_request->updated_at->diff(Carbon::now());
                    $minutes = $diff->days * 24 * 60;
                    $minutes += $diff->h * 60;
                    $minutes += $diff->i;

                    //VERIFICA SI YO FUI EL QUE PRE ASIGNO LA CITA
                    if($historical_request->user_id==Auth::id()){
                        if($minutes >= $parameter->value){
                            //YA PASO EL TIEMPO INDICADO, CAMBIA A PRE SOLICITADO PARA MI

                            $historical_request->updated_at = now();
                            $historical_request->update();
                            DB::commit();

                            return response()->json([
                                'type'                => 'correct_yo',
                                'msj'                 => 'Esta cita fue previamente abierta por mi, se actualizó el tiempo',
                                'tiempo'              => $parameter->value,
                                'fecha_hora_registro' => $historical_request->updated_at,
                                'filas'               => [],
                                'filas_actualizadas'  => $this->revisarPreRevisados(),
                                'datos_empleado'      => $datos_empleado,
                            ]);
                        }else{
                            DB::commit();

                            return response()->json([
                                'type'                => 'correct_yo',
                                'msj'                 => 'Esta cita fue previamente abierta por mi',
                                'tiempo'              => $parameter->value,
                                'fecha_hora_registro' => $historical_request->updated_at,
                                'filas'               => [],
                                'filas_actualizadas'  => $this->revisarPreRevisados(),
                                'medical_request'     => $medical_request,
                                'motivo'              => $medical_request->riskType->name,
                                'datos_empleado'      => $datos_empleado,
                            ]);
                        }
                    }else{

                        if($minutes >= $parameter->value){
                            //YA PASO EL TIEMPO INDICADO, CAMBIA A PRE SOLICITADO PARA MI
                            $historical_request = new HistoricalRequest();
                            $historical_request->user_id = Auth::id();
                            $historical_request->comment = 'PRE ASIGNACIÓN - POR INACTIVIDAD';
                            $historical_request->medical_request_id = $medical_request->id;
                            $historical_request->medical_request_status_id = 6; //ASIGNADO EL ESTADO DE 'PRE REVISIÓN'
                            $historical_request->save();

                            $medical_request->medical_request_status_id = 6; //ASIGNADO EL ESTADO DE 'PRE REVISIÓN'
                            $medical_request->update();

                            $filas[$medical_request->id] = view('tracing_administrations.partials.tr_content')
                                ->with([
                                    'medical_request' => $medical_request
                                ])->render();
                            DB::commit();

                            return response()->json([
                                'type'                => 'correct_renovar',
                                'msj'                 => 'Se cambio el estado del registro ('.$medical_request->code.')',
                                'tiempo'              => $parameter->value,
                                'fecha_hora_registro' => $historical_request->updated_at,
                                'updated_at'          => Carbon::now()->timestamp,
                                'filas'               => $filas,
                                'filas_actualizadas'  => $this->revisarPreRevisados(),
                                'medical_request'     => $medical_request,
                                'motivo'              => $medical_request->risk_type->name,
                                'datos_empleado'      => $datos_empleado,
                            ]);
                        }else{
                            return response()->json([
                                'type' => 'correct_otro',
                                'msj' => 'Esta cita fue abierta por otro usuario: '.$historical_request->user->full_name,
                                'tiempo' => $parameter->value,
                                'fecha_hora_registro' => $historical_request->updated_at,
                                'filas' => [],
                                'filas_actualizadas' => $this->revisarPreRevisados()
                            ]);
                        }

                    }
                    break;
            }

        }catch (\Throwable $e){
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj' => 'Ocurrio un error'
            ]);
        }
    }

    /**
     * @return array|\Illuminate\Http\JsonResponse = Devuelve un array del contenido td para ser agregado
     * @description = Metodo no ruteable que verifica los requisitos medicos y los actualiza
     */
    function revisarPreRevisados(){
        DB::beginTransaction();
        try {
            $filas = [];
            $medical_requests = MedicalRequest::where('medical_request_status_id','=',6) //PRE REVISADO
                ->get();
            $parameter = Parameter::find(1); //BUSCA EL PARAMETRO QUE TIENE EL TIEMPO

            foreach ($medical_requests as $medical_request){
                $historical_request = HistoricalRequest::where('medical_request_id','=',$medical_request->id)
                    ->latest()
                    ->first();

                //VERIFICA SI EL TIEMPO YA SE PASO O NO
                $diff = $historical_request->updated_at->diff(Carbon::now());
                $minutes = $diff->days * 24 * 60;
                $minutes += $diff->h * 60;
                $minutes += $diff->i;

                if($minutes >= $parameter->value){
                    //VERIFICA SI EL TIEMPO EXCEDIO EL PARAMETRO INDICADO

                    $medical_request->medical_requests_status()
                        ->attach(1, [ //ASIGNADO EL ESTADO DE 'SOLICITADO'
                            'user_id'=>null,
                            'comment'=>'ASIGNACIÓN DE SOLICITUD - AUTOMÁTICO'
                        ]);

                    $medical_request->medical_request_status_id = 1;
                    $medical_request->update();

                    $filas[$medical_request->id] = view('tracing_administrations.partials.tr_content')
                        ->with([
                            'medical_request' => $medical_request
                        ])->render();
                }
            }

            DB::commit();
            return $filas;
        }catch (\Throwable $e){
            DB::rollBack();
            return ['error'];
        }
    }

    public function assignToEmploye(TracingAdministrationValidation $request){
        DB::beginTransaction();
        try {
            $filas = [];
            $medical_request = MedicalRequest::find($request->medical_request_id);
            $employee = EmployeeRRHH::find($request->employee_id);

            if($medical_request->employee_id==null){
                $medical_request->employee_id = $request->employee_id;
                $medical_request->update();

                $filas[$medical_request->id] = view('tracing_administrations.partials.tr_content')
                    ->with([
                        'medical_request' => $medical_request
                    ])->render();
            }
            DB::commit();
            return response()->json([
                'type'  => 'correct',
                'msj'   => 'Se registraron las citas seleccionadas al empleado '.$employee->full_name,
                'filas' => $filas
            ]);
        }catch (\Throwable $e){
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj'  => 'Ocurrio un error'
            ]);
        }
    }

    function closeStatePreSol(TracingAdministrationValidation $request){
        DB::beginTransaction();
        try {
            $filas = [];

            $medical_request = MedicalRequest::find($request->medical_request_id);
            $medical_request_status = MedicalRequestStatus::find(1);

            $medical_request->medical_requests_status()
                ->attach($medical_request_status->id, [ //ASIGNADO EL ESTADO DE 'PENDIENTE'
                    'user_id'=>Auth::id(),
                    'comment'=>'ASIGNACIÓN DE SOLICITUD - AL CERRAR'
                ]);

            $medical_request->medical_request_status_id = 1; //ASIGNADO EL ESTADO DE 'SOLICITADO'
            $medical_request->update();

            $filas[$medical_request->id] = view('tracing_administrations.partials.tr_content')
                ->with([
                    'medical_request' => $medical_request
                ])->render();
            DB::commit();

            return response()->json([
                'type' => 'correct',
                'msj' => 'Su tiempo para atender el caso de la solicitud ('.$medical_request->code.') a expirado',
                'filas' => $filas,
                'filas_actualizadas' => $this->revisarPreRevisados()
            ]);
        }catch (\Throwable $e){
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj' => 'Ocurrio un error'
            ]);
        }
    }

    function closeChangeStateAuto(TracingAdministrationValidation $request){
        DB::beginTransaction();

        try {
            $filas = [];

            $medical_request = MedicalRequest::find($request->medical_request_id);
            $medical_request_status = MedicalRequestStatus::find(1);

            $medical_request->medical_requests_status()
                ->attach($medical_request_status->id, [ //ASIGNADO EL ESTADO DE 'PENDIENTE'
                    'user_id'=>Auth::id(),
                    'comment'=>'ASIGNACIÓN DE SOLICITUD - POR INACTIVIDAD'
                ]);

            $medical_request->medical_request_status_id = $medical_request_status->id; //ASIGNADO EL ESTADO DE 'SOLICITADO'
            $medical_request->update();

            $filas[$medical_request->id] = view('tracing_administrations.partials.tr_content')
                ->with([
                    'medical_request' => $medical_request
                ])->render();
            DB::commit();

            return response()->json([
                'type' => 'correct',
                'msj' => 'Se cambio el estado de la solicitud ('.$medical_request->code.') a '.$medical_request_status->name,
                'filas' => $filas
            ]);
        }catch (\Throwable $e){
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj' => 'Ocurrio un error'
            ]);
        }
    }

    function updateRegisters(){
        $filas = $this->revisarPreRevisados();
        if(count($filas) > 0){
            //ENCONTRO ACTUALIZACIONES
            return response()->json([
                'type' => 'correct_cambios',
                'msj' => 'Se cambio el estado de la solicitud ',
                'filas' => $filas
            ]);
        }else{
            //NO HAY NADA PARA ACTUALIZAR
            return response()->json([
                'type' => 'correct_sin_cambios',
                'msj' => 'Ningun cambio encontrado',
            ]);
        }
    }

    function employeeNotFound(TracingAdministrationValidation $request){
        DB::beginTransaction();

        try {
            $filas = [];

            $medical_request = MedicalRequest::find($request->medical_request_id);

            $medical_request->medical_requests_status()
                ->attach(3, [ //ASIGNADO EL ESTADO DE 'PENDIENTE'
                    'user_id'=>Auth::id(),
                    'comment'=>'ASIGNACIÓN DE SOLICITUD'
                ]);

            $medical_request->medical_request_status_id = 3; //ASIGNADO EL ESTADO DE 'PENDIENTE'
            $medical_request->update();

            $filas[$medical_request->id] = view('tracing_administrations.partials.tr_content')
                ->with([
                    'medical_request' => $medical_request
                ])->render();
            DB::commit();

            return response()->json([
                'type' => 'correct',
                'msj' => 'Se envio el caso a RRHH de la solicitud ('.$medical_request->code.') al médico: "'.Auth::user()->full_name.'"',
                'filas' => $filas,
                'filas_actualizadas' => $this->revisarPreRevisados()
            ]);
        }catch (\Throwable $e){
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj' => 'Ocurrio un error'
            ]);
        }
    }

    function assignToMe(TracingAdministrationValidation $request){
        DB::beginTransaction();

        try {
            $filas = [];

            $medical_request = MedicalRequest::find($request->medical_request_id);

            $medical_request->medical_requests_status()
                ->attach(2, [ //ASIGNADO EL ESTADO DE 'ASIGNADO A MÉDICO'
                    'user_id'=>Auth::id(),
                    'comment'=>'ASIGNACIÓN DE SOLICITUD'
                ]);

            $medical_request->user_id = Auth::id(); //ASIGNANDO MEDICO
            $medical_request->medical_request_status_id = 2; //ASIGNADO EL ESTADO DE 'ASIGNADO A MÉDICO'
            $medical_request->update();

            $filas[$medical_request->id] = view('tracing_administrations.partials.tr_content')
                ->with([
                    'medical_request' => $medical_request
                ])->render();
            DB::commit();

            return response()->json([
                'type' => 'correct',
                'msj' => 'Se asigno la solicitud ('.$medical_request->code.') al médico: "'.Auth::user()->full_name.'"',
                'filas' => $filas,
                'filas_actualizadas' => $this->revisarPreRevisados()
            ]);
        }catch (\Throwable $e){
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj' => 'Ocurrio un error'
            ]);
        }
    }

    public function assignToMedic(TracingAdministrationValidation $request){
        // dd('asi');
        DB::beginTransaction();
        try {
            $filas = [];

            $medical_request = MedicalRequest::find($request->medical_request_id);

            // $medical_request->medical_requests_status()
            //     ->attach(2, [ //ASIGNADO EL ESTADO DE 'ASIGNADO A MÉDICO'
            //         'user_id'=>Auth::id(),
            //         'comment'=>'ASIGNACIÓN DE SOLICITUD'
            //     ]);

            $medical_request->employee_id = $request->employee_id;
            $employee = EmployeeRRHH::find($request->employee_id);
            // $medical_request->user_id = Auth::id();
            // $medical_request->medical_request_status_id = 2;
            $medical_request->update();

            $filas[$medical_request->id] = view('tracing_administrations.partials.tr_content')
                ->with([
                    'medical_request' => $medical_request
                ])->render();

            // Envío de Correo electrónico
            // if ($medical_request->email != null && $medical_request->email != '')
            // {
            //     AssignToMedicJob::dispatch($medical_request);
            // }

            DB::commit();

            /*if($medical_request->whatsapp_flag)
            {
                $whats = new WhatsApp();

                $whats->wassengerSend($medical_request->phone_number, '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                    'Su solicitud médica con código de seguimiento : *' . $medical_request->code . '* \n\n' .
                    'Fue asignada al *Médico:* ' . $medical_request->medic->employee->nombres . ' ' . $medical_request->medic->employee->apellido_1 . ' ' . $medical_request->medic->employee->apellido_2);
            }*/

            return response()->json([
                'type' => 'correct',
                'msj' => 'Se asigno al paciente '.$employee->full_name.' de la solicitud ('.$medical_request->code.'): "',
                'filas' => $filas,
                'filas_actualizadas' => $this->revisarPreRevisados()
            ]);
        }catch (\Throwable $e){
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj' => 'Ocurrio un error'
            ]);
        }
    }

    public function indexMmedicalRequestClose(){
        $medical_requests = MedicalRequest::where('medical_request_status_id', 4)
            ->get();

        return view('tracing_administrations.index_cerradas')
            ->with([
                'medical_requests' => $medical_requests,
                'fecha_actual'     => Carbon::now()
            ]);
    }

    /**
     * @param TracingAdministrationValidation $request
     * @param $id = Medicalrequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function closeCase(TracingAdministrationValidation $request,$id){

        DB::beginTransaction();
        try {
            $medical_request = MedicalRequest::find($id);
            if($medical_request!=null){
                $medical_request->medical_request_status_id = 4; //CERRADO
                $medical_request->update();

                $comment = "CERRADO";
                if($request->comment!=null){
                    $comment = $request->comment;
                }

                $historical_request = new HistoricalRequest();
                $historical_request->user_id = Auth::id();
                $historical_request->comment = $comment;
                $historical_request->medical_request_id = $medical_request->id;
                $historical_request->medical_request_status_id = 4; //ASIGNADO EL ESTADO DE 'CERRADO'
                $historical_request->save();
                DB::commit();

                $filas[$medical_request->id] = view('tracing_administrations.partials.tr_content')
                    ->with([
                        'medical_request' => $medical_request
                    ])->render();

                return response()->json([
                   'type' => 'correct',
                   'msj' => 'Cierre realizado para el caso con código '.$medical_request->code,
                   'filas' => $filas
                ]);
            }else{
                DB::rollBack();

                return response()->json([
                    'type' => 'error',
                    'msj' => 'No se encontro el caso indicado'
                ]);
            }

        }catch (\Throwable $e){
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj' => 'Ocurrio un error'
            ]);
        }
    }

    public function index_rechazadas()
    {
        $medical_requests = MedicalRequest::withTrashed()->where('deleted_at', '!=', null)
            ->orderBy('created_at', 'ASC')
            ->get();

        return view('tracing_administrations.index_rechazadas')
            ->with([
                'medical_requests' => $medical_requests,
                'fecha_actual'     => Carbon::now(),
                'tab'              => 'pendiente',
            ]);
    }

    public function getMedicalRequestList(GetMedicalRequestValidation $request)
    {
        $medical_requests = MedicalRequest::whereIn('id', $request->check_historical)->get();
        $appointment_types = AppointmentType::get();
        $users_seguimiento = User::whereIn('role_id', [6,4])->orderBy('name')
            ->get();
        $users_medico = User::where('role_id', 3)->orderBy('name')
            ->get();
        // parametros necesarios
        $parametro_seguimiento = Parameter::where('name','INTERVALO_TIEMPO_SEGUIMIENTO')
            ->first();
        $parametro_cita = Parameter::where('name','INTERVALO_TIEMPO_CITA')
            ->first();
        $parametro_hora_entrada = Parameter::where('name','HORA_ENTRADA')
            ->first();
        $parametro_hora_salida = Parameter::where('name','HORA_SALIDA')
            ->first();

        $html = view('tracing_administrations.modals.content_modal_list')
                    ->with([
                        'medical_requests'       => $medical_requests,
                        'users_seguimiento'      => $users_seguimiento,
                        'users_medico'           => $users_medico,
                        'appointment_types'      => $appointment_types,
                        'parametro_seguimiento'  => $parametro_seguimiento,
                        'parametro_cita'         => $parametro_cita,
                        'parametro_hora_entrada' => $parametro_hora_entrada,
                        'parametro_hora_salida'  => $parametro_hora_salida,
                    ])
                    ->render();

        return response()->json([
            'content' => $html,
        ]);
    }

    public function get_ultimo_seguimiento($medical_request_id)
    {
        $medical_request = MedicalRequest::find($medical_request_id);

        $tracing = $medical_request->last_tracing;
        // dd($tracings);

        //DATOS PARA EL FORMULARIO
        $treatment_stages = TreatmentStage::get()
            ->pluck('name','id');
        $risk_types = RiskType::get()
            ->pluck('name','id');
        $enterprises = EnterpriseSOL::pluck('nombre','id')
            ->toArray();
        $enterprises = Arr::prepend($enterprises, '', '');

        //DATOS PARA EL MODAL DE EXAMENES COMPLEMENTARIOS
        $laboratories = Laboratory::get()
            ->pluck('name','id');
        $test_types = TestType::get()
            ->pluck('name','id');
        $medical_users = User::where('role_id', 3)
            ->orderBy('name')
            ->get();
        //DATOS PARA EL MODAL DE EXAMENES COMPLEMENTARIOS
        $laboratories = Laboratory::get()
            ->pluck('name','id');
        $test_types = TestType::get()
            ->pluck('name','id');

        $appointment_types = AppointmentType::orderBy('id', 'asc')->get();
        //Datos para los kits
        $kit_types = Kit::orderBy('id', 'asc')->get()->pluck('name','id');

        $parameter_age = Parameter::find(7)->value; //EDAD_INSEGURA
        $parameter_value = Parameter::find(8)->value; //VALOR_EDAD_INSEGURA

        $kit_tracing = kitTracing::where('tracing_id' , $tracing->id)
            ->first();

        // dd($tracing->id, $kit_tracings, $medical_request_id);
        $html = view('tracing_administrations.modals.content_modal_ultimo_seguimiento')
                    ->with([
                        'medical_request' => $medical_request,
                        'tracing'        => $tracing,
                        //DATOS PARA EL FORMULARIO
                        'treatment_stages'  => $treatment_stages,
                        'risk_types'        => $risk_types,
                        'enterprises'       => $enterprises,
                        'medical_users'     => $medical_users->pluck('name','id'),
                        'appointment_types' => $appointment_types,
                        //DATOS PARA EL MODAL DE EXAMENES COMPLEMENTARIOS
                        'laboratories'      => $laboratories,
                        'test_types'        => $test_types,
                        //kits
                        'kit_types'         => $kit_types,
                        //PARAMETROS
                        'parameter_age'     => $parameter_age,
                        'parameter_value'   => $parameter_value,
                        'kit_tracing'   => $kit_tracing,
                    ])
                    ->render();

        return response()->json([
            'content' => $html,
        ]);
    }

    public function storeSeguimiento(TracingRequestAdmin $request)
    {
        // dd($request->all(), 'paso validacion');
        DB::beginTransaction();
        // try {
            //ACTUALIZACION DE SEGUIMIENTO
            $medical_request = MedicalRequest::find($request->medical_request_id);

            $historical_request = new HistoricalRequest();
            $historical_request->user_id = Auth::id();
            $historical_request->comment = 'SEGUIMIENTO REALIZADO';
            $historical_request->medical_request_id = $medical_request->id;

            $medical_request->medical_request_status_id = 12; //ASIGNADO EL ESTADO DE 'EN SEGUIMIENTO'
            $historical_request->medical_request_status_id = 12; //ASIGNADO EL ESTADO DE 'EN SEGUIMIENTO'
            $medical_request->update();

            $historical_request->save();

            $project = null;
            if($medical_request->employee_id!=null){
                $employee = EmployeeRRHH::find($medical_request->employee_id);
                // $project = $employee->sol_proyecto_id;
                $project = $employee->proyecto_id;
            }

            $request->merge([
                'tracing_date' => Carbon::now(),
                'medical_request_id' => $request->medical_request_id,
                'user_id' => Auth::id(),
                'historical_request_id' => $historical_request->id,
                'project_id' => $project
            ]);
            $tracing = new Tracing($request->all());
            $tracing->comment = $request->comment_tracing;
            $tracing->risk_type_id = $request->risk_type_id;
            $tracing->risk_state_id = $request->risk_state_id;
            $tracing->save();

            //************************************INICIO:CALCULO DE LA PONDERACION
            $sum = 0;

            //INICIO:PREGUNTA LA EDAD
            $parameter_age = Parameter::find(7)->value; //EDAD_INSEGURA
            $edad = 0;
            if($medical_request->employee!=null){
                $edad = $medical_request->employee->edad;
            }else{
                $edad = $medical_request->edad;
            }

            if($edad >= (int) $parameter_age){
                $sum++;
            }
            //FIN:PREGUNTA LA EDAD

            //INICIO:PREGUNTA ESTADO DE RIESGO
            $risk_state = RiskState::find($request->risk_state_id);
            if($risk_state->weighting > 0){
                $sum = $sum + $risk_state->weighting;
            }
            //FIN:PREGUNTA ESTADO DE RIESGO

            //INICIO:PREGUNTA CAMPOS MARCADOS
            if($request->disease_criteria!=null){
                foreach ($request->disease_criteria as $disease_criteria){
                    $tracing->disease_criterias()->attach($disease_criteria);
                    $disease = DiseaseCriteria::find($disease_criteria);
                    $sum += $disease->weighing;
                }
            }
            //FIN:PREGUNTA CAMPOS MARCADOS

            //GUARDANDO LA ADVERTENCIA Y EL RANGO
            $weighting_level = WeightingLevel::where('minimum','<=',$sum)
                ->where('maximum','>=',$sum)
                ->where('last_level_flag','=',false)
                ->first();

            $historical_weigthing_level = new HistoricalWeightingLevel;
            $historical_weigthing_level->calc_evaluation = $sum;

            if($weighting_level==null){
                //VERIFICA SI PERTENCE AL MAXIMO
                $weighting_level_max = WeightingLevel::where('minimum','<=',$sum)
                    ->where('last_level_flag','=',true)
                    ->first();
                if($weighting_level_max!=null){
                    $historical_weigthing_level->name = $weighting_level_max->name;
                    $historical_weigthing_level->description = $weighting_level_max->description;
                    $historical_weigthing_level->color = $weighting_level_max->color;
                    $historical_weigthing_level->minimum = $weighting_level_max->minimum;
                    $historical_weigthing_level->maximum = $weighting_level_max->maximum;
                    $historical_weigthing_level->evaluation = $weighting_level_max->evaluation;

                    $historical_weigthing_level->weighting_level_id = $weighting_level_max->id;
                    $historical_weigthing_level->tracing_id = $tracing->id;
                }
            }else{
                $historical_weigthing_level->name = $weighting_level->name;
                $historical_weigthing_level->description = $weighting_level->description;
                $historical_weigthing_level->color = $weighting_level->color;
                $historical_weigthing_level->minimum = $weighting_level->minimum;
                $historical_weigthing_level->maximum = $weighting_level->maximum;
                $historical_weigthing_level->evaluation = $weighting_level->evaluation;

                $historical_weigthing_level->weighting_level_id = $weighting_level->id;
                $historical_weigthing_level->tracing_id = $tracing->id;
            }
            $historical_weigthing_level->save();
            //************************************FIN:CALCULO DE LA PONDERACION

            //GUARDANDO EXAMENES COMPLEMENTARIOS
            $files = $request->file('files');
            // dd($files);
            if($request->laboratories!=null){
                for ($i=0 ; $i<count($request->laboratories) ;$i++){
                    $complementary_exam = new ComplementaryExam;
                    $complementary_exam->tracing_id = $tracing->id;

                    $complementary_exam->laboratory_id = $request->laboratories[$i];
                    $complementary_exam->test_type_id = $request->test_types[$i];
                    $complementary_exam->cost = $request->costs[$i];
                    $complementary_exam->bill_number = $request->bill_numbers[$i];
                    $complementary_exam->enterprise_flag = ($request->enterprise_pay_flags[$i]=="true")?true:false;
                    $complementary_exam->result_flag = ($request->result_flags[$i]=="true")?true:false;
                    $complementary_exam->save();

                    if ($files != null)
                    {
                        if(array_key_exists($i,$files)){
                            $path = $files[$i]->store('complementary_exams');

                            $complementary_exam->files()->create([
                                'name' => $files[$i]->getClientOriginalName(),
                                'file_type_id' => 1,
                                'location' => $path,
                                'mime' => $files[$i]->getClientMimeType()
                            ]);
                        }
                    }
                }

            }
            //GUARDANDO KITS
            if($request->kit!=null && $request->kit==1){
                $kit_tracing = new kitTracing();
                $kit_tracing->quantity = $request->quantity;
                $kit_tracing->kit_id = $request->kit_id;
                $kit_tracing->tracing_id = $tracing->id;
                $kit_tracing->save();
            }

            DB::commit();

            $filas[$medical_request->id] = view('tracing_administrations.partials.tr_tracing_content')
                                ->with([
                                    'medical_request' => $medical_request
                                ])->render();

            return response()->json([
                'type' => 'correct',
                'msj' => 'Se registro correctamente',
                'filas' => $filas,
            ]);
        // }catch(\Exception $ex){

        //     DB::rollBack();
        //     return response()->json([
        //         'type' => 'error',
        //     ]);
        // }

    }
}
