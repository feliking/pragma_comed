<?php

namespace App\Http\Controllers;

use App\ProjectEnterprise;
use App\ProjectSOL;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ProjectSOLController extends Controller
{
    public function getProjectFromEnterprise($enterprise_id){
        $projects = ProjectSOL::where('empresa_id','=',$enterprise_id)
            ->get()
            ->pluck('full_name','id');

        return response()->json([
            'projects' => $projects
        ]);
    }

    public function getProjectsFromEnterpriseSearch($enterprise_id){
        $projects_ids = ProjectEnterprise::where('empresa_id','=',$enterprise_id)
            ->pluck('proyecto_id')
            ->toArray();

        $projects = ProjectSOL::whereIn('id',$projects_ids)
            ->pluck('nombre','id')
            ->toArray();
        $projects = Arr::prepend($projects, 'TODOS', 0);

        return response()->json([
            'projects' => $projects
        ]);
    }
}
