<?php

namespace App\Http\Controllers;

use App\Http\Requests\MedicalRequestTypeR;
use App\MedicalRequestPriority;
use App\MedicalRequestType;
use Illuminate\Http\Request;

class MedicalRequestTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $medical_request_types = MedicalRequestType::withTrashed()->get();
        
        return view('medical_request_type.index')
            ->with([
                'medical_request_types' => $medical_request_types
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $medical_request_priorities = MedicalRequestPriority::get();
        return view('medical_request_type.create')
                ->with([
                    'medical_request_priorities' => $medical_request_priorities->pluck('name','id'),
                ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MedicalRequestTypeR $request)
    {
        //dd($request->all());
        try{
            
            $medical_request_type = new MedicalRequestType($request->all());

            $medical_request_type->save();

            if(!$request->ajax()){
                flash('Se registró el tipo: '.$medical_request_type->name)
                    ->success()
                    ->important();
            }
        }catch(\Exception $ex){

            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        if($request->ajax()){
            return response()->json([
                'id' => $medical_request_type->id,
                'medical_request_type' => $medical_request_type,
            ]);
        }else{
            return redirect()
                ->route('tipo.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $medical_request_type = MedicalRequestType::withTrashed()->find($id);

        if($medical_request_type != null){
            return view('medical_request_type.show')
                ->with([
                    'medical_request_type' => $medical_request_type,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('tipo.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $medical_request_type = MedicalRequestType::withTrashed()->find($id);
        //dd($medical_request_type);
        $medical_request_priorities = MedicalRequestPriority::get();
        if($medical_request_type != null){
            return view('medical_request_type.edit')
                ->with([
                    'medical_request_type' => $medical_request_type,
                    'medical_request_priorities' => $medical_request_priorities->pluck('name','id'),
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('tipo.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MedicalRequestTypeR $request, $id)
    {
        try{
            $medical_request_type = MedicalRequestType::withTrashed()->find($id);

            if($medical_request_type != null){
                $medical_request_type->fill($request->all());

                $medical_request_type->update();

                flash('Se modificó el tipo: '.$medical_request_type->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        return redirect()
            ->route('tipo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $medical_request_type = MedicalRequestType::withTrashed()->find($id);
            if($medical_request_type != null){
                if($medical_request_type->deleted_at == null){
                    $medical_request_type->delete();
                }else{
                    $medical_request_type->restore();
                }
                
                flash('Se modificó el estado del tipo: '.$medical_request_type->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }
        return redirect()->route('tipo.index');
    }
}
