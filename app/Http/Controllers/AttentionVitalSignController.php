<?php

namespace App\Http\Controllers;

use App\AttentionVitalSign;
use Illuminate\Http\Request;

class AttentionVitalSignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AttentionVitalSign  $attentionVitalSign
     * @return \Illuminate\Http\Response
     */
    public function show(AttentionVitalSign $attentionVitalSign)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AttentionVitalSign  $attentionVitalSign
     * @return \Illuminate\Http\Response
     */
    public function edit(AttentionVitalSign $attentionVitalSign)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AttentionVitalSign  $attentionVitalSign
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AttentionVitalSign $attentionVitalSign)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AttentionVitalSign  $attentionVitalSign
     * @return \Illuminate\Http\Response
     */
    public function destroy(AttentionVitalSign $attentionVitalSign)
    {
        //
    }
}
