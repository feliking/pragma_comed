<?php

namespace App\Http\Controllers;

use App\EmployeeRRHH;
use App\EnterpriseSOL;
use App\HistoricalRequest;
use App\Http\Requests\MedicalRequestAdministrationValidation;
use App\MedicalRequest;
use App\MedicalRequestStatus;
use App\Parameter;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

// Whatsapp
use App\GeneralClass\WhatsApp;
use App\Imports\MedicalRequestImport;
// Jobs
use App\Jobs\AssignToMedicJob;

use DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class MedicalRequestAdministrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $medical_requests = MedicalRequest::where(function ($query){
            $query->orWhere('medical_request_status_id','=',1) //SOLICITADO
                    ->orWhere('medical_request_status_id','=',5) //VALIDADO RRHH
                    ->orWhere('medical_request_status_id','=',6); //PRE REVISADO
        })
            ->get();
        $enterprises = EnterpriseSOL::get();
        //dd(count($medical_requests));
        return view('medical_request_administration.index')
            ->with([
                'medical_requests' => $medical_requests,
                'enterprises' => $enterprises,
//                'medics' => $medics
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function show($id)
    {
        $historical_requests = HistoricalRequest::where('medical_request_id','=',$id)
            ->get();

        $view = view('layouts.components.modals.partials.table_modal_history')
            ->with([
                'historical_requests' => $historical_requests
            ])->render();

        return response()->json([
            'type'=>'correct',
            'view' => $view
        ]);
    }

    public function showHistory($id){
        $historical_requests = HistoricalRequest::where('medical_request_id','=',$id)
            ->orderBy('id', 'DESC')
            ->get();

        $medical_request = MedicalRequest::find($id);

        $view = view('layouts.components.modals.partials.table_modal_history')
            ->with([
                'historical_requests' => $historical_requests,
                'medical_request' => $medical_request
            ])->render();

        return response()->json([
            'type'=>'correct',
            'view' => $view
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function assignToEmploye(MedicalRequestAdministrationValidation $request){
        DB::beginTransaction();
        try {
            $filas = [];
            $medical_request = MedicalRequest::find($request->medical_request_id);
            $employee = EmployeeRRHH::find($request->employee_id);

            if($medical_request->employee_id==null){
                $medical_request->employee_id = $request->employee_id;
                $medical_request->update();

                $filas[$medical_request->id] = view('medical_request_administration.partials.tr_content')
                    ->with([
                        'medical_request' => $medical_request
                    ])->render();
            }
            DB::commit();
            return response()->json([
                'type' => 'correct',
                'msj' => 'Se registraron las citas seleccionadas al empleado '.$employee->full_name,
                'filas' => $filas
            ]);
        }catch (\Throwable $e){
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj' => 'Ocurrio un error'
            ]);
        }
    }

    public function assignToMedic(MedicalRequestAdministrationValidation $request){
        DB::beginTransaction();

        try {
            $filas = [];

            $medical_request = MedicalRequest::find($request->medical_request_id);

            $medical_request->medical_requests_status()
                ->attach(2, [ //ASIGNADO EL ESTADO DE 'ASIGNADO A MÉDICO'
                    'user_id'=>Auth::id(),
                    'comment'=>'ASIGNACIÓN DE SOLICITUD'
                ]);

            $medical_request->employee_id = $request->employee_id; //ASIGNANDO MEDICO
            $medical_request->user_id = Auth::id(); //ASIGNANDO MEDICO
            $medical_request->medical_request_status_id = 2; //ASIGNADO EL ESTADO DE 'ASIGNADO A MÉDICO'
            $medical_request->update();

            $filas[$medical_request->id] = view('medical_request_administration.partials.tr_content')
                ->with([
                    'medical_request' => $medical_request
                ])->render();

            // Envío de Correo electrónico
            if ($medical_request->email != null && $medical_request->email != '')
            {
                AssignToMedicJob::dispatch($medical_request);
            }

            DB::commit();

            if($medical_request->whatsapp_flag)
            {
                $whats = new WhatsApp();

                $whats->wassengerSend($medical_request->phone_number, '*SISTEMA DE CONSULTAS MÉDICAS* \n\n' .
                    'Su solicitud médica con código de seguimiento : *' . $medical_request->code . '* \n\n' .
                    'Fue asignada al *Médico:* ' . $medical_request->medic->employee->nombres . ' ' . $medical_request->medic->employee->apellido_1 . ' ' . $medical_request->medic->employee->apellido_2);
            }

            return response()->json([
                'type' => 'correct',
                'msj' => 'Se asigno la solicitud ('.$medical_request->code.') al médico: "'.Auth::user()->full_name.'"',
                'filas' => $filas,
                'filas_actualizadas' => $this->revisarPreRevisados()
            ]);
        }catch (\Throwable $e){
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj' => 'Ocurrio un error'
            ]);
        }
    }

    function assignToMe(MedicalRequestAdministrationValidation $request){
        DB::beginTransaction();

        try {
            $filas = [];

            $medical_request = MedicalRequest::find($request->medical_request_id);

            $medical_request->medical_requests_status()
                ->attach(2, [ //ASIGNADO EL ESTADO DE 'ASIGNADO A MÉDICO'
                    'user_id'=>Auth::id(),
                    'comment'=>'ASIGNACIÓN DE SOLICITUD'
                ]);

            $medical_request->user_id = Auth::id(); //ASIGNANDO MEDICO
            $medical_request->medical_request_status_id = 2; //ASIGNADO EL ESTADO DE 'ASIGNADO A MÉDICO'
            $medical_request->update();

            $filas[$medical_request->id] = view('medical_request_administration.partials.tr_content')
                ->with([
                    'medical_request' => $medical_request
                ])->render();
            DB::commit();

            return response()->json([
                'type' => 'correct',
                'msj' => 'Se asigno la solicitud ('.$medical_request->code.') al médico: "'.Auth::user()->full_name.'"',
                'filas' => $filas,
                'filas_actualizadas' => $this->revisarPreRevisados()
            ]);
        }catch (\Throwable $e){
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj' => 'Ocurrio un error'
            ]);
        }
    }

    function employeeNotFound(MedicalRequestAdministrationValidation $request){
        DB::beginTransaction();

        try {
            $filas = [];

            $medical_request = MedicalRequest::find($request->medical_request_id);

            $medical_request->medical_requests_status()
                ->attach(3, [ //ASIGNADO EL ESTADO DE 'PENDIENTE'
                    'user_id'=>Auth::id(),
                    'comment'=>'ASIGNACIÓN DE SOLICITUD'
                ]);

            $medical_request->medical_request_status_id = 3; //ASIGNADO EL ESTADO DE 'PENDIENTE'
            $medical_request->update();

            $filas[$medical_request->id] = view('medical_request_administration.partials.tr_content')
                ->with([
                    'medical_request' => $medical_request
                ])->render();
            DB::commit();

            return response()->json([
                'type' => 'correct',
                'msj' => 'Se envio el caso a RRHH de la solicitud ('.$medical_request->code.') al médico: "'.Auth::user()->full_name.'"',
                'filas' => $filas,
                'filas_actualizadas' => $this->revisarPreRevisados()
            ]);
        }catch (\Throwable $e){
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj' => 'Ocurrio un error'
            ]);
        }
    }

    function verifyStatePreSol(MedicalRequestAdministrationValidation $request){
        DB::beginTransaction();

        try {
            $filas = [];

            $medical_request = MedicalRequest::find($request->medical_request_id);
            $parameter = Parameter::find(1); //BUSCA EL PARAMETRO QUE TIENE EL TIEMPO

            $datos_empleado = view('medical_consultations.partials.datos_paciente')
                ->with([
                    'medical_request' => $medical_request
                ])->render();

            switch ($medical_request->medical_request_status_id){
                case 1: //SOLICITADO
                case 5: //VALIDADO
                    $historical_request = new HistoricalRequest();
                    $historical_request->user_id = Auth::id();
                    $historical_request->comment = 'PRE ASIGNACIÓN';
                    $historical_request->medical_request_id = $medical_request->id;
                    $historical_request->medical_request_status_id = 6; //ASIGNADO EL ESTADO DE 'PRE REVISIÓN'
                    $historical_request->save();

                    $medical_request->medical_request_status_id = 6; //ASIGNADO EL ESTADO DE 'PRE REVISIÓN'
                    $medical_request->update();

                    $filas[$medical_request->id] = view('medical_request_administration.partials.tr_content')
                        ->with([
                            'medical_request' => $medical_request
                        ])->render();
                    DB::commit();

                    return response()->json([
                        'type'                => 'correct_nuevo',
                        'msj'                 => 'Se cambio el estado del registro ('.$medical_request->code.')',
                        'tiempo'              => $parameter->value,
                        'fecha_hora_registro' => $historical_request->updated_at,
                        'updated_at'          => Carbon::now()->timestamp,
                        'filas'               => $filas,
                        'filas_actualizadas'  => $this->revisarPreRevisados(),
                        'medical_request'     => $medical_request,
                        'motivo'              => $medical_request->medical_requests_type->name,
                        'datos_empleado'      => $datos_empleado,
                    ]);
                break;
                case 6: //PRE ASIGNADO
                    $historical_request = HistoricalRequest::where('medical_request_id','=',$medical_request->id)
                        ->latest()
                        ->first();

                    //VERIFICA SI EL TIEMPO YA SE PASO O NO
                    $diff = $historical_request->updated_at->diff(Carbon::now());
                    $minutes = $diff->days * 24 * 60;
                    $minutes += $diff->h * 60;
                    $minutes += $diff->i;

                    //VERIFICA SI YO FUI EL QUE PRE ASIGNO LA CITA
                    if($historical_request->user_id==Auth::id()){
                        if($minutes >= $parameter->value){
                            //YA PASO EL TIEMPO INDICADO, CAMBIA A PRE SOLICITADO PARA MI

                            $historical_request->updated_at = now();
                            $historical_request->update();
                            DB::commit();

                            return response()->json([
                                'type'                => 'correct_yo',
                                'msj'                 => 'Esta cita fue previamente abierta por mi, se actualizó el tiempo',
                                'tiempo'              => $parameter->value,
                                'fecha_hora_registro' => $historical_request->updated_at,
                                'filas'               => [],
                                'filas_actualizadas'  => $this->revisarPreRevisados(),
                                'datos_empleado'      => $datos_empleado,
                            ]);
                        }else{
                            DB::commit();

                            return response()->json([
                                'type'                => 'correct_yo',
                                'msj'                 => 'Esta cita fue previamente abierta por mi',
                                'tiempo'              => $parameter->value,
                                'fecha_hora_registro' => $historical_request->updated_at,
                                'filas'               => [],
                                'filas_actualizadas'  => $this->revisarPreRevisados(),
                                'medical_request'     => $medical_request,
                                'motivo'              => $medical_request->medical_requests_type->name,
                                'datos_empleado'      => $datos_empleado,
                            ]);
                        }
                    }else{

                        if($minutes >= $parameter->value){
                            //YA PASO EL TIEMPO INDICADO, CAMBIA A PRE SOLICITADO PARA MI
                            $historical_request = new HistoricalRequest();
                            $historical_request->user_id = Auth::id();
                            $historical_request->comment = 'PRE ASIGNACIÓN - POR INACTIVIDAD';
                            $historical_request->medical_request_id = $medical_request->id;
                            $historical_request->medical_request_status_id = 6; //ASIGNADO EL ESTADO DE 'PRE REVISIÓN'
                            $historical_request->save();

                            $medical_request->medical_request_status_id = 6; //ASIGNADO EL ESTADO DE 'PRE REVISIÓN'
                            $medical_request->update();

                            $filas[$medical_request->id] = view('medical_request_administration.partials.tr_content')
                                ->with([
                                    'medical_request' => $medical_request
                                ])->render();
                            DB::commit();

                            return response()->json([
                                'type'                => 'correct_renovar',
                                'msj'                 => 'Se cambio el estado del registro ('.$medical_request->code.')',
                                'tiempo'              => $parameter->value,
                                'fecha_hora_registro' => $historical_request->updated_at,
                                'updated_at'          => Carbon::now()->timestamp,
                                'filas'               => $filas,
                                'filas_actualizadas'  => $this->revisarPreRevisados(),
                                'medical_request'     => $medical_request,
                                'motivo'              => $medical_request->medical_requests_type->name,
                                'datos_empleado'      => $datos_empleado,
                            ]);
                        }else{
                            return response()->json([
                                'type' => 'correct_otro',
                                'msj' => 'Esta cita fue abierta por otro usuario: '.$historical_request->user->full_name,
                                'tiempo' => $parameter->value,
                                'fecha_hora_registro' => $historical_request->updated_at,
                                'filas' => [],
                                'filas_actualizadas' => $this->revisarPreRevisados()
                            ]);
                        }

                    }
                    break;
            }

        }catch (\Throwable $e){
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj' => 'Ocurrio un error'
            ]);
        }
    }

    function closeStatePreSol(MedicalRequestAdministrationValidation $request){
        DB::beginTransaction();

        try {
            $filas = [];

            $medical_request = MedicalRequest::find($request->medical_request_id);
            $medical_request_status = MedicalRequestStatus::find(1);

            $medical_request->medical_requests_status()
                ->attach($medical_request_status->id, [ //ASIGNADO EL ESTADO DE 'PENDIENTE'
                    'user_id'=>Auth::id(),
                    'comment'=>'ASIGNACIÓN DE SOLICITUD - AL CERRAR'
                ]);

            $medical_request->medical_request_status_id = 1; //ASIGNADO EL ESTADO DE 'SOLICITADO'
            $medical_request->update();

            $filas[$medical_request->id] = view('medical_request_administration.partials.tr_content')
                ->with([
                    'medical_request' => $medical_request
                ])->render();
            DB::commit();

            return response()->json([
                'type' => 'correct',
                'msj' => 'Su tiempo para atender el caso de la solicitud ('.$medical_request->code.') a expirado',
                'filas' => $filas,
                'filas_actualizadas' => $this->revisarPreRevisados()
            ]);
        }catch (\Throwable $e){
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj' => 'Ocurrio un error'
            ]);
        }
    }

    function closeChangeStateAuto(MedicalRequestAdministrationValidation $request){
        DB::beginTransaction();

        try {
            $filas = [];

            $medical_request = MedicalRequest::find($request->medical_request_id);
            $medical_request_status = MedicalRequestStatus::find(1);

            $medical_request->medical_requests_status()
                ->attach($medical_request_status->id, [ //ASIGNADO EL ESTADO DE 'PENDIENTE'
                    'user_id'=>Auth::id(),
                    'comment'=>'ASIGNACIÓN DE SOLICITUD - POR INACTIVIDAD'
                ]);

            $medical_request->medical_request_status_id = $medical_request_status->id; //ASIGNADO EL ESTADO DE 'SOLICITADO'
            $medical_request->update();

            $filas[$medical_request->id] = view('medical_request_administration.partials.tr_content')
                ->with([
                    'medical_request' => $medical_request
                ])->render();
            DB::commit();

            return response()->json([
                'type' => 'correct',
                'msj' => 'Se cambio el estado de la solicitud ('.$medical_request->code.') a '.$medical_request_status->name,
                'filas' => $filas
            ]);
        }catch (\Throwable $e){
            DB::rollBack();
            return response()->json([
                'type' => 'error',
                'msj' => 'Ocurrio un error'
            ]);
        }
    }

    function updateRegisters(){
        $filas = $this->revisarPreRevisados();
        if(count($filas) > 0){
            //ENCONTRO ACTUALIZACIONES
            return response()->json([
                'type' => 'correct_cambios',
                'msj' => 'Se cambio el estado de la solicitud ',
                'filas' => $filas
            ]);
        }else{
            //NO HAY NADA PARA ACTUALIZAR
            return response()->json([
                'type' => 'correct_sin_cambios',
                'msj' => 'Ningun cambio encontrado',
            ]);
        }
    }

    /**
     * @return array|\Illuminate\Http\JsonResponse = Devuelve un array del contenido td para ser agregado
     * @description = Metodo no ruteable que verifica los requisitos medicos y los actualiza
     */
    function revisarPreRevisados(){
        DB::beginTransaction();

        try {
            $filas = [];
            $medical_requests = MedicalRequest::where('medical_request_status_id','=',6) //PRE REVISADO
                ->get();
            $parameter = Parameter::find(1); //BUSCA EL PARAMETRO QUE TIENE EL TIEMPO

            foreach ($medical_requests as $medical_request){
                $historical_request = HistoricalRequest::where('medical_request_id','=',$medical_request->id)
                    ->latest()
                    ->first();

                //VERIFICA SI EL TIEMPO YA SE PASO O NO
                $diff = $historical_request->updated_at->diff(Carbon::now());
                $minutes = $diff->days * 24 * 60;
                $minutes += $diff->h * 60;
                $minutes += $diff->i;

                if($minutes >= $parameter->value){
                    //VERIFICA SI EL TIEMPO EXCEDIO EL PARAMETRO INDICADO

                    $medical_request->medical_requests_status()
                        ->attach(1, [ //ASIGNADO EL ESTADO DE 'SOLICITADO'
                            'user_id'=>null,
                            'comment'=>'ASIGNACIÓN DE SOLICITUD - AUTOMÁTICO'
                        ]);

                    $medical_request->medical_request_status_id = 1;
                    $medical_request->update();

                    $filas[$medical_request->id] = view('medical_request_administration.partials.tr_content')
                        ->with([
                            'medical_request' => $medical_request
                        ])->render();
                }
            }

            DB::commit();
            return $filas;
        }catch (\Throwable $e){
            DB::rollBack();
            return ['error'];
        }
    }
    public function importarExcel(Request $request)
    {
        //dd($request->all(), $request->file);
        $path = storage_path('tmp/uploads');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file = $request->file('file');
        //dd($file);
        $name = uniqid() . '_' . trim($file->getClientOriginalName());
        $file->move($path, $name);
        //dd($file->getClientOriginalName()); 
        //$file->move($path, trim($file->getClientOriginalName()));
        Excel::import(new MedicalRequestImport($request->file), storage_path('tmp/uploads/'.$name));
        if (file_exists(storage_path('tmp/uploads/'.$name))) {
            unlink(storage_path('tmp/uploads/'.$name));
        }
        $medical_requests = MedicalRequest::where(function ($query){
            $query->orWhere('medical_request_status_id','=',1) //SOLICITADO
                    ->orWhere('medical_request_status_id','=',5) //VALIDADO RRHH
                    ->orWhere('medical_request_status_id','=',6); //PRE REVISADO
        })
            ->get();
        $enterprises = EnterpriseSOL::get();
        //dd(count($medical_requests));
        return view('medical_request_administration.index')
            ->with([
                'medical_requests' => $medical_requests,
                'enterprises' => $enterprises,
                //  'medics' => $medics
            ]);
    }
    /**
     * Funcion que sube el documento en un directorio temporal
     * @param  Request $request [description]
     * @return json (name, original name)
     */
    public function fileTmp(Request $request)
    {
        $path = storage_path('tmp/uploads');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file = $request->file('file');

        $name = uniqid() . '_' . trim($file->getClientOriginalName());

        $file->move($path, $name);

        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }

    /**
     * Funcion que eliminara el archivo subido temporalmete al servidor
     * @param  Request $request [description]
     * @return string           [description]
     */
    public function fileRemove(Request $request)
    {
        $filename =  $request->file;
        $path = storage_path('tmp/uploads/'.$filename);
        if (file_exists($path)) {
            unlink($path);
        }
        return $filename;
    }
}
