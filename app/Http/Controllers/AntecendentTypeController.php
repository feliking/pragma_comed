<?php

namespace App\Http\Controllers;

use App\AntecendentType;
use Illuminate\Http\Request;

class AntecendentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AntecendentType  $antecendentType
     * @return \Illuminate\Http\Response
     */
    public function show(AntecendentType $antecendentType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AntecendentType  $antecendentType
     * @return \Illuminate\Http\Response
     */
    public function edit(AntecendentType $antecendentType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AntecendentType  $antecendentType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AntecendentType $antecendentType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AntecendentType  $antecendentType
     * @return \Illuminate\Http\Response
     */
    public function destroy(AntecendentType $antecendentType)
    {
        //
    }
}
