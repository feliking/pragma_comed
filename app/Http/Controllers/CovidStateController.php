<?php

namespace App\Http\Controllers;

use App\CovidState;
use App\Http\Requests\CovidStateRequest;
use Illuminate\Http\Request;

class CovidStateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $cvd_states = CovidState::withTrashed()->get();
        return view('covid_state.index')
            ->with([
                'cvd_states' => $cvd_states
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('covid_state.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CovidStateRequest $request)
    {
        try{

            $cvd_state = new CovidState($request->all());

            $cvd_state->save();

            if(!$request->ajax()){
                flash('Se registró el estado: '.$cvd_state->name)
                    ->success()
                    ->important();
            }
        }catch(\Exception $ex){

            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        if($request->ajax()){
            return response()->json([
                'id' => $cvd_state->id,
                'cvd_state' => $cvd_state,
            ]);
        }else{
            return redirect()
                ->route('covid_estados.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cvd_state = CovidState::withTrashed()->find($id);

        if($cvd_state != null){
            return view('covid_state.show')
                ->with([
                    'cvd_state' => $cvd_state,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('covid_estados.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cvd_state = CovidState::withTrashed()->find($id);

        if($cvd_state != null){
            return view('covid_state.edit')
                ->with([
                    'cvd_state' => $cvd_state,
                ]);
        }else{
            flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                ->error()
                ->important();

            return redirect()
                ->route('covid_estados.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CovidStateRequest $request, $id)
    {
        try{
            $cvd_state = CovidState::withTrashed()->find($id);

            if($cvd_state != null){
                $cvd_state->fill($request->all());

                $cvd_state->update();

                flash('Se modificó el estado: '.$cvd_state->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }

        return redirect()
            ->route('covid_estados.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $cvd_state = CovidState::withTrashed()->find($id);
            if($cvd_state != null){
                if($cvd_state->deleted_at == null){
                    $cvd_state->delete();
                }else{
                    $cvd_state->restore();
                }

                flash('Se modificó el estado: '.$cvd_state->name)
                    ->warning()
                    ->important();
            }else{
                flash('No se ha encontrado el registro en la base de datos. Intenta nuevamente, si el problema persiste comuníquese con el Departamento de TIC\'s.')
                    ->error()
                    ->important();
            }
        }catch(\Exception $ex){
            flash('No se completó la operación solicitada, el error será enviado al Departamento TIC\'s para su revisión. Gracias por su comprensión, en unos momentos será notificado y podrá continuar utilizando el sistema.')
                ->error()
                ->important();
        }
        return redirect()->route('covid_estados.index');
    }
}
