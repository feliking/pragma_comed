<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\MedicalRequest;

use DB;

class RegularizationController extends Controller
{
    public function regularizationProjectsMedicalRequests (Request $request)
    {
        $medical_requests = MedicalRequest::where([
            // ['project_id', '=', null],
            ['employee_id', '!=', null]
        ])
            ->get();

        DB::beginTransaction();
        try
        {
            foreach ($medical_requests as $medical_request) {
                $employee = $medical_request->employee;
    
                if ($employee != null)
                {
                    $medical_request->project_id = $employee->proyecto_id;
                    $medical_request->update();

                    $tracings = $medical_request->tracings;

                    foreach ($tracings as $tracing) {
                        $tracing->project_id = $employee->proyecto_id;
                        $tracing->update();
                    }
                }
            }

            DB::commit();
        }
        catch(\Exception $ex)
        {
            DB::rollBack();
        }
    }
}
