<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckEditMedicalRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->role_id==1 ||Auth::user()->role_id==4 || Auth::user()->role_id==7 || Auth::user()->role_id==10) {
            return $next($request);
        }else{
            flash('No cuenta con los suficientes privilegios para acceder a la página solicitada')
                ->error()
                ->important();

            return redirect()->back();
        }
    }
}
