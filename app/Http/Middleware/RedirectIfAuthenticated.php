<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // dd('middle');

        if (Auth::guard($guard)->check()) {
            switch (Auth::user()->role_id){
                case 1: //ROOT
                    return redirect()->route('administracion_solicitudes.index');
                    break;
                case 2: //ADMIN
                    return redirect()->route('usuario.index');
                    break;
                case 3: //MÉDICO
                    return redirect()->route('consultas_medicas.index');
                    break;
                case 4: //ENFERMERA
                    return redirect()->route('enfermera.index');
                    // return redirect()->route('consultas_medicas.agendar');
                    break;
                case 5: //RRHH
                    return redirect()->route('consultas_medicas_pendientes.index');
                    break;
                case 6: //RES. DE SEGUIMIENTOS
                    return redirect()->route('seguimientos.index');
                    break;
                case 7: //ADM. DE SEGUIMIENTOS
                case 8: //CALL CENTER
                case 10: //ADMIN TOTAL
                case 11: //ADMIN DE SEGUIMIENTOS Y SEGUIMIENTO
                    return redirect()->route('administracion_seguimientos.index');
                    break;
                case 9: //REPORTES
                    return redirect()->route('report.global_risk_state');
                    break;
            }
        }

        return $next($request);
    }
}
