<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //return $next($request);
        // dd($request->route());

        $roleIds = ['root' => 1, 'admin' => 2, 'medic' => 3, 'nurse' => 4, 'rrhh' => 5, 'resp_seg' => 6, 'resp_adm_seg' => 7, 'call_center' => 8, 'repo' => 9, 'sup_admin'=>10, 'resp_adm_seg_resp_seg'=>11];
        $allowedRoleIds = [];


        $roles = array_slice(func_get_args(), 2); // [default, admin, manager]

        foreach ($roles as $role)
        {
            if(isset($roleIds[$role]))
            {
                $allowedRoleIds[] = $roleIds[$role];
            }
        }

        $allowedRoleIds = array_unique($allowedRoleIds);

        if(Auth::check()) {
            if(in_array(Auth::user()->role_id, $allowedRoleIds)) {
                return $next($request);
            }
        }

        if($request->ajax()){
            return response()->json(['no_access',true],403);
        }
        flash("No cuenta con los suficientes privilegios para acceder a la página solicitada")
            ->error()
            ->important();

        return redirect()->back();
    }
}
