<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class RiskStatusesRequest extends FormRequest
{
    use SanitizesInput;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $idRisk = $this->route('estados_riesgo');
        return [
            'name'          => 'required|string|min:3|max:191|unique:risk_states,name,'.$idRisk,
            'description'   => 'max:255',
            'weighting'     => 'required|numeric',
            'risk_type_id'  => 'required|exists:risk_types,id',
        ];
    }

    public function filters(){
        return [
            'name'                  => 'trim|escape|uppercase',
            'description'           => 'trim|escape|uppercase',
            'weighting'             => 'trim|escape'
        ];
    }
}
