<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WeightingLevelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $idWeight = $this->route('niveles_ponderacion');
        return [
            'name' => 'required|string|min:3|max:191|unique:weighting_levels,name,'.$idWeight,
            'description' => 'max:255',
            'color' => 'required',
            'minimum' => 'required',
            'maximum' => 'required',
            'evaluation' => 'required',
        ];
    }
}
