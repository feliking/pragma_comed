<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class ReprogrammingRequest extends FormRequest
{
    use SanitizesInput;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function filters(){
        return [
            'reprogramming_comment'         => 'trim|escape|uppercase',
            'next_reprogramming_time'       => 'trim|escape|uppercase|format_date:H:i A, H:i',
        ];
    }
}
