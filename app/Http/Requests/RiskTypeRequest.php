<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RiskTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $idTrisk = $this->route('tipos_riesgo');
        return [
            'name' => 'required|string|min:3|max:191|unique:risk_types,name,'.$idTrisk,
            'description' => 'max:255',
        ];
    }
}
