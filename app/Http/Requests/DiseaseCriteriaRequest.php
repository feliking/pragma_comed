<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiseaseCriteriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $idDisease = $this->route('criterios_enfermedad');
        return [
            'syntom' => 'required|string|min:3|max:191|unique:disease_criterias,syntom,'.$idDisease,
            'weighing' => 'required',
            'risk_type_id' => 'required|exists:risk_types,id',
        ];
    }
}
