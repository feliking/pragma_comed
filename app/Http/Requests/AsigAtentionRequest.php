<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class AsigAtentionRequest extends FormRequest
{
    use SanitizesInput;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd($this->all());
        if ($this->presave == "1")
        {
            return [
                'medical_status_id'         => 'exists:medical_request_statuses,id',
                'diagnostico'               => 'nullable',
                'tratamiento'               => 'nullable',
                'fecha_consulta'            => 'date',
                'fc'                        => 'nullable|numeric|max:150.99',
                'fr'                        => 'nullable|numeric|max:99.99',
                't'                         => 'nullable|numeric|max:99.99',
                'pa'                        => 'nullable|string|regex:/^[0-9]{1,3}(\/\d{1,3})?$/',
                'sao2'                      => 'nullable|numeric|max:99.99',
                'other'                     => 'nullable|string|max:20',
                'current_history'           => 'nullable|string',
                'physical_exam'             => 'nullable|string',
                'observacion'               => 'nullable|string',
                'variable_zoom'             => 'if:appointment_type_id,2|nullable|active_url',
                'variable_whatsapp'         => 'if:appointment_type_id,3',
                'comment'                   => 'nullable|string',
                //DATOS DEL FORMULARIO DE TRACING
                'treatment_stage_id_hidden' => 'integer|exists:treatment_stages,id',
                'risk_type_id_hidden'       => 'integer|exists:risk_types,id',
                'risk_state_id_hidden'      => 'integer|exists:risk_states,id',
                'disease_criteria.*'        => 'nullable|exists:disease_criterias,id',
                'comment_tracing'           => 'nullable|string',
                //DATOS DE EXAMENES COMPLEMENTARIOS
                'laboratories.*'            => 'nullable|exists:laboratories,id',
                'test_types.*'              => 'nullable|exists:test_types,id',
                'costs.*'                   => 'nullable|numeric',
                'bill_numbers.*'            => 'nullable|string',
            ];
        }
        else
        {
            return [
                'medical_status_id'         => 'required|exists:medical_request_statuses,id',
                'diagnostico'               => 'nullable',
                'tratamiento'               => 'nullable',
                'fecha_consulta'            => 'required|date',
                'fc'                        => 'nullable|numeric|max:150.99',
                'fr'                        => 'nullable|numeric|max:99.99',
                't'                         => 'nullable|numeric|max:99.99',
                'pa'                        => 'nullable|string|regex:/^[0-9]{1,3}(\/\d{1,3})?$/',
                'sao2'                      => 'nullable|numeric|max:99.99',
                'other'                     => 'nullable|string|max:20',
                'current_history'           => 'nullable|string',
                'physical_exam'             => 'nullable|string',
                'observacion'               => 'nullable|string',
                'variable_zoom'             => 'required_if:appointment_type_id,2|nullable|active_url',
                'variable_whatsapp'         => 'required_if:appointment_type_id,3',
                'comment'                   => 'nullable|string',
                //DATOS DEL FORMULARIO DE TRACING
                'treatment_stage_id_hidden' => 'required|integer|exists:treatment_stages,id',
                'risk_type_id_hidden'       => 'required|integer|exists:risk_types,id',
                'risk_state_id_hidden'      => 'required|integer|exists:risk_states,id',
                'disease_criteria.*'        => 'nullable|exists:disease_criterias,id',
                'comment_tracing'           => 'nullable|string',
                //DATOS DE EXAMENES COMPLEMENTARIOS
                'laboratories.*'            => 'nullable|exists:laboratories,id',
                'test_types.*'              => 'nullable|exists:test_types,id',
                'costs.*'                   => 'nullable|numeric',
                'bill_numbers.*'            => 'nullable|string',
            ];
        }
    }

    public function filters(){
        return [
            'medical_status_id'   => 'trim|escape|uppercase',
            'fecha_consulta'      => 'trim|escape|uppercase|format_date:d/m/Y, Y-m-d',
            'diagnostico'         => 'trim|escape|uppercase',
            'tratamiento'         => 'trim|escape|uppercase',
            'current_history'     => 'trim|escape|uppercase',
            'physical_exam'       => 'trim|escape|uppercase',
            'observacion'         => 'trim|escape|uppercase',
            'fc'                  => 'trim|escape|uppercase',
            'fr'                  => 'trim|escape|uppercase',
            't'                   => 'trim|escape|uppercase',
            'pa'                  => 'trim|escape|uppercase',
            'sao2'                => 'trim|escape|uppercase',
            'other'               => 'trim|escape|uppercase',
            'comment_tracing'     => 'trim|escape|uppercase',
            'next_attention_date' => 'trim|escape|uppercase|format_date:d/m/Y H:i A, Y-m-d H:i',
            'variable_zoom'       => 'trim|escape',
            'variable_whatsapp'   => 'trim|escape',
            'comment'             => 'trim|escape|uppercase',
        ];
    }
}
