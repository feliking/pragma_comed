<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class TestTypeRequest extends FormRequest
{
    use SanitizesInput;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $idTestType = $this->route('tipos_prueba');
        return [
            'name' => 'required|string|min:3|max:191|unique:test_types,name,'.$idTestType,
            'description' => 'max:255',
        ];
    }

    public function filters(){
        return [
            // 'medical_request_id'     => 'trim|escape|uppercase',
            'name'              => 'trim|escape|uppercase',
            'description'       => 'trim|escape|uppercase',
        ];
    }
}
