<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AttentionVitalSignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fc'    => 'required|numeric|max:150.99',
//            'fr'    => 'required|numeric|max:99.99',
            't'     => 'required|numeric|max:99.99',
            'pa'    => 'required|string|regex:/^[0-9]{1,3}(\/\d{1,3})?$/',
            'sao2'  => 'required|numeric|max:99.99',
            'other' => 'nullable|string|max:20',
        ];
    }
}
