<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class MedicalRequestValidation extends FormRequest
{
    use SanitizesInput;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'POST':
                return [
                    'ci'                      => 'required|string|min:6|max:11',
                    'ci_expedition'           => 'required|in:LP,CB,SC,CH,OR,PT,TJ,BE,PD',
                    'enterprise'              => 'required|string|min:3|max:190',
                    'email'                   => 'nullable|email|max:190',
                    'employee_code'           => 'nullable|string|min:1|max:5',
                    'full_name'               => 'required|string|min:5|max:190',
                    'phone_number'            => 'required|string|min:4|max:8',
                    'reference_full_name'     => 'nullable|string|min:5|max:190',
                    'reference_phone_number'  => 'nullable|string|min:7|max:8',
                    'short_phone_number'      => 'nullable|string|min:4|max:4',
                    'whatsapp_flag'           => 'required|boolean',
                    'comment'                 => 'nullable|string|max:191',
                    'gender'                  => 'required|in:Femenino,Masculino',
                    'birthday'                => 'required|date',
                    'risk_type_id'            => 'required|integer|exists:risk_types,id'
                ];
                break;
            case 'PUT':
                return [
                    'ci'                      => 'required|string|min:6|max:11',
                    'ci_expedition'           => 'required|in:LP,CB,SC,CH,OR,PT,TJ,BE,PD',
                    'enterprise'              => 'required|string|min:3|max:190',
                    'email'                   => 'nullable|email|max:190',
                    'employee_code'           => 'nullable|string|min:1|max:5',
                    'full_name'               => 'required|string|min:5|max:190',
                    'phone_number'            => 'required|string|min:4|max:8',
                    'reference_full_name'     => 'nullable|string|min:5|max:190',
                    'reference_phone_number'  => 'nullable|string|min:7|max:8',
                    'short_phone_number'      => 'nullable|string|min:4|max:4',
                    'whatsapp_flag'           => 'required|boolean',
                    'comment'                 => 'nullable|string|max:191',
                    'gender'                  => 'required|in:Femenino,Masculino',
                    'birthday'                => 'required|date',
                    'risk_type_id'            => 'required|integer|exists:risk_types,id'
                ];
                break;
        }
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters(){
        return [
            'ci'                => 'trim|escape|uppercase',
            'ci_expedition'     => 'trim|escape|uppercase',
            'enterprise'        => 'trim|escape|uppercase',
            'email'             => 'trim|escape',
            'employee_code'     => 'trim|escape|uppercase',
            'full_name'         => 'trim|escape|uppercase',
            'phone_number'      => 'trim|escape|uppercase',
            'reference_full_name'     => 'trim|escape|uppercase',
            'reference_phone_number'  => 'trim|escape|uppercase',
            'short_phone_number'      => 'trim|escape|uppercase',
            'whatsapp_flag'     => 'trim|escape|cast:boolean',
            'gender'            => 'trim|escape',
            'birthday'          => 'trim|format_date:d/m/Y,Y-m-d',
            'comment'           => 'trim|escape|uppercase',
        ];
    }
}
