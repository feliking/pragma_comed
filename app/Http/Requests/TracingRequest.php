<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class TracingRequest extends FormRequest
{
    use SanitizesInput;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        $idMedicalRequest = $this->route('seguimiento');

        return [
            //DATOS DEL FORMULARIO DE TRACING
            'treatment_stage_id_hidden'    => 'required|integer|exists:treatment_stages,id',
//            'enterprise_id'         => 'required|integer|exists:solicitudes.empresas,id',
//            'project_id'            => 'required|integer|exists:solicitudes.proyectos,id',
            'risk_type_id_hidden'          => 'required|integer|exists:risk_types,id',
            //'risk_state_id'         => 'required|integer|exists:risk_states,id',
            'disease_criteria.*'    => 'nullable|exists:disease_criterias,id',
            'comment_tracing'       => 'nullable|string',
            //DATOS DE EXAMENES COMPLEMENTARIOS
            'laboratories.*'        => 'nullable|exists:laboratories,id',
            'test_types.*'          => 'nullable|exists:test_types,id',
            'costs.*'               => 'nullable|numeric',
            'bill_numbers.*'        => 'nullable|string',
//            'enterprise_pay_flags.*'=> 'nullable|boolean'
        ];
    }

    public function filters(){
        return [
            'comment_tracing'           => 'trim|escape|uppercase',
            'next_attention_date'       => 'trim|escape|uppercase|format_date:d/m/Y H:i A, Y-m-d H:i',
            'variable_zoom'             => 'trim|escape',
            'variable_whatsapp'         => 'trim|escape',
            'comment'                   => 'trim|escape|uppercase'
//            'enterprise_pay_flags.*'    => 'cast:boolean'
        ];
    }
}
