<?php

namespace App\Http\Requests;

use App\Rules\MedicalRequestStateRule;
use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class AppointmentValidation extends FormRequest
{
    use SanitizesInput;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'POST':
                return [
                    'medical_request_id'    => ['required','integer','exists:medical_requests,id'/*,new MedicalRequestStateRule([2,8,9,10,11])*/],
                    'next_attention_date'   => 'required|date_format:Y-m-d',
                    'next_attention_time'   => 'required|date_format:H:i',
                    'appointment_type_id'   => 'required|integer|exists:appointment_types,id',
                    'variable_zoom'         => 'required_if:appointment_type_id,2|nullable|active_url',
                    'variable_whatsapp'     => 'required_if:appointment_type_id,3',
                    'comment'               => 'nullable|string',
                ];
                break;
            case 'PUT':
                return [
                    'medical_request_id'    => ['required','integer','exists:medical_requests,id'/*,new MedicalRequestStateRule([2,8,9,10,11])*/],
                    'next_attention_date'   => 'required|date_format:Y-m-d',
                    'next_attention_time'   => 'required|date_format:H:i',
                    'appointment_type_id'    => 'required|integer|exists:appointment_types,id',
                    'variable_zoom'         => 'required_if:appointment_type_id,2|nullable|active_url',
                    'variable_whatsapp'     => 'required_if:appointment_type_id,3|nullable',
                    'comment'               => 'nullable|string',
                ];
                break;
        }
    }

    public function filters(){
        return [
            'next_attention_date'   => 'trim|escape|uppercase|format_date:d/m/Y, Y-m-d',
            'next_attention_time'   => 'trim|escape|uppercase|format_date:h:i A, H:i',
            'variable_zoom'         => 'trim|escape',
            'variable_whatsapp'     => 'trim|escape',
            'comment'               => 'trim|escape|uppercase'
        ];
    }

    public function attributes()
    {
        return [
            'next_attention_date'   => '"próxima fecha de atención"',
            'appointment_type_id'   => '"tipo de cita"',
            'variable_whatsapp'     => '"número de whatsapp"',
            'variable_zoom'         => '"dirección zoom"',
            'comment'               => '"comentario u observación"',
        ];
    }

    public function messages()
    {
        return [
            'variable_zoom.required_if'         => 'El campo dirección zoom es obligatorio cuando el campo "tipo de cita" es "ZOOM".',
            'variable_whatsapp.required_if'     => 'El campo dirección zoom es obligatorio cuando el campo "tipo de cita" es "WHATSAPP".'
        ];
    }

}
