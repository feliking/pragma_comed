<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class AtecedentRequest extends FormRequest
{
    use SanitizesInput;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'descripcion_antecedente'  => 'required|max:500',
                    'modal_antecedent_type_id' => 'required|exists:antecendent_types,id',
                    'modal_medical_request_id' => 'required|exists:medical_requests,id',
                ];
                break;

            case 'PUT':
                 return [
                    'descripcion_antecedente'  => 'required|max:500',
                    'modal_antecedent_type_id' => 'required|exists:antecendent_types,id',
                    'modal_antecedent_id' => 'required|exists:antecendents,id',
                    'modal_medical_request_id' => 'required|exists:medical_requests,id',
                ];
                break;
        }

    }

    public function filters(){
        return [
            'descripcion_antecedente'      => 'trim|escape|uppercase',
        ];
    }
}
