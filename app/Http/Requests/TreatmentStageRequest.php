<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TreatmentStageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $idTreat = $this->route('covid_estados');
        return [
            'name' => 'required|string|min:3|max:191|unique:treatment_stages,name,'.$idTreat,
            'description' => 'max:255',
        ];
    }
}
