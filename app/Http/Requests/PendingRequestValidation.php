<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Rules\MedicalRequestStateRule;

use Waavi\Sanitizer\Laravel\SanitizesInput;

class PendingRequestValidation extends FormRequest
{
    use SanitizesInput;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd($this->all());
        switch ($this->route()->getName()){
            case "consultas_medicas_pendientes.asignar_empleado.assignToEmployee":
                return [
                    'employee_id'        => 'required|integer|exists:rrhh.empleados,id',
                    'full_name'          => 'required|string',
                    'medical_request_id' => ['required','integer','exists:medical_requests,id',new MedicalRequestStateRule([3, 7])] //PENDIENTE
                ];
            break;

            case 'consultas_medicas_pendientes.asignar.assign':
                return [
                    'full_name'          => 'required|string',
                    'ci'                 => 'required|string|min:7|max:11',
                    'ci_expedition'      => 'required|in:LP,CB,SC,CH,OR,PT,TJ,BE,PD',
                    'enterprise'         => 'required|string|min:3|max:190',
                    'medical_request_id' => ['required','integer','exists:medical_requests,id',new MedicalRequestStateRule([3, 7])] //PENDIENTE
                ];
            break;

            case 'consultas_medicas_pendientes.eliminar_solicitud.deleteRequest':
                return [
                    'medical_request_id' => ['required','integer','exists:medical_requests,id',new MedicalRequestStateRule([3, 7])] //PENDIENTE
                ];
            break;
        }
    }
}
