<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\MedicalRequestStateRule;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class TracingAdministrationValidation extends FormRequest
{
    use SanitizesInput;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->route()->getName()){
            case "administracion_seguimientos.asignar_empleado.assignToEmploye": //RUTA CUANDO SE ASIGNA A UN EMPLEADO
            case "administracion_seguimientos.asignar_medico.assignToMedic":
                return [
                    'employee_id'           => 'required|integer|exists:rrhh.empleados,id',
                    'medical_request_id'    => ['required','integer','exists:medical_requests,id',new MedicalRequestStateRule([1,5,6])] //SOLICITADO - VALIDO - PRE SOLICITADO
                ];
                break;
            case "administracion_seguimientos.asignarme.assignToMe": //RUTA PARA ASIGNARME PACIENTE
            case "administracion_seguimientos.no_encontrado.employeeNotFound": //RUTA PARA CAMBIAR ESTADO A NO ENCONTRADO
                return [
                    'medical_request_id'    => ['required','integer','exists:medical_requests,id',new MedicalRequestStateRule([1,5,6])] //SOLICITADO - VALIDO - PRE SOLICITADO
                ];
                break;
            case "administracion_seguimientos.verificar.verifyStatePreSol": //RUTA QUE VERIFICA EL ESTADO DEL MEDICAL REQUEST
                return [

                ];
                break;
            case "administracion_seguimientos.cerrar_pre.closeStatePreSol": //RUTA QUE CIERRA EL PRE ESTADO
            case "administracion_seguimientos.cerrar_pre.closeChangeStateAuto": //RUTA QUE CAMBIA EL ESTADO DE MANERA AUTOMATICA
                return [
                    'medical_request_id'    => ['required','integer','exists:medical_requests,id',new MedicalRequestStateRule([6])] //PRE SOLICITADO
                ];
                break;
            case "administracion_seguimientos.cerrar_caso.closeCase":
                return [
                ];
                break;

        }
    }

    public function filters(){
        return [
            'comment'                   => 'trim|escape|uppercase'
        ];
    }
}
