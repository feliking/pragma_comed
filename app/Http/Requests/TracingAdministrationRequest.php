<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class TracingAdministrationRequest extends FormRequest
{
    use SanitizesInput;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->radio_option == "seguimiento") {
            // dd('validacion', $this->all());
            if (isset($this->parametro_asignacion)) {
                return [
                    'medical_request_id.*'        => ['required','integer','exists:medical_requests,id'],

                    'personal_id'                 => 'required|exists:users,id',
                    //DATOS DE CITAS
                    'next_attention_date_tracing' => 'required|date_format:Y-m-d',
                    'hora_asignada.*'             => 'required|date_format:H:i',
                    'appointment_type_tracing_id' => 'required|integer|exists:appointment_types,id',
                    'variable_tracing_zoom'       => 'required_if:appointment_type_id,2|nullable|active_url',
                    'variable_tracing_whatsapp'   => 'required_if:appointment_type_id,3',
                    'comment_tracing'             => 'nullable|string',
                ];
            }else{
                return [
                    'medical_request_id'            => ['required','integer','exists:medical_requests,id'],
                    'personal_id'                   => 'required|exists:users,id',
                    //DATOS DE CITAS
                    'next_attention_date_tracing'   => 'required|date_format:Y-m-d',
                    'next_attention_time_tracing'   => 'required|date_format:H:i',
                    'appointment_type_tracing_id'   => 'required|integer|exists:appointment_types,id',
                    'variable_tracing_zoom'         => 'required_if:appointment_type_id,2|nullable|active_url',
                    'variable_tracing_whatsapp'     => 'required_if:appointment_type_id,3',
                    'comment_tracing'               => 'nullable|string',
                ];
            }
        }else{ //CONSULTA MEDICA
            if (isset($this->parametro_asignacion)) {
                return [
                    'medical_request_id.*' => ['required','integer','exists:medical_requests,id'],
                    'medico_id'            => 'required|exists:users,id',
                    //DATOS DE CITAS
                    'next_attention_date'  => 'required|date_format:Y-m-d',
                    'hora_asignada.*'      => 'required|date_format:H:i',
                    'appointment_type_id'  => 'required|integer|exists:appointment_types,id',
                    'variable_zoom'        => 'required_if:appointment_type_id,2|nullable|active_url',
                    'variable_whatsapp'    => 'required_if:appointment_type_id,3',
                    'comment'              => 'nullable|string',
                ];
            }else{
                return [
                    'medical_request_id'            => ['required','integer','exists:medical_requests,id'],
                    'medico_id'                     => 'required|exists:users,id',
                    //DATOS DE CITAS
                    'next_attention_date'           => 'required|date_format:Y-m-d',
                    'next_attention_time'           => 'required|date_format:H:i',
                    'appointment_type_id'           => 'required|integer|exists:appointment_types,id',
                    'variable_zoom'                 => 'required_if:appointment_type_id,2|nullable|active_url',
                    'variable_whatsapp'             => 'required_if:appointment_type_id,3',
                    'comment'                       => 'nullable|string',
                ];
            }
        }
    }

    public function filters(){
        return [
            //DATOS DE ATENCION
            'next_attention_date'           => 'trim|escape|uppercase|format_date:d/m/Y, Y-m-d',
            'next_attention_time'           => 'trim|escape|uppercase|format_date:h:i A, H:i',
            'variable_zoom'                 => 'trim|escape',
            'variable_whatsapp'             => 'trim|escape',
            'comment'                       => 'trim|escape|uppercase',
            //DATOS DE SEGUIMIENTO
            'next_attention_date_tracing'   => 'trim|escape|uppercase|format_date:d/m/Y, Y-m-d',
            'next_attention_time_tracing'   => 'trim|escape|uppercase|format_date:h:i A, H:i',
            'hora_asignada.*'   => 'trim|escape|uppercase|format_date:h:i, H:i',
            'variable_tracing_zoom'         => 'trim|escape',
            'variable_tracing_whatsapp'     => 'trim|escape',
            'comment_tracing'               => 'trim|escape|uppercase',
        ];
    }
}
