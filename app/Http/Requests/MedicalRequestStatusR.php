<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MedicalRequestStatusR extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $idStatus = $this->route('estado');
        //dd($idStatus);
        return [
            'name' => 'required|string|min:3|max:100|unique:medical_request_statuses,name,'.$idStatus,
            'description' => 'max:255',
        ];
    }
}
