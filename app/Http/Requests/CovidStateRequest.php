<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class CovidStateRequest extends FormRequest
{
    use SanitizesInput;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $idStat = $this->route('covid_estado');
        return [
            'name' => 'required|string|min:3|max:191|unique:covid_states,name,'.$idStat,
            'description' => 'max:255',
        ];
    }

    public function filters(){
        return [
            'name'                  => 'trim|escape|uppercase',
            'description'           => 'trim|escape|uppercase',
        ];
    }
}
