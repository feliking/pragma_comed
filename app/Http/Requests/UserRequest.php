<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => 'required',
            'username' => 'required|string|min:3|max:255|unique:users,name',
            'role_id' => 'required|exists:roles,id',
            'rrhh_office_id' => 'required_if:role_id,3,4',
            'password' => 'required|min:5|confirmed',
        ];
    }
    public function messages()
    {
        return [
            'rrhh_office_id.required_if' =>  'El campo oficina es obligatorio cuando el campo rol es médico o enfermer@',
        ];
    }
}
