<?php

namespace App\Http\Requests;

use App\Rules\MedicalRequestStateRule;
use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class MedicalRequestAdministrationValidation extends FormRequest
{
    use SanitizesInput;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->route()->getName()){
            case "administracion_solicitudes.asignar_empleado.assignToEmploye": //RUTA CUANDO SE ASIGNA A UN EMPLEADO
            case "administracion_solicitudes.asignar_medico.assignToMedic":
                return [
                    'employee_id'           => 'required|integer|exists:rrhh.empleados,id',
                    'medical_request_id'    => ['required','integer','exists:medical_requests,id',new MedicalRequestStateRule([1,5,6])] //SOLICITADO - VALIDO - PRE SOLICITADO
                ];
                break;
            case "administracion_solicitudes.asignarme.assignToMe": //RUTA PARA ASIGNARME PACIENTE
            case "administracion_solicitudes.no_encontrado.employeeNotFound": //RUTA PARA CAMBIAR ESTADO A NO ENCONTRADO
                return [
                    'medical_request_id'    => ['required','integer','exists:medical_requests,id',new MedicalRequestStateRule([1,5,6])] //SOLICITADO - VALIDO - PRE SOLICITADO
                ];
                break;
            case "administracion_solicitudes.verificar.verifyStatePreSol": //RUTA QUE VERIFICA EL ESTADO DEL MEDICAL REQUEST
                return [

                ];
                break;
            case "administracion_solicitudes.cerrar_pre.closeStatePreSol": //RUTA QUE CIERRA EL PRE ESTADO
            case "administracion_solicitudes.cerrar_pre.closeChangeStateAuto": //RUTA QUE CAMBIA EL ESTADO DE MANERA AUTOMATICA
                return [
                    'medical_request_id'    => ['required','integer','exists:medical_requests,id',new MedicalRequestStateRule([6])] //PRE SOLICITADO
                ];
                break;

        }
    }
}
