<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MedicalRequestTypeR extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $idType = $this->route('tipo');
        //dd($idType);
        return [
            'name' => 'required|string|min:3|max:100|unique:medical_request_types,name,'.$idType,
            'description' => 'max:255',
            'medical_request_priority_id' => 'required|exists:medical_request_priorities,id',
        ];
    }
}
