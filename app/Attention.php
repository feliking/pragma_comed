<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attention extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'attentions';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'attention_date',
        'diagnosis',
        'treatment',
        'fc',
        'fr',
        'pa',
        't',
        'sao2',
        'otros',
        'current_history',
        'physical_exam',
        'observation',
    ];

    /**
     * Indica que campos son fecha
     *
     * @var array
     */
    protected $dates = [
        'attention_date'
    ];

    public function historical_request(){
        return $this->belongsTo(HistoricalRequest::class,'historical_request_id','id');
    }
}
