<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    // Conexión
    protected $connection = 'rrhh';

    // Tabla asociada al modelo
    protected $table = 'oficinas';

    // ¿Utiliza timestamps?
    public $timestamps = true;

    /*
     * Relationships
     */
    // One to Many
    public function user(){
        return $this->hasMany('App\User', 'rrhh_office_id');
    }
}
