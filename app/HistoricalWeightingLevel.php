<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HistoricalWeightingLevel extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'historical_weighting_levels';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'color', 'minimum', 'maximum', 'evaluation', 'calc_evaluation','weighting_level_id', 'tracing_id'
    ];
    /*
     * Mutators
     */
    // SET Nombre
    public function setNameAttribute($value){
        $this->attributes['name'] = mb_strtoupper($value, 'UTF-8');
    }

    // SET Descripción
    public function setDescriptionAttribute($value){
        if($value != null){
            $this->attributes['description'] = mb_strtoupper($value, 'UTF-8');
        }else{
            $this->attributes['description'] = null;
        }
    }
}
