<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectSOL extends Model
{
    // protected $connection = 'solicitudes';
    protected $connection = 'rrhh';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'proyectos';

    // protected $appends = array('full_name');

    // public function getFullNameAttribute(){
    //     return $this->recursive_name($this);
    // }

    // private function recursive_name(ProjectSOL $project){
    //     if($project->padre_id==null){
    //         return $project->nombre;
    //     }else{
    //         $project_parent = $project->parent;
    //         return $project->nombre.' > '.$this->recursive_name($project_parent);
    //     }
    // }

    // public function parent(){
    //     return $this->belongsTo(ProjectSOL::class,'padre_id','id');
    // }
}
