<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricalRequest extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'historical_requests';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'comment', 'medical_request_id', 'user_id', 'medical_request_status_id'
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function medical_request_status(){
        return $this->belongsTo(MedicalRequestStatus::class,'medical_request_status_id','id');
    }

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public function attention(){
        return $this->hasOne(Attention::class);
    }

    public function medical_request(){
        return $this->belongsTo(MedicalRequest::class);
    }

    public function tracing()
    {
        return $this->hasOne('App\Tracing', 'historical_request_id');
    }

    public function vaccines () {
        return $this->belongsToMany(Vaccine::class, 'vaccine_historical');
    }
}
