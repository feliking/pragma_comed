<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vaccine extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vaccines';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vaccine_type_id',
        'consent',
        'doce'
    ];

    public function vaccine_type () {
        return $this->belongsTo(VaccineType::class);
    }

    public function historical_requests () {
        return $this->belongsToMany(HistoricalRequest::class, 'vaccine_historical' );
    }

    public function employees () {
        return $this->belongsToMany(EmployeeRRHH::class, 'vaccine_employee', 'employee_id', 'vaccine_id');
    }
}
