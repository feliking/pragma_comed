<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appointment extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'appointments';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'next_attention_date', 'variable_text','comment', 'appointment_type_id', 'medical_request_id','user_id'
    ];

    /**
     * Indica que campos son fecha
     *
     * @var array
     */
    protected $dates = [
        'next_attention_date',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @description = Relacion hacia el tipo de cita
     */
    public function appointment_type(){
        return $this->belongsTo(AppointmentType::class,'appointment_type_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @description Muchos a uno
     */
    public function medical_request(){
        return $this->belongsTo(MedicalRequest::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     * @description Muchos a muchos
     */
    public function user(){
        return $this->belongsToMany(User::class,'user_id');
    }
}
