<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MedicalRequestStatus extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'medical_request_statuses';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    /*
     * Mutators
     */
    // SET Nombre
    public function setNameAttribute($value){
        $this->attributes['name'] = mb_strtoupper($value, 'UTF-8');
    }

    // SET Descripción
    public function setDescriptionAttribute($value){
        if($value != null){
            $this->attributes['description'] = mb_strtoupper($value, 'UTF-8');
        }else{
            $this->attributes['description'] = null;
        }
    }
    public function medicalRequests()
    {
        return $this->hasMany(MedicalRequest::class);
    }
    
}
