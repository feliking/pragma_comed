<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;

class MedicalRequest extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'medical_requests';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'ci','ci_expedition','enterprise','email','employee_code','full_name','phone_number','reference_full_name','reference_phone_number','short_phone_number','whatsapp_flag','comment', 'gender', 'birthday', 'health_insurance_id', 'medical_request_status_id','risk_type_id','user_id','employee_id', 'project_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'birthday',
    ];
    protected $appends = array('last_appointment', 'last_appointment_penul','consulta_proxima','last_tracing');
    public function setFullNameAttribute($value)
    {
        $this->attributes['full_name'] = mb_strtoupper($value);
    }

    public function setReferenceFullNameAttribute($value)
    {
        $this->attributes['reference_full_name'] = mb_strtoupper($value);
    }

    public function setHealthInsuranceAttribute($value)
    {
        $this->attributes['health_insurance'] = mb_strtoupper($value);
    }


    public function getFullCiAttribute(){
        return $this->ci.' '.$this->ci_expedition;
    }

    public function getEdadAttribute()
    {
        $edad = Carbon::parse($this->birthday)->age;
        return $edad;
    }

    public function getConsultaProximaAttribute(){
        $fecha = DB::select("SELECT obtine_fecha_proxima_consulta($this->id) as fecha_proxima_consulta;");
        $fecha = $fecha[0]->fecha_proxima_consulta;
        $carbon = Carbon::create($fecha);

        return $carbon;
    }

    public function getLastTracingAttribute(){
        $tracing = Tracing::where('medical_request_id','=',$this->id)
            ->latest()
            ->first();

        return $tracing;
    }

    public function getLastAppointmentAttribute(){
        $appointment = Appointment::where('medical_request_id','=',$this->id)
            ->latest()
            ->first();

        return $appointment;
    }
    public function getLastAppointmentPenulAttribute(){
        $appointments = Appointment::where('medical_request_id','=',$this->id)
            ->pluck('id')
            ->toArray();
        $appointments_last = array_splice($appointments, count($appointments)-1, 1);
        if(count($appointments)==0){
            $appointment = null;
        }else{
            $appointment = Appointment::whereIn('id',$appointments)->latest()
            ->first();
        }
        return $appointment;
    }
    public function getLastTracingDayAttribute(){

        $tracing_first = Tracing::where('medical_request_id','=',$this->id)
            ->first();

        $days = '';
        if ($tracing_first!=null) {
            HistoricalRequest::find($tracing_first->historical_request_id);
            $today = \Carbon\Carbon::now();
            $date_tracing = $tracing_first->tracing_date;
            $days = ($date_tracing->diff($today)->days < 1)? '0': $date_tracing->diffInDays($today);

        }else{
            $days = 'S/S';
        }
        return $days;
    }
    public function getNextSuggestDateAttribute(){
        $fecha_sugerida = null;
        $last_tracing = $this->last_tracing;
        if($last_tracing!=null){
            $historical_weighting_level_evaluation = $last_tracing->historical_weighting_level->evaluation;

                if($historical_weighting_level_evaluation!=0){
                    $fecha_sugerida = \Carbon\Carbon::parse($last_tracing->tracing_date);
                    if($this->risk_type_id == 1){
                        $fecha_sugerida->addDay($historical_weighting_level_evaluation);
                    }else{
                        $fecha_sugerida->addDay(7);
                    }
                }
        }
        return $fecha_sugerida;
    }

    public function getHistoryValidStatusAttribute()
    {
        $historical_requests = HistoricalRequest::where('medical_request_id', '=', $this->id)
            ->where('medical_request_status_id', '=', 5) //PREGUNTA SI A SIDO VALIDADO ALGUNA VEZ
            ->first();

        if($historical_requests==null)
            return false;
        else
            return true;
    }

    public function getReassignCountAttribute(){
        $historical_requests = HistoricalRequest::where('medical_request_id', '=', $this->id)
            ->where('medical_request_status_id', '=', 14) //RE ASIGNADO
            ->count();

        return $historical_requests;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @description = Obtiene el tipo de requisito medico al que pertenece
     */

    public function medical_request_status(){
        return $this->belongsTo(MedicalRequestStatus::class,'medical_request_status_id','id');
    }

    public function riskType(){
        return $this->belongsTo(RiskType::class,'risk_type_id','id');
    }

    // public function medical_requests_type(){
    //     return $this->belongsTo(MedicalRequestType::class,'medical_request_type_id','id');
    // }

    public function medical_requests_state(){
        return $this->belongsTo(MedicalRequestStatus::class,'medical_request_status_id','id');
    }

    public function healthInsurance()
    {
        return $this->belongsTo('App\HealthInsuranceRRHH', 'health_insurance_id');
    }

    public function employee(){
        return $this->hasOne(EmployeeRRHH::class,'id','employee_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     * @description Muchos a muchos
     */
    public function user(){
        return $this->belongsToMany(User::class,'historical_requests','medical_request_id','user_id')
            ->withTimestamps();
    }

    public function actual_user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function medic(){
        return $this->hasOne(User::class,'id','user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     * @description Muchos a muchos
     */
    public function medical_requests_status(){
        return $this->belongsToMany(MedicalRequestStatus::class,'historical_requests','medical_request_id','medical_request_status_id')
            ->withTimestamps();
    }

    public function historial_requests(){
        return $this->hasMany(HistoricalRequest::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @description Uno a muchos
     */
    public function appointments(){
        return $this->hasMany(Appointment::class);
    }

    public function lastAppointment(){
        return $this->hasOne(Appointment::class)
            ->orderBy('next_attention_date', 'DESC');
    }

    // public function patientStatus()
    // {
    //     return $this->belongsTo(PatientStatus::class, 'patient_status_id');
    // }

    public function risk_type(){
        return $this->belongsTo(RiskType::class, 'risk_type_id');
    }

    public function tracings(){
        return $this->hasMany(Tracing::class);
    }

    public function attention_vital_sign(){
        return $this->hasMany(AttentionVitalSign::class);
    }

    public function project(){
        return $this->belongsTo(ProjectSOL::class,'project_id','id');
    }
}
