<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HealthInsuranceEnterpriseRRHH extends Model
{
    protected $connection = 'rrhh';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'caja_empresa';
}
