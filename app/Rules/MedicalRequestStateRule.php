<?php

namespace App\Rules;

use App\MedicalRequest;
use Illuminate\Contracts\Validation\Rule;

class MedicalRequestStateRule implements Rule
{
    private $medical_request_status_id;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($medical_request_status_id)
    {
        $this->medical_request_status_id = $medical_request_status_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $medical_request = MedicalRequest::withTrashed()
            ->where('id','=',$value)
            ->whereIn('medical_request_status_id',$this->medical_request_status_id)
            ->first();

        // dd($value, $this->medical_request_status_id);

        return ($medical_request!=null) ? true :  false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'La solicitud médica no se encuentra en el estado indicado.';
    }
}
