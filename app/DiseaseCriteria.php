<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiseaseCriteria extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'disease_criterias';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'syntom', 'weighing', 'risk_type_id'
    ];
    /*
     * Mutators
     */
    // SET Nombre
    public function setSyntomAttribute($value){
        $this->attributes['syntom'] = mb_strtoupper($value, 'UTF-8');
    }

    public function risk_type(){
        return $this->belongsTo('App\RiskType', 'risk_type_id');
    }
}
