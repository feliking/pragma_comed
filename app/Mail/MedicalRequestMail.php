<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\MedicalRequest;

class MedicalRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    public $medical_request;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(MedicalRequest $medical_request)
    {
        $this->medical_request = $medical_request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Consulta médica registrada')
            ->with([
                'medical_request' => $this->medical_request
            ])
            ->view('mail.medical_request');
    }
}
