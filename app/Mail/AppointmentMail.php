<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Appointment;
use App\MedicalRequest;

class AppointmentMail extends Mailable
{
    use Queueable, SerializesModels;

    public $medical_request;
    public $appointment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(MedicalRequest $medical_request, Appointment $appointment)
    {
        $this->medical_request = $medical_request;
        $this->appointment = $appointment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Cita registrada')
            ->with([
                'medical_request'   => $this->medical_request,
                'appointment'       => $this->appointment
            ])
            ->view('mail.appointment');
    }
}
