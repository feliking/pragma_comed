<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HealthInsuranceRRHH extends Model
{
    protected $connection = 'rrhh';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cajas';
}
