<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PatientStatus extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'patient_statuses';

    public $timestamps = true;

    protected $fillable = [
        'name', 'description'
    ];

    public function setNameAttribute($value){
        $this->attributes['name'] = mb_strtoupper($value, 'UTF-8');
    }

    // SET Descripción
    public function setDescriptionAttribute($value){
        if($value != null){
            $this->attributes['description'] = mb_strtoupper($value, 'UTF-8');
        }else{
            $this->attributes['description'] = null;
        }
    }

    // relation ship
    public function medicalRequests()
    {
        return $this->hasMany(MedicalRequest::class);
    }
}

