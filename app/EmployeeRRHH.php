<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class EmployeeRRHH extends Model
{
    protected $connection = 'rrhh';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pragma_serrhh.empleados';

//    protected $dates = 'fecha_nacimiento';

    public function getEdadAttribute(){
        return Carbon::parse($this->fecha_nacimiento)->age;
    }

    public function getFullNameAttribute(){
        return "{$this->nombres} {$this->apellido_1} {$this->apellido_2} {$this->apellido_3}";
    }

    public function getFullCiAttribute(){
        return "{$this->ci_numero} {$this->ci_expedido}";
    }
    protected $appends = array('full_name');
    public function user(){
        return $this->hasOne('App\User', 'employee_id');
    }
    public function enterprise(){
        return $this->belongsTo(EnterpriseSOL::class,'sol_empresa_id','id');
    }
    public function project(){
        return $this->belongsTo(ProjectSOL::class,'proyecto_id','id');
    }
    public function vaccines () {
        return $this->belongsToMany(Vaccine::class, 'vaccine_employee', 'vaccine_id', 'employee_id');
    }
    //Mutators
    /* public function getFullNameAttribute(){
        return $this->apellido_1 . '  ' . $this->apellido_2.' '.$this->nombres;
    } */
}
