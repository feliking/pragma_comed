<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AntecendentType extends Model
{
    protected $table = 'antecendent_types';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    protected $fillable = [
        'name', 'description'
    ];

    public function antecendents()
    {
        return $this->hasMany('App\Antecendent');
    }

}
