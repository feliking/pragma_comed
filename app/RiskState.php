<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RiskState extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'risk_states';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'weighting', 'risk_type_id'
    ];

    protected $appends = [
        'full_name'
    ];

    public function getFullNameAttribute(){
        if($this->weighting==0)
            return $this->name;
        else
            return $this->name. ' ('.$this->weighting.')';
    }

    /*
     * Mutators
     */
    // SET Nombre
    public function setNameAttribute($value){
        $this->attributes['name'] = mb_strtoupper($value, 'UTF-8');
    }

    // SET Descripción
    public function setDescriptionAttribute($value){
        if($value != null){
            $this->attributes['description'] = mb_strtoupper($value, 'UTF-8');
        }else{
            $this->attributes['description'] = null;
        }
    }

    public function risk_type(){
        return $this->belongsTo('App\RiskType', 'risk_type_id');
    }
}
