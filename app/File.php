<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model
{
//    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'files';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'location', 'file_type_id', 'mime'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @description = Uno a Muchos
     */
    public function file_type(){
        return $this->belongsTo(FileType::class);
    }
}
