<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnterpriseSOL extends Model
{
    protected $connection = 'solicitudes';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'empresas';
}
