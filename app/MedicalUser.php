<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicalUser extends Model
{
    protected $table = 'medical_users';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
}
