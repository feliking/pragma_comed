<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect()->route('solicitudes_medicas.create');
});

Route::resource('solicitudes_medicas','MedicalRequestController');
Route::get('solicitudes_medica', 'MedicalRequestController@view_call_center')
        ->name('call_center.solicitudes_medica');
Route::get('busca', 'MedicalRequestController@search_code_view')
        ->name('busca.solicitudes_medicas');
Route::get('solicitudes_medica/{id}', 'MedicalRequestController@view_edit_data_request')
        ->name('call_center.edicion_solicitudes_medica');

Route::post('buscar_codigo', 'MedicalRequestController@search_code')
        ->name('buscar_codigo.buscarcodigo');

Route::post('mostrar_timeline', 'MedicalRequestController@mostrar_timeline')
    ->name('buscar_codigo.mostrarTimeline');
Route::post('buscar_ci', 'MedicalRequestController@search_ci')
    ->name('buscar_ci.buscarci');
Route::post('buscar_solicitud', 'MedicalRequestController@search_medical_request')
    ->name('medical_request.search');
Route::post('buscar_empleado', 'MedicalRequestController@search_medical_request_employee')
    ->name('medical_request.search_more');
Route::get('imprimir/{code}', 'MedicalRequestController@print')
    ->name('medical_request.print');

Route::get('{id}/mostrar_archivos', 'MedicalRequestController@showFiles')
    ->name('mostrar_archivos.showFiles');

Route::get('getimage/{filename}', 'MedicalRequestController@displayImage')->name('image.displayImage');
Route::get('download_file/{filename}', 'MedicalRequestController@downloadFile')->name('image.downloadFile');

Route::get('importar_datos', 'MedicalRequestController@edit_import')
    ->name('importar_datos.edit');
Route::post('importar_datos', 'MedicalRequestController@update_import')
    ->name('importar_datos.update');
Route::group(['middleware' => ['auth']], function() {

    /**
     * RUTAS PARA LOS MEDICOS
     */
    Route::group(['middleware' => ['check_role:root,admin,medic,nurse']], function() {
        Route::resource('administracion_solicitudes','MedicalRequestAdministrationController');
        Route::prefix('administracion_solicitudes')->group(function () {
            Route::post('asignar_empleado', 'MedicalRequestAdministrationController@assignToEmploye')
                ->name('administracion_solicitudes.asignar_empleado.assignToEmploye');
            Route::post('asignar_medico', 'MedicalRequestAdministrationController@assignToMedic')
                ->name('administracion_solicitudes.asignar_medico.assignToMedic');
            Route::post('asignarme', 'MedicalRequestAdministrationController@assignToMe')
                ->name('administracion_solicitudes.asignarme.assignToMe');
            Route::post('no_encontrado', 'MedicalRequestAdministrationController@employeeNotFound')
                ->name('administracion_solicitudes.no_encontrado.employeeNotFound');
            Route::post('verificar_pre', 'MedicalRequestAdministrationController@verifyStatePreSol')
                ->name('administracion_solicitudes.verificar.verifyStatePreSol');
            Route::post('cerrar_pre', 'MedicalRequestAdministrationController@closeStatePreSol')
                ->name('administracion_solicitudes.cerrar_pre.closeStatePreSol');
            Route::post('cerrar_pre/automatic_close', 'MedicalRequestAdministrationController@closeChangeStateAuto')
                ->name('administracion_solicitudes.cerrar_pre.closeChangeStateAuto');
            Route::post('actualizar_tabla', 'MedicalRequestAdministrationController@updateRegisters')
                ->name('administracion_solicitudes.actualizar_tabla.updateRegisters');
            Route::post('importarExcel', 'MedicalRequestAdministrationController@importarExcel')
                ->name('administracion_solicitudes.importarExcel');
            Route::post('file', 'MedicalRequestAdministrationController@fileTmp')
                ->name('administracion_solicitudes.fileTmp');
            Route::post('file_remove', 'MedicalRequestAdministrationController@fileRemove')
                ->name('administracion_solicitudes.fileRemove');
        });

        Route::resource('consultas_medicas', 'MedicalConsultationsController')
            ->except(['show']);
        Route::prefix('consultas_medicas')->group(function () {
            Route::get('consultas_medicas/{medical_request_id}/get_attention_historial', 'MedicalConsultationsController@getAttentionHistorial')
                ->name('consultas_medicas.getAttentionHistorial');
            Route::get('realizadas', 'MedicalConsultationsController@indexMmedicalRequestClose')
                ->name('consultas_medicas.realizadas.indexMmedicalRequestClose');
            Route::get('agendar/citas', 'MedicalConsultationsController@indexMedicalRequestAgend')
                ->name('consultas_medicas.agendar');
            Route::get('modal/resumen', 'MedicalConsultationsController@getModalResumen')
                ->name('consultas_medicas.get_resumen');
            Route::get('consultas_medicas/imprimir/{attention_id}', 'MedicalConsultationsController@printAttention')
                ->name('consultas_medicas.imprimir');
            Route::get('get_signos_vitales/{medical_request_id}', 'MedicalConsultationsController@getSignosVitales')
                ->name('consultas_medicas.get_signos_vitales');

            Route::get('consultas_medicas/imprimir_receta/', 'MedicalConsultationsController@printPrescription')
                ->name('consultas_medicas.imprimir_receta');

        });
        Route::resource('antecedentes', 'AntecendentController');

    });

    Route::prefix('administracion_solicitudes')->group(function () {
        Route::get('{id}/mostrar_historial', 'MedicalRequestAdministrationController@showHistory')
            ->name('administracion_solicitudes.mostrar_historial.showHistory');
    });

    /**
     * RUTAS PARA LAS  ENFERMERAS Y MEDICOS
     */
    Route::group(['middleware' => ['check_role:root,admin,medic,nurse,resp_adm_seg,sup_admin,resp_seg,resp_adm_seg_resp_seg']], function() {

        Route::resource('citas', 'AppointmentController')
            ->except(['show']);
        Route::prefix('citas')->group(function () {
            Route::post('form', 'AppointmentController@getFormAppointment')
                ->name('citas.form.getFormAppointment');
            Route::post('{id}/edit_form', 'AppointmentController@getEditFormAppointment')
                ->name('citas.form.getEditFormAppointment');
            Route::post('get_appointments', 'AppointmentController@getAppointmentsFromUserDate')
                ->name('citas.get_appointments.getAppointmentsFromUserDate');
        });
    });

    Route::prefix('empleados')->group(function () {
        Route::get('busqueda_empresa/{enterprise_id}', 'EmployeeRRHHController@getEmployeeFromEnterprise')
            ->name('empleados.busqueda_empresa.getEmployeeFromEnterprise');
        Route::get('busqueda_carnet/{ci}/{ci_expedition}', 'EmployeeRRHHController@getEmployeeFromCI')
            ->name('empleados.busqueda_ci.getEmployeeFromCI');
    });

    /**
     * RUTAS PARA ENFERMERAS
     */
    Route::group(['middleware' => ['check_role:root,nurse,call_center,resp_adm_seg,sup_admin,resp_adm_seg_resp_seg']], function () {

        Route::resource('enfermera', 'NurseController')->except('show', 'destroy');
        Route::prefix('enfermera')->group(function () {
            Route::post('enfermera/buscar', 'NurseController@search_by_nurse')
                ->name('enfermera.search_by_nurse');
            Route::get('casos_doctor', 'NurseController@view_cases')
                ->name('enfermera.casos_doctor');
            Route::get('signos_vitales/{medical_request_id}', 'NurseController@getSignosVitales')
                ->name('enfermera.get_signos_vitales');
            Route::post('signos_vitales/store', 'NurseController@storeVitalSigns')
                ->name('enfermera.storeVitalSigns');
            Route::get('signos_vitales/edit/{medical_request_id}', 'NurseController@editSignosVitales')
                ->name('enfermera.editSignosVitales');
            Route::post('signos_vitales/update', 'NurseController@updateVitalSigns')
                ->name('enfermera.updateVitalSigns');
        });
    });

    /**
     * RUTAS DE ADMINISTRADORES
     */
    Route::group(['middleware' => ['check_role:root,admin']], function() {
        Route::resource('usuario', 'UserController');
        Route::post('medical_user', 'UserController@getpersonalmedical')
        ->name('usuario.getmedics');

        //Configuracion
        Route::prefix('configuracion')->group(function () {
            Route::resource('prioridades', 'MedicalRequestPrioritiesController');
            Route::resource('tipo', 'MedicalRequestTypesController');
            Route::resource('estado', 'MedicalRequestStatusController');
        });

        //Cvd States
        Route::resource('covid_estados', 'CovidStateController');

        //Treatment Stages
        Route::resource('tratamiento_etapas', 'TreatmentStageController');

        //Risk Types
        Route::resource('tipos_riesgo', 'RiskTypeController');

        //Laboratories
        Route::resource('laboratorios', 'LaboratoryController');

        //Weighting Levels
        Route::resource('niveles_ponderacion', 'WeightingLevelController');

        //Disease Criterias
        Route::resource('criterios_enfermedad', 'DiseaseCriteriaController');
        //Test Type
        Route::resource('tipos_prueba', 'TestTypeController');

        //Risk Stauses
        Route::resource('estados_riesgo', 'RiskStatusesController');

    });

    // Usuarios - Editar contraseña
    Route::get('usuario/editar.contrasena/{id}', 'UserController@editContrasena')
        ->name('usuario.edit_contrasena');
    Route::put('usuario/editar.contrasena/{id}', 'UserController@updateContrasena')
        ->name('usuario.update_contrasena');

    Route::get('/home', 'HomeController@index')->name('home');

    /**
     * RUTAS PARA LOS DE RECURSOS HUMANOS
     */
    Route::group(['middleware' => ['check_role:root,rrhh']], function() {
        Route::resource('consultas_medicas_pendientes', 'PendingRequestController')->except('show', 'destroy');
        Route::prefix('consultas_medicas_pendientes')->group(function () {
            Route::post('asignar_empleado', 'PendingRequestController@assignToEmployee')
                ->name('consultas_medicas_pendientes.asignar_empleado.assignToEmployee');
            Route::post('asignar', 'PendingRequestController@assign')
                ->name('consultas_medicas_pendientes.asignar.assign');
            Route::post('eliminar_solicitud', 'PendingRequestController@deleteRequest')
                ->name('consultas_medicas_pendientes.eliminar_solicitud.deleteRequest');
            Route::get('solicitudes_rechazadas', 'PendingRequestController@reject_request')
                ->name('solicitudes_rechazadas.rejectRequest');
            Route::get('solicitudes_reasignar', 'PendingRequestController@reassignment_request')
                ->name('solicitudes_reasignar.reassignmentRequest');
            Route::post('solicitudes_reasignar_post', 'PendingRequestController@reassignment_request_post')
                ->name('solicitudes_reasignar_post.reassignmentRequestPost');
        });
    });

    /**
     * RUTAS PARA EL RESPONSABLE DEL SEGUIMIENTO
     */
    Route::group(['middleware' => ['check_role:resp_seg,sup_admin,nurse,resp_adm_seg_resp_seg']], function() {
        Route::resource('seguimientos', 'TracingController')
            ->except('show', 'destroy');
        Route::prefix('seguimientos')->group(function () {
            //RE PROGRAMACION
            Route::post('{id}/get_reprogramming_form', 'TracingController@getReprogrammingFormFromMedicalRequest')
                ->name('seguimientos.get_reprogramming_form.getReprogrammingFormFromMedicalRequest');
            Route::post('{id}/update_reprogramming', 'TracingController@updateReprogramming')
                ->name('seguimientos.update_reprogramming.updateReprogramming');

        });
    });

    Route::group(['middleware' => ['check_role:resp_seg,sup_admin,nurse,medic,sup_admin,resp_adm_seg_resp_seg']], function() {
        Route::resource('seguimientos', 'TracingController')
            ->except('show', 'destroy');
        Route::prefix('seguimientos')->group(function () {
            //RE EMISION
            Route::post('{id}/get_remit_form', 'TracingController@getRemitFormFromMedicalRequest')
                ->name('seguimientos.get_remit_form.getRemitFormFromMedicalRequest');
            Route::post('{id}/update_remit', 'TracingController@updateRemit')
                ->name('seguimientos.update_remit.updateRemit');
        });
    });

    Route::prefix('seguimientos')->group(function () {
        Route::post('{id}/get_disease_tracing', 'TracingController@getDiseaseCriteriaFromTracing')
            ->name('seguimientos.get_disease_tracing.getDiseaseCriteriaFromTracing');
        Route::post('{id}/get_tracings_history', 'MedicalRequestController@getTracingsFromMedicalRequest')
            ->name('seguimientos.get_tracings_history.getTracingsFromMedicalRequest');
    });

    // ADMINISTRACION DE SEGUIMIENTOS
    Route::group(['middleware' => ['check_role:root,call_center,resp_adm_seg,sup_admin,resp_adm_seg_resp_seg']], function() {

        Route::resource('administracion_seguimientos', 'TracingAdministrationController')
            ->except(['show']);
        Route::prefix('administracion_seguimientos')->group(function () {
            Route::get('{medical_request_id}/get_patient', 'TracingAdministrationController@get_patient')
                ->name('administracion_seguimientos.get_patient');
            Route::post('store/medic_asign', 'TracingAdministrationController@storeMedic')
                ->name('administracion_seguimientos.storeMedic');
            Route::post('verificar_pre', 'TracingAdministrationController@verifyStatePreSol')
                ->name('administracion_seguimientos.verificar.verifyStatePreSol');
            Route::post('asignar_empleado', 'TracingAdministrationController@assignToEmploye')
                ->name('administracion_seguimientos.asignar_empleado.assignToEmploye');
            Route::post('cerrar_pre', 'TracingAdministrationController@closeStatePreSol')
                ->name('administracion_seguimientos.cerrar_pre.closeStatePreSol');
            Route::post('actualizar_tabla', 'TracingAdministrationController@updateRegisters')
                ->name('administracion_seguimientos.actualizar_tabla.updateRegisters');
            Route::post('cerrar_pre/automatic_close', 'TracingAdministrationController@closeChangeStateAuto')
                ->name('administracion_seguimientos.cerrar_pre.closeChangeStateAuto');
            Route::post('no_encontrado', 'TracingAdministrationController@employeeNotFound')
                ->name('administracion_seguimientos.no_encontrado.employeeNotFound');
            Route::post('asignarme', 'TracingAdministrationController@assignToMe')
                ->name('administracion_seguimientos.asignarme.assignToMe');
            Route::post('asignar_medico', 'TracingAdministrationController@assignToMedic')
                ->name('administracion_seguimientos.asignar_medico.assignToMedic');
            Route::post('{id}/cerrar_caso', 'TracingAdministrationController@closeCase')
                ->name('administracion_seguimientos.cerrar_caso.closeCase');

            Route::get('cerradas', 'TracingAdministrationController@indexMmedicalRequestClose')
                ->name('administracion_seguimientos.cerradas.indexMmedicalRequestClose');
            Route::get('rechazadas', 'TracingAdministrationController@index_rechazadas')
                ->name('administracion_seguimientos.cerradas.index_rechazadas');
            Route::post('get_medical_request', 'TracingAdministrationController@getMedicalRequestList')
                ->name('administracion_seguimientos.get_medical_request');
            Route::get('{medical_request_id}/get_ultimo_seguimiento', 'TracingAdministrationController@get_ultimo_seguimiento')
                ->name('administracion_seguimientos.get_ultimo_seguimiento');
            Route::post('store/seguimiento', 'TracingAdministrationController@storeSeguimiento')
                ->name('administracion_seguimientos.storeSeguimiento');
        });
    });

    Route::prefix('proyectos')->group(function () {
        Route::get('busqueda_proyectos/{enterprise_id}', 'ProjectSOLController@getProjectFromEnterprise')
            ->name('proyectos.busqueda_proyectos.getProjectFromEnterprise');
        Route::get('para_buscador/{enterprise_id}', 'ProjectSOLController@getProjectsFromEnterpriseSearch')
            ->name('proyectos.para_buscador.getProjectsFromEnterpriseSearch');
    });
    Route::prefix('tipos_riesgo')->group(function () {
        Route::get('busqueda_enfermedades/{enterprise_id}', 'RiskTypeController@getDisaeseCriteriaFromRiskType')
            ->name('tipos_riesgo.busqueda_enfermedades.getDisaeseCriteriaFromRiskType');
        Route::get('busqueda_estados_riesgo/{enterprise_id}', 'RiskTypeController@getRiskStatesFromRiskType')
            ->name('tipos_riesgo.busqueda_estados_resgo.getRiskStatesFromRiskType');
    });

    /**
     * REPORTES
     */
    Route::group(['middleware' => ['check_role:root,repo,,sup_admin']], function() {
        Route::get('reporte/casos', 'ReportController@index_casos')
            ->name('report.casos');
        Route::get('reporte/global', 'ReportController@globalMedicalRequest')
            ->name('report.global_medical_request');
        Route::get('reporte/estado_riesgo', 'ReportController@globalRiskState')
            ->name('report.global_risk_state');
        Route::get('reporte/casos_reportados', 'ReportController@globalReportCases')
            ->name('report.global_report_cases');

        // Por casos de riesgo
        Route::post('reporte/ajax_estados_riesgo', 'ReportController@ajaxEnterpriseRiskState')
            ->name('report.ajax_enterpriseRiskState');

        // Por casos reportados
        Route::post('reporte/ajax_casos_reportados', 'ReportController@ajaxReportCases')
            ->name('report.ajax_reportCases');

        // Comparación entre empresas
        Route::post('reporte/ajax_casos_empresas', 'ReportController@ajaxEnterpriseCases')
            ->name('report.ajax_enterpriseCases');
    });

    /**
     * REGULARIZACIONES
     */
    Route::group(['middleware' => ['check_role:root']], function() {
        Route::get('regularizacion_proyecto_solicitud', 'RegularizationController@regularizationProjectsMedicalRequests')
            ->name('reg.project_medical_request');
    });

    /**
     * VACUNAS
     */
    Route::group(['middleware' => ['check_role:root']], function() {
        Route::resource('tipos_vacuna', 'VaccineTypeController');
    });

});
Auth::routes();
