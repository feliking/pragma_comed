function mostrarHistorial(id) {
    var url = showHistory;
    url = url.replace('_id',id);

    $.ajax({
        url: url,
        method: 'get',
        // headers: {'X-CSRF-TOKEN': token},
        beforeSend: function(e){
            // Loading
            // blockCard('#form_content');
        }
    })
    .done(function(response) {
        switch (response['type']) {
            case "correct":

                $('div#body_modal_history').empty();
                $('div#body_modal_history').append(response['view']);

                $('div#modal_history').modal('show');
                break;
            case "error":
                toastr.error(
                    response['mje'],
                    '¡Error!'
                );
                break;
        }
        // unblockCard('#form_content');
    })
    .fail(function() {
        // unblockCard('#form_content');
    });
}
