function mostrarHistorialTimeline(id) {
    var url = showHistoryTimeline;
    var token = $("meta[name=csrf-token]").attr("content");

    $.ajax({
        url: url,
        method: 'post',
        data: { code: id} ,
        headers: {'X-CSRF-TOKEN': token},
        beforeSend: function(e){
            // Loading
            // blockCard('#form_content');
        }
    })
    .done(function(response) {

        $('div#body_modal_history_timeline').empty();
        $('div#body_modal_history_timeline').append(response);

        $('div#modal_history_timeline').modal('show');
    })
    .fail(function() {
        // unblockCard('#form_content');
    });
}
