var start_tracing_date_search = null;
var end_tracing_date_search = null;

$(document).ready(function() {
    $('#medical_request_id_search').select2({
        theme: 'bootstrap4',
        placeholder: 'Seleccione un estado',
        with: 'auto'
    });

    $('#enterprise_id_search').select2({
        theme: 'bootstrap4',
        placeholder: 'Seleccione una empresa',
        with: 'auto'
    });

    $('#project_id_search').select2({
        theme: 'bootstrap4',
        placeholder: 'Seleccione un proyecto',
        with: 'auto'
    });

    $('#weighting_level_id_search').select2({
        theme: 'bootstrap4',
        placeholder: 'Seleccione un riesgo',
        with: 'auto'
    });

    $('select[name^=risk_state_id_search]').select2({
        theme: 'bootstrap4',
        placeholder: 'Seleccione una etapa',
        with: 'auto'
    }).val('').trigger('change');

    $('#option_date_search').select2({
        theme: 'bootstrap4',
        placeholder: 'Seleccione una opción',
        with: 'auto'
    });

    $('#start_date_search').datetimepicker({
        format: 'DD/MM/YYYY',
    });
    $('#end_date_search').datetimepicker({
        useCurrent: false,
        format: 'DD/MM/YYYY',
    });
    $("#start_date_search").on("change.datetimepicker", function (e) {
        $('#end_date_search').datetimepicker('minDate', e.date);
        // getValues();
    });
    $("#end_date_search").on("change.datetimepicker", function (e) {
        $('#start_date_search').datetimepicker('maxDate', e.date);
        // getValues();
    });

    //INICIO: OPCIONES BUSCADOR DE FECHAS DE SEGUIMIENTOS
    $('#start_tracing_date_search').datetimepicker({
        format: 'DD/MM/YYYY',
    });
    $('#end_tracing_date_search').datetimepicker({
        useCurrent: false,
        format: 'DD/MM/YYYY',
    });
    $("#start_tracing_date_search").on("change.datetimepicker", function (e) {
        $('#end_tracing_date_search').datetimepicker('minDate', e.date);
        start_tracing_date_search = moment(e.date._d).format('YYYY-MM-DD') || null;
        // getValues();
    });
    $("#end_tracing_date_search").on("change.datetimepicker", function (e) {
        $('#start_tracing_date_search').datetimepicker('maxDate', e.date);
        end_tracing_date_search = moment(e.date._d).format('YYYY-MM-DD') || null;
        // getValues();
    });
    //FIN: OPCIONES BUSCADOR DE FECHAS DE SEGUIMIENTOS
});

$('#medical_request_id_search').change(function(event) {
    // getValues();
});

$('#enterprise_id_search').change(function(event) {

    var selected_op = $('#enterprise_id_search option:selected').val();
    if(selected_op!=null && selected_op!=""){
        var url = forSearchProjects.replace('_id',selected_op);

        $.ajax({
            url: url,
            method: 'get',
            beforeSend: function(e){
                // Loading
                // blockCard('#form_content');
                $('select[name=project_id_search]').attr('disabled', 'disabled');
            }
        })
        .done(function(response) {
            $('select[name=project_id_search]').empty();

            $.each(response['projects'], function (index, value) {
                var newOption = new Option(value, index, false, false);
                $('select[name=project_id_search]').append(newOption);
            });

            $('select[name=project_id_search]').trigger('change');
            $('select[name=project_id_search]').removeAttr('disabled');
            // unblockCard('#form_content');
        })
        .fail(function() {
            // unblockCard('#form_content');
        });
    }else{
        $('select[name=project_id_search]').empty();
        $('select[name=project_id_search]').val('').trigger('change');
        $('select[name=project_id_search]').attr('disabled', 'disabled');
    }

    // getValues();
});

$('#project_id_search').change(function(event) {
    // getValues();
});

$('#option_date_search').change(function(event) {
    // getValues();
});

$('#weighting_level_id_search').change(function(event) {
    // getValues();
});

$('select[name^=risk_state_id_search]').change(function(event) {
    // getValues();
});
