$('select[name=appointment_type_id]').change(function (e) {
    var selected_op = $('select[name=appointment_type_id]').val();
    if(selected_op!=null && selected_op!=""){
        hideVariableText();
        switch (parseInt(selected_op)) {
            case 1: //PRESENCIAL
                break;
            case 2: //ZOOM
                $('input[name=variable_whatsapp]').val('');
                $('input[name=variable_whatsapp]').prop('required',false);
                $('input[name=variable_whatsapp]').prop('disabled',true);

                $('input[name=variable_zoom]').prop('disabled',false);
                $('input[name=variable_zoom]').prop('required',true);
                $('div#div_zoom').removeAttr('hidden');
                break;
            case 3: //WHATSAPP
                $('input[name=variable_zoom]').val('');
                $('input[name=variable_zoom]').prop('required',false);
                $('input[name=variable_zoom]').prop('disabled',true);

                $('input[name=variable_whatsapp]').prop('disabled',false);
                $('input[name=variable_whatsapp]').prop('required',true);
                $('div#div_whatsapp').removeAttr('hidden');
                break;
        }
    }else{
        // hideVariableText();
    }
});

function hideVariableText() {
    $('.variable_text').each(function (index, value) {
        $(value).attr('hidden', true);
    });
}
