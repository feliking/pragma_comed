function mostrarAppointment(medical_request_id, object) {
    $('button#btnRegistrarCitaMedica').attr('onclick','').unbind('click');
    var button_cita_medica = document.getElementById('btnRegistrarCitaMedica');
    button_cita_medica.innerHTML = "<i class='fa fa-save'></i> Registrar";
    button_cita_medica.className = "btn btn-success";

    if(!$(object).data('function')){
        button_cita_medica.setAttribute("onClick","registrarCita("+medical_request_id+")");
    }else{
        button_cita_medica.setAttribute("onClick",$(object).data('function'));
    }

    var url = getForm;
    var token = $("meta[name=csrf-token]").attr("content");

    $.ajax({
        url: url,
        method: 'post',
        data: {
            medical_request_id:medical_request_id,
        },
        headers: {'X-CSRF-TOKEN': token},
        beforeSend: function(e){
            // Loading
            // blockCard('#form_content');
        }
    })
    .done(function(response) {
        $('div#body_modal_assing_appointment').empty();
        $('div#body_modal_assing_appointment').append(response['form']);

        $('div#modal_assing_appointment').modal('show');

        //INICIALIZACION DE VARIABLES
        inicializarCampos();
    })
    .fail(function() {
        toastr.error(
            'Ocurrio un error al iniciar el formulario',
            '¡Error!'
        );
    });
}

function editarAppointment(medical_request_id, appointment_id,object) {
    $('button#btnRegistrarCitaMedica').attr('onclick','').unbind('click');
    var button_cita_medica = document.getElementById('btnRegistrarCitaMedica');
    button_cita_medica.innerHTML = "<i class='fa fa-edit'></i> Editar";
    button_cita_medica.className = "btn btn-warning";

    if(!$(object).data('function')){
        button_cita_medica.setAttribute("onClick","editarCita("+medical_request_id+","+appointment_id+")");
    }else{
        button_cita_medica.setAttribute("onClick",$(object).data('function'));
    }

    var url = getEditForm;
    url = url.replace('_id',appointment_id);
    var token = $("meta[name=csrf-token]").attr("content");

    $.ajax({
        url: url,
        method: 'post',
        data: {
            medical_request_id:medical_request_id,
        },
        headers: {'X-CSRF-TOKEN': token},
        beforeSend: function(e){
            // Loading
            // blockCard('#form_content');
        }
    })
        .done(function(response) {
            $('div#body_modal_assing_appointment').empty();
            $('div#body_modal_assing_appointment').append(response['form']);

            $('div#modal_assing_appointment').modal('show');

            //INICIALIZACION DE VARIABLES
            inicializarCampos();
        })
        .fail(function() {
            toastr.error(
                'Ocurrio un error al iniciar el formulario',
                '¡Error!'
            );
        });
}

function registrarCita(medical_request_id) {
    var forms = document.getElementById('form-appointment');
    var form_data = $(forms).serialize();

    var url = storeForm;
    var token = $("meta[name=csrf-token]").attr("content");

    form_data = form_data+'&'+$.param({
        medical_request_id: medical_request_id
    });
    $.ajax({
        url: url,
        headers: {'X-CSRF-TOKEN': token},
        data: form_data,
        method: 'post',
        beforeSend: function(){

        }
    }).done(function (response){
        switch (response['type']) {
            case "correct":
                // removeRowsTable(response['filas']);
                // updateRowsTable(response['filas_actualizadas']);

                updateRowsTable(response['filas']);
                $('[data-toggle="tooltip"]').tooltip();

                $('div#modal_assing_appointment').modal('hide');

                toastr.success(
                    response['msj'],
                    '¡Cita completa!'
                );
                break;
            case "error":
                toastr.error(
                    response['msj'],
                    '¡Error!'
                );
                break;
        }
    }).fail(function (response){
        toastr.error(
            'Revise el formulario',
            '¡Error!'
        );
        if(response.responseJSON['errors']){
            $.each(response.responseJSON['errors'], function(index, value){
                $('[name="'+ index +'"]').addClass('is-invalid');
                $('[name="'+ index +'"]').siblings('span.invalid-feedback').html('<strong>'+value+'</strong>');
            });
        }
    });
}

function editarCita(medical_request_id, appointment_id) {
    var forms = document.getElementById('form-appointment');
    var form_data = $(forms).serialize();

    var url = updateForm;
    var token = $("meta[name=csrf-token]").attr("content");
    url = url.replace('_id',appointment_id);

    form_data = form_data+'&'+$.param({
        medical_request_id: medical_request_id
    });
    $.ajax({
        url: url,
        headers: {'X-CSRF-TOKEN': token},
        data: form_data,
        method: 'put',
        beforeSend: function(){

        }
    }).done(function (response){
        switch (response['type']) {
            case "correct":
                // removeRowsTable(response['filas']);
                // updateRowsTable(response['filas_actualizadas']);

                updateRowsTable(response['filas']);
                $('[data-toggle="tooltip"]').tooltip();

                $('div#modal_assing_appointment').modal('hide');

                toastr.success(
                    response['msj'],
                    '¡Cita completa!'
                );

                break;
            case "error":
                toastr.error(
                    response['msj'],
                    '¡Error!'
                );
                break;
        }
    }).fail(function (response){
        toastr.error(
            'Revise el formulario',
            '¡Error!'
        );
        if(response.responseJSON['errors']){
            $.each(response.responseJSON['errors'], function(index, value){
                $('[name="'+ index +'"]').addClass('is-invalid');
                $('[name="'+ index +'"]').siblings('span.invalid-feedback').html('<strong>'+value+'</strong>');
            });
        }
    });
}

function inicializarCampos() {
    /*$('input[name=next_attention_date]').daterangepicker({
        "singleDatePicker": true,
        "showDropdowns": true,
        "autoApply": true,
        "linkedCalendars": false,
        "showCustomRangeLabel": false,
        "drops": "up",
        "minDate": new Date(),
        autoUpdateInput: false,
        timePicker: true,
        timePickerIncrement: 15,
        locale: {
            format: 'DD/MM/YYYY hh:mm A',
            applyLabel: "Aceptar",
            cancelLabel: "Cancelar",
            fromLabel: "Desde",
            toLabel: "Hasta",
            customRangeLabel: "Custom",
            daysOfWeek: [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            monthNames: [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
        }
    }, function(start, end, label) {
        this.element.val(start.format('DD/MM/YYYY hh:mm A'));
    });*/
    $('#next_attention_date').datetimepicker({
        format: 'DD/MM/YYYY',
    });

    $('#next_attention_time').datetimepicker({
        format: 'LT'
    });

    $('[data-mask]').inputmask({
        removeMaskOnSubmit: true,
    });
    $('select[name=appointment_type_id]').select2({
        theme: 'bootstrap4',
        placeholder: '* Seleccione el tipo de atención'
    });

}

function updateRowsTable(filas) {
    $.each(filas, function (index, value) {
        $('#tr_medical_consultation_'+index).empty();
        $('#tr_medical_consultation_'+index).append(value);
    });
}
