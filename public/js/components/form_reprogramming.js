$('form#frm-reprogramming').validate({
    rules: {
        reprogramming_comment: {
            required: true,
            minlength: 10,
            maxlength: 255,
        },
        next_reprogramming_time: {
            required: true,
        }
    },
    messages: {
        reprogramming_comment: {
            required: 'El campo "Comentario u Observación" es obligatorio.',
            minlength: 'El campo "Comentario u Observación" completo debe contener al menos 10 caracteres.',
            maxlength: 'El campo "Comentario u Observación" completo no debe contener más de 255 caracteres.',
        },
        next_reprogramming_time:{
            required: 'El campo "Hora de atención" es obligatorio.',
        }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
        element.closest('.input-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    }
});
