$('form#frm-remit').validate({
    rules: {
        comment_remit: {
            required: true,
            minlength: 10,
            maxlength: 255,
        },
    },
    messages: {
        comment_remit: {
            required: 'El campo "Comentario u Observación" es obligatorio.',
            minlength: 'El campo "Comentario u Observación" completo debe contener al menos 10 caracteres.',
            maxlength: 'El campo "Comentario u Observación" completo no debe contener más de 255 caracteres.',
        },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
        element.closest('.input-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    }
});
