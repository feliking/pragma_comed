function blockCard(block_ele){
    var block_ele = $(block_ele).closest('.card');
    $(block_ele).block({
        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'transparent'
        }
    });
}

function unblockCard(block_ele){
    var block_ele = $(block_ele).closest('.card');
    $(block_ele).unblock();
}

function getTableRender(url, data = '', clase=''){
    $.ajax({
        url: url,
        cache: false,
        data: data,
        // cache:  false,
        method: 'get',
        beforeSend: function(){
            blockCard('div#wrapper_table');
        }
    }).done(function (response){
        $('div.card-body div#wrapper_table').html(response.table);
        unblockCard('div#wrapper_table');
    }).fail(function (response){
        toastr.error(
            response,
            '¡Error!'
        );
        unblockCard('div#wrapper_table');
    });
}
