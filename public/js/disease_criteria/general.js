$('form#frm-diseas_id').validate({
    rules: {
        syntom: {
            required: true,
            minlength: 3,
            maxlength: 191,
        },
        weighing: {
            required: true,
            minlength: 2,
            maxlength: 255,
        },
        risk_type_id: {
            required: true,
            /* minlength: 2,
            maxlength: 255, */
        }
    },
    messages: {
        syntom: {
            required: 'El campo de síntoma es obligatorio.',
            minlength: 'El campo síntoma debe contener al menos 5 caracteres.',
            maxlength: 'El campo síntoma no debe contener más de 191 caracteres.',
        },
        weighing: {
            required: 'El campo peso es obligatorio.',
            minlength: 'El campo peso debe contener al menos 2 caracteres.',
            maxlength: 'El campo peso no debe contener más de 255 caracteres.',
        },
        risk_type_id: {
            required: 'El campo tipo de riesgo es obligatorio.',
        }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
        element.closest('.input-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    }
});
