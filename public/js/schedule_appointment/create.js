$('input[type=checkbox][name^=asigna]').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
    radioClass: 'iradio_flat-blue'
});
$('input[type=checkbox][name^=asigna]').on('ifChecked', function(event){
    $('div#dates').removeAttr('hidden');
    //$('div#doctor_staff').attr('hidden', true);
});
$('input[type=checkbox][name^=asigna]').on('ifUnchecked', function(event){
    $('div#dates').attr('hidden', true);
    $('input[name=next_attention_date]').val('');
    $('select#user_id').val('').trigger('change');
    $('select[name=appointment_type_id]').val('').trigger('change');
});
$('input[name=next_attention_date]').daterangepicker({
    "singleDatePicker": true,
    "showDropdowns": true,
    "autoApply": true,
    "linkedCalendars": false,
    "showCustomRangeLabel": false,
    "drops": "up",
    "minDate": new Date(),
    autoUpdateInput: false,
    timePicker: true,
    timePickerIncrement: 15,
    locale: {
        format: 'DD/MM/YYYY hh:mm A',
        applyLabel: "Aceptar",
        cancelLabel: "Cancelar",
        fromLabel: "Desde",
        toLabel: "Hasta",
        customRangeLabel: "Custom",
        daysOfWeek: [
            "Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa"
        ],
        monthNames: [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ],
    }
}, function(start, end, label) {
    this.element.val(start.format('DD/MM/YYYY hh:mm A'));
});

$('select[name=appointment_type_id]').change(function (e) {
    var selected_op = $('select[name=appointment_type_id]').val();
    if(selected_op!=null && selected_op!=""){
        hideVariableText();
        switch (parseInt(selected_op)) {
            case 1: //PRESENCIAL
                break;
            case 2: //ZOOM
                $('input[name=variable_whatsapp]').val('');
                $('input[name=variable_whatsapp]').prop('required',false);
                $('input[name=variable_whatsapp]').prop('disabled',true);

                $('input[name=variable_zoom]').prop('disabled',false);
                $('input[name=variable_zoom]').prop('required',true);
                $('div#div_zoom').removeAttr('hidden');
                break;
            case 3: //WHATSAPP
                $('input[name=variable_zoom]').val('');
                $('input[name=variable_zoom]').prop('required',false);
                $('input[name=variable_zoom]').prop('disabled',true);

                $('input[name=variable_whatsapp]').prop('disabled',false);
                $('input[name=variable_whatsapp]').prop('required',true);
                $('div#div_whatsapp').removeAttr('hidden');
                break;
        }
    }else{
        // hideVariableText();
    }
});
function hideVariableText() {
    $('.variable_text').each(function (index, value) {
        $(value).attr('hidden', true);
    });
}
