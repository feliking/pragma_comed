
function abrirModalSignosVitales (medical_request_id, tipo) {
    // if (tipo == 1) {
    //     var url = getSignosVitales.replace('_ID',medical_request_id);
    // } else {
    //     var url = editVitalSigns.replace('_ID',medical_request_id);
    // }
    var url = getSignosVitales.replace('_ID',medical_request_id);
    $.ajax({
        url: url,
        beforeSend: function(e){
            //
        }
    })
    .done(function(response) {
        $('#frm_content_signos_vitales').empty();
        $('#frm_content_signos_vitales').append(response.content);
        $('#medical_request_id').val(medical_request_id);
        $('#modal_signos_vitales').modal({
            show: true
        });
        $('#btn_guardar_signos').removeAttr('hidden');
        $('#btn_editar_signos').attr('hidden', 'hidden');
        // if (tipo == 1) {
        //     $('#btn_guardar_signos').removeAttr('hidden');
        //     $('#btn_editar_signos').attr('hidden', 'hidden');
        // } else {
        //     $('#btn_editar_signos').removeAttr('hidden');
        //     $('#btn_guardar_signos').attr('hidden', 'hidden');
        // }
        $(function () {
            $(":input").inputmask();
        });
    })
    .fail(function() {
        toastr.error(
                'Error al cargar el mensaje',
                '¡Error!'
            );
    })
    .always(function() {
    });
}


function registrar_signos_vitales () {
    var data = $("#frm_signos_vitales").serialize();
    var token = $("meta[name=csrf-token]").attr("content");
    var url = storeVitalSigns;

    $.ajax({
        url: url,
        method: 'post',
        headers: {'X-CSRF-TOKEN': token},
        data:data,
        beforeSend: function(e){
            // Loading
            // blockCard('#form_content');
        }
    })
    .done(function(response) {
        switch (response['type']) {
            case "correct":
                updateRowsTable(response['filas']);

                toastr.success(
                    response['mje'],
                    '¡Registro de signos vitales!'
                );
                $('#modal_signos_vitales').modal('hide');
                break;
            case "error":
                toastr.error(
                    response['mje'],
                    '¡Error!'
                );
                break;
        }
        // unblockCard('#form_content');
    })
    .fail(function(response) {
        toastr.error(
            response,
            '¡Error!'
        );
        if(response.responseJSON['errors']){
            $.each(response.responseJSON['errors'], function(index, value){
                $('[name="'+ index +'"]').addClass('is-invalid');
                $('[name="'+ index +'"]').siblings('span.invalid-feedback').html('<strong>'+value+'</strong>');
            });
        }
    });
}


function editar_signos_vitales () {
    var data = $("#frm_signos_vitales").serialize();
    var token = $("meta[name=csrf-token]").attr("content");
    var url = updateVitalSigns;

    $.ajax({
        url: url,
        method: 'post',
        headers: {'X-CSRF-TOKEN': token},
        data:data,
        beforeSend: function(e){
            // Loading
            // blockCard('#form_content');
        }
    })
    .done(function(response) {
        switch (response['type']) {
            case "correct":
                updateRowsTable(response['filas']);

                toastr.success(
                    response['mje'],
                    '¡Modificación de signos vitales correcto!'
                );
                $('#modal_signos_vitales').modal('hide');
                break;
            case "error":
                toastr.error(
                    response['mje'],
                    '¡Error!'
                );
                break;
        }
        // unblockCard('#form_content');
    })
    .fail(function(response) {
        toastr.error(
            response,
            '¡Error!'
        );
        if(response.responseJSON['errors']){
            $.each(response.responseJSON['errors'], function(index, value){
                $('[name="'+ index +'"]').addClass('is-invalid');
                $('[name="'+ index +'"]').siblings('span.invalid-feedback').html('<strong>'+value+'</strong>');
            });
        }
    });
}
function getTableRender1(url, data = '', clase=''){
    $.ajax({
        url: url,
        cache: false,
        data: data,
        // cache:  false,
        method: 'get',
        beforeSend: function(){
            blockCard('div#wrapper_table');
        }
    }).done(function (response){
        $('div.card-body div#wrapper_table').html(response.table);
        unblockCard('div#wrapper_table');
    }).fail(function (response){
        toastr.error(
            response,
            '¡Error!'
        );
        unblockCard('div#wrapper_table');
    });
}