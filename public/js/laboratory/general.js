$('form#frm-laboratory_id').validate({
    rules: {
        name: {
            required: true,
            minlength: 3,
            maxlength: 191,
        },
        description: {
            //required: true,
            minlength: 5,
            maxlength: 255,
        }
    },
    messages: {
        name: {
            required: 'El campo de nombre es obligatorio.',
            minlength: 'El campo nombre completo debe contener al menos 5 caracteres.',
            maxlength: 'El campo nombre completo no debe contener más de 191 caracteres.',
        },
        description: {
            //required: 'El campo descripción es obligatorio.',
            minlength: 'El campo descripción debe contener al menos 5 caracteres.',
            maxlength: 'El campo descripción no debe contener más de 255 caracteres.',
        }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
        element.closest('.input-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    }
});
