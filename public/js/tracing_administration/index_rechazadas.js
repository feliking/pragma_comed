var oTable = $('#medical_requests_administration').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "responsive": true,
    //ESTA OPCION DE DOM ES PARA ESCOGER ELEMENTOS QUE APARECEN POR DEFECTO EN EL DATA TABLE
    "dom": 'lrtp',
    "pageLength": 10
});
$('input#search_data').keyup(function () {
    oTable.search($(this).val()).draw();
});
