var oTable_adm = $('#medical_requests_administration_tracing').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "responsive": true,
    //ESTA OPCION DE DOM ES PARA ESCOGER ELEMENTOS QUE APARECEN POR DEFECTO EN EL DATA TABLE
    "dom": 'lrtp',
    "pageLength": 10
});
$('input#search_data_seguimiento').keyup(function () {
    oTable_adm.search($(this).val()).draw();
});

var oTable = $('#medical_requests_administration').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "responsive": true,
    //ESTA OPCION DE DOM ES PARA ESCOGER ELEMENTOS QUE APARECEN POR DEFECTO EN EL DATA TABLE
    "dom": 'lrtp',
    "pageLength": 10
});
$('input#search_data').keyup(function () {
    oTable.search($(this).val()).draw();
});


function abrirModalAsignacion (medical_request_id) {
    var url = patientAsign.replace('_ID',medical_request_id);
    $.ajax({
        url: url,
        beforeSend: function(e){
            //
        }
    })
    .done(function(response) {
        $('#frm_content').empty();
        $('#frm_content').append(response.content);
        $('#modal_patient_assign').modal({
            show: true
        });
    })
    .fail(function() {
        toastr.error(
                'Error al cargar el mensaje',
                '¡Error!'
            );
    })
    .always(function() {
    });
}


function registrar_seguimiento () {
    var data = $("#frm_atencion").serialize();
    var token = $("meta[name=csrf-token]").attr("content");
    var url = patientAsignStore;
    $.ajax({
        url: url,
        method: 'post',
        headers: {'X-CSRF-TOKEN': token},
        data:data,
        beforeSend: function(e){
            // Loading
            // blockCard('#form_content');
        }
    })
    .done(function(response) {
        switch (response['type']) {
            case "correct":
                console.log('response',response.status);
                removeRowsTable(response['filas']);
                toastr.success(
                    response['mje'],
                    '¡Asignación completa!'
                );
                $('#modal_patient_assign').modal('hide');
                break;
            case "error":
                toastr.error(
                    response['mje'],
                    '¡Error!'
                );
                break;
        }
        // unblockCard('#form_content');
    })
    .fail(function(response) {
        console.log('response', response);
        toastr.error(
            response,
            '¡Error!'
        );
    });
}

function registrar_cita () {
    var data = $("#frm_atencion").serialize();
    var token = $("meta[name=csrf-token]").attr("content");
    var url = patientAsignMedicStore;
    $.ajax({
        url: url,
        method: 'post',
        headers: {'X-CSRF-TOKEN': token},
        data:data,
        beforeSend: function(e){
            // Loading
            // blockCard('#form_content');
        }
    })
    .done(function(response) {
        switch (response['type']) {
            case "correct":
                console.log('response',response.status);

                removeRowsTable(response['filas']);

                toastr.success(
                    response['mje'],
                    '¡Asignación completa!'
                );
                $('#modal_patient_assign').modal('hide');
                break;
            case "error":
                toastr.error(
                    response['mje'],
                    '¡Error!'
                );
                break;
        }
    })
    .fail(function(response) {
        toastr.error(
            response,
            '¡Error!'
        );
        if(response.responseJSON['errors']){
            $.each(response.responseJSON['errors'], function(index, value){
                $('[name="'+ index +'"]').addClass('is-invalid');
                $('[name="'+ index +'"]').siblings('span.invalid-feedback').html('<strong>'+value+'</strong>');
            });
        }
    });
}


function removeRowsTable(filas) {
    $.each(filas, function (index, value) {
        $('#tr_medical_consultation_'+index).remove();
    });
}
