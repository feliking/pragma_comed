$('form#frm-seguimientos_id').validate({
    rules: {
        work_situation_id: {
            required: true,
        },
        treatment_stage_id: {
            required: true,
        },
        enterprise_id: {
            required: true,
        },
        project_id: {
            required: true,
        },
        risk_type_id: {
            required: true,
        },
        risk_state_id: {
            required: true,
        },
    },
    messages: {
        work_situation_id: {
            required: 'El campo de situación laboral es obligatoria.',
        },
        treatment_stage_id: {
            required: 'El campo etapa de tratamiento es obligatorio.',
        },
        enterprise_id: {
            required: 'El campo de empresa es obligatoria.',
        },
        project_id: {
            required: 'El campo de proyecto es obligatoria.',
        },
        risk_type_id: {
            required: 'El campo de tipo de riesgo es obligatoria.',
        },
        risk_state_id: {
            required: 'El campo de tipo de riesgo es obligatoria.',
        },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
        element.closest('.input-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    }
});
