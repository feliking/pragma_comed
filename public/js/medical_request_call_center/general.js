//Initialize Select2 Elements
$('select[name="ci_expedition"]').select2({
    theme: 'bootstrap4'
});

$('select[name="whatsapp_flag"]').select2({
    theme: 'bootstrap4',
    placeholder: '* ¿Su número cuenta con Whatsapp?'
});

$('select[name="gender"]').select2({
    theme: 'bootstrap4',
    placeholder: '* Seleccione su género'
});

$('select[name="risk_type_id"]').select2({
    theme: 'bootstrap4',
    placeholder: '* Seleccione el motivo de su consulta'
});

$('select[name="health_insurance_id"]').select2({
    theme: 'bootstrap4',
    placeholder: '* Seleccione su seguro médico'
});

// Init
$(document).ready(function () {
    //Date range picker
    $('#birthday_field').datetimepicker({
        viewMode: 'years',
        format: 'DD/MM/YYYY',
        locale: 'es',
        useCurrent: false,
        maxDate: moment(),
        minDate: moment().subtract(100, 'y')
    });
});

// Validación
$.validator.setDefaults({
    submitHandler: function (form) {
        // console.log($(form).attr('id'));
        // alert($(form));

        if ($(form).attr('id') == 'frm-medical_request')
        {
            $('form#frm-medical_request button[type="submit"]')
                .attr('disabled', true)
                .html("Enviando la solicitud, un momento por favor...");

            $(form).unbind().submit();
        }
    }
});

$('form#frm-medical_request').validate({
    rules: {
        ci: {
            required: true,
            minlength: 7,
            maxlength: 11,
        },
        ci_expedition: {
            required: true,
        },
        gender: {
            required: true
        },
        birthday: {
            required: true,
        },
        enterprise: {
            required: true,
            minlength: 3,
            maxlength: 190,
        },
        health_insurance_id: {
            required: true
        },
        email: {
            email: true,
        },
        employee_code: {
            minlength: 1,
            maxlength: 5,
        },
        full_name: {
            required: true,
            minlength: 5,
            maxlength: 190,
        },
        phone_number: {
            required: true,
            minlength: 4,
            maxlength: 8,
        },
        whatsapp_flag: {
            required: true,
        },
        comment: {
            maxlength: 190,
        },
        risk_type_id: {
            required: true,
        }
    },
    messages: {
        ci: {
            required: 'El campo cédula de indentidad es obligatorio.',
            minlength: 'El campo cédula de indentidad debe contener al menos 7 caracteres.',
            maxlength: 'El campo cédula de indentidad no debe contener más de 11 caracteres.',
        },
        ci_expedition: {
            required: 'El campo expedición de la cédula de identidad es obligatorio.',
        },
        gender: {
            required: 'El campo género es obligatorio.'
        },
        birthday: {
            required: 'El campo fecha de nacimiento es obligatorio.',
        },
        enterprise: {
            required: 'El campo empresa es obligatorio.',
            minlength: 'El campo empresa debe contener al menos 3 caracteres.',
            maxlength: 'El campo empresa no debe contener más de 190 caracteres.',
        },
        health_insurance_id: {
            required: 'El campo seguro médico es obligatorio.'
        },
        email: {
            email: 'El campo correo electrónico debe ser una dirección de correo válida.',
        },
        employee_code: {
            minlength: 'El campo código de empleado debe contener al menos 1 caracteres.',
            maxlength: 'El campo código de empleado no debe contener más de 5 caracteres.',
        },
        full_name: {
            required: 'El campo nombre completo es obligatorio.',
            minlength: 'El campo nombre completo debe contener al menos 5 caracteres.',
            maxlength: 'El campo nombre completo no debe contener más de 190 caracteres.',
        },
        phone_number: {
            required: 'El campo número de teléfono es obligatorio.',
            minlength: 'El campo número de teléfono debe contener al menos 4 caracteres.',
            maxlength: 'El campo número de teléfono no debe contener más de 8 caracteres.',
        },
        whatsapp_flag: {
            required: 'El campo whatsapp es obligatorio.',
        },
        comment: {
            maxlength: 'El campo comentario u observación no debe contener más de 190 caracteres.',
        },
        risk_type_id: {
            required: 'El campo motivo de consulta es obligatorio.',
        }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
        element.closest('.input-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    }
});

// Search
function search_by_nurse ()
{
    $.ajax({
        url: url_search_by_nurse,
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr("content") },
        method: 'POST',
        dataType: 'JSON',
        data: 'ci=' + $('input[name="ci"]').val() + '&ci_expedition=' + $('select[name="ci_expedition"]').val(),
        beforeSend: function (e) {

        }
    }).done(function (response) {
        //$('form#frm-reqsol_id').trigger('reset');
        $('input[name="ci"]').val('');
        $('select[name="ci_expedition"]').val('LP').trigger('change');
        $('input[name="full_name"]').val('');
        $('select[name="gender"]').val('').trigger('change');
        $('input[name="birthday"]').val('');
        $('input[name="enterprise"]').val('');
        $('input[name="employee_code"]').val('');
        $('select[name="health_insurance_id"]').val('').trigger('change');
        $('input[name="phone_number"]').val('');
        $('select[name="whatsapp_flag"]').val('').trigger('change');
        $('input[name="email"]').val('');
        $('input[name="short_phone_number"]').val('');
        $('input[name="reference_full_name"]').val('');
        $('input[name="reference_phone_number"]').val('');

        // Medical Request
        if (response.msg_type == 'success')
        {
            if (response.medical_request != null)
            {
                $('input[name="ci"]').val(response.employee.ci_numero);
                $('select[name="ci_expedition"]').val(response.employee.ci_expedido).trigger('change');
                $('input[name="full_name"]').val(response.employee.full_name);
                $('select[name="gender"]').val(response.medical_request.gender).trigger('change');

                var birthday = moment(response.medical_request.birthday);
                $('input[name="birthday"]').val(birthday.format('DD/MM/YYYY'));
                $('input[name="enterprise"]').val(response.employee.enterprise.nombre);
                $('input[name="employee_code"]').val(response.employee.cubo_id);
                $('select[name="health_insurance_id"]').val(response.medical_request.health_insurance_id).trigger('change');
                $('input[name="phone_number"]').val(response.medical_request.phone_number);
                $('select[name="whatsapp_flag"]').val(response.medical_request.whatsapp_flag).trigger('change');
                $('input[name="email"]').val(response.medical_request.email);
            }
            else
            {
                $('input[name="ci"]').val(response.employee.ci_numero);
                $('select[name="ci_expedition"]').val(response.employee.ci_expedido).trigger('change');
                $('input[name="full_name"]').val(response.employee.full_name);
                if (response.employee.sexo == 'MASCULINO')
                {
                    $('select[name="gender"]').val('Masculino').trigger('change');
                }
                else
                {
                    $('select[name="gender"]').val('Femenino').trigger('change');
                }
                var birthday = moment(response.employee.fecha_nacimiento);
                $('input[name="birthday"]').val(birthday.format('DD/MM/YYYY'));
                $('input[name="enterprise"]').val(response.employee.enterprise.nombre);
                $('input[name="employee_code"]').val(response.employee.cubo_id);
                $('select[name="health_insurance_id"]').val(response.health_insurance.id).trigger('change');
            }

            toastr.success(
                '', response.msg
            );

            // Verificamos si tiene una consulta abierta
            var title_medical_request = '';
            var msg_medical_request = '';
            if (response.medical_request != null && response.medical_request.medical_request_status_id != 4)
            {
                title_medical_request = 'Ya tiene una consulta en curso';

                msg_medical_request = 'El empleado <b>' + response.employee.full_name + '</b> ya tiene una consulta en curso con <b>' + response.medical_request.medic.name + ' (' + response.medical_request.medic.role.name + ').</b>';

                // Verificamos si tiene citas vigentes
                if (response.medical_request.last_appointment != null)
                {
                    var appointment_date = moment(response.medical_request.last_appointment.next_attention_date);

                    msg_medical_request += "<br><br>" +
                        'Y se encontró una cita para el: <b>' + appointment_date.format('DD/MM/YYYY hh:mm A') + '</b> con el siguiente comentario: <b>' + response.medical_request.last_appointment.comment + '</b>';
                }

                Swal.fire(
                    title_medical_request,
                    msg_medical_request,
                    'warning'
                );
            }

        }
        else if (response.msg_type == 'danger')
        {
            $('input[name="ci"]').val(response.ci);
            $('select[name="ci_expedition"]').val(response.ci_expedition).trigger('change');

            toastr.error(
                '', response.msg
            );
        }
    });
}
