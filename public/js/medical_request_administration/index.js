var oTable = $('#medical_requests_table').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "responsive": true,
    //ESTA OPCION DE DOM ES PARA ESCOGER ELEMENTOS QUE APARECEN POR DEFECTO EN EL DATA TABLE
    "dom": 'lrtp',
    "pageLength": 10
});
$('input#search_data').keyup(function () {
    oTable.search($(this).val()).draw();
});

$(function () {
    disableControls();

    //Initialize Select2 Elements
    $('.select2').select2({
        theme: 'bootstrap4',
        placeholder: 'Seleccione una opción'
    });
});

function tableVerifyCheckBox() {
    return $('input[name="check_medical_request[]"]:checked').length > 0;
}

$('input[type=checkbox]').change(function() {
    disableControls();
});

function disableControls() {
    var verify_check_box = tableVerifyCheckBox();
    if(verify_check_box)
        $('.disable_if_no_check').removeAttr('disabled');
    else
        $('.disable_if_no_check').attr('disabled', 'disabled');
}

$('select#enterprise_id').change(function (e) {
    var selected_op = $('select#enterprise_id option:selected').val();
    if(selected_op!=null && selected_op!=""){
        var url = search.replace('_id',selected_op);

        $.ajax({
            url: url,
            method: 'get',
            beforeSend: function(e){
                // Loading
                // blockCard('#form_content');
                $('select[name=employee_id]').attr('disabled', 'disabled');
            }
        })
        .done(function(response) {
            $('select[name=employee_id]').empty();

            $.each(response['employees'], function (index, value) {
                var newOption = new Option(value, index, false, false);
                $('select[name=employee_id]').append(newOption);
            });

            $('select[name=employee_id]').trigger('change');
            $('select[name=employee_id]').removeAttr('disabled');
            // unblockCard('#form_content');
        })
        .fail(function() {
            // unblockCard('#form_content');
        });
    }else{
        $('select[name=employee_id]').empty();
        $('select[name=employee_id]').val('').trigger('change');
        $('select[name=employee_id]').attr('disabled', 'disabled');
    }
});

$('select[name=employee_id]').change(function (e) {
    var selected_op = $('select[name=employee_id] option:selected').val();

    if(selected_op!=null && selected_op!=""){
        $('#btnAsignarmePaciente').removeAttr('disabled');
    }else{
        $('#btnAsignarmePaciente').attr('disabled', 'disabled');
    }
});


/**
 *
 * @returns {[]}
 * @description = Obtiene los checkboxes seleccionados y los devuelve como array
 */
function getSelectMedicalRequests() {
    //DATOS A ENVIAR
    var data = [];
    $('input[name="check_medical_request[]"]:checked').each(function (index, value) {
        data.push($(value).val());
    });

    return data;
}

/**
 *
 * @param filas
 * @description = Funcion que actualiza las filas de la tabla de manera dinamica
 */
function updateRowsTable(filas) {
    $.each(filas, function (index, value) {
        $('#tr_medical_request_'+index).empty();
        $('#tr_medical_request_'+index).append(value);
    });
}

function removeRowsTable(filas) {
    $.each(filas, function (index, value) {
        $('#tr_medical_request_'+index).remove();
    });
}

/*function registrarEmpleado(id) {
    var empleado = $('select[name=employee_id] option:selected');

    var url = assingToEmp;
    var token = $("meta[name=csrf-token]").attr("content");

    $.ajax({
        url: url,
        method: 'post',
        headers: {'X-CSRF-TOKEN': token},
        data:{
            employee_id: empleado.val(),
            medical_request_id: id
        },
        beforeSend: function(e){
            // Loading
            // blockCard('#form_content');
        }
    })
    .done(function(response) {
        switch (response['type']) {
            case "correct":
                $('#modal_employee_assign').modal('hide');

               updateRowsTable(response['filas']);

                toastr.success(
                    response['mje'],
                    '¡Asignación completa!'
                );
                break;
            case "error":
                toastr.error(
                    response['mje'],
                    '¡Error!'
                );
                break;
        }
        // unblockCard('#form_content');
    })
    .fail(function() {
        // unblockCard('#form_content');
    });
}*/

function asignarmePaciente(medical_request_id) {
    var empleado = $('select[name=employee_id] option:selected');

    var url = assingToMed;
    var token = $("meta[name=csrf-token]").attr("content");

    $.ajax({
        url: url,
        method: 'post',
        headers: {'X-CSRF-TOKEN': token},
        data:{
            employee_id: empleado.val(),
            medical_request_id: medical_request_id
        },
        beforeSend: function(e){
            // Loading
            // blockCard('#form_content');
        }
    })
    .done(function(response) {
        switch (response['type']) {
            case "correct":
                removeRowsTable(response['filas']);
                updateRowsTable(response['filas_actualizadas']);

                $('div#modal_employee_assign').modal('hide');

                toastr.success(
                    response['msj'],
                    '¡Asignación completa!'
                );

                break;
            case "error":
                toastr.error(
                    response['msj'],
                    '¡Error!'
                );
                break;
        }
        // unblockCard('#form_content');
    })
    .fail(function() {
        // unblockCard('#form_content');
    });
}

function registrarPacienteNoEncontrado(medical_request_id, codigo) {
    Swal.fire({
        title: '¿Desea pasar el caso a recursos humanos?',
        text: "Se enviara la solicitud "+codigo+" para su revisión",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#dc3545',
        confirmButtonText: 'Si, enviar!',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            var url = employyeNotFound;
            var token = $("meta[name=csrf-token]").attr("content");

            $.ajax({
                url: url,
                method: 'post',
                headers: {'X-CSRF-TOKEN': token},
                data:{
                    medical_request_id: medical_request_id
                },
                beforeSend: function(e){
                    // Loading
                    // blockCard('#form_content');
                }
            })
                .done(function(response) {
                    switch (response['type']) {
                        case "correct":
                            removeRowsTable(response['filas']);
                            updateRowsTable(response['filas_actualizadas']);

                            toastr.success(
                                response['msj'],
                                '¡Registro Enviado!'
                            );
                            $('div#modal_employee_assign').modal('hide');
                            /*Swal.fire(
                                'Asignación completa!',
                                response['msj'],
                                'success'
                            );*/
                            break;
                        case "error":
                            /*toastr.error(
                                response['msj'],
                                '¡Error!'
                            );*/
                            Swal.fire(
                                'Ocurrio un error!',
                                response['msj'],
                                'danger'
                            );
                            break;
                    }
                    // unblockCard('#form_content');
                })
                .fail(function() {
                    // unblockCard('#form_content');
                });
        }
    });
}

var global_medical_request_id = null;
function abrirModalRegistrarEmpleado(medical_request_id, object) {
    //ANTES DE ABRIR EL MODAL DEBE VERIFICA EL ESTADO EN QUE SE ENCUENTRA
    var url = verifyStatePre;
    var token = $("meta[name=csrf-token]").attr("content");

    $.ajax({
        url: url,
        method: 'post',
        headers: {'X-CSRF-TOKEN': token},
        data:{
            medical_request_id: medical_request_id
        },
        beforeSend: function(e){
            // Loading
            // blockCard('#form_content');
        }
    })
    .done(function(response) {
        var tr = $(object).parent().parent().parent();
        switch (response['type']) {
            case "correct_nuevo":
                updateRowsTable(response['filas']);
                updateRowsTable(response['filas_actualizadas']);

                countDownDate = new Date(response['fecha_hora_registro']).getTime() + response['tiempo']*60000;

                toastr.success(
                    response['msj'],
                    '¡Asignación completa!'
                );

                var code = $(tr).find('[data-code]').text().trim();
                var fullname = $(tr).find('[data-full_name]').text().trim();
                var full_ci = $(tr).find('[data-full_ci]').text().trim();
                var enterprise = $(tr).find('[data-enterprise]').text().trim();
                var ci = $(tr).find('[data-ci]').val().trim();
                var ci_expedition = $(tr).find('[data-ci_expedition]').val().trim();
                var phone = $(tr).find('[data-phone-all]').text().trim();

                // $('#datos_paciente').empty();
                // $('#datos_paciente').append(response.datos_empleado);
                $('p#p_modal_code').text(code);
                $('p#p_modal_fullname').text(fullname);
                $('p#p_modal_full_ci').text(full_ci);
                $('p#p_modal_enterprise').text(enterprise);
                $('input#ci').val(ci);
                $('select#ci_expedition').val(ci_expedition).trigger('change');
                $('p#p_modal_phone').empty();
                if (response.medical_request.whatsapp_flag) {
                    var whatsapp_flag = '<div class="badge badge-success">'+
                                        '<i class="fab fa-whatsapp"></i>'+
                                        '</div>';
                    $('p#p_modal_phone').append(whatsapp_flag + ' ' + phone);
                } else {
                    $('p#p_modal_phone').text(phone);
                }
                $('p#p_modal_email').text(response.medical_request.email);
                $('p#p_modal_type').text(response.motivo);
                $('p#p_modal_comment').text(response.medical_request.comment);

                $('button#btnCerrarModalAsignacion').attr('onclick','').unbind('click');
                $('button#btnAsignarmePaciente').attr('onclick','').unbind('click');
                $('button#btnPacienteNoEncotrado').attr('onclick','').unbind('click');

                document.getElementById('btnCerrarModalAsignacion').setAttribute("onClick","cerrarModalRegistrarEmpleado("+medical_request_id+")");
                document.getElementById('btnAsignarmePaciente').setAttribute("onClick","asignarmePaciente("+medical_request_id+")");
                document.getElementById('btnPacienteNoEncotrado').setAttribute("onClick","registrarPacienteNoEncontrado("+medical_request_id+", '"+code+"')");

                global_medical_request_id = medical_request_id;

                $('select[name=enterprise_id]').val('').trigger('change');
                $('select[name=employee_id]').val('').trigger('change');

                $('#modal_employee_assign').modal('show');
                break;
            case "correct_yo":
                toastr.warning(
                    response['msj'],
                    '¡Advertencia!'
                );

                updateRowsTable(response['filas']);
                updateRowsTable(response['filas_actualizadas']);

                countDownDate = new Date(response['fecha_hora_registro']).getTime() + response['tiempo']*60000;

                var code = $(tr).find('[data-code]').text().trim();
                var fullname = $(tr).find('[data-full_name]').text().trim();
                var full_ci = $(tr).find('[data-full_ci]').text().trim();
                var enterprise = $(tr).find('[data-enterprise]').text().trim();
                var ci = $(tr).find('[data-ci]').val().trim();
                var ci_expedition = $(tr).find('[data-ci_expedition]').val().trim();
                var phone = $(tr).find('[data-phone-all]').text().trim();

                // $('#datos_paciente').empty();
                // $('#datos_paciente').append(response.datos_empleado);
                $('p#p_modal_code').text(code);
                $('p#p_modal_fullname').text(fullname);
                $('p#p_modal_full_ci').text(full_ci);
                $('p#p_modal_enterprise').text(enterprise);
                $('input#ci').val(ci);
                $('select#ci_expedition').val(ci_expedition).trigger('change');
                $('p#p_modal_phone').empty();
                if (response.medical_request.whatsapp_flag) {
                    var whatsapp_flag = '<div class="badge badge-success">'+
                                        '<i class="fab fa-whatsapp"></i>'+
                                        '</div>';
                    $('p#p_modal_phone').append(whatsapp_flag + ' ' + phone);
                } else {
                    $('p#p_modal_phone').text(phone);
                }
                $('p#p_modal_email').text(response.medical_request.email);
                $('p#p_modal_type').text(response.motivo);
                $('p#p_modal_comment').text(response.medical_request.comment);

                $('button#btnCerrarModalAsignacion').attr('onclick','').unbind('click');
                $('button#btnAsignarmePaciente').attr('onclick','').unbind('click');
                $('button#btnPacienteNoEncotrado').attr('onclick','').unbind('click');

                document.getElementById('btnCerrarModalAsignacion').setAttribute("onClick","cerrarModalRegistrarEmpleado("+medical_request_id+")");
                document.getElementById('btnAsignarmePaciente').setAttribute("onClick","asignarmePaciente("+medical_request_id+")");
                document.getElementById('btnPacienteNoEncotrado').setAttribute("onClick","registrarPacienteNoEncontrado("+medical_request_id+", '"+code+"')");

                global_medical_request_id = medical_request_id;

                $('select[name=enterprise_id]').val('').trigger('change');
                $('select[name=employee_id]').val('').trigger('change');

                $('#modal_employee_assign').modal('show');
                break;
            case "correct_otro":
                Swal.fire({
                    icon: 'warning',
                    title: 'Advertencia!',
                    text: response['msj'],
                    html: response['msj']+
                        '<p>Esta terminara en: </p><span class="badge badge-warning" id="swet_alert_time_timer"></span>',
                    onOpen: function () {
                        $('.swal2-confirm').attr('id','btnConfirmCorrectOtro');
                    }
                });

                updateRowsTable(response['filas']);
                updateRowsTable(response['filas_actualizadas']);

                countDownDateSweet = new Date(response['fecha_hora_registro']).getTime() + response['tiempo']*60000;

                global_medical_request_id = medical_request_id;
                break;
            case "correct_renovar":
                updateRowsTable(response['filas']);
                updateRowsTable(response['filas_actualizadas']);

                countDownDate = new Date(response['fecha_hora_registro']).getTime() + response['tiempo']*60000;

                toastr.success(
                    response['msj'],
                    '¡Asignación completa!'
                );

                var code = $(tr).find('[data-code]').text().trim();
                var fullname = $(tr).find('[data-full_name]').text().trim();
                var full_ci = $(tr).find('[data-full_ci]').text().trim();
                var enterprise = $(tr).find('[data-enterprise]').text().trim();
                var ci = $(tr).find('[data-ci]').text().trim();
                var ci_expedition = $(tr).find('[data-ci_expedition]').text().trim();
                var phone = $(tr).find('[data-phone-all]').text().trim();

                // $('#datos_paciente').empty();
                // $('#datos_paciente').append(response.datos_empleado);
                $('p#p_modal_code').text(code);
                $('p#p_modal_fullname').text(fullname);
                $('p#p_modal_full_ci').text(full_ci);
                $('p#p_modal_enterprise').text(enterprise);
                $('input#ci').val(ci);
                $('select#ci_expedition').val(ci_expedition).trigger('change');
                $('p#p_modal_phone').empty();
                if (response.medical_request.whatsapp_flag) {
                    var whatsapp_flag = '<div class="badge badge-success">'+
                                        '<i class="fab fa-whatsapp"></i>'+
                                        '</div>';
                    $('p#p_modal_phone').append(whatsapp_flag + ' ' + phone);
                } else {
                    $('p#p_modal_phone').text(phone);
                }
                $('p#p_modal_email').text(response.medical_request.email);
                $('p#p_modal_type').text(response.motivo);
                $('p#p_modal_comment').text(response.medical_request.comment);

                $('button#btnCerrarModalAsignacion').attr('onclick','').unbind('click');
                $('button#btnAsignarmePaciente').attr('onclick','').unbind('click');
                $('button#btnPacienteNoEncotrado').attr('onclick','').unbind('click');

                document.getElementById('btnCerrarModalAsignacion').setAttribute("onClick","cerrarModalRegistrarEmpleado("+medical_request_id+")");
                document.getElementById('btnAsignarmePaciente').setAttribute("onClick","asignarmePaciente("+medical_request_id+")");
                document.getElementById('btnPacienteNoEncotrado').setAttribute("onClick","registrarPacienteNoEncontrado("+medical_request_id+", '"+code+"')");

                global_medical_request_id = medical_request_id;

                $('select[name=enterprise_id]').val('').trigger('change');
                $('select[name=employee_id]').val('').trigger('change');

                $('#modal_employee_assign').modal('show');
                break;
            case "error":
                toastr.error(
                    response['msj'],
                    '¡Error!'
                );
                break;
        }
        // unblockCard('#form_content');
    })
    .fail(function() {
        // unblockCard('#form_content');
    });
}

function cerrarModalRegistrarEmpleado(medical_request_id) {
    //ANTES DE ABRIR EL MODAL DEBE VERIFICA EL ESTADO EN QUE SE ENCUENTRA
    var url = closeStatePre;
    var token = $("meta[name=csrf-token]").attr("content");

    $.ajax({
        url: url,
        method: 'post',
        headers: {'X-CSRF-TOKEN': token},
        data:{
            medical_request_id: medical_request_id
        },
        beforeSend: function(e){
            // Loading
            // blockCard('#form_content');
        }
    })
    .done(function(response) {
        switch (response['type']) {
            case "correct":
                updateRowsTable(response['filas']);
                updateRowsTable(response['filas_actualizadas']);

                countDownDate = new Date().getTime() + response['tiempo']*60000;

                toastr.success(
                    response['msj'],
                    '¡Cambio de estado!'
                );

                $('#modal_employee_assign').modal('hide');

                break;
            case "error":
                toastr.error(
                    response['msj'],
                    '¡Error!'
                );
                break;
        }
        // unblockCard('#form_content');
    })
    .fail(function() {
        // unblockCard('#form_content');
    });
}

var add_minutes =  function (dt, minutes) {
    return new Date(dt.getTime() + minutes*60000);
}

function registrarMedico() {
    var data = getSelectMedicalRequests();

    var medico = $('select[name=medic_id] option:selected');

    var url = assingToMed;
    var token = $("meta[name=csrf-token]").attr("content");

    $.ajax({
        url: url,
        method: 'post',
        headers: {'X-CSRF-TOKEN': token},
        data:{
            medic_id: medico.val(),
            medical_requests: data
        },
        beforeSend: function(e){
            // Loading
            // blockCard('#form_content');
        }
    })
    .done(function(response) {
        switch (response['type']) {
            case "correct":
                $('#modal_medic_assign').modal('hide');

                updateRowsTable(response['filas']);

                toastr.success(
                    response['mje'],
                    '¡Asignación completa!'
                );
                break;
            case "error":
                toastr.error(
                    response['mje'],
                    '¡Error!'
                );
                break;
        }

        // unblockCard('#form_content');
    })
    .fail(function() {
        // unblockCard('#form_content');
    });
}

var buscando = false;
var empresa = 0;
var empleado = 0;
function buscarEmpleadoCI() {
    var url = searchCi;

    var ci = $('input#ci').val().trim();
    var ci_expedition = $('select#ci_expedition option:selected').val();

    url = url.replace('_ci_exp',ci_expedition);
    url = url.replace('_ci',ci);

    if(ci != "" && ci_expedition != ""){
        $.ajax({
            url: url,
            method: 'get',
            // headers: {'X-CSRF-TOKEN': token},
            beforeSend: function(e){
                // Loading
                // blockCard('#form_content');
            }
        })
        .done(function(response) {
            switch (response['type']) {
                case "correct_uno":
                    var emp = response['data']['employee']['sol_empresa_id'];
                    var empl = response['data']['employee']['id'];

                    seleccionarEmpleado(emp, empl);

                    toastr.success(
                        response['msj'],
                        '¡Búsqueda realizada!'
                    );
                    break;
                case "correct_mas_de_uno":
                    $('div#modal_search_ci').modal('show');

                    $('div#body_modal_search_ci').empty();
                    $('div#body_modal_search_ci').append(response['data']);

                    break;
                case "warning":
                    $('select#enterprise_id').val('').trigger('change');
                    buscando = false;

                    toastr.warning(
                        response['msj'],
                        '¡Atención!'
                    );
                    break;
            }
            // unblockCard('#form_content');
        })
        .fail(function() {
            // unblockCard('#form_content');
        });
    }

}

function seleccionarEmpleadoModal(emp, empl) {
    seleccionarEmpleado(emp, empl);
    $('div#modal_search_ci').modal('hide');
}

function seleccionarEmpleado(emp, empl) {
    empresa = emp;
    empleado = empl;

    buscando = true;

    $('select#enterprise_id').val(empresa).trigger('change');
}

$(document).ajaxSuccess(function(event, xhr, settings) {
    if(settings.url.includes('/empleados/busqueda_empresa')){
        if(buscando){
            buscando = false;
            $('select[name=employee_id]').val(empleado).trigger('change');
        }
    }
});

function abrirModalAsignarme(medical_request_id, code, object) {
    Swal.fire({
        title: '¿Desea asignarse el caso?',
        text: "Se le asignara este caso con codigo "+code+"",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#28a745',
        confirmButtonText: 'Si, asignarmelo!',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            var url = assingToMe;
            var token = $("meta[name=csrf-token]").attr("content");

            $.ajax({
                url: url,
                method: 'post',
                headers: {'X-CSRF-TOKEN': token},
                data:{
                    medical_request_id: medical_request_id
                },
                beforeSend: function(e){
                    // Loading
                    // blockCard('#form_content');
                }
            })
            .done(function(response) {
                switch (response['type']) {
                    case "correct":
                        removeRowsTable(response['filas']);
                        updateRowsTable(response['filas_actualizadas']);

                        Swal.fire(
                            'Asignación completa!',
                            response['msj'],
                            'success'
                        );
                        break;
                    case "error":
                        /*toastr.error(
                            response['msj'],
                            '¡Error!'
                        );*/
                        Swal.fire(
                            'Ocurrio un error!',
                            response['msj'],
                            'danger'
                        );
                        break;
                }
                // unblockCard('#form_content');
            })
            .fail(function() {
                // unblockCard('#form_content');
            });
        }
    });
}

function updateRecords() {
    var url = updateRegisters;
    var token = $("meta[name=csrf-token]").attr("content");

    $.ajax({
        url: url,
        method: 'post',
        headers: {'X-CSRF-TOKEN': token},
        beforeSend: function(e){
            // Loading
            // blockCard('#form_content');
        }
    })
    .done(function(response) {
        switch (response['type']) {
            case "correct_cambios":
                updateRowsTable(response['filas']);

                toastr.success(
                    response['msj'],
                    '¡Asignación completa!'
                );
                break;
            case "correct_sin_cambios":
                toastr.error(
                    response['msj'],
                    '¡Error!'
                );
                break;
        }
        // unblockCard('#form_content');
    })
    .fail(function() {
        // unblockCard('#form_content');
    });
}

/**
 *
 * @type {number}
 */
var countDownDate = new Date("Jan 5, 2030 15:37:25").getTime();
var x = setInterval(function() {

    // Get today's date and time
    var now = new Date().getTime();

    // Find the distance between now and the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    // var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Output the result in an element with id="demo"
    $('#mi_modal_timer').removeClass();
    $('#mi_modal_timer').addClass('badge badge-warning');

    $('#mi_modal_timer').html(minutes + "m " + seconds + "s ");

    // If the count down is over, write some text
    if (distance < 0) {
        // clearInterval(x);
        $('#mi_modal_timer').removeClass();
        $('#mi_modal_timer').addClass('badge badge-danger');
        $('#mi_modal_timer').html("Expirado");
        // $('div#modal_employee_assign').modal('hide');


        if(($('div#modal_employee_assign').data('bs.modal') || {})._isShown ){
            /**
             * SI EL MODAL EXPIRTO MIENTRAS ESTABA ABIERTO SE INTENTARA ACTUALIZAR EL REGISTRO MEDIANTE
             * UNA PETICION AJAX
             */

            var url = closeStateAuto;
            var token = $("meta[name=csrf-token]").attr("content");

            $.ajax({
                url: url,
                method: 'post',
                headers: {'X-CSRF-TOKEN': token},
                data:{
                    medical_request_id: global_medical_request_id
                },
                beforeSend: function(e){
                    // Loading
                    // blockCard('#form_content');
                }
            })
            .done(function(response) {
                switch (response['type']) {
                    case "correct":
                        updateRowsTable(response['filas']);

                        Swal.fire(
                            'Expirado!',
                            response['msj'],
                            'error'
                        );

                        $('div#modal_employee_assign').modal('hide');
                        break;
                    case "error":
                        toastr.error(
                            response['msj'],
                            '¡Error!'
                        );
                        break;
                }
                // unblockCard('#form_content');
            })
            .fail(function() {
                Swal.fire(
                    'Expirado!',
                    'Su sesión para la atencion de esta solicitud a expirado',
                    'danger'
                );

                $('div#modal_employee_assign').modal('hide');
            });
        }
    }
}, 1000);

var countDownDateSweet = new Date("Jan 5, 2030 15:37:25").getTime();
var y = setInterval(function() {

    // Get today's date and time
    var now = new Date().getTime();

    // Find the distance between now and the count down date
    var distance = countDownDateSweet - now;

    // Time calculations for days, hours, minutes and seconds
    // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    // var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Output the result in an element with id="demo"
    $('#swet_alert_time_timer').removeClass();
    $('#swet_alert_time_timer').addClass('badge badge-warning');

    $('#swet_alert_time_timer').html(minutes + "m " + seconds + "s ");

    // If the count down is over, write some text
    if (distance < 0) {
        // clearInterval(x);
        $('#swet_alert_time_timer').removeClass();
        $('#swet_alert_time_timer').addClass('badge badge-success');
        $('#swet_alert_time_timer').html("Atención");

        $('#btnConfirmCorrectOtro').click();
        // $('div#modal_employee_assign').modal('hide');
    }
}, 1000);
