$('form#frm-weigth_id').validate({
    rules: {
        name: {
            required: true,
            minlength: 3,
            maxlength: 191,
        },
        laboratory_id:{
            required: true,
        },
    },
    messages: {
        name: {
            required: 'El campo nombre es obligatorio.',
            minlength: 'El campo nombre completo debe contener al menos 5 caracteres.',
            maxlength: 'El campo nombre completo no debe contener más de 191 caracteres.',
        },
        laboratory_id: {
            required: 'El campo laboratorio es obligatorio.',
        },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
        element.closest('.input-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    }
});
