$('form#frm-complementary_exam').validate({
    rules: {
        laboratory_id:{
            required: true,
        },
        test_type_id:{
            required: true,
        },
    },
    messages: {
        laboratory_id: {
            required: 'El campo laboratorio es obligatorio.',
        },
        test_type_id: {
            required: 'El campo de tipo de prueba es obligatorio.',
        },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
        element.closest('.input-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    }
});
