$("#weighting").mask("ZZ", { translation: { 'Z': { pattern: /^([0-9]+)$/ } } });

$('form#frm-risks_id').validate({
    rules: {
        name: {
            required: true,
            minlength: 3,
            maxlength: 191,
        },
        description: {
            minlength: 5,
            maxlength: 255,
        },
        risk_type_id: {
            required: true,
            /* minlength: 2,
            maxlength: 255, */
        },
        weighting: {
            required: true,
        }
    },
    messages: {
        name: {
            required: 'El campo nombre es obligatorio.',
            minlength: 'El campo nombre completo debe contener al menos 5 caracteres.',
            maxlength: 'El campo nombre completo no debe contener más de 191 caracteres.',
        },
        description: {
            //required: 'El campo descripción es obligatorio.',
            minlength: 'El campo descripción debe contener al menos 5 caracteres.',
            maxlength: 'El campo descripción no debe contener más de 255 caracteres.',
        },
        risk_type_id: {
            required: 'El campo tipo de riesgo es obligatorio.',
        },
        weighting: {
            required: 'El campo ponderación es obligatorio.',
        }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
        element.closest('.input-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    }
});
