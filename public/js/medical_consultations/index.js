var d = new Date();

var month = d.getMonth()+1;
var day = d.getDate();
var hour = d.getHours();
var minutes = d.getMinutes();
var seconds = d.getSeconds();

var output = d.getFullYear() + '/' +
    ((''+month).length<2 ? '0' : '') + month + '/' +
    ((''+day).length<2 ? '0' : '') + day + "_" +hour+minutes+seconds;

var oTable = $('#medical_consultations_table').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "responsive": true,
    "columnDefs": [
        { "targets": 0, "bSortable": false },
        { "type": "html", "targets": 1 },
        { "type": "date", "targets": 2 },
        { "type": "html-num", "targets": 3 }
    ],
    //ESTA OPCION DE DOM ES PARA ESCOGER ELEMENTOS QUE APARECEN POR DEFECTO EN EL DATA TABLE
    "dom": 'lrBtp',
    "order": [[2, 'desc']],
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [0] }],
    "language": {
        "decimal": ",",
        "thousands": ".",
        "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "infoPostFix": "",
        "infoFiltered": "(filtrado de un total de _MAX_ registros)",
        "loadingRecords": "Cargando...",
        "lengthMenu": "Mostrar _MENU_ registros",
        "paginate": {
            "first": "Primero",
            "last": "Último",
            "next": "Siguiente",
            "previous": "Anterior"
        },
        "processing": "Procesando...",
        "search": "Buscar:",
        "searchPlaceholder": "Término de búsqueda",
        "zeroRecords": "No se encontraron resultados",
        "emptyTable": "Ningún dato disponible en esta tabla",
        "aria": {
            "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sortDescending": ": Activar para ordenar la columna de manera descendente"
        },
    },
    buttons: [
        {"extend": 'excelHtml5',
            "text":'<i class="fa fa-file-excel"></i> Exportar a Excel',
            "className": 'btn btn-success',"title": "MisConsultas_"+output,
            exportOptions: {
                columns: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
            }
        },
        {"extend": 'copyHtml5',
            "text":'<i class="fa fa-copy"></i> Copiar',
            "className": 'btn btn-info',
            exportOptions: {
                columns: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
            }
        },
    ]
});
$('input#search_data').keyup(function () {
    oTable.search($(this).val()).draw();
});


function modalAtention (medical_request_id) {
    var url = getAttenHist.replace('_ID',medical_request_id);
    $.ajax({
        url: url,
        beforeSend: function(e){
            //
        }
    })
    .done(function(response) {
        $('#frm_content').empty();
        $('#frm_content').append(response.content);
        $('#modal_patient_assign').modal({
            show: true
        });
    })
    .fail(function() {
        toastr.error(
                'Error al cargar el mensaje',
                '¡Error!'
            );
    })
    .always(function() {
    });
}

function limpiar_modal () {
    $('#medical_status_id').select2({
        theme: 'bootstrap4',
        placeholder: 'Seleccione una opción'
    }).val('').trigger('change');
    $('#diagnostico').text('');
    $('#tratamiento').text('');
}

function registrar_atencion () {
    var data = $("#frm_atencion").serialize();
    var token = $("meta[name=csrf-token]").attr("content");
    var url = assingToAten
    $.ajax({
        url: url,
        method: 'post',
        headers: {'X-CSRF-TOKEN': token},
        data:data,
        beforeSend: function(e){
            // Loading
            // blockCard('#form_content');
        }
    })
    .done(function(response) {
        switch (response['type']) {
            case "correct":
                console.log('response',response.status);
                if(response.status == 4){
                    removeRowsTable(response['filas']);
                }else{
                    updateRowsTable(response['filas']);
                }
                toastr.success(
                    response['mje'],
                    '¡Asignación completa!'
                );
                $('#modal_patient_assign').modal('hide');
                break;
            case "error":
                toastr.error(
                    response['mje'],
                    '¡Error!'
                );
                break;
        }
        // unblockCard('#form_content');
    })
    .fail(function(response) {
        console.log('response', response);
        toastr.error(
            response,
            '¡Error!'
        );
    });
}


function updateRowsTable(filas) {
    $.each(filas, function (index, value) {
        $('#tr_medical_consultation_'+index).empty();
        $('#tr_medical_consultation_'+index).append(value);
    });
}

$(document).ready(function() {
    $('[data-mask]').inputmask();
});

function removeRowsTable(filas) {
    $.each(filas, function (index, value) {
        $('#tr_medical_consultation_'+index).remove();
    });
}
function getTableRender(url, data = '', clase=''){
    $.ajax({
        url: url,
        cache: false,
        data: data,
        // cache:  false,
        method: 'get',
        beforeSend: function(){
            blockCard('div#wrapper_table');
        }
    }).done(function (response){
        $('div.card-body div#wrapper_table').html(response.table);
        unblockCard('div#wrapper_table');
    }).fail(function (response){
        toastr.error(
            response,
            '¡Error!'
        );
        unblockCard('div#wrapper_table');
    });
}