document.addEventListener('DOMContentLoaded', function() {
});

$('input[type=checkbox][name=check_attention]').change(function (e) {
    var checked = $('input[type=checkbox][name=check_attention]').iCheck('update')[0].checked;
    $('#btnCollapse').click();
    if(checked){
        enableAttentionOptions();
    }else{
        disableAttentionOptions()
    }
});

function printReceta(medical) {
    var fecha = $( "#fecha_consulta" ).val();
    var token = $("meta[name=csrf-token]").attr("content");
    var url = printRecetaRoute;


    url = url + '?medical=' + medical + '&date=' + fecha;

    console.log(url);
    window.open(url,"_blank");

    // $.ajax({
    //     url: url,
    //     method: 'post',
    //     headers: {'X-CSRF-TOKEN': token},
    //     data:{'date':fecha, 'name':name, 'code':code},
    //     beforeSend: function(e){
    //         // Loading
    //         // blockCard('#form_content');
    //     }
    // })
    //     .done(function(response) {
    //
    //         // unblockCard('#form_content');
    //     })
    //     .fail(function(response) {
    //
    //     });
}
function enableAttentionOptions() {
    $('input[name=next_attention_date]').prop('disabled',false);
    $('select[name=appointment_type_id]').prop('disabled',false);
    $('input[name=variable_zoom]').prop('disabled',false);
    $('input[name=variable_whatsapp]').prop('disabled',false);
    $('textarea[name=comment]').prop('disabled',false);
}

function disableAttentionOptions() {
    $('input[name=next_attention_date]').prop('disabled',true);
    $('select[name=appointment_type_id]').prop('disabled',true);
    $('input[name=variable_zoom]').prop('disabled',true);
    $('input[name=variable_whatsapp]').prop('disabled',true);
    $('textarea[name=comment]').prop('disabled',true);
}

// funciones antecedentes
function añadirAntecedente (antecedent_type_id, nombre_antecedente){
    $('#modal_antecedent_type_id').val(antecedent_type_id);
    $('#descripcion_antecedente').val('');
    $('#tituloModalAntecedente').text('Registro de '+nombre_antecedente);
    $('#btnModalRegistrar').removeAttr('hidden');
    $('#btnModalModificar').attr('hidden', 'true');
    $('#modal_antecedente').modal({
        show: true,
        backdrop: 'static',
        keyboard: false,
    });
}

function registrar_antecedente () {
    var data = $("#frm_antecedente").serialize();
    var token = $("meta[name=csrf-token]").attr("content");
    var url = assingAecenden;
    $.ajax({
        url: url,
        method: 'post',
        headers: {'X-CSRF-TOKEN': token},
        data:data,
        beforeSend: function(e){
            // Loading
            // blockCard('#form_content');
        }
    })
    .done(function(response) {
        switch (response['type']) {
            case "correct":
                $('#antescedente_'+response.tipo_antecedente).empty();
                $('#antescedente_'+response.tipo_antecedente).append(response.lista);

                $('#cantidad_antecedente_'+response.tipo_antecedente).empty();
                $('#cantidad_antecedente_'+response.tipo_antecedente).append(response.cantidad);

                toastr.success(
                    response['mje'],
                    '¡Asignación completa!'
                );
                $('#modal_antecedente').modal('hide');
                break;
            case "error":
                toastr.error(
                    response['mje'],
                    '¡Error!'
                );
                break;
        }
        // unblockCard('#form_content');
    })
    .fail(function(response) {
        toastr.error(
            response,
            '¡Error!'
        );
        if(response.responseJSON['errors']){
            $.each(response.responseJSON['errors'], function(index, value){
                $('[name="'+ index +'"]').addClass('is-invalid');
                $('[name="'+ index +'"]').siblings('span.invalid-feedback').html('<strong>'+value+'</strong>');
            });
        }
    });
}


function editarAntecedente(id_tipo_antecedente, nombre_antecedente, id_antecedente, element) {
    $('#modal_antecedent_type_id').val(id_tipo_antecedente);
    $('#modal_antecedent_id').val(id_antecedente);
    $('#tituloModalAntecedente').text('Modificación de '+nombre_antecedente);
    var descripcion = $(element).parent().parent().parent().find('[data-description]').text();

    $('#descripcion_antecedente').val(descripcion);
    $('#btnModalModificar').removeAttr('hidden');
    $('#btnModalRegistrar').attr('hidden', 'true');
    $('#modal_antecedente').modal({
        show: true,
        backdrop: 'static',
        keyboard: false,
    });
}

function editar_atecedente () {
    console.log('asdasd');
    var data = $("#frm_antecedente").serialize();
    var id_antecedente = $('#modal_antecedent_id').val();
    var token = $("meta[name=csrf-token]").attr("content");
    var url = editAntecenden.replace('_ID',id_antecedente);
    $.ajax({
        url: url,
        method: 'put',
        headers: {'X-CSRF-TOKEN': token},
        data:data,
        beforeSend: function(e){
            // Loading
            // blockCard('#form_content');
        }
    })
    .done(function(response) {
        switch (response['type']) {
            case "correct":
                $('#antescedente_'+response.tipo_antecedente).empty();
                $('#antescedente_'+response.tipo_antecedente).append(response.lista);
                console.log('antecedente '+ response.cantidad);
                $('#cantidad_antecedente_'+response.tipo_antecedente).empty();
                $('#cantidad_antecedente_'+response.tipo_antecedente).append(response.cantidad);

                toastr.info(
                    response['mje'],
                    '¡Modificación completa!'
                );
                $('#modal_antecedente').modal('hide');
                break;
            case "error":
                toastr.error(
                    response['mje'],
                    '¡Error!'
                );
                break;
        }

    })
    .fail(function(response) {
        toastr.error(
            response,
            '¡Error!'
        );
        if(response.responseJSON['errors']){
            $.each(response.responseJSON['errors'], function(index, value){
                $('[name="'+ index +'"]').addClass('is-invalid');
                $('[name="'+ index +'"]').siblings('span.invalid-feedback').html('<strong>'+value+'</strong>');
            });
        }
    });
}


function eliminarAntecedente (antecedente_id, element) {
    var descripcion = $(element).parent().parent().parent().find('[data-description]').text();
    Swal.fire({
        title: '¿Desea eliminar el antecedente?',
        text: descripcion,
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#dc3545',
        confirmButtonText: 'Eliminar',
        cancelButtonText: 'Cancelar',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            var url = deleteAntecenden.replace('_ID',antecedente_id);
            var token = $("meta[name=csrf-token]").attr("content");
            $.ajax({
                url: url,
                method: 'delete',
                headers: {'X-CSRF-TOKEN': token},
                beforeSend: function(e){
                    // Loading
                    // blockCard('#form_content');
                }
            })
            .done(function(response) {
                switch (response['type']) {
                    case "correct":
                        $('#antescedente_'+response.tipo_antecedente).empty();
                        $('#antescedente_'+response.tipo_antecedente).append(response.lista);

                        $('#cantidad_antecedente_'+response.tipo_antecedente).empty();
                        $('#cantidad_antecedente_'+response.tipo_antecedente).append(response.cantidad);

                        toastr.info(
                            response['mje'],
                            '¡Eliminacion correcta!'
                        );
                        $('#modal_antecedente').modal('hide');
                        break;
                    case "error":
                        toastr.error(
                            response['mje'],
                            '¡Error!'
                        );
                        break;
                }
            })
            .fail(function(response) {
                console.log('respuest '+ response.msj)
                toastr.error(
                    'No se pudo eliminar el antecedente',
                    '¡Error!'
                );
            });
        }
    });
}

$('button#btn_presave').on('click', function (e) {
    $('input[name="presave"]').val('1');
0
    registrar_atencion(1);
});

function registrar_atencion (tipo) {
    if(tipo == 1){
        // $("#frm_atencion").submit();
        var url = $("#frm_atencion").attr('action');
        console.log('url '+ url);
        var extra_data = $('#frm_atencion_modal').serialize();
        var data = $('#frm_atencion').serialize();
        var token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            url: url,
            method: 'post',
            headers: {'X-CSRF-TOKEN': token},
            // data:data,
            data: new FormData(document.getElementById('frm_atencion')),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(e){
                // Loading
                // blockCard('#frm_atencion_modal');
            }
        })
        .done(function(response) {
            console.log('correct: '+response.route);
            console.log('correct: '+response.route_pdf);
            console.log('tipo: '+response['type']);
            switch (response['type']) {
                case "correct":
                    window.location.href = response.route;
                    toastr.success(
                        response['mje'],
                        '¡Asignación completa!'
                    );
                    $('#modal_resumen').modal('hide');
                    break;
                case "error":
                    toastr.error(
                        response['mje'],
                        '¡Error!'
                    );
                    break;
            }
            $('input[name="presave"]').val('0');
        })
        .fail(function(response) {

            toastr.error(
                response,
                '¡Error!'
            );

            $('input[name="presave"]').val('0');
        });
    }else{
        var formData = new FormData(document.getElementById('frm_atencion'));
        var frm_atencion_modal = jQuery(document.getElementById('frm_atencion_modal')).serializeArray();
        for (var i=0; i<frm_atencion_modal.length; i++){
            formData.append(frm_atencion_modal[i].name, frm_atencion_modal[i].value);
        }

        console.log(formData);
        var url = $("#frm_atencion").attr('action');
        var token = $("meta[name=csrf-token]").attr("content");
        $.ajax({
            url: url,
            method: 'post',
            headers: {'X-CSRF-TOKEN': token},
            data: formData,
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(e){
                // Loading
                // blockCard('#frm_atencion_modal');
            }
        })
        .done(function(response) {
            console.log('correct: '+response.route);
            console.log('correct: '+response.route_pdf);
            console.log('tipo: '+response['type']);
            var extra_data = $('#frm_atencion_modal').serialize();
            switch (response['type']) {
                case "correct":
                    $('#modal_resumen').modal('hide');
                    toastr.success(
                        response['mje'],
                        '¡Asignación completa!'
                        );

                    window.open(response.route_pdf+'?'+extra_data, '_blank');

                    window.location.href=response.route;

                    // setTimeout(() => window.location.href=response.route, 2000);
                    break;
                case "error":
                    toastr.error(
                        response['mje'],
                        '¡Error!'
                    );
                    break;
            }
            $('input[name="presave"]').val('0');
        })
        .fail(function(response) {
            console.log('response', response);
            toastr.error(
                response,
                '¡Error!'
            );
            $('input[name="presave"]').val('0');
        });
    }
}

$('form#frm_atencion').validate({
    rules: {
        work_situation_id: {
            required: true,
        },
        treatment_stage_id: {
            required: true,
        },
        enterprise_id: {
            required: true,
        },
        project_id: {
            required: true,
        },
        risk_type_id: {
            required: true,
        },
        risk_state_id: {
            required: true,
        },
    },
    messages: {
        work_situation_id: {
            required: 'El campo de situación laboral es obligatoria.',
        },
        treatment_stage_id: {
            required: 'El campo etapa de tratamiento es obligatorio.',
        },
        enterprise_id: {
            required: 'El campo de empresa es obligatoria.',
        },
        project_id: {
            required: 'El campo de proyecto es obligatoria.',
        },
        risk_type_id: {
            required: 'El campo de tipo de riesgo es obligatoria.',
        },
        risk_state_id: {
            required: 'El campo de tipo de riesgo es obligatoria.',
        },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
        element.closest('.input-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    },
    submitHandler: function(form, event) {
        event.preventDefault();
        var mi_url = getResumen;
        var data = $('#frm_atencion').serialize();
        $.ajax({
            url: mi_url,
            method: 'get',
            data:data,
            beforeSend: function(e){
                // blockCard('#frm_atencion_modal');
            }
        })
        .done(function(response) {
            $('#content_resumen').empty();
            $('#content_resumen').append(response.resumen);
            $('#modal_resumen').modal({
                show: true,
                backdrop: 'static',
                keyboard: false,
            });
        })
        .fail(function(response) {
            console.log('response', response);
            toastr.error(
                response,
                '¡Error!'
            );
        });
    }

});
