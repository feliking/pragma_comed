$('form#frm-tvaccine_id').validate({
    rules: {
        name: {
            required: true,
            maxlength: 191,
        },
        doces: {
            required: true,
            maxlength: 11,
        }
    },
    messages: {
        name: {
            required: 'El campo de nombre es obligatorio.',
            maxlength: 'El campo nombre completo no debe contener más de 191 caracteres.',
        },
        doces: {
            required: 'El campo número de dosis es obligatorio.',
            maxlength: 'El campo descripción no debe contener más de 255 caracteres.',
        }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
        element.closest('.input-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    }
});
