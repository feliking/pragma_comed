var d = new Date();

var month = d.getMonth()+1;
var day = d.getDate();
var hour = d.getHours();
var minutes = d.getMinutes();
var seconds = d.getSeconds();

var output = d.getFullYear() + '/' +
    ((''+month).length<2 ? '0' : '') + month + '/' +
    ((''+day).length<2 ? '0' : '') + day + "_" +hour+minutes+seconds;

var oTable = $('#medical_requests_table').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "responsive": true,
    //ESTA OPCION DE DOM ES PARA ESCOGER ELEMENTOS QUE APARECEN POR DEFECTO EN EL DATA TABLE
    "dom": 'Blrtp',
    "order": [[2, 'desc']],
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [0] }],
    "language": {
        "decimal": ",",
        "thousands": ".",
        "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "infoPostFix": "",
        "infoFiltered": "(filtrado de un total de _MAX_ registros)",
        "loadingRecords": "Cargando...",
        "lengthMenu": "Mostrar _MENU_ registros",
        "paginate": {
            "first": "Primero",
            "last": "Último",
            "next": "Siguiente",
            "previous": "Anterior"
        },
        "processing": "Procesando...",
        "search": "Buscar:",
        "searchPlaceholder": "Término de búsqueda",
        "zeroRecords": "No se encontraron resultados",
        "emptyTable": "Ningún dato disponible en esta tabla",
        "aria": {
            "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sortDescending": ": Activar para ordenar la columna de manera descendente"
        },
    },
    buttons: [
        {"extend": 'excelHtml5',
            "text":'<i class="fa fa-file-excel"></i> Exportar a Excel',
            "className": 'btn btn-success',"title": "CasosValidar_"+output,
            exportOptions: {
                columns: [ 1, 2, 3, 4, 5, 6, 7 ]
            }
        },
        {"extend": 'copyHtml5',
            "text":'<i class="fa fa-copy"></i> Copiar',
            "className": 'btn btn-info',
            exportOptions: {
                columns: [ 1, 2, 3, 4, 5, 6, 7 ]
            }
        },
    ]
});
$('input#search_data').keyup(function () {
    oTable.search($(this).val()).draw();
});

$(function () {
    disableControls();
    //Initialize Select2 Elements
    $('.select2').select2({
        theme: 'bootstrap4',
        placeholder: 'Seleccione una opción'
    });
});

function tableVerifyCheckBox() {
    return $('input[name="check_medical_request[]"]:checked').length > 0;
}

$('input[type=checkbox]').change(function() {
    disableControls();
});

function disableControls() {
    var verify_check_box = tableVerifyCheckBox();
    if(verify_check_box)
        $('.disable_if_no_check').removeAttr('disabled');
    else
        $('.disable_if_no_check').attr('disabled', 'disabled');
}

$('select#enterprise_id').change(function (e) {
    var selected_op = $('select#enterprise_id option:selected').val();
    if(selected_op!=null && selected_op!=""){
        var url = search.replace('_id',selected_op);

        $.ajax({
            url: url,
            method: 'get',
            beforeSend: function(e){
                // Loading
                // blockCard('#form_content');
                $('select[name=employee_id]').attr('disabled', 'disabled');
            }
        })
        .done(function(response) {
            $('select[name=employee_id]').empty();

            $.each(response['employees'], function (index, value) {
                var newOption = new Option(value, index, false, false);
                $('select[name=employee_id]').append(newOption);
            });

            $('select[name=employee_id]').trigger('change');
            $('select[name=employee_id]').removeAttr('disabled');
            // unblockCard('#form_content');
        })
        .fail(function() {
            // unblockCard('#form_content');
        });
    }else{
        $('select[name=employee_id]').empty();
        $('select[name=employee_id]').val('').trigger('change');
        $('select[name=employee_id]').attr('disabled', 'disabled');
    }
});

$('select[name=employee_id]').change(function (e) {
    var selected_op = $('select[name=employee_id] option:selected').val();

    if(selected_op!=null && selected_op!=""){
        $('#btnAsignarEmpleado').removeAttr('disabled');
        $('#btnFavorAtender').attr('disabled', 'disabled');
        $('#btnEliminarSolicitud').attr('disabled', 'disabled');
    }else{
        $('#btnAsignarEmpleado').attr('disabled', 'disabled');
        $('#btnFavorAtender').removeAttr('disabled');
        $('#btnEliminarSolicitud').removeAttr('disabled');
    }
});


/**
 *
 * @returns {[]}
 * @description = Obtiene los checkboxes seleccionados y los devuelve como array
 */
function getSelectMedicalRequests() {
    //DATOS A ENVIAR
    var data = [];
    $('input[name="check_medical_request[]"]:checked').each(function (index, value) {
        data.push($(value).val());
    });

    return data;
}

/**
 *
 * @param filas
 * @description = Funcion que actualiza las filas de la tabla de manera dinamica
 */
function updateRowsTable(filas) {
    $.each(filas, function (index, value) {
        $('#tr_medical_request_'+index).empty();
        $('#tr_medical_request_'+index).append(value);
    });
}

function removeRowsTable(filas) {
    $.each(filas, function (index, value) {
        $('#tr_medical_request_'+index).remove();
    });
}

function asignarEmpleado(medical_request_id) {
    var empleado = $('select[name=employee_id] option:selected');
    var full_name = $('input#txt_modal_full_name');

    var url = assingToEmp;
    var token = $("meta[name=csrf-token]").attr("content");

    $.ajax({
        url: url,
        method: 'post',
        headers: {'X-CSRF-TOKEN': token},
        data:{
            employee_id: empleado.val(),
            full_name: full_name.val(),
            medical_request_id: medical_request_id
        },
        beforeSend: function(e){}
    })
    .done(function(response) {
        switch (response['type']) {
            case "correct":
                removeRowsTable(response['filas']);

                $('div#modal_employee_assign').modal('hide');

                toastr.success(
                    response['msj'],
                    '¡Asignación completa!'
                );
                break;
            case "error":
                toastr.error(
                    response['msj'],
                    '¡Error!'
                );
                break;
        }
        // unblockCard('#form_content');
    })
    .fail(function() {
        // unblockCard('#form_content');
    });
}

function asignar(medical_request_id) {
    var full_name = $('input#txt_modal_full_name');
    var ci = $('input#ci');
    var ci_expedition = $('select#ci_expedition');
    var enterprise = $('input#enterprise');

    var url = assing;
    var token = $("meta[name=csrf-token]").attr("content");

    $.ajax({
        url: url,
        method: 'post',
        headers: {'X-CSRF-TOKEN': token},
        data:{
            full_name: full_name.val(),
            ci: ci.val(),
            ci_expedition: ci_expedition.val(),
            enterprise: enterprise.val(),
            medical_request_id: medical_request_id
        },
        beforeSend: function(e){}
    })
    .done(function(response) {
        switch (response['type']) {
            case "correct":
                removeRowsTable(response['filas']);

                $('div#modal_employee_assign').modal('hide');

                toastr.success(
                    response['msj'],
                    '¡Asignación completa!'
                );
                break;
            case "error":
                toastr.error(
                    response['msj'],
                    '¡Error!'
                );
                break;
        }
        // unblockCard('#form_content');
    })
    .fail(function(response) {
        toastr.error(
            response['msj'],
            '¡Error!'
        );
    });
}

function eliminarSolicitud(medical_request_id, codigo) {
    Swal.fire({
        title: '¿Desea rechazar la solicitud?',
        text: "Se rechazará la solicitud "+codigo+" permanentemente",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#dc3545',
        confirmButtonText: 'Si, rechazar!',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            var url = employyeNotFound;
            var token = $("meta[name=csrf-token]").attr("content");

            $.ajax({
                url: url,
                method: 'post',
                headers: {'X-CSRF-TOKEN': token},
                data:{
                    medical_request_id: medical_request_id
                },
                beforeSend: function(e){
                    // Loading
                    // blockCard('#form_content');
                }
            })
                .done(function(response) {
                    switch (response['type']) {
                        case "correct":
                            removeRowsTable(response['filas']);

                            toastr.success(
                                response['msj'],
                                '¡Registro Eliminado!'
                            );
                            $('div#modal_employee_assign').modal('hide');
                            /*Swal.fire(
                                'Asignación completa!',
                                response['msj'],
                                'success'
                            );*/
                            break;
                        case "error":
                            /*toastr.error(
                                response['msj'],
                                '¡Error!'
                            );*/
                            Swal.fire(
                                'Ocurrio un error!',
                                response['msj'],
                                'danger'
                            );
                            break;
                    }
                    // unblockCard('#form_content');
                })
                .fail(function() {
                    // unblockCard('#form_content');
                });
        }
    });
}

function abrirModalRegistrarEmpleado(id, object) {
    $('#modal_employee_assign').modal('show');
    var tr = $(object).parent().parent().parent();

    var code = $(tr).find('[data-code]').text().trim();
    var fullname = $(tr).find('[data-full_name]').text().trim();
    var full_ci = $(tr).find('[data-full_ci]').text().trim();
    var enterprise = $(tr).find('[data-enterprise]').text().trim();
    var ci = $(tr).find('[data-ci]').val().trim();
    var ci_expedition = $(tr).find('[data-ci_expedition]').val().trim();
    var phone = $(tr).find('#data-phone').val();
    var whatsapp_flag = $(tr).find('#data-phone-whatsapp_flag').val();
    var email = $(tr).find('[data-email]').text().trim();
    var rejected = $(tr).find('[data-del]').val();
    // console.log(email);
    // console.log(ci_expedition);

    $('p#p_modal_code').text(code);
    $('input#txt_modal_full_name').val(fullname);
    $('p#p_modal_fullname').text(fullname);
    $('p#p_modal_full_ci').text(full_ci);
    $('p#p_modal_enterprise').text(enterprise);
    $('#enterprise').val(enterprise);
    $('input#ci').val(ci);
    $('select#ci_expedition').val(ci_expedition).trigger('change');
    $('p#p_modal_phone').empty();
    if (whatsapp_flag) {
        var whatsapp_flag_div = '<div class="badge badge-success">'+
                            '<i class="fab fa-whatsapp"></i>'+
                            '</div>';
        $('p#p_modal_phone').append(whatsapp_flag_div + ' ' + phone);
    } else {
        $('p#p_modal_phone').text(phone);
    }
    $('p#p_modal_email').text(email);
    $('p#p_modal_short').text($(tr).find('[data-short_phone_number]').val());
    $('p#p_modal_refc').text($(tr).find('[data-reference_full_name]').val());
    $('p#p_modal_refn').text($(tr).find('[data-reference_phone_number]').val());

    $('button#btnAsignarEmpleado').attr('onclick','').unbind('click');
    $('button#btnFavorAtender').attr('onclick','').unbind('click');
    if (rejected!='') {
        $('button#btnEliminarSolicitud').remove();
    } else {
        $('button#btnEliminarSolicitud').attr('onclick','').unbind('click');
        document.getElementById('btnEliminarSolicitud').setAttribute("onClick","eliminarSolicitud("+id+", '"+code+"')");
    }


    document.getElementById('btnAsignarEmpleado').setAttribute("onClick","asignarEmpleado("+id+")");
    document.getElementById('btnFavorAtender').setAttribute("onClick","asignar("+id+")");

}

function registrarMedico() {
    var data = getSelectMedicalRequests();

    var medico = $('select[name=medic_id] option:selected');

    var url = assingToMed;
    var token = $("meta[name=csrf-token]").attr("content");

    $.ajax({
        url: url,
        method: 'post',
        headers: {'X-CSRF-TOKEN': token},
        data:{
            medic_id: medico.val(),
            medical_requests: data
        },
        beforeSend: function(e){
            // Loading
            // blockCard('#form_content');
        }
    })
    .done(function(response) {
        switch (response['type']) {
            case "correct":
                $('#modal_medic_assign').modal('hide');

                updateRowsTable(response['filas']);

                toastr.success(
                    response['mje'],
                    '¡Asignación completa!'
                );
                break;
            case "error":
                toastr.error(
                    response['mje'],
                    '¡Error!'
                );
                break;
        }
        console.log(response);

        // unblockCard('#form_content');
    })
    .fail(function() {
        // unblockCard('#form_content');
    });
}

var buscando = false;
var empresa = 0;
var empleado = 0;
function buscarEmpleadoCI() {
    var url = searchCi;

    var ci = $('input#ci').val().trim();
    var ci_expedition = $('select#ci_expedition option:selected').val();

    url = url.replace('_ci_exp',ci_expedition);
    url = url.replace('_ci',ci);

    if(ci != "" && ci_expedition != ""){
        $.ajax({
            url: url,
            method: 'get',
            // headers: {'X-CSRF-TOKEN': token},
            beforeSend: function(e){
                // Loading
                // blockCard('#form_content');
            }
        })
        .done(function(response) {
            switch (response['type']) {
                case "correct_uno":
                    var emp = response['data']['employee']['sol_empresa_id'];
                    var empl = response['data']['employee']['id'];

                    seleccionarEmpleado(emp, empl);

                    toastr.success(
                        response['msj'],
                        '¡Búsqueda realizada!'
                    );
                    break;
                case "correct_mas_de_uno":
                    $('div#modal_search_ci').modal('show');

                    $('div#body_modal_search_ci').empty();
                    $('div#body_modal_search_ci').append(response['data']);

                    break;
                case "warning":
                    $('select#enterprise_id').val('').trigger('change');
                    buscando = false;

                    toastr.warning(
                        response['msj'],
                        '¡Atención!'
                    );
                    break;
            }
            // unblockCard('#form_content');
        })
        .fail(function() {
            // unblockCard('#form_content');
        });
    }

}

function seleccionarEmpleadoModal(emp, empl) {
    seleccionarEmpleado(emp, empl);
    $('div#modal_search_ci').modal('hide');
}

function seleccionarEmpleado(emp, empl) {
    empresa = emp;
    empleado = empl;

    buscando = true;

    $('select#enterprise_id').val(empresa).trigger('change');
}

$(document).ajaxSuccess(function(event, xhr, settings) {
    if(settings.url.includes('/empleados/busqueda_empresa')){
        if(buscando){
            buscando = false;
            $('select[name=employee_id]').val(empleado).trigger('change');
        }
    }
});

$('input[type=radio][name^=radio_c]').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
    radioClass: 'iradio_flat-blue'
});

$('input[type=radio][name^=radio_c]').on('ifChecked', function(event){
    switch ($(this).val()) {
        case "1":
            // Favor atender
            console.log('1');
            $('div#div_asigna_empleado').attr('hidden', 'hidden');
            $('div#div_favor_atender').removeAttr('hidden');
            $('#btnFavorAtender').removeAttr('disabled');
            $('#btnFavorAtender').removeAttr('hidden');
            $('#btnAsignarEmpleado').attr({
                disabled: 'disabled',
                hidden: 'hidden'
            });
            $('#div_favor_atender').find('input').removeAttr('diabled');
            $('#div_asigna_empleado').find('input').attr('disable', 'disabled');
            break;
        case "2":
            console.log('2');
            $('div#div_asigna_empleado').removeAttr('hidden');
            $('div#div_favor_atender').attr('hidden', 'hidden');
            $('#btnAsignarEmpleado').removeAttr('disabled');
            $('#btnAsignarEmpleado').removeAttr('hidden');
            $('#btnFavorAtender').attr({
                disabled: 'disabled',
                hidden: 'hidden'
            });
            break;
    }

});
