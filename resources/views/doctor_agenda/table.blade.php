@if(count($appointments)>0)
<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Fech. Aten.</th>
        <th>Tipo de Atención</th>
        <th>Comentario</th>
    </tr>
    </thead>
    <tbody>
    @foreach($appointments as $appointment)
        <tr>
            <td>{{$appointment->next_attention_date->format('d/m/Y H:i A')}}</td>
            <td>
                @switch($appointment->appointment_type_id)
                    @case(1)
                    <span class="badge badge-warning" data-toggle="tooltip" data-placement="top" title="La cita será presencial">
                    <i class="fa fa-walking"></i>
                </span>
                    @break
                    @case(2)
                    <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="La cita sera via Zoom">
                    <i class="fa fa-video"></i>
                </span>
                    @break
                    @case(3)
                    <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Se hara la cita via WhatsApp">
                    <i class="fab fa-whatsapp"></i>
                </span>
                    @break
                    @default
                    @break
                @endswitch
                {{$appointment->appointment_type->name}}
            </td>
            <td>
                {{$appointment->comment}}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@else
    <div class="alert alert-info alert-dismissible">
        <h5><i class="icon fas fa-info"></i> ¡Sin Citas!</h5>
        <p>No se encontraron citas programadas para la fecha <b>{{$attention_date}}</b> del personal <b>{{$user->full_name}}</b></p>
    </div>
@endif
