<table id="table_report_cases" class="table table-striped table-hover">
    <thead>
    <tr>
        <th></th>
        <th>Estado</th>
        <th>Ult. Consulta</th>
        <th>Sug. Prox. F.</th>
        <th>Días Aislamiento</th>
        <th>Riesgo</th>
        <th>Etapa Trat.</th>
        <th>Cód. de Solicitud</th>
        <th>Nombre Completo</th>
        <th>CI</th>
        <th>Empresa</th>
        <th>Proyecto</th>
        <th>Nro de Teléfono</th>
        <th>Resp. Seg.</th>
    </tr>
    </thead>
    <tbody>
    @foreach($medical_requests as $medical_request)
        <tr id="tr_medical_consultation_{{$medical_request->id}}">
            @include('report.partials.tr_report_cases',['medical_request'=>$medical_request])
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th></th>
        <th>Estado</th>
        <th>Ult. Consulta</th>
        <th>Sug. Prox. F.</th>
        <th>Días Aislamiento</th>
        <th>Riesgo</th>
        <th>Etapa Trat.</th>
        <th>Cód. de Solicitud</th>
        <th>Nombre Completo</th>
        <th>CI</th>
        <th>Empresa</th>
        <th>Proyecto</th>
        <th>Nro de Teléfono</th>
        <th>Resp. Seg.</th>
    </tr>
    </tfoot>
</table>
<script>
    $(document).ready(function() {
        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();
        var hour = d.getHours();
        var minutes = d.getMinutes();
        var seconds = d.getSeconds();

        var output = d.getFullYear() + '/' +
            ((''+month).length<2 ? '0' : '') + month + '/' +
            ((''+day).length<2 ? '0' : '') + day + "_" +hour+minutes+seconds;

        var oTable_adm = $('#table_report_cases').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            "columnDefs": [
                { "targets": 0, "bSortable": false },
                { "type": "html", "targets": 1 },
                { "type": "date", "targets": 2 },
                { "type": "html-num", "targets": 3 }
            ],
            //ESTA OPCION DE DOM ES PARA ESCOGER ELEMENTOS QUE APARECEN POR DEFECTO EN EL DATA TABLE
            "dom": 'Blrtp',
            "pageLength": 10,
            "order": [[2, 'desc']],
            "aoColumnDefs": [{ "bSortable": false, "aTargets": [0] }],
            "language": {
                "decimal": ",",
                "thousands": ".",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoPostFix": "",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                "lengthMenu": "Mostrar _MENU_ registros",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "processing": "Procesando...",
                "search": "Buscar:",
                "searchPlaceholder": "Término de búsqueda",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla",
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },
            },
            buttons: [
                {"extend": 'excelHtml5',
                    "text":'<i class="fa fa-file-excel"></i> Exportar a Excel',
                    "className": 'btn btn-success',"title": "Listado de Casos_"+output,
                    exportOptions: {
                        columns: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 ]
                    }
                },
                {"extend": 'copyHtml5',
                    "text":'<i class="fa fa-copy"></i> Copiar',
                    "className": 'btn btn-info',
                    exportOptions: {
                        columns: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 ]
                    }
                },
            ],
        });
        $('input#search_data').keyup(function () {
            oTable_adm.search($(this).val()).draw();
        });

    });
</script>
