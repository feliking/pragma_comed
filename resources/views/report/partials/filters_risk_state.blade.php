<div class="row">
<div class="icheck-gray form-group col-md-4 offset-8">
    <input type="checkbox" id="enterprise_all" name="enterprise_all" onclick="enterpriseChange(this)" checked/>
    <label for="enterprise_all">TODOS</label>
</div>
</div>
<div class="row">
    <!-- Empresas -->
    <div class="col-md-12">
        @foreach ($enterprises->chunk(3) as $chunk)
        <div class="row">
        @foreach ($chunk as $enterprise)
        <div class="form-group col-md-4">
            <div class="icheck-success">
                @php
                    $projects_enterprise = \App\ProjectEnterprise::where('empresa_id', $enterprise->id)
                        ->orderBy('proyecto_id', 'ASC')
                        ->pluck('proyecto_id')
                        ->toArray();
                @endphp
                <input type="checkbox" id="enterprise_{{ $enterprise->id }}" name="enterprises[]" value="{{ $enterprise->id }}" data-projects="@json($projects_enterprise)" checked/>
                <label for="enterprise_{{ $enterprise->id }}">{{ $enterprise->nombre }}</label>
            </div>
        </div>
        @endforeach
        </div>
        @endforeach
    </div>
    <!-- Empresas -->
</div>

<div class="dropdown-divider"></div>
<div class="row">
<div class="icheck-gray form-group col-md-4 offset-8">
    <input type="checkbox" id="project_all" name="project_all" onclick="projectChange(this)" checked/>
    <label for="project_all">TODOS</label>
</div>
</div>
<div class="row">
    <!-- Empresas -->
    <div class="col-md-12">
        @foreach ($projects->chunk(3) as $chunk)
        <div class="row">
        @foreach ($chunk as $project)
        <div class="form-group col-md-4">
            <div class="icheck-primary">
                @php
                    $enterprises_project = \App\ProjectEnterprise::where('proyecto_id', $project->id)
                        ->orderBy('empresa_id', 'ASC')
                        ->pluck('empresa_id')
                        ->toArray();
                @endphp
                <input type="checkbox" id="project_{{ $project->id }}" name="projects[]" value="{{ $project->id }}" data-enterprises="@json($enterprises_project)" checked/>
                <label for="project_{{ $project->id }}">{{ $project->nombre }}</label>
            </div>
        </div>
        @endforeach
        </div>
        @endforeach
    </div>
    <!-- Empresas -->
</div>

<div class="dropdown-divider"></div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="input-group mb-3 date" id="init_date_field" data-target-input="nearest">
                <input type="text" id="init_date" name="init_date" class="form-control datetimepicker-input" data-target="#init_date_field" placeholder="* Inicio" data-inputmask='"mask": "99/99/9999"' data-greedy="false" data-mask/>
                <div class="input-group-append" data-target="#init_date_field" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="input-group mb-3 date" id="end_date_field" data-target-input="nearest">
                <input type="text" id="end_date" name="end_date" class="form-control datetimepicker-input" data-target="#end_date_field" placeholder="* Fin" data-inputmask='"mask": "99/99/9999"' data-greedy="false" data-mask/>
                <div class="input-group-append" data-target="#end_date_field" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                </div>
            </div>
        </div>
    </div>
</div>
