<div class="row">
   <div class="col-lg-2 col-6 box-risk_state">
       <!-- small box -->
       <div class="small-box bg-info">
           <div class="inner">
               <h3>TOTAL: {{ $total_medical_requests }} <small>casos</small></h3>
           </div>
           <div class="icon">
               <i class="fas fa-viruses"></i>
           </div>
       </div>
   </div>
    <!-- ./col -->

    <div class="col-lg-2 col-6 box-risk_state">
        <!-- small box -->
        <div class="small-box bg-danger">
            <div class="inner">
                <h3>
                    <i class="fas fa-virus"></i>
                    <span id="total_confirmed"></span>
                    &nbsp;
                    <small>confirmados</small>
                </h3>
            </div>
            <div class="icon">
                <i class="fas fa-virus"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->

    <div class="col-lg-2 col-6 box-risk_state">
        <!-- small box -->
        <div class="small-box bg-success">
            <div class="inner">
                <h3>
                    <i class="fas fa-shield-virus"></i>
                    <span id="total_recovered"></span>
                    &nbsp;
                    <small>recuperados</small>
                </h3>
            </div>
            <div class="icon">
                <i class="fas fa-shield-virus"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->

    <div class="col-lg-2 col-6 box-risk_state">
        <!-- small box -->
        <div class="small-box bg-warning">
            <div class="inner">
                <h3>
                    <i class="fas fa-disease"></i>
                    <span id="total_suspect"></span>
                    &nbsp;
                    <small>sospechosos</small>
                </h3>
            </div>
            <div class="icon">
                <i class="fas fa-disease"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->

    <div class="col-lg-2 col-6 box-risk_state">
        <!-- small box -->
        <div class="small-box bg-default">
            <div class="inner">
                <h3>
                    <i class="fas fa-virus-slash"></i>
                    <span id="total_discarted"></span>
                    &nbsp;
                    <small>descartados</small>
                </h3>
            </div>
            <div class="icon">
                <i class="fas fa-virus-slash"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->

    <div class="col-lg-2 col-6 box-risk_state">
        <!-- small box -->
        <div class="small-box bg-purple">
            <div class="inner">
                <h3>
                    <i class="fas fa-cross"></i>
                    <span id="total_deceased"></span>
                    &nbsp;
                    <small>fallecidos</small>
                </h3>
            </div>
            <div class="icon">
                <i class="fas fa-cross"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
</div>
