<table id="data-medical_request" class="table table-responsive">
    <thead>
        <tr>
            <th>#</th>
            <th></th>
            <th>Empleado</th>
            <th>Código de caso</th>
            <th>Empresa</th>
            <th>Proyecto</th>
            <!-- Fechas -->
            @foreach ($array_dates as $date)
            <th>{{ $date }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach ($matrix_risk_state as $medical_request_id => $evolution)
        @php
            $medical_request = \App\MedicalRequest::find($medical_request_id);
        @endphp
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>
                @include('layouts.components.buttons.button_tracings',[
                        'medical_request_id' => $medical_request->id
                    ])
            </td>
            <td>{{ $medical_request->employee->full_name }}</td>
            <td>{{ $medical_request->code }}</td>
            <td>{{ $medical_request->employee->enterprise->nombre }}</td>
            <td>{{ $medical_request->project->nombre }}</td>
            @foreach ($evolution as $risk_state_id)
            <td>
                @switch($risk_state_id)
                    @case(0)
                        -
                        @break
                    @case(1)
                        <span class="badge badge-danger">C</span>
                        @break
                    @case(2)
                        <span class="badge badge-default bg-black">D</span>
                        @break
                    @case(3)
                        <span class="badge badge-purple bg-purple">F</span>
                        @break
                    @case(4)
                        <span class="badge badge-success">R</span>
                        @break
                    @case(5)
                        <span class="badge badge-warning">S</span>
                        @break
                @endswitch
            </td>
            @endforeach
        </tr>
        @endforeach
    </tbody>
</table>
