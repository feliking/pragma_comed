<div class="row">

    @php
        $count_medical_requests = 0;
    @endphp

    @foreach ($percentages_enterprises as $enterprise_id => $medical_requests)

    @if (count($medical_requests) > 0)
    @php
        $count_medical_requests += count($medical_requests);

        $no_tracing = $count_confirmed = $count_discarted = $count_deceased = $count_recovered = $count_suspect = 0;

        foreach ($medical_requests as $medical_request)
        {
            $last_tracing = $medical_request->tracings()
                ->orderBy('tracings.id', 'DESC')
                ->first();

            if ($last_tracing != null)
            {
                switch ($last_tracing->risk_state_id)
                {
                    case 1: // Confirmado
                        $count_confirmed += 1;
                        break;
                    case 2: // Descartado
                        $count_discarted += 1;
                        break;
                    case 3: // Fallecido
                        $count_deceased += 1;
                        break;
                    case 4: // Recuperado
                        $count_recovered += 1;
                        break;
                    case 5: // Sospechoso
                        $count_suspect += 1;
                        break;
                }
            }
            else
            {
                $no_tracing += 1;
            }
        }
    @endphp
    <div class="col-lg-2 col-6 box-enterprise">
        <!-- small box -->
        <div class="small-box bg-gradient-primary">
            <div class="inner row">
                <h3 class="col-sm-5">
                    {{ number_format((count($medical_requests) * 100) / $total_medical_requests, 2, '.', '') }}
                    <sup style="font-size: 15px">%</sup>
                </h3>
                <div class="icheck-info d-inline col-sm-7" style="z-index: 1;">
                    <input type="checkbox" id="chk_enterprise_{{ $enterprise_id }}" name="chk_enterprises[]" value="{{ $enterprise_id }}" data-enterprise="{{ \App\EnterpriseSOL::find($enterprise_id)->nombre }}" data-confirmed="{{ $count_confirmed }}" data-discarted="{{ $count_discarted }}" data-deceased="{{ $count_deceased }}" data-recovered="{{ $count_recovered }}" data-suspect="{{ $count_suspect }}"/>
                    <label for="chk_enterprise_{{ $enterprise_id }}">
                        Casos: {{ count($medical_requests) }}
                    </label>
                </div>
            </div>
            <div class="icon">
                <i class="far fa-chart-bar"></i>
            </div>
            <span class="small-box-footer">
                <b>{{ \App\EnterpriseSOL::find($enterprise_id)->nombre }} <i class="far fa-building"></i></b>
            </span>
        </div>
    </div>
    <!-- ./col -->
    @endif

    @endforeach

    <div class="col-lg-2 col-6 box-enterprise">
        <!-- small box -->
        <div class="small-box bg-gradient-warning">
            <div class="inner row">
                <h3 class="col-sm-5">
                    {{ number_format((($total_medical_requests - $count_medical_requests) * 100) / $total_medical_requests, 2, '.', '') }}
                    <sup style="font-size: 15px">%</sup>
                </h3>
                <p>Sin seg.: {{ $total_medical_requests - $count_medical_requests }}</p>
            </div>
            <div class="icon">
                <i class="far fa-chart-bar"></i>
            </div>
            <span class="small-box-footer">
                <b>Sin empresa <i class="far fa-building"></i></b>
            </span>
        </div>
    </div>
    <!-- ./col -->
</div>
