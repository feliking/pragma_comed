<div class="row">
    <div class="col-md-2 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info">
                <i class="fas fa-head-side-mask"></i>
            </span>

            <div class="info-box-content">
                <span class="info-box-text">
                    Riesgos
                </span>
                <span class="info-box-number"></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <div class="col-md-2 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-default">
                <i class="fas fa-angle-double-down"></i>
            </span>

            <div class="info-box-content">
                <span class="info-box-text">
                    Nulo
                </span>
                <span class="info-box-number">
                    @if ($level_1 != null)
                    {{ count($level_1) }}
                    @else
                    0
                    @endif
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <div class="col-md-2 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-success">
                <i class="fas fa-angle-down"></i>
            </span>

            <div class="info-box-content">
                <span class="info-box-text">
                    Bajo
                </span>
                <span class="info-box-number">
                    @if ($level_2 != null)
                    {{ count($level_2) }}
                    @else
                    0
                    @endif
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <div class="col-md-2 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-warning">
                <i class="fas fa-minus"></i>
            </span>

            <div class="info-box-content">
                <span class="info-box-text">
                    Medio
                </span>
                <span class="info-box-number">
                    @if ($level_3 != null)
                    {{ count($level_3) }}
                    @else
                    0
                    @endif
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <div class="col-md-2 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-danger">
                <i class="fas fa-angle-up"></i>
            </span>

            <div class="info-box-content">
                <span class="info-box-text">
                    Alto
                </span>
                <span class="info-box-number">
                    @if ($level_4 != null)
                    {{ count($level_4) }}
                    @else
                    0
                    @endif
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <div class="col-md-2 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-danger disabled color-palette">
                <i class="fas fa-angle-double-up"></i>
            </span>

            <div class="info-box-content">
                <span class="info-box-text">
                    Muy alto
                </span>
                <span class="info-box-number">
                    @if ($level_5 != null)
                    {{ count($level_5) }}
                    @else
                    0
                    @endif
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
