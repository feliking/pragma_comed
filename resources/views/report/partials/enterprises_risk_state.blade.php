<div class="row">
    <div class="form-group col-md-12">
        <select class="form-control" name="risk_state_id" id="risk_state_id">
            <option value="0" selected>Todos</option>
            <option value="1">Confirmados</option>
            <option value="4">Recuperados</option>
            <option value="5">Sospechosos</option>
            <option value="2">Descartados</option>
            <option value="3">Fallecidos</option>
        </select>
    </div>
</div>

<hr/>

<div id="ajax_enterprise_risk_state">
    @include('report.partials.ajax_enterprise_risk_state')
</div>
