<table id="data-tracings-reported" class="table table-responsive">
    <thead>
        <tr>
            <th>Estado de riesgo</th>
            <!-- Fechas -->
            @foreach ($array_dates as $date)
            <th>{{ $date }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @php
            $array_total = [];
        @endphp
        <tr>
            <td>
                <span class="badge badge-danger">CONFIRMADOS</span>
            </td>
            @foreach ($array_confirmed_per_date as $confirmed)
            <td>{{ $confirmed }}</td>
            @endforeach
        </tr>
        <tr>
            <td>
                <span class="badge badge-success">RECUPERADOS</span>
            </td>
            @foreach ($array_recovered_per_date as $recovered)
            <td>{{ $recovered }}</td>
            @endforeach
        </tr>
        <tr>
            <td>
                <span class="badge badge-warning">SOSPECHOSOS</span>
            </td>
            @foreach ($array_suspect_per_date as $suspect)
            <td>{{ $suspect }}</td>
            @endforeach
        </tr>
        <tr>
            <td>
                <span class="badge badge-default bg-black">DESCARTADOS</span>
            </td>
            @foreach ($array_discarted_per_date as $discarted)
            <td>{{ $discarted }}</td>
            @endforeach
        </tr>
        <tr>
            <td>
                <span class="badge badge-purple bg-purple">FALLECIDOS</span>
            </td>
            @foreach ($array_deceased_per_date as $deceased)
            <td>{{ $deceased }}</td>
            @endforeach
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td>
                <b>TOTAL</b>
            </td>
            @for ($i = 0; $i < count($array_confirmed_per_date); $i++)
            <td>
                @php
                    $total = intval($array_confirmed_per_date[$i]) + intval($array_recovered_per_date[$i]) + intval($array_suspect_per_date[$i]) + intval($array_discarted_per_date[$i]) + intval($array_deceased_per_date[$i]);
                @endphp
                <b>{{ $total }}</b>
            </td>
            @endfor
        </tr>
    </tfoot>
</table>
