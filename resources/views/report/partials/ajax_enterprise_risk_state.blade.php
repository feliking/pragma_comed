@foreach ($array_enterprise_cases as $enterprise_id => $quantity)

@if ($quantity > 0)

@php
    $percentage = number_format(($quantity * 100) / $total_medical_requests, 2, '.', '')
@endphp

<div class="progress-group">
    {{ \App\EnterpriseSOL::find($enterprise_id)->nombre }} <b>({{ $percentage }}%)</b>
    <span class="float-right"><b>{{ $quantity }}</b>/{{ $total_medical_requests }}</span>

    <div class="progress progress-sm">
        @switch($risk_state_id)
            @case(0)
                <!-- TODOS -->
                <div class="progress-bar bg-primary" style="width: {{ $percentage }}%"></div>
                @break
            @case(1)
                <!-- Confirmados -->
                <div class="progress-bar bg-danger" style="width: {{ $percentage }}%"></div>
                @break
            @case(2)
                <!-- Descartados -->
                <div class="progress-bar bg-gray-dark" style="width: {{ $percentage }}%"></div>
                @break
            @case(3)
                <!-- Fallecidos -->
                <div class="progress-bar bg-purple" style="width: {{ $percentage }}%"></div>
                @break
            @case(4)
                <!-- Recuperados -->
                <div class="progress-bar bg-success" style="width: {{ $percentage }}%"></div>
                @break
            @case(5)
                <!-- Sospechosos -->
                <div class="progress-bar bg-warning" style="width: {{ $percentage }}%"></div>
                @break
        @endswitch
    </div>
</div>
<!-- /.progress-group -->

@endif

@endforeach
