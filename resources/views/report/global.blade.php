@extends('layouts.app_menu')

@section('style')
    <style>
        .box-enterprise .small-box{
            margin-bottom: 5px !important;
        }

        .box-enterprise .small-box h3{
            font-size: 20px;
            text-align: center;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Reporte Global</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <!-- Risk states -->
            @include('report.partials.risk_states')

            <!-- Levels -->
            @include('report.partials.weighting_levels')

            <!-- Enterprises -->
            <div class="enterprises">
                <h4>Empresas</h4>
                @include('report.partials.enterprises')
            </div>

            <!-- Projects -->
            <div class="projects">
                <h4>Proyectos</h4>
                @include('report.partials.projects')
            </div>

            <hr/>

            <!-- Bar -->
            @include('report.partials.bar')
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- .content -->
@endsection

@section('script')
    <!-- FLOT CHARTS -->
    <script src="{{ asset('adminlte/plugins/flot/jquery.flot.js') }}"></script>
    <!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
    <script src="{{ asset('adminlte/plugins/flot-old/jquery.flot.resize.min.js') }}"></script>
    <!-- FLOT PIE PLUGIN - also used to draw donut charts -->
    <script src="{{ asset('adminlte/plugins/flot-old/jquery.flot.pie.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/flot-old/excanvas.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('a#nav_report_global').addClass('active');
        });

        var checked_ticks = [];

        // Por Empresa
        $('input[type=checkbox][name^=chk_enterprises]').change(function (e) {
            checked_ticks = [];
            var bar_data_confirmed = [];
            var bar_data_discarted = [];
            var bar_data_deceased = [];
            var bar_data_recovered = [];
            var bar_data_suspect = [];
            var correlative = 1;

            $("input[type=checkbox][name^=chk_enterprises]:not(:checked)").map(function() {
                $(this).parent()
                    .removeClass('icheck-success')
                    .addClass('icheck-info')
                    .parent().parent()
                    .removeClass('bg-gradient-success')
                    .addClass('bg-gradient-primary');
            });

            $("input[type=checkbox][name^=chk_projects]:checked").map(function() {
                $(this).parent()
                    .removeClass('icheck-success')
                    .addClass('icheck-info')
                    .parent().parent()
                    .removeClass('bg-gradient-success')
                    .addClass('bg-gradient-info');
                
                $(this).prop('checked', false);
            });

            $("input[type=checkbox][name^=chk_enterprises]:checked").map(function() {
                bar_data_confirmed.push([
                    correlative, parseInt($(this).attr('data-confirmed'))
                ]);
                bar_data_discarted.push([
                    correlative, parseInt($(this).attr('data-discarted'))
                ]);
                bar_data_deceased.push([
                    correlative, parseInt($(this).attr('data-deceased'))
                ]);
                bar_data_recovered.push([
                    correlative, parseInt($(this).attr('data-recovered'))
                ]);
                bar_data_suspect.push([
                    correlative, parseInt($(this).attr('data-suspect'))
                ]);

                checked_ticks.push([
                    correlative, $(this).attr('data-enterprise')
                ]);

                correlative += 1;

                $(this).parent()
                    .removeClass('icheck-info')
                    .addClass('icheck-success')
                    .parent().parent()
                    .removeClass('bg-gradient-info')
                    .addClass('bg-gradient-success');
            });

            $.plot('#bar-chart', [
                {color: "#dc3545", data: bar_data_confirmed, label: "Confirmados"},
                {color: "#28a745", data: bar_data_recovered, label: "Recuperados"},
                {color: "#ffc107", data: bar_data_suspect, label: "Sospechosos"},
                {color: "#000000", data: bar_data_discarted, label: "Descartados"},
                {color: "#dc3545", dara: bar_data_deceased, label: "Fallecidos"}
                ], {
                grid  : {
                    borderWidth: 1,
                    borderColor: '#f3f3f3',
                    tickColor  : '#f3f3f3'
                },
                series: {
                    stack: true,
                    bars: {
                        show: true, barWidth: 0.8, align: 'center',
                    },
                },
                xaxis : {
                    ticks: checked_ticks
                },
            });
        });

        // Por Proyecto
        $('input[type=checkbox][name^=chk_projects]').change(function (e) {
            checked_ticks = [];
            var bar_data_confirmed = [];
            var bar_data_discarted = [];
            var bar_data_deceased = [];
            var bar_data_recovered = [];
            var bar_data_suspect = [];
            var correlative = 1;

            $("input[type=checkbox][name^=chk_projects]:not(:checked)").map(function() {
                $(this).parent()
                    .removeClass('icheck-success')
                    .addClass('icheck-info')
                    .parent().parent()
                    .removeClass('bg-gradient-success')
                    .addClass('bg-gradient-info');
            });

            $("input[type=checkbox][name^=chk_enterprises]:checked").map(function() {
                $(this).parent()
                    .removeClass('icheck-success')
                    .addClass('icheck-info')
                    .parent().parent()
                    .removeClass('bg-gradient-success')
                    .addClass('bg-gradient-primary');
                
                $(this).prop('checked', false);
            });

            $("input[type=checkbox][name^=chk_projects]:checked").map(function() {
                bar_data_confirmed.push([
                    correlative, parseInt($(this).attr('data-confirmed'))
                ]);
                bar_data_discarted.push([
                    correlative, parseInt($(this).attr('data-discarted'))
                ]);
                bar_data_deceased.push([
                    correlative, parseInt($(this).attr('data-deceased'))
                ]);
                bar_data_recovered.push([
                    correlative, parseInt($(this).attr('data-recovered'))
                ]);
                bar_data_suspect.push([
                    correlative, parseInt($(this).attr('data-suspect'))
                ]);

                checked_ticks.push([
                    correlative, $(this).attr('data-project')
                ]);

                correlative += 1;

                $(this).parent()
                    .removeClass('icheck-info')
                    .addClass('icheck-success')
                    .parent().parent()
                    .removeClass('bg-gradient-info')
                    .addClass('bg-gradient-success');
            });

            $.plot('#bar-chart', [
                {color: "#dc3545", data: bar_data_confirmed, label: "Confirmados"},
                {color: "#28a745", data: bar_data_recovered, label: "Recuperados"},
                {color: "#ffc107", data: bar_data_suspect, label: "Sospechosos"},
                {color: "#000000", data: bar_data_discarted, label: "Descartados"},
                {color: "#dc3545", dara: bar_data_deceased, label: "Fallecidos"}
                ], {
                grid  : {
                    borderWidth: 1,
                    borderColor: '#f3f3f3',
                    tickColor  : '#f3f3f3'
                },
                series: {
                    stack: true,
                    bars: {
                        show: true, barWidth: 0.8, align: 'center',
                    },
                },
                xaxis : {
                    ticks: checked_ticks
                },
            });
        });
    </script>
@endsection
