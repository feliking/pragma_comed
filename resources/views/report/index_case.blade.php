@extends('layouts.app_menu')

@section('styles')
    <style>
        .dataTables_filter {
            display: none !important;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
    </section>

    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <div class="card card-success card-outline card-tabs">
                        <div class="card-header p-0 pt-1">
                            <ul class="nav nav-tabs" id="report-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link" id="tab1" href="{{ route('report.global_risk_state') }}" role="tab" aria-controls="tabs-tab1" aria-selected="true">Estados de Riesgo</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="tab2" href="{{ route('report.global_report_cases') }}" role="tab" aria-controls="tabs-tab2" aria-selected="false">Casos reportados</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" id="tab3" href="{{ route('report.casos') }}" role="tab" aria-controls="tabs-tab3" aria-selected="false">Listado de casos</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Datos de consultas médicas</h3>

                            {{--<div class="card-tools">
                                <div class="input-group input-group-sm">
                                    <input id="search_data" type="text" class="form-control" placeholder="Buscar">
                                </div>
                            </div>--}}
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12" id="div_buscador">
                                    @include('layouts.components.cards.search_report_cases')
                                </div>
                            </div>
                            <div id="wrapper_table">
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
        </div>
    </section>
    <!--MODALS-->
    @include('layouts.components.modals.modal_history')
    @include('tracing_administrations.modals.modal_close')
    @include('layouts.components.modals.modal_covid_file')
    @include('layouts.components.modals.modal_history_timeline')
    @include('layouts.components.modals.modal_tracings')

@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('a#nav_index_report_cases').addClass('active');

            $('#enterprise_id').select2({
                theme: 'bootstrap4',
                width: 'auto',
                placeholder: 'Selecione una empresa'
            });
            $('#ci_expedition').select2({
                theme: 'bootstrap4',
                width: 'auto',
                placeholder: 'Selecione expedicion'
            });
            $('select[name=employee_id]').select2({
                theme: 'bootstrap4',
                width: 'auto',
                placeholder: 'Selecionde un empleado'
            });

            getValues();
        });
        /*RUTAS DEL SISTEMA*/
        const showHistory = @json(route('administracion_solicitudes.mostrar_historial.showHistory','_id'));
        const showHistoryTimeline = @json(route('buscar_codigo.mostrarTimeline'));
        const showHistoryFiles = @json(route('mostrar_archivos.showFiles','_id'));
        const forSearchProjects = @json(route('proyectos.para_buscador.getProjectsFromEnterpriseSearch','_id'));
        const reportIndex = @json(route('report.casos'));

        /*FUNCIONES*/
        function getValues() {
            $('#search_data').val('');

            var medical_request_id_search = $('#medical_request_id_search option:selected').val();
            var enterprise_id_search = $('#enterprise_id_search option:selected').val();
            var project_id_search = $('#project_id_search option:selected').val();
            var weighting_level_id_search = $('#weighting_level_id_search option:selected').val();
            var risk_state_id_search = $('select[name^=risk_state_id_search]').find(':selected').toArray().map(item => item.value);

            // console.log(start_tracing_date_search);
            // console.log(end_tracing_date_search);

            var url = reportIndex;
            getTableRender(url,{
                'medical_request_id_search': medical_request_id_search,
                'enterprise_id_search': enterprise_id_search,
                'project_id_search': project_id_search,
                'weighting_level_id_search': weighting_level_id_search,
                'risk_state_id_search': risk_state_id_search,
                'start_tracing_date_search': start_tracing_date_search,
                'end_tracing_date_search': end_tracing_date_search
            });
        }
    </script>
{{--    <script src="{{asset('js/tracing_administration/index.js')}}"></script>--}}
    <script src="{{asset('js/components/button_historial.js')}}"></script>
    <script src="{{asset('js/components/button_historial_timeline.js')}}"></script>
    <script src="{{asset('js/components/search.js')}}"></script>
@endsection
