@extends('layouts.app_menu')

@section('style')
    @parent
    <style>
        .box-risk_state .small-box{
            margin-bottom: 2px !important;
            padding-bottom: 2px !important;
        }
        .box-risk_state .small-box .inner{
            padding: 5px 10px;
        }
        .box-risk_state .small-box h3{
            font-size: 25px;
            margin-bottom: 2px;
        }
        .box-risk_state .small-box p{
            margin-bottom: 2px;
        }
        .box-risk_state .icon i{
            font-size: 25px !important;
            margin-top: -10px;
        }
        .dropdown-menu {
            width: 250px;
        }
        .dropdown-menu .form-check {
            padding-left: 10px;
        }
        .dropdown-menu .form-check label{
            font-size: 14px;
            font-weight: normal;
            padding-left: 5px;
            vertical-align: middle;
        }
        .progress-group{
            font-size: 13px;
        }

        /* Filters */
        #form-filters .form-group{
            margin-bottom: 2px;
        }
        #form-filters .form-group label{
            font-size: 12px;
            font-weight: normal;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
    </section>

    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <div class="card card-success card-outline card-tabs">
                        <div class="card-header p-0 pt-1">
                            <ul class="nav nav-tabs" id="report-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="tab1" href="{{ route('report.global_risk_state') }}" role="tab" aria-controls="tabs-tab1" aria-selected="true">Estados de Riesgo</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="tab2" href="{{ route('report.global_report_cases') }}" role="tab" aria-controls="tabs-tab2" aria-selected="false">Casos reportados</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="tab3" href="{{ route('report.casos') }}" role="tab" aria-controls="tabs-tab3" aria-selected="false">Listado de casos</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="card card-success card-outline">
                        <div class="card-header">
                            <h3 class="card-title">COVID 19 - Evolución de Casos</h3>

                            <div class="card-tools">
                                <div id="filter_dropdown" class="btn-group">
                                    <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                                        <i class="fas fa-filter"></i> Filtrar
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" role="menu" style="width: 550px !important;">
                                        <h6 class="dropdown-header">
                                            <i class="fas fa-filter"></i> Seleccione algunas opciones para filtrar la información
                                        </h6>
                                        <div class="dropdown-divider"></div>
                                        <!-- Form -->
                                        <form id="form-filters" method="post" action="" style="padding: 10px;">
                                            @include('report.partials.filters_risk_state')

                                            <div class="row" style="padding: 0 10px;">
                                                <button id="btn_close" type="button" class="col-md-3 btn btn-default">
                                                    <i class="fas fa-times"></i> Cerrar
                                                </button>
                                                <button id="btn_filter" type="button" class="col-md-3 offset-md-6 btn btn-success">
                                                    <i class="fas fa-filter"></i> Filtrar
                                                </button>
                                            </div>
                                        </form>
                                        <!-- Form -->
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body" style="display: block;">
                            <p class="text-center" style="font-size: 25px;">
                                <strong id="date_range">
                                    {{-- Casos por día de {{ $init_date->format('d/m/Y') }} hasta {{ $now->format('d/m/Y') }} --}}
                                </strong>
                            </p>
                            @include('report.partials.risk_states')

                            <hr/>

                            <div class="row">
                                <div class="col-md-12">

                                    <div class="row">
                                        <!-- Line Graph -->
                                        <div class="col-md-12">
                                            <div id="ajax_enterprise_cases">
                                                @include('report.partials.line')
                                            </div>
                                        </div>
                                        <!-- Line Graph -->


                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->


                </div>
                <!-- Compare -->
                <div class="col-md-2 align-items-stretch">
                    <div class="card card-success card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Comparación por Empresa</h3>
                            <!-- /.card-tools -->
                            <div class="card-tools">
                            </div>
                        </div>

                        <!-- /.card-header -->
                        <div class="card-body" style="display: block;">
                            @include('report.partials.enterprises_risk_state')
                        </div>

                </div>
                <!-- Compare -->
                </div>
            <!-- Risk states -->
            </div>
        <!-- /.container-fluid -->
        </div>

        <div class="card card-primary card-outline card collapsed-card">
            <div class="card-header">
                <h3 class="card-title">Datos del gráfico</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        Click aquí para mostrar / ocultar &nbsp;<i class="fas fa-plus"></i>
                    </button>
                </div>
            </div>

            <div class="card-body" style="display: none;">
                <div id="data-medical_requests"></div>
            </div>
        </div>

    </section>
    <!-- .content -->

    <!--MODALS-->
    @include('layouts.components.modals.modal_tracings')
    @include('layouts.components.modals.modal_covid_file')

@endsection

@section('script')
    @parent
    <!-- overlayScrollbars -->
    <script src="{{ asset('adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/chart.js/Chart.min.js') }}"></script>
    <!-- jQuery Mapael -->
    <script src="{{ asset('adminlte/plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/raphael/raphael.min.js') }}"></script>

    <script>
        var labels = [];
        var data_confirmed = [];
        var data_recovered = [];
        var data_suspect = [];
        var data_discarted = [];
        var data_deceased = [];


        $(document).ready(function() {
            $('a#nav_report_risk').addClass('active');

            $('body').addClass('sidebar-collapse');

            $('select[name=risk_state_id]').select2();

            $('#init_date_field').datetimepicker({
                format: 'DD/MM/YYYY',
                locale: 'es',
                useCurrent: false,
                maxDate: moment(),
            });

            $('#end_date_field').datetimepicker({
                format: 'DD/MM/YYYY',
                locale: 'es',
                useCurrent: true,
                maxDate: moment(),
            });

            // Init Line Graph
            ajaxEnterpriseRiskState();

            $("#init_date_field").on("change.datetimepicker", function (e) {
            $('#end_date_field').datetimepicker('minDate', e.date);
            });
            $("#end_date_field").on("change.datetimepicker", function (e) {
                $('#init_date_field').datetimepicker('maxDate', e.date);
            });

            // Ajax - Comparación entre empresas
            $('select[name=risk_state_id]').on('change', function (e) {
                ajaxEnterpriseCases();
            });

            // Dropdown
            $('div#filter_dropdown').on('hide.bs.dropdown', function (e) {
                if (e.clickEvent) {
                   e.preventDefault();
                }
            });

            $('button#btn_filter').on('click', function (e) {
                $('div#filter_dropdown button').dropdown('hide');

                ajaxEnterpriseRiskState();
            });

            $('button#btn_close').on('click', function (e) {
                $('div#filter_dropdown button').dropdown('hide');
            });


            // Check control
            $('input[name="enterprises[]"]').on('change', function(e) {
                var project_checks = [];
                $('input[name="enterprises[]"]:checked').each(function (index, value){
                    var projects = eval($(value).attr('data-projects'));
                    $('input[name^="project_all"]').each(function (index, value) {
                        $(value).attr('disabled', false);
                    });
                    for (var i = 0; i < projects.length; i++)
                    {
                        var located = false;
                        for (var j = 0; j < project_checks.length; j++)
                        {
                            if (project_checks[j] == projects[i])
                            {
                                located = true;
                                break;
                            }
                        }
                        if (!located)
                        {
                            project_checks.push(projects[i]);
                        }
                    }
                });

                $('input[name="projects[]"]').each(function (index, value){
                    var id = $(value).attr('id').split('_')[1];
                    console.log(id);
                    for (var k = 0; k < project_checks.length; k++)
                    {
                        var located = false;
                        if (id == project_checks[k])
                        {
                            located = true;
                            $(value).removeAttr('disabled');
                            break;
                        }
                    }
                    if  (!located)
                    {
                        $(value).prop('checked', false)
                            .attr('disabled', true);
                    }
                });
            });

            lineGraphic();
        });

        function enterpriseChange(object) {
            const valueC = $(object).iCheck('update')[0].checked;
            if(valueC){
                $('input[name^="enterprises[]"]').each(function (index, value) {
                    $(value).iCheck('check');
                });
                $('input[name^="projects[]"]').each(function (index, value) {
                    $(value).attr('disabled', false);
                });
                $('input[name^="project_all"]').each(function (index, value) {
                    $(value).attr('disabled', false);
                });
            }else{
                $('input[name^="enterprises[]"]').each(function (index, value) {
                    $(value).iCheck('uncheck')
                });
                $('input[name^="projects[]"]').each(function (index, value) {
                    $(value).iCheck('uncheck').attr('disabled', true);
                });
                $('input[name^="project_all"]').each(function (index, value) {
                    $(value).iCheck('uncheck').attr('disabled', true);
                });
            }
        }

        function projectChange(object) {
            const valueC = $(object).iCheck('update')[0].checked;
            if(valueC){
                $('input[name^="projects[]"]').each(function (index, value) {
                    if(!$(value).is(":disabled"))
                        $(value).iCheck('check');
                });
            }else{
                $('input[name^="projects[]"]').each(function (index, value) {
                    $(value).iCheck('uncheck');
                });
            }
        }
        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();
        var hour = d.getHours();
        var minutes = d.getMinutes();
        var seconds = d.getSeconds();

        var output = d.getFullYear() +
            ((''+month).length<2 ? '0' : '') + month +
            ((''+day).length<2 ? '0' : '') + day + "_" +hour+minutes+seconds;

        // Ajax - Casos por empresa
        function ajaxEnterpriseRiskState ()
        {
            var url = '{{ route('report.ajax_enterpriseRiskState') }}';
            var data = $("form#form-filters").serialize();

            $.ajax({
                url: url,
                method: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: data,
                beforeSend: function(e){
                    // Loading
                    blockCard('div#ajax_enterprise_cases');

                    var context = $('#lineChart').get(0).getContext('2d');
                    context.clearRect(0, 0, document.getElementById('lineChart').width, document.getElementById('lineChart').height);

                    if (lineChart != null)
                        lineChart.destroy();
                }
            })
            .done(function(response) {
                lineChartData.labels = response.array_dates;
                lineChartData.datasets[0].data = response.array_confirmed_per_date;
                lineChartData.datasets[1].data = response.array_recovered_per_date;
                lineChartData.datasets[2].data = response.array_suspect_per_date;
                lineChartData.datasets[3].data = response.array_discarted_per_date;
                lineChartData.datasets[4].data = response.array_deceased_per_date;

                $('span#total_confirmed').html(response.array_confirmed_per_date[response.array_confirmed_per_date.length - 1]);
                $('span#total_recovered').html(response.array_recovered_per_date[response.array_recovered_per_date.length - 1]);
                $('span#total_suspect').html(response.array_suspect_per_date[response.array_suspect_per_date.length - 1]);
                $('span#total_discarted').html(response.array_discarted_per_date[response.array_discarted_per_date.length - 1]);
                $('span#total_deceased').html(response.array_deceased_per_date[response.array_deceased_per_date.length - 1]);

                $('strong#date_range').html('Evolución de casos de ' + response.init_date + ' hasta ' + response.end_date);

                lineGraphic();

                $('div#data-medical_requests').html(response.view_data);

                $('#data-medical_request').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "responsive": true,
                    //ESTA OPCION DE DOM ES PARA ESCOGER ELEMENTOS QUE APARECEN POR DEFECTO EN EL DATA TABLE
                    "dom": 'Blrtp',
                    "order": [[0, 'asc']],
                    "aoColumnDefs": [{ "bSortable": false, "aTargets": [0,1] }],
                    "language": {
                        "decimal": ",",
                        "thousands": ".",
                        "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "infoPostFix": "",
                        "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "loadingRecords": "Cargando...",
                        "lengthMenu": "Mostrar _MENU_ registros",
                        "paginate": {
                            "first": "Primero",
                            "last": "Último",
                            "next": "Siguiente",
                            "previous": "Anterior"
                        },
                        "processing": "Procesando...",
                        "search": "Buscar:",
                        "searchPlaceholder": "Término de búsqueda",
                        "zeroRecords": "No se encontraron resultados",
                        "emptyTable": "Ningún dato disponible en esta tabla",
                        "aria": {
                            "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sortDescending": ": Activar para ordenar la columna de manera descendente"
                        },
                    },
                    buttons: [
                        {"extend": 'excelHtml5',
                            "text":'<i class="fa fa-file-excel"></i> Exportar a Excel',
                            "className": 'btn btn-success',"title": "Reporte de Casos_"+output,
                        },
                    ],
                    "pageLength": 10
                });

                unblockCard('div#ajax_enterprise_cases');
            })
            .fail(function(response) {
                toastr.error(
                    response,
                    '¡Error!'
                );

                unblockCard('div#ajax_enterprise_cases');
            });
        }

        // Ajax - Comparación entre empresas
        function ajaxEnterpriseCases ()
        {
            var risk_state_id = $('select[name=risk_state_id]').val();
            var url = '{{ route('report.ajax_enterpriseCases') }}';

            $.ajax({
                url: url,
                method: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: 'risk_state_id=' + risk_state_id,
                beforeSend: function(e){
                    // Loading
                    blockCard('div#ajax_enterprise_risk_state');
                }
            })
            .done(function(response) {
                $('div#ajax_enterprise_risk_state').html(response.view);

                unblockCard('div#ajax_enterprise_risk_state');
            })
            .fail(function(response) {
                toastr.error(
                    response,
                    '¡Error!'
                );

                unblockCard('div#ajax_enterprise_risk_state');
            });
        }

        /* ChartJS
        * -------
        * Here we will create a few charts using ChartJS
        */
        var lineChartCanvas = $('#lineChart').get(0).getContext('2d')

        var lineChartData = {
            labels  : labels,
            datasets: [
                {
                    label                 : 'Confirmados',
                    backgroundColor       : 'rgba(220,53,69,0.7)',
                    borderColor           : 'rgba(220,53,69,0.7)',
                    pointRadius           : false,
                    fill                  : false,
                    data                  : data_confirmed
                },
                {
                    label                 : 'Recuperados',
                    backgroundColor       : 'rgba(40,167,69,0.7)',
                    borderColor           : 'rgba(40,167,69,0.7)',
                    pointRadius           : false,
                    fill                  : false,
                    data                  : data_recovered
                },
                {
                    label                 : 'Sospechosos',
                    backgroundColor       : 'rgba(255,193,7,0.7)',
                    borderColor           : 'rgba(255,193,7,0.7)',
                    pointRadius           : false,
                    fill                  : false,
                    data                  : data_suspect
                },
                {
                    label                 : 'Descartados',
                    backgroundColor       : 'rgba(0,0,0,0.7)',
                    borderColor           : 'rgba(0,0,0,0.7)',
                    pointRadius           : false,
                    fill                  : false,
                    data                  : data_discarted
                },
                {
                    label                 : 'Fallecidos',
                    backgroundColor       : 'rgba(111,66,193,0.7)',
                    borderColor           : 'rgba(111,66,193,0.7)',
                    pointRadius           : false,
                    fill                  : false,
                    data                  : data_deceased
                }
            ]
        }

        var lineChartOptions = {
            maintainAspectRatio : false,
            responsive : true,
            scales: {
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'PANDEMIA COVID-19 (GESTIÓN 2020)',
                    }
                }],
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'NÚMERO DE CASOS',
                    }
                }]
            },
            /*tooltips: {
                mode: 'index',
                intersect: false,
            },*/
        }

        // This will get the first returned node in the jQuery collection.
        var lineChart = null;

        function lineGraphic () {
            lineChart = new Chart(lineChartCanvas, {
                type: 'line',
                data: lineChartData,
                options: lineChartOptions
            });
            lineChart.render();
        }
    </script>
@endsection
