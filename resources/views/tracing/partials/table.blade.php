<table id="medical_requests_tracing" class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Op.</th>
        <th>Estado</th>
        <th>Fecha. Seg.</th>
        <th>Días Aislamiento</th>
        <th>Riesgo</th>
        <th>Cód. de Solicitud</th>
        <th>Nombre Completo</th>
        <th>CI</th>
        <th>Empresa</th>
        <th>Email</th>
        <th>Cód. de Empleado</th>
        <th>Nro de Teléfono</th>
        <th>Empleado Asignado</th>
        <th>Resp. Seg.</th>
    </tr>
    </thead>

    <tbody>
    @foreach($medical_requests as $medical_request)
        <tr id="tr_medical_consultation_{{$medical_request->id}}">
            @include('tracing.partials.tr_content',['medical_request'=>$medical_request])
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th>Op.</th>
        <th>Estado</th>
        <th>Fecha. Seg.</th>
        <th>Días Aislamiento</th>
        <th>Riesgo</th>
        <th>Cód. de Solicitud</th>
        <th>Nombre Completo</th>
        <th>CI</th>
        <th>Empresa</th>
        <th>Email</th>
        <th>Cód. de Empleado</th>
        <th>Nro de Teléfono</th>
        <th>Empleado Asignado</th>
        <th>Resp. Seg.</th>
    </tr>
    </tfoot>
</table>
