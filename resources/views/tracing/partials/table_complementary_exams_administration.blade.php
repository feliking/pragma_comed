<div class="table-responsive">
    @if (isset($medical_request) && count($tracing->complementaryExams) > 0)
    <table class="table table-hover" id="table_complementary_exams">
    @else
    <table class="table table-hover" id="table_complementary_exams" hidden>
    @endif
        <thead>
        <tr>
            <th>Laboratorio</th>
            <th>Tipo de Prueba</th>
            <th>Costo</th>
            <th>Número de Factura</th>
            <th>¿La Empresa Pagó?</th>
            <th>Archivo</th>
            <th></th>
        </tr>
        </thead>
        <tbody id="tbody_complementary_exams">
            @if (isset($medical_request))
                @if (count($tracing->complementaryExams) > 0)
                    @foreach ($tracing->complementaryExams as $complementary_exam)
                    <tr>
                        <td><p>{{ $complementary_exam->laboratory->name }}</p></td>
                        <td><p> @if($complementary_exam->testType != null)
                                                            {{ $complementary_exam->testType->name }}
                                                            @else
                                                                S/R
                                                            @endif</p></td>
                        <td><p>{{ $complementary_exam->cost }}</p></td>
                        <td><p>{{ $complementary_exam->bill_number }}</p></td>
                        <td>
                            @if ($complementary_exam->enterprise_flag == true)
                            <span class="badge badge-success">SI</span>
                            @else
                            <span class="badge badge-danger">NO</span>
                            @endif
                        </td>
                        <td>
                            @if (count($complementary_exam->files) > 0)
                            {{ $complementary_exam->files()->first()->name }}
                            @else
                            <span class="badge badge-danger">Sin Archivo</span>
                            @endif
                        </td>
                        <td></td>
                    </tr>
                    @endforeach
                @endif
            @endif
        </tbody>
    </table>
    @if (isset($medical_request) && count($tracing->complementaryExams) > 0)
    <div id="alert_table_complementary" class="alert alert-info mb-12" role="alert" style="display: none;">
    @else
    <div id="alert_table_complementary" class="alert alert-info mb-12" role="alert">
    @endif
        <h4 class="alert-heading"><i class="fa fa-info"></i> Sin Registro</h4>
        <p>No hay pruebas de laboratorio registradas.</p>
    </div>
    <div id="files"></div>
</div>
