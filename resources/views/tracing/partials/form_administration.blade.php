<div class="form-row">
    <div class="form-group col-md-12 col-sm-12 {{ $errors->has('risk_type_id') ? ' has-error' : '' }}" id="wrapper-risk_type_id">
        {!! Form::label('risk_type_id', '* Tipo de Riesgo', ['class' => 'control-label col-md-6 col-xs-12']) !!}
        <div class="col-md-12 col-sm-12 col-xs-12">
            {!! Form::select('risk_type_id', $risk_types, null, ['class' => 'form-control select2 '.( $errors->has('risk_type_id') ? ' is-invalid' : '' ), 'style'=>'with: 100%;']) !!}

            @error('risk_type_id')
            <span class="invalid-feedback">
                <strong>{{ $errors->first('risk_type_id') }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>

<div class="form-group" id="div_disaese_criteria">
</div>

<div class="form-group col-md-12">
    <label>{{ __('Comentario u Observación') }}</label>
    <div class="input-group mb-3">
        <textarea name="comment_tracing" rows="3" class="form-control @error('comment_tracing') is-invalid @enderror" placeholder="{{ __('Comentario u Observación') }}">{{ $tracing->comment }}</textarea>

        @error('comment_tracing')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-row col-md-12 col-sm-12">
    <div class="form-group col-md-6 col-sm-12 {{ $errors->has('treatment_stage_id') ? ' has-error' : '' }}" id="wrapper-treatment_stage_id">
        {!! Form::label('treatment_stage_id', '* Etapa de Tratamiento', ['class' => 'control-label col-md-12 col-md-12 col-xs-12']) !!}
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if (isset($tracing_last))
                {!! Form::select('treatment_stage_id', $treatment_stages, null, ['class' => 'form-control select2 '.( $errors->has('treatment_stage_id') ? ' is-invalid' : '' ), 'style'=>'with: 100%;', 'disabled'=> true]) !!}
                <input type="hidden" name="treatment_stage_id_hidden" id="treatment_stage_id_hidden" value="{{ isset($tracing_last_data) ? $tracing_last_data->treatment_stage_id : 1 }}">
            @else
                {!! Form::select('treatment_stage_id', $treatment_stages, null, ['class' => 'form-control select2 '.( $errors->has('treatment_stage_id') ? ' is-invalid' : '' ), 'style'=>'with: 100%;']) !!}
                <input type="hidden" name="treatment_stage_id_hidden" id="treatment_stage_id_hidden" value="{{ isset($tracing_last_data) ? $tracing_last_data->treatment_stage_id : 1 }}">
            @endif

            @error('treatment_stage_id')
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('treatment_stage_id') }}</strong>
                </span>
            @enderror

        </div>
    </div>
    <div class="form-group col-md-6 col-sm-12 {{ $errors->has('risk_state_id') ? ' has-error' : '' }}" id="wrapper-risk_state_id">
        {!! Form::label('risk_state_id', '* Estado de Riesgo', ['class' => 'control-label col-md-12 col-xs-12']) !!}
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if (isset($tracing_last))
                {!! Form::select('risk_state_id', array(), null, ['class' => 'form-control select2 '.( $errors->has('risk_state_id') ? ' is-invalid' : '' ), 'style'=>'with: 100%;', 'disabled' => true]) !!}
                <input type="hidden" name="risk_state_id_hidden" id="risk_state_id_hidden" value="{{ isset($tracing_last_data) ? $tracing_last_data->risk_state_id : 1 }}">
            @else
                {!! Form::select('risk_state_id', array(), null, ['class' => 'form-control select2 '.( $errors->has('risk_state_id') ? ' is-invalid' : '' ), 'style'=>'with: 100%;']) !!}
                <input type="hidden" name="risk_state_id_hidden" id="risk_state_id_hidden" value="{{ isset($tracing_last_data) ? $tracing_last_data->risk_state_id : 1 }}">
            @endif

            @error('risk_state_id')
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('risk_state_id') }}</strong>
                </span>
            @enderror

        </div>
    </div>
</div>

<div class="form-row col-md-12 col-sm-12">
    <label class="my-1">Registro de Laboratorio</label>
    <button type="button" class="btn btn-sm btn-info mx-2 mb-1 mt-0" data-toggle="modal" data-target="#modal_complementary_exam"><i class="fa fa-plus"></i></button>
    @include('tracing.partials.table_complementary_exams_administration')
</div>

<div class="form-row">
    <label for="medical_attention_flag">Si considera que el paciente necesita una proxima atención médica marque la siguiente casilla: &nbsp;</label>
    <input type="checkbox" name="medical_attention_flag" id="medical_attention_flag" value="1" {{ ($tracing->medical_attention_flag == true) ? 'checked' : '' }}>
</div>
<div class="form-row col-md-12 col-sm-12">
    <label for="kit">¿Se entregará un kit al paciente? &nbsp;</label>
    <input type="checkbox" name="kit" id="kit" value="1" {{ (!is_null($kit_tracing)) ? 'checked' : '' }}>
</div>

<div id="div_kit" class="form-row col-md-12 col-sm-12" {{ (is_null($kit_tracing)) ? 'hidden' : '' }}>
    <div class="form-group col-md-6 col-sm-12{{ $errors->has('kit_id') ? ' has-error' : '' }} wrapper-kit_id " id="kit_id">
        {!! Form::label('kit_id', '* Tipo de Kit', ['class' => 'control-label col-md-12 col-sm-4 col-xs-12 ']) !!}
        <div class="col-md-12 col-sm-9 col-xs-12 input-group mb-3">
            {!! Form::select('kit_id', $kit_types, (is_null($kit_tracing) ? null:$kit_tracing->kit_id ), ['class' => 'form-control  select2', 'placeholder' => 'Seleccione un kit']) !!}

        </div>
    </div>
    <div class="form-group col-md-6 col-sm-12{{ $errors->has('quantity') ? ' has-error' : '' }} wrapper-quantity">
        <label>{{ __('* Cantidad:') }}</label>
        <div class="col-md-12 col-sm-9 col-xs-12 input-group mb-3">
            <input name="quantity" id="quantity" type="text" class="form-control"
                    value="{{ is_null($kit_tracing) ? '':$kit_tracing->quantity }}"
                    placeholder="{{ __('Introduzca una cantidad') }}">
            <span class="invalid-feedback" role="alert">
                @error('quantity')
                    <strong>{{ $message }}</strong>
                @enderror
            </span>
        </div>
    </div>
</div>
    @php
        $edad_paciente = 0;
        if(isset($medical_request)){
            if($medical_request->employee!=null){
                $edad_paciente = $medical_request->employee->edad;
            }else{
                $edad_paciente = $medical_request->edad;
            }
        }
    @endphp
    <script>
        var search_disaese = @json(route('tipos_riesgo.busqueda_enfermedades.getDisaeseCriteriaFromRiskType','_id'));
        var search_risk_type = @json(route('tipos_riesgo.busqueda_estados_resgo.getRiskStatesFromRiskType','_id'));
        var parameter_age = @json($parameter_age);
        var edad_paciente = @json($edad_paciente);
        var parameter_value = @json($parameter_value);

        $(document).ready(function() {
            $('select[name=risk_type_id]').select2({
                theme: 'bootstrap4',
                placeholder: 'SELECCIONE UNA OPCIÓN',
                width: 'auto'
            }).val({{ $tracing->risk_type_id }}).trigger('change');

            $('select[name=treatment_stage_id]').select2({
                theme: 'bootstrap4',
                placeholder: 'SELECCIONE UNA OPCIÓN',
                width: 'auto'
            }).val({{ $tracing->treatment_stage_id }}).trigger('change');


            $('select[name=risk_state_id]').select2({
                theme: 'bootstrap4',
                placeholder: 'SELECCIONE UNA OPCIÓN',
                width: 'auto'
            }).val('').trigger('change');

            $('select[name=kit_id]').select2({
                theme: 'bootstrap4',
                placeholder: 'SELECCIONE UNA OPCIÓN',
                width: 'auto'
            }).val('').trigger('change');

            $('input[type=checkbox][name=medical_attention_flag]').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
            $('input[type=checkbox][name=kit]').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

            $('#quantity').inputmask({
                alias: 'numeric',
                allowMinus: false,
                digits: 2,
                max: 99,
                min: 1
            });

        });
        $('input[type=checkbox][name^=kit]').on('ifChecked', function(event){
            $('div#div_kit').removeAttr('hidden');
            $('input[name=quantity]').attr('required', true);
            $('select[name=kit_id]').attr('required', true);
            //$('input[name=quantity]').attr('min', 1);
        });
        $('input[type=checkbox][name^=kit]').on('ifUnchecked', function(event){
            $('div#div_kit').attr('hidden', true);
            $('input[name=quantity]').val('');
            $('select[name=kit_id]').val('').trigger('change');
            $('input[name=quantity]').removeAttr('required');
            $('select[name=kit_id]').removeAttr('required');
            //$('input[name=quantity]').removeAttr('min');
        });


        $('select#risk_type_id').change(function (e) {
            $('#risk_type_id_hidden').val($(this).val());
            var selected_op = $('select#risk_type_id option:selected').val();
            if(selected_op!=null && selected_op!=""){

                //CARGA LAS ENFERMEDADES
                var url = search_disaese.replace('_id',selected_op);
                $.ajax({
                    url: url,
                    method: 'get',
                    beforeSend: function(e){
                        // Loading
                        // blockCard('#form_content');
                    }
                })
                .done(function(response) {
                    switch (response['type']) {
                        case "correct":
                            $('#div_disaese_criteria').empty();
                            $('#div_disaese_criteria').append(response['view']);

                            @foreach($tracing->diseaseCriterias as $disease_criteria)
                            $('input#disaese_{{ $disease_criteria->id }}').prop('checked', true);
                            @endforeach

                            break;
                        case "error":
                            $('#div_disaese_criteria').empty();
                            toastr.error(
                                response['error'],
                                '¡Error!'
                            );
                            break;
                    }
                })
                .fail(function() {
                    // unblockCard('#form_content');
                });

                //CARGA LOS ESTADOS DE RIESGO
                url = search_risk_type.replace('_id',selected_op);
                $.ajax({
                    url: url,
                    method: 'get',
                    beforeSend: function(e){
                        // blockCard('#form_content');
                        $('select[name=risk_state_id]').attr('disabled', 'disabled');
                    }
                })
                .done(function(response) {
                    $('select[name=risk_state_id]').empty();

                    $.each(response['risk_states'], function (index, value) {
                        var newOption = new Option(value['full_name'], value['id'], false, false);
                        newOption.setAttribute('data-weighting',value['weighting']);
                        $('select[name=risk_state_id]').append(newOption);
                    });

                    $('select[name=risk_state_id]').val({{ $tracing->risk_state_id }}).trigger('change');
                    $('select[name=risk_state_id]').removeAttr('disabled');
                })
                .fail(function() {
                    // unblockCard('#form_content');
                });
            }else{
            }
        });

        $('select#risk_state_id').change(function (e) {
            var selected_op = $('select#risk_state_id option:selected').val();
            $('#risk_state_id_hidden').val($(this).val());
            if(selected_op!=null && selected_op!=""){
                calcWeigthignMessage();
            }
        });

        function disaeseChange(object, id) {
            if($(object).data('weighing') == 0){
                $('input[name^=disease_criteria]').each(function (index, value) {
                    if(id != $(value).val()){
                        $(value).iCheck('uncheck');
                    }
                });
            }else{
                $('input[name^=disease_criteria]').each(function (index, value) {
                    if($(value).data('weighing')==0)
                        $(value).iCheck('uncheck');
                });
            }
            // var selected = $(object).iCheck('update')[0].checked;
            calcWeigthignMessage();
       }

        function calcWeigthignMessage() {
            var valor = 0;

            $('input[name^=disease_criteria]').each(function (index, value) {
                var selected = $(value).iCheck('update')[0].checked;
                if(selected){
                    valor += parseFloat($(value).data('weighing'));
                }
            });

           //CALCULO DE LA EDAD
            if(parseFloat(edad_paciente) >= parseFloat(parameter_age)){
                valor = valor + parseFloat(parameter_value);
                $('#span_edad').text('Edad > '+parameter_age+' ('+parameter_value+')');
            }else{
                $('#span_edad').text('Edad < '+parameter_age);
            }

            //CALCULO DEL TIPO DE RIESGO
            var selected_op = $('select#risk_state_id option:selected');
            var risk_pond = $(selected_op).data('weighting');
            if(risk_pond>0){
                $('span#span_risk_type').text($(selected_op).text());
                valor = valor + parseFloat(risk_pond);
            }else{
                $('span#span_risk_type').text('');
            }

            $('span#span_puntaje').text(valor);

            var en_rango = true;
            var maximo = null;

            if(typeof obj_weighting_levels !== 'undefined'){
                $.each(obj_weighting_levels, function( index, value ) {
                    if(value['last_level_flag'] == false){
                        //RECORRE SOLO LOS QUE NO SON ULTIMO NIVEL
                        if(value['minimum'] <= valor && value['maximum'] >= valor){
                            $('#span_weigthing').text(value['name']);
                            $('#span_weigthing').css('background-color', value['color']);

                            if(en_rango){
                                en_rango = false;
                            }
                        }
                    }else{
                        maximo = value;
                    }
                });

                if(en_rango){
                    //SIGNIFICA QUE DEBE BUSCAR EL MAXIMO
                    if(maximo['minimum'] <= valor){
                        $('#span_weigthing').text(maximo['name']);
                        $('#span_weigthing').css('background-color', maximo['color']);
                    }
                }
            }
       }

    </script>
