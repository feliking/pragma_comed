@extends('layouts.app_menu')

@section('styles')
    <style>
        .dataTables_filter {
            display: none !important;
        }
        div.dt-buttons {
            position: relative !important;
            float: right !important;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Listado de Casos</h1>
                </div>
                {{--<div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">DataTables</li>
                    </ol>
                </div>--}}
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Mis seguimientos</h3>

                            <div class="card-tools">
                                <p id="demo"></p>

                                <div class="input-group input-group-sm">
                                    <input id="search_data" type="text" class="form-control" placeholder="Buscar">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @include('tracing.partials.table')
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
        </div>
    </section>

    <!--MODALS-->
    @include('layouts.components.modals.modal_history')
    @include('tracing_administrations.modals.modal_close')
    @include('layouts.components.modals.modal_covid_file')
    @include('layouts.components.modals.modal_history_timeline')
    @include('layouts.components.modals.modal_tracings')
    @include('layouts.components.modals.modal_files')
    @include('layouts.components.modals.modal_reprogramming')
    @include('layouts.components.modals.modal_remit')
{{--    @include('medical_request_administration.modals.modal_employee_assign')--}}
    {{--    @include('medical_request_administration.modals.modal_medic_assign')--}}
{{--    @include('layouts.components.modals.modal_history')--}}
{{--    @include('employee.modals.modal_search_result_ci')--}}
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('a#nav_mis_seguimientos').addClass('active');

            $('.select2').select2({
                theme: 'bootstrap4',
                width: 'auto'
            });
        });

        // $( ".div.dt-buttons" ).addClass( "float-right");
        /*RUTAS DEL SISTEMA*/
        /*RUTAS DEL SISTEMA*/
        const patientAsign = @json(route('administracion_seguimientos.get_patient', '_ID'));
        const patientAsignStore = @json(route('administracion_seguimientos.store'));
        const patientAsignMedicStore = @json(route('administracion_seguimientos.storeMedic'));
        const seguimientoIndex = @json(route('administracion_seguimientos.index'));
        const forSearchProjects = @json(route('proyectos.para_buscador.getProjectsFromEnterpriseSearch','_id'));

        const search = @json(route('empleados.busqueda_empresa.getEmployeeFromEnterprise','_id'));
        const searchCi = @json(route('empleados.busqueda_ci.getEmployeeFromCI',['ci'=>'_ci','ci_expedition'=>'_ci_exp']));
        const assingToEmp = @json(route('administracion_seguimientos.asignar_empleado.assignToEmploye'));
        const assingToMed = @json(route('administracion_seguimientos.asignar_medico.assignToMedic'));
        const assingToMe = @json(route('administracion_seguimientos.asignarme.assignToMe'));
        const employyeNotFound = @json(route('administracion_seguimientos.no_encontrado.employeeNotFound'));
        const verifyStatePre = @json(route('administracion_seguimientos.verificar.verifyStatePreSol'));
        const closeStatePre = @json(route('administracion_seguimientos.cerrar_pre.closeStatePreSol'));
        const closeStateAuto = @json(route('administracion_seguimientos.cerrar_pre.closeChangeStateAuto'));
        const updateRegisters = @json(route('administracion_seguimientos.actualizar_tabla.updateRegisters'));
        const showHistory = @json(route('administracion_solicitudes.mostrar_historial.showHistory','_id'));
        const closeCase = @json(route('administracion_seguimientos.cerrar_caso.closeCase','_id'));
        const citasAppointment = @json(route('citas.get_appointments.getAppointmentsFromUserDate'));
        const showHistoryTimeline = @json(route('buscar_codigo.mostrarTimeline'));
        const showHistoryFiles = @json(route('mostrar_archivos.showFiles','_id'));
    </script>
    <script src="{{asset('js/tracing/index.js')}}"></script>
    <script src="{{asset('js/tracing_administration/index.js')}}"></script>
    <script src="{{asset('js/components/button_historial.js')}}"></script>
    <script src="{{asset('js/components/button_historial_timeline.js')}}"></script>
@endsection
