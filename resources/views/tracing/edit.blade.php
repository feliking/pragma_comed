@extends('layouts.app_menu')

@section('styles')
    <style>
        .dataTables_filter {
            display: none !important;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Registro de seguimiento</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @include('medical_request.partials.div_card_pacient_data')
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">HISTORIAL CLÍNICO</h3>
                        </div>
                        <div class="card-body" style="transform: traslate(0,0); height: 605px;overflow: auto;padding: 10px">
                            <div class="col-12" style="">
                                @if ($cantidad == 0)
                                    <div class="alert alert-light" role="alert">
                                        Sin registros
                                    </div>
                                @else
                                    @if ($historial_total)
                                        @include('medical_consultations.partials.time_line_employee', ['medical_request' => $medical_requests ])
                                    @else
                                        @include('medical_consultations.partials.time_line', ['history' => $historial_requests ])
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    {!! Form::open(['route' => ['seguimientos.update',$medical_request->id], 'method' => 'PUT', 'id' => 'frm-seguimientos_id','enctype' => 'multipart/form-data']) !!}
                    <div class="card card-info">
                        <!-- /.card-header -->
                        <div class="card-header">
                            <h3 class="card-title">FORMULARIO DE SEGUIMIENTO</h3>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-body">
                            @if (!is_null($mi_mje))
                                <div class="alert alert-warning alert-dismissible">
                                    <h5><i class="icon fas fa-exclamation-triangle"></i> Advertencia</h5>
                                    <p>El paciente se encuntra en <strong>{{ $mi_mje }}</strong></p>
                                </div>
                            @endif
                            @include('tracing.partials.form')
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <div class="form-group">
                                <div class="col-xs-12 text-right">
                                    <a href="{{ route('seguimientos.index') }}" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Volver</a>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Registrar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>

    <!-- MODALS -->
    @include('complementary_exams.modals.modal_complementary_exam')
@endsection

@section('script')
    <script src="{{ asset('/js/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('/js/tracing/general.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('a#nav_mis_seguimientos').addClass('active');

            $('.select2').select2({
                theme: 'bootstrap4',
                placeholder: 'SELECCIONE UNA OPCIÓN',
                width: 'auto'
            });
        });
    </script>
@endsection
