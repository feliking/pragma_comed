<table id="example2" class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Op.</th>
        <th>Nombre</th>
        <th>Descripción</th>
        <th>Estado</th>
    </tr>
    </thead>

    <tbody>
    @foreach($test_types as $test_type)
        <tr>
            <td>
                <div class="btn-group" role="group">
                    <a href="{{ route('tipos_prueba.edit', $test_type->id) }}" class="btn btn-sm btn-default" title="Editar">
                        <i class="fa fa-edit"></i>
                        <span class="sr-only">Editar</span>
                    </a>
                    <a href="{{ route('tipos_prueba.show', $test_type->id) }}" class="btn btn-sm btn-default" title="{{($test_type->deleted_at == null)?'Desactivar':'Activar'}}">
                        <i class="fa fa-{{($test_type->deleted_at == null)?'times':'check'}}"></i>
                        <span class="sr-only">{{($test_type->deleted_at == null)?'Desactivar':'Activar'}}</span>
                    </a>
                </div>
            </td>
            <td>
                {{$test_type->name}}
            </td>
            <td>
                {{$test_type->description}}
            </td>
            <td>
                @if($test_type->deleted_at == null)
                <span class="badge badge-success">Activo</span>
                @elseif($test_type->deleted_at != null)
                <span class="badge badge-danger">Inactivo</span>
                @endif
            </td>

        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Op.</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Estado</th>
        </tr>
    </tfoot>
</table>
