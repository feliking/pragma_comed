<!-- Modal -->
<div class="modal fade" id="modal_employee_assign" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    {{ __('Registro de Empleado') }} <span id="mi_modal_timer" class="badge badge-warning">20:23</span>
                </h5>
            </div>
            <div class="modal-body">
                <div class="container-fluid" id="datos_paciente">
                    <div class="row">
                        <div class="col-md-4">
                            <label><i class="fa fa-barcode"></i><b> Codigo de Solicitud</b></label>
                            <p id="p_modal_code"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fa fa-user"></i><b> Nombre de empleado</b></label>
                            <p id="p_modal_fullname"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fa fa-sort-numeric-desc"></i><b> CI</b></label>
                            <p id="p_modal_full_ci"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fa fa-building"></i><b> Empresa</b></label>
                            <p id="p_modal_enterprise"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fa fa-phone"></i><b> Telefono</b></label>
                            <p id="p_modal_phone"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fa fa-envelope"></i><b> Email</b></label>
                            <p id="p_modal_email"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label><i class="fa fa-edit"></i><b> Motivo de solicitud</b></label>
                            <p id="p_modal_type"></p>
                        </div>
                        <div class="col-md-6">
                            <label><i class="fa fa-comment"></i><b> Comentario u Observacion</b></label>
                            <p id="p_modal_comment"></p>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-md-11 col-sm-12">
                        <div class="input-group mb-3">
                            <input id="ci" name="ci" type="text" value="{{ old('ci') }}" class="form-control @if ($errors->has('ci') || $errors->has('ci_expedition')) is-invalid @endif" placeholder="{{ __('* Cédula') }}" data-inputmask='"mask": "9999999[9][-9A]"' data-greedy="false" data-mask/>
                            <div class="input-group-append append-ci">
                                <select id="ci_expedition" name="ci_expedition" class="select2">
                                    <option value="LP">LP</option>
                                    <option value="CB">CB</option>
                                    <option value="SC">SC</option>
                                    <option value="CH">CH</option>
                                    <option value="OR">OR</option>
                                    <option value="PT">PT</option>
                                    <option value="TJ">TJ</option>
                                    <option value="BE">BE</option>
                                    <option value="PD">PD</option>
                                </select>
                            </div>

                            @if ($errors->has('ci') || $errors->has('ci_expedition'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('ci') ? : $errors->first('ci_expedition') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-1 col-sm-12">
                        <button class="btn btn-primary" onclick="buscarEmpleadoCI()">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
                <hr>
                <div class="form-group wrapper-empresa_id" id="wrapper-empresa_id">
                    <label for="empresa_id" class="control-label col-md-4 col-sm-4 col-xs-12">{{ __('*Empresa') }}</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <select id="enterprise_id" required value="{{ old('enterprise_id') }}" class="form-control select2 @error('enterprise_id') is-invalid @enderror" style="width: 100%;">
                                <option></option>
                                @foreach($enterprises as $enterprise)
                                    <option value="{{$enterprise->id}}">{{$enterprise->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <span class="help-block">
                            <strong></strong>
                        </span>
                    </div>
                </div>

                <div class="form-group wrapper-rrhh_empleado_id" id="wrapper-rrhh_empleado_id">
                    <label for="rrhh_empleado_id" class="control-label col-md-4 col-sm-4 col-xs-12">{{ __('*Empleado') }}</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <select disabled name="employee_id" required value="{{ old('employee_id') }}" class="form-control select2 @error('employee_id') is-invalid @enderror" style="width: 100%;">
                                <option></option>
                            </select>
                        </div>
                        <span class="help-block">
                        <strong></strong>
                    </span>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnCerrarModalAsignacion" type="button" class="btn btn-secondary">
                    <i class="fa fa-close"></i> Cerrar
                </button>
                <button id="btnPacienteNoEncotrado" class="btn btn-danger">
                    <i class="fa fa-minus-circle"></i> Paciente no encontrado
                </button>
                <button disabled id="btnAsignarmePaciente" type="button" class="btn btn-info">
                    <i class="fa fa-file-medical"></i> Asignarme Paciente
                </button>
            </div>
        </div>
    </div>
</div>
