<!-- Modal -->
<div class="modal fade" id="modal_medic_assign" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('Asignación de Médico') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group wrapper-rrhh_empleado_id" id="wrapper-rrhh_empleado_id">
                    <label for="rrhh_empleado_id" class="control-label col-md-4 col-sm-4 col-xs-12">{{ __('*Médico') }}</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <select name="medic_id" required value="{{ old('medic_id') }}" class="form-control select2 @error('medic_id') is-invalid @enderror" style="width: 100%;">
                                <option></option>
                                @foreach($medics as $medic)
                                    <option value="{{$medic->id}}">{{$medic->full_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <span class="help-block">
                        <strong></strong>
                    </span>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                </button>
                <button type="button" class="btn btn-success" onclick="registrarMedico()">
                    <i class="fa fa-save"></i> Registrar
                </button>
            </div>
        </div>
    </div>
</div>
