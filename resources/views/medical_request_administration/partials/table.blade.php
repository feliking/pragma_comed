<table id="medical_requests_table" class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Op.</th>
            <th>Estado</th>
            <th>Código de Solicitud</th>
            <th>Nombre Completo</th>
            <th>CI</th>
            <th>Empresa</th>
            <th>Email</th>
            <th>Código de Empleado</th>
            <th>Número de Teléfono</th>
            <th>Tipo de Solicitud</th>
            <th>Empleado Asignado</th>
            <th>Médico Asignado</th>
        </tr>
    </thead>

    <tbody>
    @foreach($medical_requests as $medical_request)
        <tr id="tr_medical_request_{{$medical_request->id}}">
            @include('medical_request_administration.partials.tr_content',['medical_request'=>$medical_request])
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Op.</th>
            <th>Estado</th>
            <th>Código de Solicitud</th>
            <th>Nombre Completo</th>
            <th>CI</th>
            <th>Empresa</th>
            <th>Email</th>
            <th>Código de Empleado</th>
            <th>Número de Teléfono</th>
            <th>Tipo de Solicitud</th>
            <th>Empleado Asignado</th>
            <th>Médico Asignado</th>
        </tr>
    </tfoot>
</table>
