<td>
    <div class="btn-group">
        @include('layouts.components.buttons.button_historial',[
            'medical_request_id' => $medical_request->id
        ])
        @if($medical_request->medical_request_status_id==1 or $medical_request->medical_request_status_id==6) <!--PRE ASIGNADO-->
            <button type="button" class="btn btn-sm btn-warning" onclick="abrirModalRegistrarEmpleado({{$medical_request->id}}, this)">
                <i class="fa fa-user"></i>
            </button>
        @endif
        @if($medical_request->medical_request_status_id==5) <!--VALIDADO-->
            <button type="button" class="btn btn-sm btn-success" onclick="abrirModalAsignarme({{$medical_request->id}}, '{{$medical_request->code}}', this)">
                <i class="fa fa-check"></i>
            </button>
        @endif
        {{--@if($medical_request->user_id==null)
            <button type="button" class="btn btn-sm btn-info" onclick="abrirModalRegistrarEmpleado({{$medical_request->id}})">
                <i class="fa fa-file-medical"></i>
            </button>
        @endif--}}
    </div>
</td>
<td>
    <span class="badge badge-info">
        {{$medical_request->medical_request_status->name}}
    </span>
</td>
<td data-code>
    {{$medical_request->code}}
</td>
<td data-full_name>
    {{$medical_request->full_name}}
</td>
<td data-full_ci>
    <input type="hidden" data-ci value="{{$medical_request->ci}}">
    <input type="hidden" data-ci_expedition value="{{$medical_request->ci_expedition}}">
    {{$medical_request->full_ci}}
</td>
<td data-enterprise>
    @if($medical_request->employee!=null)
        <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
        {{$medical_request->employee->enterprise->nombre}}
    @else
        @if($medical_request->history_valid_status)
            <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
        @else
            <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
        @endif
        {{$medical_request->enterprise}}
    @endif
</td>
<td data-email>
    @if($medical_request->email!=null)
        {{$medical_request->email}}
    @else
        <span class="badge badge-warning">
            S/N
        </span>
    @endif
</td>
<td>
    {{$medical_request->employee_code}}
</td>
<td data-phone-all>
    <input type="hidden" id="data-phone" value="{{$medical_request->phone_number}}">
    <input type="hidden" id="data-phone-whatsapp_flag" value="{{$medical_request->whatsapp_flag}}">
    @if($medical_request->whatsapp_flag)
        <div class="badge badge-success">
            <i class="fab fa-whatsapp"></i>
        </div>
    @endif
    {{$medical_request->phone_number}}
</td>
<td>
    {{-- {{$medical_request->medical_requests_type->name}} --}}
</td>
<td>
    @if($medical_request->employee!=null)
        {{$medical_request->employee->full_name}}
    @else
        <span class="badge badge-warning">
            S/A
        </span>
    @endif
</td>
<td>
    @if($medical_request->user_id!=null)
        {{$medical_request->medic->full_name}}
    @else
        <span class="badge badge-warning">
            S/A
        </span>
    @endif
</td>
