@extends('layouts.app_menu')

@section('styles')
    <style>
        .dataTables_filter {
            display: none !important;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Listado de Solicitudes</h1>
                </div>
                {{--<div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">DataTables</li>
                    </ol>
                </div>--}}
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Datos de consultas</h3>

                            <div class="card-tools">
                                <p id="demo"></p>

                                <div class="input-group input-group-sm">
                                    <input id="search_data" type="text" class="form-control" placeholder="Buscar">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            {{--<div class="mailbox-controls">
                                <!-- Check all button -->
                                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="far fa-square"></i>
                                </button>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                    <button id="btnAssingEmployee" type="button" data-toggle="modal" data-target="#modal_employee_assign" class="btn btn-default btn-sm disable_if_no_check">
                                        <i class="fas fa-share"></i>
                                    </button>
                                    <button id="btnAssingMedic" type="button" data-toggle="modal" data-target="#modal_medic_assign" class="btn btn-default btn-sm disable_if_no_check">
                                        <i class="fas fa-file-medical"></i>
                                    </button>
                                </div>
                                <!-- /.btn-group -->
                                <button type="button" class="btn btn-default btn-sm">
                                    <i class="fas fa-sync-alt"></i>
                                </button>
                            </div>--}}
                            <div class="mailbox-controls">
                                <!-- /.btn-group -->
                                {!! Form::open(['route' => 'administracion_solicitudes.importarExcel', 'method' => 'post', 'id' => 'frm-file', 'files' => true]) !!}
                                <div class="form-group">
                                    <div class="input-group">

                                        <button onclick="updateRecords()" type="button" class="btn btn-default btn-sm ">
                                            <i class="fas fa-sync-alt"></i>
                                        </button>
                                        <a href="{{ route('solicitudes_medicas.create') }}" class="btn btn-success btn-sm">
                                            <i class="fas fa-plus"></i>
                                        </a>

                                        @if (\Illuminate\Support\Facades\Auth::user()->role_id==1 && count($medical_requests)==0)

                                        {!! Form::label('file', 'Archivo', ['class' => 'control-label col-md-1 col-sm-1 col-xs-2 offset-md-6', 'style'=>'text-align: right;']) !!}

                                        <div class="custom-file">
                                            {!! Form::file('file', ['class' => 'control-label col-md-5 col-sm-6 col-xs-12']) !!}
                                            {!! Form::label('file', 'Escoge un archivo a importar', ['class' => 'custom-file-label']) !!}
                                        </div>
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-success" id="file_import" onclick="Import();">
                                                <i class="fa fa-file"></i> Importar
                                            </button>
                                        </div>

                                        @endif
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>

                            @include('medical_request_administration.partials.table')
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
        </div>
    </section>

    <!--MODALS-->
    @include('medical_request_administration.modals.modal_employee_assign')
{{--    @include('medical_request_administration.modals.modal_medic_assign')--}}
    @include('layouts.components.modals.modal_history')
    @include('layouts.components.modals.modal_history_timeline')
    @include('employee.modals.modal_search_result_ci')
@endsection

@section('script')
    <script src="{{ asset('adminlte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('a#solicitud_medica').addClass('active');
        });
        $(function () {
            bsCustomFileInput.init();
        });
        /*RUTAS DEL SISTEMA*/
        const search = @json(route('empleados.busqueda_empresa.getEmployeeFromEnterprise','_id'));
        const searchCi = @json(route('empleados.busqueda_ci.getEmployeeFromCI',['ci'=>'_ci','ci_expedition'=>'_ci_exp']));
        const assingToEmp = @json(route('administracion_solicitudes.asignar_empleado.assignToEmploye'));
        const assingToMed = @json(route('administracion_solicitudes.asignar_medico.assignToMedic'));
        const showHistory = @json(route('administracion_solicitudes.mostrar_historial.showHistory','_id'));
        const showHistoryTimeline = @json(route('buscar_codigo.buscarcodigo'));
        const assingToMe = @json(route('administracion_solicitudes.asignarme.assignToMe'));
        const employyeNotFound = @json(route('administracion_solicitudes.no_encontrado.employeeNotFound'));
        const verifyStatePre = @json(route('administracion_solicitudes.verificar.verifyStatePreSol'));
        const closeStatePre = @json(route('administracion_solicitudes.cerrar_pre.closeStatePreSol'));
        const closeStateAuto = @json(route('administracion_solicitudes.cerrar_pre.closeChangeStateAuto'));
        const updateRegisters = @json(route('administracion_solicitudes.actualizar_tabla.updateRegisters'));
        function Import(){
            var file = $('input#file');
            console.log(file);
            var urlFile = $('#frm-file').prop('action');
            var url = '{{ route('administracion_solicitudes.importarExcel') }}';
            var token = $("meta[name=csrf-token]").attr("content");
            var formData = new FormData($('#frm-file')[0]);
            $.ajax({
                url: url,
                method: 'post',
                headers: {'X-CSRF-TOKEN': token},
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function(e){}
            })
            .done(function(response) {

            })
            .fail(function(response) {
                toastr.error(
                    response['msj'],
                    '¡Error!'
                );
            });
        }
    </script>
    <script src="{{asset('js/medical_request_administration/index.js')}}"></script>
    <script src="{{asset('js/components/button_historial.js')}}"></script>
    <script src="{{asset('js/components/button_historial_timeline.js')}}"></script>
@endsection
