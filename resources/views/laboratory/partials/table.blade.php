<table id="example2" class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Op.</th>
        <th>Nombre</th>
        <th>Descripción</th>
        <th>Estado</th>
    </tr>
    </thead>

    <tbody>
    @foreach($laboratories as $laboratory)
        <tr>
            <td>
                <div class="btn-group" role="group">
                    <a href="{{ route('laboratorios.edit', $laboratory->id) }}" class="btn btn-sm btn-default" title="Editar">
                        <i class="fa fa-edit"></i>
                        <span class="sr-only">Editar</span>
                    </a>
                    <a href="{{ route('laboratorios.show', $laboratory->id) }}" class="btn btn-sm btn-default" title="{{($laboratory->deleted_at == null)?'Desactivar':'Activar'}}">
                        <i class="fa fa-{{($laboratory->deleted_at == null)?'times':'check'}}"></i>
                        <span class="sr-only">{{($laboratory->deleted_at == null)?'Desactivar':'Activar'}}</span>
                    </a>
                </div>
            </td>
            <td>
                {{$laboratory->name}}
            </td>
            <td>
                {{$laboratory->description}}
            </td>
            <td>
                @if($laboratory->deleted_at == null)
                <span class="badge badge-success">Activo</span>
                @elseif($laboratory->deleted_at != null)
                <span class="badge badge-danger">Inactivo</span>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Op.</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Estado</th>
        </tr>
    </tfoot>
</table>
