<div class="col-md-8 offset-md-3">
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} wrapper-name">
        {!! Form::label('name', '* Nombre', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('name', null, ['class' => 'form-control'.( $errors->has('name') ? ' is-invalid' : '' )]) !!}
    
            @error('name')
            <span class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
            @enderror
    
        </div>
    </div>
    
    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }} wrapper-description">
        {!! Form::label('description', 'Descripción', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('description', null, ['class' => 'form-control '.( $errors->has('description') ? ' is-invalid' : '' )]) !!}
    
            @error('description')
            <span class="invalid-feedback">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
            @enderror
    
        </div>
    </div>
</div>