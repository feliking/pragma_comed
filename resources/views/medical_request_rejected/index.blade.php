@extends('layouts.app_menu')

@section('styles')
    <style>
        .dataTables_filter {
            display: none !important;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Listado de solicitudes rechazadas</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Datos de consultas rechazadas</h3>

                            <div class="card-tools">
                                <div class="input-group input-group-sm">
                                    <input id="search_data" type="text" class="form-control" placeholder="Buscar">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @include('pending_request.partials.table')
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
        </div>
    </section>

    <!--MODALS-->
    @include('pending_request.modals.modal_employee_assign')
    @include('layouts.components.modals.modal_history')
    @include('employee.modals.modal_search_result_ci')
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('a#rejected').addClass('active');
        });

        /*RUTAS DEL SISTEMA*/
        const search = @json(route('empleados.busqueda_empresa.getEmployeeFromEnterprise','_id'));
        const searchCi = @json(route('empleados.busqueda_ci.getEmployeeFromCI',['ci'=>'_ci','ci_expedition'=>'_ci_exp']));
        const assingToEmp = @json(route('consultas_medicas_pendientes.asignar_empleado.assignToEmployee'));
        const assing = @json(route('consultas_medicas_pendientes.asignar.assign'));
        const employyeNotFound = @json(route('consultas_medicas_pendientes.eliminar_solicitud.deleteRequest'));
        const showHistory = @json(route('administracion_solicitudes.mostrar_historial.showHistory','_id'));

    </script>
    <script src="{{asset('js/pending_request/index.js')}}"></script>
    <script src="{{asset('js/components/button_historial.js')}}"></script>
@endsection
