<table id="example2" class="table table-striped table-hover">
  <thead>
  <tr>
      <th>Op.</th>
      <th>Nombre</th>
      <th>Dosis</th>
  </tr>
  </thead>

  <tbody>
  @foreach($vaccine_types as $vaccine_type)
      <tr>
          <td>
              <div class="btn-group" role="group">
                  <a href="{{ route('tipos_vacuna.edit', $vaccine_type->id) }}" class="btn btn-sm btn-default" title="Editar">
                      <i class="fa fa-edit"></i>
                      <span class="sr-only">Editar</span>
                  </a>
                  <a href="{{ route('tipos_vacuna.show', $vaccine_type->id) }}" class="btn btn-sm btn-default" title="{{($vaccine_type->deleted_at == null)?'Desactivar':'Activar'}}">
                      <i class="fa fa-{{($vaccine_type->deleted_at == null)?'times':'check'}}"></i>
                      <span class="sr-only">{{($vaccine_type->deleted_at == null)?'Desactivar':'Activar'}}</span>
                  </a>
              </div>
          </td>
          <td>
              {{$vaccine_type->name}}
          </td>
          <td>
              {{$vaccine_type->doces}}
          </td>
      </tr>
  @endforeach
  </tbody>
  <tfoot>
      <tr>
          <th>Op.</th>
          <th>Nombre</th>
          <th>Dosis</th>
      </tr>
  </tfoot>
</table>
