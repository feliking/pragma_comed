<!-- Modal -->
<div class="modal fade" id="modal_complementary_exam" tabindex="-1" role="dialog" aria-labelledby="exampleModalComplementaryExam" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('Examenes Complementarios') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['id' => 'frm-complementary_exam']) !!}
            <div id="body_modal_complementary_exam" class="modal-body">
                @include('complementary_exams.modals.partials.form')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                </button>
                <button type="submit" class="btn btn-success">
                    <i class="fa fa-save"></i> Registrar
                </button>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
</div>

@section('script')
    @parent
    <script src="{{ asset('adminlte/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/complementary_exam/modal_complementary_exam.js') }}"></script>
    <!-- bs-custom-file-input -->
    <script src="{{ asset('adminlte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <script>
        $('#cost').inputmask({
            alias: 'numeric',
            allowMinus: false,
            digits: 2,
            max: 999999.99
        });

        $(function () {
            bsCustomFileInput.init();
        });

        $('#frm-complementary_exam').submit(function (event) {
            event.preventDefault();
            if($('#frm-complementary_exam').valid()){
                registerComplementaryExam();
            }
        });

        $( "#modal_complementary_exam" ).on('shown.bs.modal', function(){
            resetComplementaryExamFields();
        });

        var aux = 1;
        function registerComplementaryExam() {
            // var laboratory = $('select[name=laboratory_id]');
            // var test_type = $('select[name=test_type_id]');
            var cost = $('input[name=cost]');
            var bill_number = $('input[name=bill_number]');

            var laboratory_select  = $('select[name=laboratory_id] option:selected');
            var test_type_select  = $('select[name=test_type_id] option:selected');

            var enterprise_flag = $('input[type=checkbox][name=enterprise_flag]').iCheck('update')[0].checked;

            var result_flag = $('input[type=checkbox][name=result_flag]').iCheck('update')[0].checked;

            var file = $('input[type=file][name=file]');

            var span_file = '';

            var copy_file = file.clone();
            copy_file.attr('id','file_'+aux);
            copy_file.attr('name','files[]');

            if(file[0].files.length > 0){
                //TIENE ARCHIVO
                span_file = file[0].files[0].name +' <span class="badge badge-warning">'+humanFileSize(file[0].files[0].size)+'</span>';
            }else{
                //NO TIENE ARCHIVO
                span_file = '<span class="badge badge-danger">Sin Archivo</span>'
            }

            var span_flag = "";
            if(enterprise_flag)
                span_flag = '<span class="badge badge-success">SI</span>';
            else
                span_flag = '<span class="badge badge-danger">NO</span>';
            
            var r_span_flag = "";
            if(result_flag)
                r_span_flag = '<span class="badge badge-success">SI</span>';
            else
                r_span_flag = '<span class="badge badge-danger">NO</span>';

            var tr = '<tr>' +
                '<td><input type="hidden" name="laboratories[]" value="'+laboratory_select.val()+'"><p>'+laboratory_select.text()+'</p></td>' +
                '<td><input type="hidden" name="test_types[]" value="'+test_type_select.val()+'"><p>'+test_type_select.text()+'</p></td>' +
                '<td><input type="hidden" name="result_flags[]" value="'+result_flag+'">'+r_span_flag+'</td>' +
                '<td><input type="hidden" name="costs[]" value="'+cost.val()+'"><p>'+cost.val()+'</p></td>' +
                '<td><input type="hidden" name="bill_numbers[]" value="'+bill_number.val()+'"><p>'+bill_number.val()+'</p></td>' +
                '<td><input type="hidden" name="enterprise_pay_flags[]" value="'+enterprise_flag+'">'+span_flag+'</td>' +
                '<td><div id="div_file_'+aux+'" hidden></div>'+span_file+'</td>'+
                '<td><button type="button" class="btn btn-sm btn-danger" onclick="removeComplementaryExamRegister(this)"><i class="fa fa-trash"></i></button></td>'+
                '</tr>';

            $('tbody#tbody_complementary_exams').append(tr);
            //AGREGANDO EL ARCHIVO
            $('#div_file_'+aux).append(copy_file);

            aux++;
            hideShowTable();
            $('#modal_complementary_exam').modal('hide');
            resetComplementaryExamFields();
        }

        function resetComplementaryExamFields() {
            $('select[name=laboratory_id]').select2({
                theme: 'bootstrap4',
                placeholder: 'SELECCIONE UNA OPCIÓN',
                width: 'auto'
            }).val('').trigger('change');
            $('select[name=test_type_id]').select2({
                theme: 'bootstrap4',
                placeholder: 'SELECCIONE UNA OPCIÓN',
                width: 'auto'
            }).val('').trigger('change');
            $('input[name=cost]').val('');
            $('input[name=bill_number]').val('');
            $('input[type=checkbox][name=enterprise_flag]').iCheck('uncheck');
            $('input[type=checkbox][name=result_flag]').iCheck('uncheck');
            $('input[type=file][name=file]').val(null);
            $('label[for=file]').text('Escoge un archivo');
        }

        function hideShowTable() {
            if ($('tbody#tbody_complementary_exams').children().length == 0) {
                //NO HAY HIJOS
                $('#table_complementary_exams').attr('hidden', true);
                $('#alert_table_complementary').removeAttr('hidden');
            }else{
                //HAY HIJOS
                $('#table_complementary_exams').removeAttr('hidden');
                $('#alert_table_complementary').attr('hidden', true);
            }
        }

        function removeComplementaryExamRegister(object) {
            $(object).parent().parent().remove();
            hideShowTable();
        }

        function humanFileSize(bytes, si=false, dp=1) {
            const thresh = si ? 1000 : 1024;

            if (Math.abs(bytes) < thresh) {
                return bytes + ' B';
            }

            const units = si
                ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
                : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
            let u = -1;
            const r = 10**dp;

            do {
                bytes /= thresh;
                ++u;
            } while (Math.round(Math.abs(bytes) * r) / r >= thresh && u < units.length - 1);


            return bytes.toFixed(dp) + ' ' + units[u];
        }
    </script>
@endsection
