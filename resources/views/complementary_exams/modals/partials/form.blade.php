<div class="form-group {{ $errors->has('laboratory_id') ? ' has-error' : '' }}" id="wrapper-laboratory_id">
    {!! Form::label('laboratory_id', '* Laboratorio', ['class' => 'control-label col-md-6 col-xs-12']) !!}
    <div class="col-md-12 col-sm-12 col-xs-12">
        {!! Form::select('laboratory_id', $laboratories, null, ['class' => 'form-control select2 '.( $errors->has('laboratory_id') ? ' is-invalid' : '' ), 'style'=>'with: 100%;']) !!}

        @error('laboratory_id')
        <span class="invalid-feedback">
            <strong>{{ $errors->first('laboratory_id') }}</strong>
        </span>
        @enderror

    </div>
</div>

<div class="form-group {{ $errors->has('test_type_id') ? ' has-error' : '' }}" id="wrapper-test_type_id">
    {!! Form::label('test_type_id', '* Tipo de prueba', ['class' => 'control-label col-md-6 col-xs-12']) !!}
    <div class="col-md-12 col-sm-12 col-xs-12">
        {!! Form::select('test_type_id', $test_types, null, ['class' => 'form-control select2 '.( $errors->has('test_type_id') ? ' is-invalid' : '' ), 'style'=>'with: 100%;']) !!}

        @error('test_type_id')
        <span class="invalid-feedback">
            <strong>{{ $errors->first('test_type_id') }}</strong>
        </span>
        @enderror

    </div>
</div>

<div class="form-row {{ $errors->has('cost') ? ' has-error' : '' }} wrapper-cost">
    {!! Form::label('cost', 'Costo', ['class' => 'control-label col-md-12 col-sm-12 col-xs-12']) !!}

    <div class="col-md-4 col-sm-6 col-xs-12">
        {!! Form::text('cost', isset($complementary_exam)?$complementary_exam->cost:null, ['class' => 'form-control'.( $errors->has('cost') ? ' is-invalid' : '' )]) !!}

        @error('cost')
        <span class="invalid-feedback">
            <strong>{{ $errors->first('cost') }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group col-md-4 col-sm-6 col-xs-12">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" name="enterprise_flag" id="enterprise_flag">
            <label for="enterprise_flag">
                Pagó la empresa
            </label>
        </div>
    </div>
    <div class="form-group col-md-4 col-sm-3 col-xs-12">
        <div class="icheck-danger">
            <input class="form-check-input" type="checkbox" name="result_flag" id="result_flag">
            <label for="result_flag">
                Dió Positivo
            </label>
        </div>
    </div>
</div>


<div class="form-group{{ $errors->has('bill_number') ? ' has-error' : '' }} wrapper-bill_number">
    {!! Form::label('bill_number', 'Número de Factura', ['class' => 'control-label col-md-12 col-sm-12 col-xs-12']) !!}
    <div class="col-md-12 col-sm-12 col-xs-12">
        {!! Form::text('bill_number', isset($complementary_exam)?$complementary_exam->bill_number:null, ['class' => 'form-control'.( $errors->has('bill_number') ? ' is-invalid' : '' )]) !!}

        @error('bill_number')
        <span class="invalid-feedback">
            <strong>{{ $errors->first('bill_number') }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group">
    {!! Form::label('file', 'Archivo', ['class' => 'control-label col-md-12 col-sm-12 col-xs-12']) !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('file', ['class' => 'control-label col-md-12 col-sm-12 col-xs-12']) !!}
            {!! Form::label('file', 'Escoge un archivo', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
