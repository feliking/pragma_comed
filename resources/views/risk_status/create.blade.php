@extends('layouts.app_menu')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Creación de Estados de Riesgo</h1>
                </div>

            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            {!! Form::open(['route' => 'estados_riesgo.store', 'method' => 'post', 'id' => 'frm-risks_id']) !!}
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Crear Estado de Riesgo</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @include('risk_status.partials.form')
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <div class="form-group">
                                <div class="col-xs-12 text-right">
                                    <a href="{{ route('estados_riesgo.index') }}" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Volver</a>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Registrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@endsection

@section('script')
    <script src="{{ asset('/js/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('/js/risk_statuses/general.js') }}"></script>

    <script>
        $(document).ready(function() {
            //$('li#nav_config').addClass('menu-open');
            //$('li#nav_config').children().addClass('active');
            $('a#nav_rstatus').addClass('active');
        });
        $('.select2').select2({
            theme: 'bootstrap4',
            placeholder: 'SELECCIONE UNA OPCIÓN'
        });
    </script>
@endsection
