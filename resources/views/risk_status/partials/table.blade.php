<table id="example2" class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Op.</th>
        <th>Nombre</th>
        <th>Descripción</th>
        <th>Ponderación</th>
        <th>Tipo de Riesgo</th>
        <th>Estado</th>
    </tr>
    </thead>

    <tbody>
    @foreach($risk_statuses as $risk_status)
        <tr>
            <td>
                <div class="btn-group" role="group">
                    <a href="{{ route('estados_riesgo.edit', $risk_status->id) }}" class="btn btn-sm btn-default" title="Editar"
                        style="{{ ($risk_status->system_flag)? 'pointer-events: none; cursor: default;' : ''}}">
                        <i class="fa fa-edit"></i>
                        <span class="sr-only">Editar</span>
                    </a>
                    <a href="{{ route('estados_riesgo.show', $risk_status->id) }}" class="btn btn-sm btn-default" title="{{($risk_status->deleted_at == null)?'Desactivar':'Activar'}}">
                        <i class="fa fa-{{($risk_status->deleted_at == null)?'times':'check'}}"></i>
                        <span class="sr-only">{{($risk_status->deleted_at == null)?'Desactivar':'Activar'}}</span>
                    </a>
                </div>
            </td>
            <td>
                {{$risk_status->name}}
            </td>
            <td>
                {{$risk_status->description}}
            </td>
            <td>
                {{$risk_status->weighting}}
            </td>
            <td>
                {{$risk_status->risk_type->name}}
            </td>
            <td>
                @if($risk_status->deleted_at == null)
                <span class="badge badge-success">Activo</span>
                @elseif($risk_status->deleted_at != null)
                <span class="badge badge-danger">Inactivo</span>
                @endif
            </td>

        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Op.</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Ponderación</th>
            <th>Tipo de Riesgo</th>
            <th>Estado</th>
        </tr>
    </tfoot>
</table>
