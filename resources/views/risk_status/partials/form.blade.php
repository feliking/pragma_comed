<div class="col-md-8 offset-md-3">
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} wrapper-name">
        {!! Form::label('name', '* Nombre', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('name', null, ['class' => 'form-control'.( $errors->has('name') ? ' is-invalid' : '' )]) !!}

            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>

        </div>
    </div>

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }} wrapper-description">
        {!! Form::label('description', 'Descripción', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('description', null, ['class' => 'form-control'.( $errors->has('description') ? ' is-invalid' : '' )]) !!}

            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>

        </div>
    </div>

    <div class="form-group{{ $errors->has('weighting') ? ' has-error' : '' }} wrapper-weighting">
        {!! Form::label('weighting', '* Ponderación', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('weighting', null, ['class' => 'form-control  '.( $errors->has('weighting') ? ' is-invalid' : '' )]) !!}

            @error('weighting')
            <span class="invalid-feedback">
                <strong>{{ $errors->first('weighting') }}</strong>
            </span>
            @enderror

        </div>
    </div>

    <div class="form-group{{ $errors->has('risk_type_id') ? ' has-error' : '' }} wrapper-risk_type_id " id="risk_type_id">
        {!! Form::label('risk_type_id', '* Tipo de Riesgo', ['class' => 'control-label col-md-4 col-sm-4 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::select('risk_type_id', $risk_types, null, ['class' => 'form-control select2'.( $errors->has('risk_type_id') ? ' is-invalid' : '' ), 'placeholder' => 'Seleccione una criterio']) !!}
            <span class="help-block">
                <strong>{{ $errors->first('risk_type_id') }}</strong>
            </span>
        </div>
    </div>
</div>
