@extends('layouts.app_menu')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edición de Estados de Riesgo</h1>
                </div>

            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            {!! Form::model($risk_status, ['route' => ['estados_riesgo.update', $risk_status->id], 'method' => 'put', 'id' => 'frm-risks_id']) !!}
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Editar Estado de Riesgo</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @include('risk_status.partials.form')
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <div class="form-group">
                                <div class="col-xs-12 text-right">
                                    <a href="{{ route('estados_riesgo.index') }}" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Volver</a>
                                    <button type="submit" class="btn btn-warning"><i class="fa fa-check"></i> Registrar cambios</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@endsection

@section('script')
    <script src="{{ asset('/js/jquery.mask.min.js') }}"></script>

    <script src="{{ asset('/js/risk_statuses/general.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('a#nav_rstatus').addClass('active');
        });
        $('.select2').select2({
            theme: 'bootstrap4',
            placeholder: 'SELECCIONE UNA OPCIÓN'
        });
    </script>
@endsection
