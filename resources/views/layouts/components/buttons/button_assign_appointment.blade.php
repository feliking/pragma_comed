@if($pass_date)
<button type="button" class="btn btn-sm btn-dark" onclick="mostrarAppointment({{$medical_request_id}}, this)" @isset($function) data-function="{{$function}}" @endisset>
    <i class="fa fa-calendar-check"></i>
</button>
@else
    <button type="button" class="btn btn-sm btn-warning" onclick="editarAppointment({{$medical_request_id}}, {{$last_appointment->id}}, this)" @isset($function) data-function="{{$function}}" @endisset>
        <i class="fa fa-calendar-check"></i>
    </button>
@endif
