<div class="mailbox-controls">
    <div class="btn-group">
        <button type="button" class="btn btn-dark btn-sm" id="btn_asignar_list" data-toggle="tooltip" data-placement="top" title="Asignar a seguimiento o consulta" {{ ($tab == 'pendiente') ? 'hidden' : ''}}>
            <i class="fa fa-file-medical"> Asignación en lote</i>
        </button>
    </div>
</div>
