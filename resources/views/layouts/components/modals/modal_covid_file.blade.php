<!-- Modal -->
<div class="modal fade" id="modal_covid_file" tabindex="-1" style="z-index: 1051;" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    {{ __('Ficha Covid') }}
                </h5>
            </div>
            <div id="modal_covid_file_body" class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-close"></i> Cancelar
                </button>
            </div>
        </div>
    </div>
</div>

@section('script')
    @parent
    <script>
        const covidFile = @json(route('seguimientos.get_disease_tracing.getDiseaseCriteriaFromTracing', '_id'));

        function mostrarFichaCovid(tracing_id) {
            var url = covidFile;
            url = url.replace('_id',tracing_id);
            var token = $("meta[name=csrf-token]").attr("content");

            $.ajax({
                url: url,
                method: 'post',
                headers: {'X-CSRF-TOKEN': token},
                beforeSend: function(e){
                    // Loading
                    // blockCard('#form_content');
                }
            })
            .done(function(response) {
                switch (response['type']) {
                    case "correct":
                        $('#modal_covid_file_body').empty();
                        $('#modal_covid_file_body').append(response['view']);

                        $('#modal_covid_file').modal('show');
                        break;
                    case "error":
                        toastr.error(
                            response['msj'],
                            '¡Error!'
                        );
                        break;
                }
                // unblockCard('#form_content');
            })
            .fail(function(response) {
                console.log('response', response);
                toastr.error(
                    response,
                    '¡Error!'
                );
            });
        }
    </script>
@endsection
