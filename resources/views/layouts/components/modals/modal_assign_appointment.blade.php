<!-- Modal -->
<div class="modal fade" id="modal_assing_appointment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('Registro de Citas') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="body_modal_assing_appointment" class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                </button>
                <button id="btnRegistrarCitaMedica" type="button" class="btn btn-success">
                    <i class="fa fa-save"></i> Registrar
                </button>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/components/button_appointment.js') }}"></script>
<script>
    /*RUTAS DEL FORMULARIO*/
    const getForm = @json(route('citas.form.getFormAppointment'));
    const getEditForm = @json(route('citas.form.getEditFormAppointment',['id'=>'_id']));
    const storeForm = @json(route('citas.store'));
    const updateForm = @json(route('citas.update','_id'));
</script>
