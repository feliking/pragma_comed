<!-- Modal -->
<div class="modal fade" id="modal_tracings_history" tabindex="-1" role="dialog" style="z-index: 1050;" aria-labelledby="examplemodal_tracings_history" data-keyboard="false" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="examplemodal_tracings_history">
                    {{ __('Ficha Covid') }}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="modal_tracings_history_body" class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-close"></i> Cancelar
                </button>
            </div>
        </div>
    </div>
</div>

@section('script')
    @parent
    <script>
        const tracingsHistorial = @json(route('seguimientos.get_tracings_history.getTracingsFromMedicalRequest', '_id'));

        function mostrarHistorialSeguimientos(medical_request_id) {
            var url = tracingsHistorial;
            url = url.replace('_id',medical_request_id);
            var token = $("meta[name=csrf-token]").attr("content");

            $.ajax({
                url: url,
                method: 'post',
                headers: {'X-CSRF-TOKEN': token},
                beforeSend: function(e){
                    // Loading
                    // blockCard('#form_content');
                }
            })
                .done(function(response) {
                    switch (response['type']) {
                        case "correct":
                            $('#modal_tracings_history_body').empty();
                            $('#modal_tracings_history_body').append(response['view']);

                            $('#modal_tracings_history').modal('show');
                            break;
                        case "error":
                            toastr.error(
                                response['msj'],
                                '¡Error!'
                            );
                            break;
                    }
                    // unblockCard('#form_content');
                })
                .fail(function(response) {
                    toastr.error(
                        response,
                        '¡Error!'
                    );
                });
        }
    </script>
@endsection
