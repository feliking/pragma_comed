<!-- Modal -->
<div class="modal fade" id="modal_remit" tabindex="-1" role="dialog" aria-labelledby="examplemodal_remit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="examplemodal_remit">{{ __('Re Emisión') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['id' => 'frm-remit']) !!}

            <div id="body_modal_remit" class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                </button>
                <button id="btnRemit" type="submit" class="btn btn-info">
                    <i class="fa fa-envelope"></i> Si, re emitir!
                </button>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>

@section('script')
    @parent
    <script src="{{ asset('js/components/form_remit.js') }}"></script>

    <script>
        /*RUTAS DEL FORMULARIO*/
        const getRemitForm = @json(route('seguimientos.get_remit_form.getRemitFormFromMedicalRequest','_id'));
        const updateRemit = @json(route('seguimientos.update_remit.updateRemit','_id'));

        function modalAbrirRemit(medical_request_id) {
            var url = getRemitForm;
            url = url.replace('_id',medical_request_id);
            var token = $("meta[name=csrf-token]").attr("content");

            $.ajax({
                url: url,
                method: 'post',
                headers: {'X-CSRF-TOKEN': token},
                beforeSend: function(e){
                    // Loading
                    // blockCard('#form_content');
                }
            })
            .done(function(response) {
                switch (response['type']) {
                    case "correct":
                        $('button#btnRemit').attr('onclick','').unbind('click');
                        document.getElementById('btnRemit').setAttribute("onClick","actualizarRemicion("+medical_request_id+")");

                        $('div#body_modal_remit').empty();
                        $('div#body_modal_remit').append(response['view']);

                        $('div#modal_remit').modal('show');
                        break;
                    case "error":
                        toastr.error(
                            response['msj'],
                            '¡Error!'
                        );
                        break;
                }

            })
            .fail(function() {
                toastr.error(
                    'Ocurrio un error al iniciar el formulario',
                    '¡Error!'
                );
            });
        }

        $("form#frm-remit").submit(function(e){
            e.preventDefault();
        });

        function actualizarRemicion(medical_request_id) {
            if($("form#frm-remit").valid()){
                var url = updateRemit;
                url = url.replace('_id',medical_request_id);
                var token = $("meta[name=csrf-token]").attr("content");

                var data = $("form#frm-remit").serialize();
                $.ajax({
                    url: url,
                    method: 'post',
                    headers: {'X-CSRF-TOKEN': token},
                    data: data,
                    beforeSend: function(e){
                        // Loading
                        // blockCard('#form_content');
                    }
                })
                .done(function(response) {
                    switch (response['type']) {
                        case "correct":
                            $('div#modal_remit').modal('hide');
                            removeRowsTable(response['filas'])

                            break;
                        case "error":
                            toastr.error(
                                response['msj'],
                                '¡Error!'
                            );
                            break;
                    }

                })
                .fail(function() {
                    toastr.error(
                        'Ocurrio un error al iniciar el formulario',
                        '¡Error!'
                    );
                });
            }
        }
    </script>
@endsection
