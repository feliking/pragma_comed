@if(count($tracings) > 0)
    <table id="medical_requests_table" class="table table-striped table-hover table-responsive">
        <thead>
        <tr>
            <th>Fecha</th>
            <th>Usuario Asignado</th>
            <th>Riesgo.</th>
            <th>Etapa Tratamiento</th>
            <th>Considera que necesita cita</th>
            <th>Comentario</th>
            <th>Op</th>
        </tr>
        </thead>
        <tbody>
        @foreach($tracings as $tracing)
            <tr>
                <td>{{$tracing->tracing_date->format('d/m/Y')}}</td>
                <td>{{$tracing->user->full_name}}</td>
                <td>
                    <span class="badge" style="background-color: {{$tracing->historical_weighting_level->color}}">
                        {{$tracing->historical_weighting_level->name}}
                    </span>
                </td>
                <td>
                    {{$tracing->riskState->name}}
                </td>
                <td>
                    @if($tracing->medical_attention_flag)
                        <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Necesita atención médica">SI</span>
                    @else
                        <span class="badge badge-danger" data-toggle="tooltip" data-placement="top" title="No necesita atención médica">NO</span>
                    @endif
                </td>
                <td>{{$tracing->comment}}</td>
                <td>
                    @include('layouts.components.buttons.button_covid_file',[
                                'tracing_id' => $tracing->id
                            ])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <div class="alert alert-info alert-dismissible">
        <h5><i class="icon fas fa-info"></i> Información!</h5>
        Por el momento no cuenta con registros de seguimiento
    </div>
@endif
