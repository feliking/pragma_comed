@include('medical_consultations.partials.datos_paciente')
<hr>
<h3><span class="badge badge-danger"><i class="fa fa-question"></i> <b>¿Desea Reprogramación?</b></span></h3>
<div class="form-row">
    <div class="col-md-6 col-sm-12">
        <h4>Datos de Próxima Atención</h4>
        <hr>
        <label>{{ __('*Proxima atención: ') }}</label>
        <div class="form-row">
            <div class="col-sm-12">
                <div class="form-group">
                    <div class="input-group date" id="next_reprogramming_date" data-target-input="nearest">
                        <input type="text" name="next_reprogramming_date" disabled value="{{ $medical_request->last_appointment->next_attention_date->addDays($days)->format('d/m/Y') }}" class="form-control datetimepicker-input" placeholder="Fecha de atención" data-target="#next_reprogramming_date" data-inputmask='"mask": "99/99/9999"' data-greedy="false" data-mask/>
                        <div class="input-group-append" data-target="#next_reprogramming_date" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <div class="input-group date" id="next_reprogramming_time" data-target-input="nearest">
                        <input type="text" name="next_reprogramming_time" class="form-control datetimepicker-input" placeholder="Hora de atención" data-target="#next_reprogramming_time"/>
                        <div class="input-group-append" data-target="#next_reprogramming_time" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-clock"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <h4>Citas Programadas</h4>
        <hr>
        <div class="form-row" id="div_seguim_agenda" style="transform: traslate(0,0); height: 200px;overflow: auto;padding: 10px">

        </div>
    </div>
</div>

<div class="form-group col-md-12">
    <p>Por favor introduzca el motivo por el cual se reprogramara el seguimiento indicado:</p>
    <input type="hidden" name="medical_request_id" value="{{$medical_request->id}}">
    <label>{{ __('* Comentario u Observación') }}</label>
    <div class="input-group mb-3">
        <textarea name="reprogramming_comment" rows="3" class="form-control @error('comment') is-invalid @enderror" placeholder="{{ __('* Comentario') }}"></textarea>

        <span class="invalid-feedback" role="alert">
        @error('reprogramming_comment')
            <strong>{{ $message }}</strong>
        @enderror
        </span>
    </div>
</div>

<script>
    $(document).ready(function() {
        var fecha_reprogramada = @json($medical_request->last_appointment->next_attention_date->addDays($days)->format('Y-m-d'));
        var usuario_reprogramado = @json(\Illuminate\Support\Facades\Auth::id());

        $('[data-mask]').inputmask({
            removeMaskOnSubmit: true,
        });

        $('#next_reprogramming_date').datetimepicker({
            format: 'DD/MM/YYYY',
        });

        $('#next_reprogramming_time').datetimepicker({
            format: 'LT'
        });

        searchReprogrammingAppointments(usuario_reprogramado, fecha_reprogramada);
    });

    function searchReprogrammingAppointments(usuario, fecha) {
        var url = citasAppointment;
        var token = $("meta[name=csrf-token]").attr("content");

        $.ajax({
            url: url,
            method: 'post',
            headers: {'X-CSRF-TOKEN': token},
            data: {
                user_id: usuario,
                next_attention_date: fecha
            },
            beforeSend: function(e){
                var div_alert = '<div class="alert alert-warning col-12">'+
                    '<h5><i class="icon fa fa-spin fa-spinner"></i> ¡Cargando!</h5>'+
                    '<p>Cargando datos de la cita, espere porfavor...</p>'+
                    '</div>';
                $('div#div_seguim_agenda').empty();
                $('div#div_seguim_agenda').append(div_alert);
            }
        })
        .done(function(response) {
            switch (response['type']) {
                case "correct":
                    $('div#div_seguim_agenda').empty();
                    $('div#div_seguim_agenda').append(response['view']);
                    break;
                case "error":
                    toastr.error(
                        response['msj'],
                        '¡Error!'
                    );
                    var div_alert = '<div class="alert alert-danger col-12">'+
                        '<h5><i class="icon fa fa-warning"></i> ¡Error!</h5>'+
                        '<p>Existió algun error a la hora de cargar los datos de cita</p>'+
                        '</div>';
                    $('div#div_seguim_agenda').empty();
                    $('div#div_seguim_agenda').append(div_alert);
                    break;
            }
        })
        .fail(function() {
            var div_alert = '<div class="alert alert-danger col-12">'+
                '<h5><i class="icon fa fa-warning"></i> ¡Error!</h5>'+
                '<p>Existió algun error a la hora de cargar los datos de cita</p>'+
                '</div>';
            $('div#div_seguim_agenda').empty();
            $('div#div_seguim_agenda').append(div_alert);
        });
    }
</script>

