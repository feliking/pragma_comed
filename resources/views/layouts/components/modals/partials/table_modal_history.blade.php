@include('medical_consultations.partials.datos_paciente')
@if(count($historical_requests) > 0)
    <table id="medical_history_tracings_table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Comentario</th>
            <th>Usuario</th>
            <th>Estado</th>
            <th>Actualización</th>
        </tr>
        </thead>
        <tbody>
        @foreach($historical_requests as $historical_request)
            <tr>
                <td>{{$historical_request->comment}}</td>
                <td>
                    @if($historical_request->user!=null)
                        {{$historical_request->user->full_name}}
                    @else
                        <span class="badge badge-warning">SISTEMA</span>
                    @endif
                </td>
                <td>
                    <span class="badge badge-info">
                        {{$historical_request->medical_request_status->name}}
                    </span>
                </td>
                <td>
                    {{$historical_request->created_at->format('d/m/Y')}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <div class="alert alert-info alert-dismissible">
        <h5><i class="icon fas fa-info"></i> Información!</h5>
        Por el momento no cuenta con registros.
    </div>
@endif
