<div class="form-group col-md-12 col-sm-12">
    <label>{{ __('*Fecha de proxima atención: ') }}</label>
    <div class="form-row">
        <div class="col-sm-12">
            <div class="form-group">
                <div class="input-group date" id="next_attention_date_tracing" data-target-input="nearest">
                    <input type="text" name="next_attention_date_tracing" class="form-control datetimepicker-input" data-target="#next_attention_date_tracing" data-inputmask='"mask": "99/99/9999"' data-greedy="false" data-mask/>
                    <div class="input-group-append" data-target="#next_attention_date_tracing" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- TIPO DE CONSULTA -->
<div class="form-group col-md-12">
    <label>{{ __('*Tipo de atención:') }}</label>
    <div class="input-group mb-3">
        <select name="appointment_type_tracing_id" id="appointment_type_tracing_id" class="form-control @error('appointment_type_tracing_id') is-invalid @enderror" required>
            <option></option>
            @foreach($appointment_types as $appointment_type)
                <option
                    value="{{ $appointment_type->id }}" @if(isset($appointment)){{ ($appointment->appointment_type_tracing_id==$appointment_type->id)?'selected':'' }}@else{{ (old('appointment_type_tracing_id') == $appointment_type->id)?'selected':'' }}@endif>
                    {{ $appointment_type->name }}
                </option>
            @endforeach
        </select>

        <span class="invalid-feedback" role="alert">
        @error('appointment_type_id')
            <strong>{{ $message }}</strong>
        @enderror
        </span>
    </div>
</div>

<!-- URL ZOOM -->
<div id="div_tracing_zoom" class="form-group col-md-12 col-sm-12 variable_text" @if(isset($appointment)){{ ($appointment->appointment_type_id==2)?'':'hidden' }}@else hidden @endif>
    <label>{{ __('*Ingrese la dirección de la reunión Zoom:') }}</label>
    <div class="input-group mb-3">
        <input name="variable_tracing_zoom" type="text"
               value="@if(isset($appointment)){{ ($appointment->appointment_type_id==2)?$appointment->variable_text:'' }}@else{{ old('variable_tracing_zoom') }}@endif"
               class="form-control @error('variable_tracing_zoom') is-invalid @enderror" placeholder="{{ __('INGRESE LA URL') }}">
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="fas fa-arrow-alt-circle-right"></span>
            </div>
        </div>

        <span class="invalid-feedback" role="alert">
        @error('variable_tracing_zoom')
            <strong>{{ $message }}</strong>
        @enderror
        </span>
    </div>
</div>

<!-- WHATSAPP -->
<div id="div_tracing_whatsapp" class="form-group col-md-12 col-sm-12 variable_text" @if(isset($appointment)){{ ($appointment->appointment_type_id==3)?'':'hidden' }}@else hidden @endif>
    <label>{{ __('*Ingrese su número de WhatsApp') }}</label>
    <div class="input-group mb-3">
        <input name="variable_tracing_whatsapp" type="text"
               value="@if(isset($appointment)){{ ($appointment->appointment_type_id==3)?$appointment->variable_text:'' }}@else{{ old('variable_tracing_whatsapp') }}@endif"
               class="form-control @error('variable_tracing_whatsapp') is-invalid @enderror" placeholder="{{ __('* Número de Teléfono') }}" data-inputmask='"mask": "9999[9999]"' data-mask>
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="fab fa-whatsapp"></span>
            </div>
        </div>

        <span class="invalid-feedback" role="alert">
        @error('variable_tracing_whatsapp')
            <strong>{{ $message }}</strong>
        @enderror
        </span>
    </div>
</div>

<!-- OBSERVACIÓN O COMENTARIO -->
<div class="form-group col-md-12">
    <label>{{ __('Comentario u Observación') }}</label>
    <div class="input-group mb-3">
        <textarea name="comment_tracing" rows="3" class="form-control @error('comment_tracing') is-invalid @enderror" placeholder="{{ __('Comentario u Observación') }}">@if(isset($appointment)){{ $appointment->comment_tracing }}@else{{ old('comment_tracing') }}@endif</textarea>

        <span class="invalid-feedback" role="alert">
        @error('comment_tracing')
            <strong>{{ $message }}</strong>
        @enderror
        </span>
    </div>
</div>
