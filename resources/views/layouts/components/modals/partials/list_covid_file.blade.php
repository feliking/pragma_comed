@include('medical_consultations.partials.datos_paciente')
<hr>
<h4>Datos de Ficha</h4>

<div class="row">
    <div class="col-md-6 col-xs-12">
        <p><b><i class="fa fa-user"></i> Ficha Realizada por:</b> {{ $tracing->user->name }}</p>
    </div>
    <div class="col-md-6 col-xs-12">
        <p><b><i class="fa fa-calendar-check"></i> Fecha de Registro:</b> {{ $tracing->tracing_date->format('d/m/Y') }}</p>
    </div>
</div>
@if($historical_weigthing_level->calc_evaluation>0)
    @php
        $sum = 0;
    @endphp
    <div class="row">
        @forelse($disease_criterias as $disease_criteria)
            @php
                $sum = $sum + $disease_criteria->weighing;
            @endphp
            <div class="col-md-4 col-sm-6">
                <span class="badge badge-info"><i class="fa fa-check"></i></span> {{$disease_criteria->syntom}} ({{ $disease_criteria->weighing }})
            </div>
        @empty
            <div class="col-md-4 col-sm-6">
                No existe ninguna casilla marcada
            </div>
        @endif
    </div>
    <br>
    <div class="row">
        <p><b>Ponderación:</b> {{$historical_weigthing_level->calc_evaluation}}
            <span class="badge" style="background-color: {{$historical_weigthing_level->color}}">
                {{$historical_weigthing_level->name}}
            </span>
            @if($edad >= $parameter_age)
                <span class="badge badge-info">Edad > {{$parameter_age}} ({{$parameter_value}})</span>
            @endif
            @if($tracing->riskState->weighting > 0)
                <span class="badge badge-info">{{$tracing->riskState->full_name}}</span>
            @endif
         </p>
    </div>
@else
    <div class="alert alert-info alert-dismissible">
        <h5><i class="icon fas fa-info"></i> ¡Ficha Covid!</h5>
        <p>No se encontraron registros llenaddos de la ficha covid</p>
    </div>
@endif
<h5>Registro de Laboratorios</h5>
@if (count($laboratories)>0)
    <div class="row">
        <table id="medical_history_tracings_table" class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Laboratorio</th>
                <th>Tipo de Prueba</th>
                <th>Dió Positivo</th>
                <th>Nro Factura</th>
                <th>Costo</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($laboratories as $laboratory)
                    <tr>
                        <td>
                            {{ $laboratory->laboratory->name }}
                        </td>
                        <td>
                            {{ $laboratory->testType->name }}
                        </td>
                        <td>
                            @if ($laboratory->result_flag == true)
                                <span class="badge badge-danger">SI</span>
                            @else
                                @if (is_null($laboratory->result_flag))
                                    <span class="badge badge-warning">S/R</span>
                                @else
                                    <span class="badge badge-success">NO</span>
                                @endif
                            @endif
                        </td>
                        <td>
                            @if ($laboratory->bill_number!=null)
                                {{ $laboratory->bill_number }}    
                            @else
                                <span class="badge badge-warning">
                                    S/F
                                </span>
                            @endif
                        </td>
                        <td>
                            @if ($laboratory->cost!=null)
                                {{ $laboratory->cost }}    
                            @else
                                <span class="badge badge-warning">
                                    S/F
                                </span>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@else
<div class="alert alert-info alert-dismissible">
    <h5><i class="icon fas fa-info"></i> ¡Laboratorios!</h5>
    <p>No se encontraron registros llenaddos de laboratorios</p>
</div>
@endif