<form id="form-appointment" method="post" autocomplete="off">
    @include('medical_consultations.partials.datos_paciente')
    <hr>
    @include('layouts.components.modals.partials.form_appointment')
</form>

<script src="{{ asset('js/components/appointment.js') }}"></script>
