@if(count($files) > 0)

        @foreach($files as $file)
            @php
                $prefix = 'complementary_exams/';
                $loca = substr($file->location, strlen($prefix));
            @endphp
            @if($file->mime == 'application/pdf')

                <div style="height: 30rem" id="example1"></div>
                <script src="/js/pdfobject/pdfobject.js"></script>
                <script>PDFObject.embed("{{ route('image.displayImage',$loca) }}", "#example1");</script>
                @elseif(substr($file->mime, 0, strlen("image/")) === "image/")

                <img style="max-width: 650px" src="{{ route('image.displayImage',$loca) }}" alt="" title="">
            @else
                    NO SE PUEDE OBTENER VISTA PREVIA
            @endif
        @endforeach

@else
    <div class="alert alert-info alert-dismissible">
        <h5><i class="icon fas fa-info"></i> Información!</h5>
        Por el momento no cuenta con archivos.
    </div>
@endif
