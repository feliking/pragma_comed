@include('medical_consultations.partials.datos_paciente')
<!-- OBSERVACIÓN O COMENTARIO -->
<div class="form-group col-md-12">
    <label>{{ __('*Comentario u Observación') }}</label>
    <div class="input-group mb-3">
        <textarea name="comment_remit" rows="3" class="form-control @error('comment_remit') is-invalid @enderror" placeholder="{{ __('Comentario u Observación') }}">@if(isset($appointment)){{ $appointment->comment_remit }}@else{{ old('comment_remit') }}@endif</textarea>

        <span class="invalid-feedback" role="alert">
        @error('comment_remit')
            <strong>{{ $message }}</strong>
        @enderror
        </span>
    </div>
</div>
