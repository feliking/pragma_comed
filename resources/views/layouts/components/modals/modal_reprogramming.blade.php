<!-- Modal -->
<div class="modal fade" id="modal_reprogramming" tabindex="-1" role="dialog" aria-labelledby="examplemodal_reprogramming" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="examplemodal_reprogramming">{{ __('Reprogramación') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['id' => 'frm-reprogramming']) !!}
            <div id="body_modal_reprogramming" class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                </button>
                <button id="btnRemision" type="button" class="btn btn-info" data-dismiss="modal">
                    <i class="fa fa-envelope"></i> Remitir!
                </button>
                <button id="buttonReprogramming" type="submit" class="btn btn-danger">
                    <i class="fa fa-save"></i> Si, reprogramar!
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@section('script')
    @parent
    <script src="{{ asset('js/components/form_reprogramming.js') }}"></script>
    <script>
        /*RUTAS DEL FORMULARIO*/
        const getReproForm = @json(route('seguimientos.get_reprogramming_form.getReprogrammingFormFromMedicalRequest','_id'));
        const updateRepro = @json(route('seguimientos.update_reprogramming.updateReprogramming','_id'));

        function mostrarReprogramacionSeguimientos(medical_request_id) {
            var url = getReproForm;
            url = url.replace('_id',medical_request_id);
            var token = $("meta[name=csrf-token]").attr("content");

            $.ajax({
                url: url,
                method: 'post',
                headers: {'X-CSRF-TOKEN': token},
                beforeSend: function(e){
                    // Loading
                    // blockCard('#form_content');
                }
            })
            .done(function(response) {
                switch (response['type']) {
                    case "correct":
                        $('button#buttonReprogramming').attr('onclick','').unbind('click');
                        document.getElementById('buttonReprogramming').setAttribute("onClick","actualizarReprogramacion("+medical_request_id+")");

                        $('button#btnRemision').attr('onclick','').unbind('click');
                        document.getElementById('btnRemision').setAttribute("onClick","modalAbrirRemit("+medical_request_id+")");

                        $('div#body_modal_reprogramming').empty();
                        $('div#body_modal_reprogramming').append(response['view']);

                        $('div#modal_reprogramming').modal('show');
                        break;
                    case "error":
                        toastr.error(
                            response['msj'],
                            '¡Error!'
                        );
                        break;
                }

            })
            .fail(function() {
                toastr.error(
                    'Ocurrio un error al iniciar el formulario',
                    '¡Error!'
                );
            });
        }

        $("form#frm-reprogramming").submit(function(e){
            e.preventDefault();
        });

        function actualizarReprogramacion(medical_request_id) {
            if($("form#frm-reprogramming").valid()){
                var url = updateRepro;
                url = url.replace('_id',medical_request_id);
                var token = $("meta[name=csrf-token]").attr("content");

                var data = $("form#frm-reprogramming").serialize();
                $.ajax({
                    url: url,
                    method: 'post',
                    headers: {'X-CSRF-TOKEN': token},
                    data: data,
                    beforeSend: function(e){
                        // Loading
                        // blockCard('#form_content');
                    }
                })
                .done(function(response) {
                    switch (response['type']) {
                        case "correct":
                            $('div#modal_reprogramming').modal('hide');
                            updateRowsTable(response['filas'])

                            break;
                        case "error":
                            toastr.error(
                                response['msj'],
                                '¡Error!'
                            );
                            break;
                    }

                })
                .fail(function() {
                    toastr.error(
                        'Ocurrio un error al iniciar el formulario',
                        '¡Error!'
                    );
                });
            }
        }
    </script>
@endsection
