<div class="form-row">
    <div class="col-md-6 col-sm-12">
        <div class="form-group">
            {!! Form::label('weighting_level_id_search', 'Riesgo') !!}
            <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::select('weighting_level_id_search', $weighting_levels, null, ['class' => 'form-control select2 '.( $errors->has('weighting_level_id_search') ? ' is-invalid' : '' ), 'style'=>'with: 100%;']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="form-group">
            {!! Form::label('risk_state_id_search', 'Etapa de Tratamiento') !!}
            <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::select('risk_state_id_search[]', $risk_states, null, ['class' => 'form-control select2 '.( $errors->has('risk_state_id_search') ? ' is-invalid' : '' ), 'style'=>'with: 100%;', 'multiple']) !!}
            </div>
        </div>
    </div>
</div>
