<div class="form-row">
    <div class="col-md-6 col-sm-12">
        <div class="form-group">
            {!! Form::label('start_tracing_date_search', 'Inicio de fecha de seguimiento', ['class' => 'control-label col-md-12 col-xs-12']) !!}
            <div class="input-group date" id="start_tracing_date_search" data-target-input="nearest">
                <input name="start_tracing_date_search" type="text" class="form-control datetimepicker-input" data-target="#start_tracing_date_search"/>
                <div class="input-group-append" data-target="#start_tracing_date_search" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="form-group">
            {!! Form::label('end_tracing_date_search', 'Fin de fecha de seguimiento', ['class' => 'control-label col-md-12 col-xs-12']) !!}
            <div class="input-group date" id="end_tracing_date_search" data-target-input="nearest">
                <input name="end_tracing_date_search" type="text" class="form-control datetimepicker-input" data-target="#end_tracing_date_search"/>
                <div class="input-group-append" data-target="#end_tracing_date_search" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
        </div>
    </div>
</div>
