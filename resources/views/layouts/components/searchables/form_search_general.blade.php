<div class="form-row">
    <div class="col-md-12 col-sm-12">
        {!! Form::label('Busqueda', 'Busqueda', ['class' => 'control-label col-md-12 col-xs-12 mb-1']) !!}
        <div class="input-group input-group">
            <input id="search_data" type="text" class="form-control" placeholder="Busqueda general...">
        </div>
    </div>
</div>

<div class="form-row">
    <div class="col-md-4 col-sm-12">
        <div class="form-group">
            {!! Form::label('medical_request_id_search', 'Estado') !!}
            <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::select('medical_request_id_search', $medical_request_statuses, null, ['class' => 'form-control select2 '.( $errors->has('medical_request_id_search') ? ' is-invalid' : '' ), 'style'=>'with: 100%;']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-12">
        <div class="form-group">
            {!! Form::label('enterprise_id_search', 'Empresa') !!}
            <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::select('enterprise_id_search', $enterprises_search, null, ['class' => 'form-control select2 '.( $errors->has('enterprise_id_search') ? ' is-invalid' : '' ), 'style'=>'with: 100%;']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-12">
        <div class="form-group">
            {!! Form::label('project_id_search', 'Proyecto') !!}
            <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::select('project_id_search', $projects_search, null, ['class' => 'form-control select2 '.( $errors->has('project_id_search') ? ' is-invalid' : '' ), 'style'=>'with: 100%;']) !!}
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/components/search.js') }}"></script>
