<div class="form-row">
    <div class="col-md-4 col-sm-12">
        <div class="form-group">
            {!! Form::label('option_date_search', 'Opción para fechas', ['class' => 'control-label col-md-12 col-xs-12']) !!}
            <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::select('option_date_search', $option_date_search, null, ['class' => 'form-control select2 '.( $errors->has('option_date_search') ? ' is-invalid' : '' ), 'style'=>'with: 100%;']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-12">
        <div class="form-group">
            {!! Form::label('start_date_search', 'Inicio de fecha', ['class' => 'control-label col-md-12 col-xs-12']) !!}
            <div class="input-group date" id="start_date_search" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input" data-target="#start_date_search"/>
                <div class="input-group-append" data-target="#start_date_search" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-12">
        <div class="form-group">
            {!! Form::label('end_date_search', 'Fin de fecha', ['class' => 'control-label col-md-12 col-xs-12']) !!}
            <div class="input-group date" id="end_date_search" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input" data-target="#end_date_search"/>
                <div class="input-group-append" data-target="#end_date_search" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
        </div>
    </div>
</div>
