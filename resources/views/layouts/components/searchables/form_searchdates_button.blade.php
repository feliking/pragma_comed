{!! Form::label('date', 'Fecha Prox. Consulta') !!}
        <div class="row"> 
            <div class="col-md-5">
                <div class="form-group">
                    <div class="input-group mb-3 date" id="init_date_field" data-target-input="nearest">
                        <input type="text" id="init_date" name="init_date" class="form-control datetimepicker-input" data-target="#init_date_field" placeholder="Fecha Inicio" data-inputmask='"mask": "99/99/9999"' data-greedy="false" data-mask/>
                        <div class="input-group-append" data-target="#init_date_field" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <div class="input-group mb-3 date" id="end_date_field" data-target-input="nearest">
                        <input type="text" id="end_date" name="end_date" class="form-control datetimepicker-input" data-target="#end_date_field" placeholder="Fecha Fin" data-inputmask='"mask": "99/99/9999"' data-greedy="false" data-mask/>
                        <div class="input-group-append" data-target="#end_date_field" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 text-center">
                <button type="button" class="btn btn-secondary" onclick="CleanDates()"><i class="fas fa-calendar"></i> Limpiar</button>
            </div>
        </div>