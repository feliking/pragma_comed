<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fa fa-search"></i>
            Busqueda
        </h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <!--OBLIGATORIO-->
        @include('layouts.components.searchables.form_search_general')
        
        <!--OPCIONALES-->
        @include('layouts.components.searchables.form_searchdates_button')
    </div>
    <div class="card-footer clearfix text-right">
        <div class="btn-group" role="group" aria-label="Basic example">
            <button type="button" class="btn btn-warning float-right" onclick="clearValues()"><i class="fas fa-eraser"></i> Limpiar</button>
            <button type="button" class="btn btn-info float-right" onclick="getValues()"><i class="fas fa-filter"></i> Filtrar</button>
        </div>
    </div>
</div>
