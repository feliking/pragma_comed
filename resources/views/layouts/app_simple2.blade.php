<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">         <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }} | @yield('title')</title>         <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/bootstrap') }}">         <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/fontawesome-free/css/all.min.css') }}">         <!-- Ionicons -->
{{--    <link rel="stylesheet" href="/plugins/fontawesome-free/css/all.min.css">         <!-- Ionicons -->--}}
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->

    <link rel="stylesheet" href="{{ asset('adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
{{--    <link rel="stylesheet" href="/plugins/icheck-bootstrap/icheck-bootstrap.min.css">         <!-- Select2 -->--}}
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2/css/select2.min.css') }}">
{{--    <link rel="stylesheet" href="/plugins/select2/css/select2.min.css">--}}
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
{{--    <link rel="stylesheet" href="/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">--}}
    <!-- Theme style -->

    <link rel="stylesheet" href="{{ asset('adminlte/dist/css/adminlte.min.css') }}">
{{--    <link rel="stylesheet" href="/dist/css/adminlte.min.css">         <!-- Google Font: Source Sans Pro -->--}}
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/skins/flat/blue.css') }}">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo"><a href="#">
            <b>R</b>egistro de <b>I</b>ngreso y <b>S</b>alida de <b>O</b>ficinas </a>
    </div> @yield('content')         </div>         <!-- jQuery -->
<script src="/plugins/jquery/jquery.min.js"></script>         <!-- Bootstrap 4 -->
<script src="/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>         <!-- Select2 -->
<script src="/plugins/select2/js/select2.full.min.js"></script>         <!-- jquery-validation -->
<script src="/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="/plugins/jquery-validation/additional-methods.min.js"></script>         <!-- AdminLTE App -->
<script src="/dist/js/adminlte.min.js"></script>
<script src="/js/icheck.js"></script>
<script src="/js/icheck.min.js"></script>
<script>
    $(function () {                 //Initialize Select2 Elements
        $('.select2').select2()                 //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
    })
</script>
@yield('script')
</body>
</html>

