<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8"/>
        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Theme style -->
         <link rel="stylesheet" href="{{ public_path('adminlte/dist/css/adminlte.min.css') }}">

        @yield('style')
    </head>

    <body>
        <div class="container-fluid">
            @yield('content')
        </div>

        @yield('script')
    </body>
</html>
