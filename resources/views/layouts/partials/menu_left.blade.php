 <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="{{ asset('adminlte/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">S.C.M.</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <img src="{{ asset('img/usu.png') }}" class="img-circle elevation-2" alt="User Image">
            <div class="info">
                <a href="#" class="d-block">{{\Illuminate\Support\Facades\Auth::user()->username}}</a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
        {{--<div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>--}}

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                @if(\Illuminate\Support\Facades\Auth::user()->role_id==1)
                        {{-- expr --}}
                    <li class="nav-item">
                        <a href="{{route('administracion_solicitudes.index')}}" class="nav-link" id="solicitud_medica">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Consultas
                            </p>
                        </a>
                    </li>
                @endif
                @if (\Illuminate\Support\Facades\Auth::user()->role_id==1 || \Illuminate\Support\Facades\Auth::user()->role_id==3)
                    <li class="nav-item">
                        <a href="{{route('consultas_medicas.index')}}" class="nav-link" id="mis_consultas">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Mis consultas
                            </p>
                        </a>
                    </li>
                @endif
                @if(\Illuminate\Support\Facades\Auth::user()->role_id==1 || \Illuminate\Support\Facades\Auth::user()->role_id==3)
                    <li class="nav-item">
                        <a href="{{route('consultas_medicas.realizadas.indexMmedicalRequestClose')}}" class="nav-link" id="consultas_cerradas">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Consultas Realizadas
                            </p>
                        </a>
                    </li>
                @endif
                @if(\Illuminate\Support\Facades\Auth::user()->role_id==1 || \Illuminate\Support\Facades\Auth::user()->role_id==5)
                    <li class="nav-item">
                        <a href="{{ route('consultas_medicas_pendientes.index') }}" class="nav-link" id="pendientes">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Casos por Validar
                            </p>
                        </a>
                    </li>
                @endif

                @if(\Illuminate\Support\Facades\Auth::user()->role_id==1 || \Illuminate\Support\Facades\Auth::user()->role_id==5)
                    <li class="nav-item">
                        <a href="{{ route('solicitudes_rechazadas.rejectRequest') }}" class="nav-link" id="rejected">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Casos Rechazados
                            </p>
                        </a>
                    </li>
                @endif

                {{-- @if(\Illuminate\Support\Facades\Auth::user()->role_id==1 || \Illuminate\Support\Facades\Auth::user()->role_id==5)
                    <li class="nav-item">
                        <a href="{{ route('solicitudes_reasignar.reassignmentRequest') }}" class="nav-link" id="reassignment">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Reasignación
                            </p>
                        </a>
                    </li>
                @endif --}}
                @if (\Illuminate\Support\Facades\Auth::user()->role_id==6
                    || \Illuminate\Support\Facades\Auth::user()->role_id==1
                    || \Illuminate\Support\Facades\Auth::user()->role_id==4
                    || \Illuminate\Support\Facades\Auth::user()->role_id==10
                    || \Illuminate\Support\Facades\Auth::user()->role_id==11
                    )
                    <li class="nav-item">
                        <a href="{{route('seguimientos.index')}}" class="nav-link" id="nav_mis_seguimientos">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Mis Casos
                            </p>
                        </a>
                    </li>
                @endif

                @if (\Illuminate\Support\Facades\Auth::user()->role_id==1
                    || \Illuminate\Support\Facades\Auth::user()->role_id==7
                    || \Illuminate\Support\Facades\Auth::user()->role_id==8
                    || \Illuminate\Support\Facades\Auth::user()->role_id==10
                    || \Illuminate\Support\Facades\Auth::user()->role_id==11
                    )
                    <li class="nav-item">
                        <a href="{{route('administracion_seguimientos.index')}}" class="nav-link" id="nav_administracion_seguimientos">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Listado de Casos
                            </p>
                        </a>
                    </li>

                @endif

                {{--@if(\Illuminate\Support\Facades\Auth::user()->role_id==1
                    || \Illuminate\Support\Facades\Auth::user()->role_id==7
                    || \Illuminate\Support\Facades\Auth::user()->role_id==10)
                    <li class="nav-item">
                        <a href="{{route('administracion_seguimientos.cerradas.indexMmedicalRequestClose')}}" class="nav-link" id="nav_close_requests">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Consultas Cerradas
                            </p>
                        </a>
                    </li>
                @endif--}}

                {{--@if(\Illuminate\Support\Facades\Auth::user()->role_id==1
                    || \Illuminate\Support\Facades\Auth::user()->role_id==7
                    || \Illuminate\Support\Facades\Auth::user()->role_id==10)
                    <li class="nav-item">
                        <a href="{{route('administracion_seguimientos.cerradas.index_rechazadas')}}" class="nav-link" id="nav_rechazadas">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Consultas Rechazadas
                            </p>
                        </a>
                    </li>
                @endif--}}

                @if(\Illuminate\Support\Facades\Auth::user()->role_id==1)
                    <li class="nav-item">
                        <a href="{{ route('consultas_medicas.agendar') }}" class="nav-link" id="agendar">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Asignar cita
                            </p>
                        </a>
                    </li>
                @endif

                @if (\Illuminate\Support\Facades\Auth::user()->role_id == 1 || \Illuminate\Support\Facades\Auth::user()->role_id == 4)
                    <li class="nav-item">
                        <a href="{{ route('enfermera.index') }}" class="nav-link" id="nav_enfermera">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Mis pacientes
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('enfermera.casos_doctor') }}" class="nav-link" id="nav_docs_cases">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Consultas - Doctores
                            </p>
                        </a>
                    </li>
                @endif
                @if (\Illuminate\Support\Facades\Auth::user()->role_id == 1)
                    <li class="nav-item">
                        <a href="{{ route('importar_datos.edit') }}" class="nav-link" id="nav_import">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Importar
                            </p>
                        </a>
                    </li>
                @endif

                @if (\Illuminate\Support\Facades\Auth::user()->role_id == 1
                    || \Illuminate\Support\Facades\Auth::user()->role_id == 9
                    || \Illuminate\Support\Facades\Auth::user()->role_id==10)
                <li class="nav-header" >REPORTES</li>
                {{--<li class="nav-item">
                    <a href="{{ route('report.global_medical_request') }}" class="nav-link" id="nav_report_global">
                        <i class="far fa-chart-bar"></i>
                        <p>
                            Global
                        </p>
                    </a>
                </li>--}}
                <li class="nav-item">
                    <a href="{{ route('report.global_risk_state') }}" class="nav-link" id="nav_report_risk">
                        <i class="fa fa-chart-line"></i>
                        <p>
                            Reportes
                        </p>
                    </a>
                </li>
                {{-- <li class="nav-item">
                    <a href="{{ route('report.global_report_cases') }}" class="nav-link" id="nav_report_cases">
                        <i class="far fa-chart-bar"></i>
                        <p>
                            Casos reportados
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('report.casos') }}" class="nav-link" id="nav_index_report_cases">
                        <i class="fa fa-book-open"></i>
                        <p>
                            Reporte Listado Casos
                        </p>
                    </a>
                </li> --}}
                @endif

                @if(\Illuminate\Support\Facades\Auth::user()->role_id==1 || \Illuminate\Support\Facades\Auth::user()->role_id==2)
                    <li class="nav-header" >CONFIGURACIÓN</li>

                    <li class="nav-item" >
                        <a href="{{ route('prioridades.index') }}" class="nav-link" id="nav_prioridades">
                            <i class="far fa-circle nav-icon text-danger"></i>
                            <p>Prioridades</p>
                        </a>
                    </li>
                    <li class="nav-item" >
                        <a href="{{ route('tipo.index') }}" class="nav-link" id="nav_tipo">
                            <i class="far fa-circle nav-icon text-info"></i>
                            <p>Tipos</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('estado.index') }}" class="nav-link" id="nav_estado">
                            <i class="far fa-circle nav-icon text-warning"></i>
                            <p>Estados</p>
                        </a>
                    </li>
                    <li class="nav-item" >
                        <a href="{{ route('niveles_ponderacion.index') }}" class="nav-link" id="nav_lvl">
                            <i class="fas fa-level-down-alt nav-icon text-info"></i>
                            <p>Niveles de Ponderación</p>
                        </a>
                    </li>


                    <li class="nav-header" >ADM. DEL SISTEMA</li>
                    <li class="nav-item" >
                        <a href="{{ route('usuario.index') }}" class="nav-link" id="nav_users">
                            <i class="far fa-user nav-icon text-info"></i>
                            <p>Usuarios</p>
                        </a>
                    </li>
                    {{-- <li class="nav-item" >
                        <a href="{{ route('covid_estados.index') }}" class="nav-link" id="nav_cvd">
                            <i class="fas fa-biohazard nav-icon text-danger"></i>
                            <p>Estados de Enfermedad</p>
                        </a>
                    </li> --}}

                    <li class="nav-item" >
                        <a href="{{ route('tratamiento_etapas.index') }}" class="nav-link" id="nav_treat">
                            <i class="fas fa-notes-medical nav-icon text-info"></i>
                            <p>Etapas de Tratamiento</p>
                        </a>
                    </li>

                    <li class="nav-item" >
                        <a href="{{ route('tipos_riesgo.index') }}" class="nav-link" id="nav_risk">
                            <i class="fas fa-exclamation-triangle nav-icon text-danger"></i>
                            <p>Tipos de Riesgo</p>
                        </a>
                    </li>

                    <li class="nav-item" >
                        <a href="{{ route('laboratorios.index') }}" class="nav-link" id="nav_laboratory">
                            <i class="fas fa-clinic-medical nav-icon text-info"></i>
                            <p>Laboratorios</p>
                        </a>
                    </li>
                    <li class="nav-item" >
                        <a href="{{ route('criterios_enfermedad.index') }}" class="nav-link" id="nav_disease">
                            <i class="fas fa-prescription-bottle-alt nav-icon text-success"></i>
                            <p>Criterios de Enfermedad</p>
                        </a>
                    </li>
                    <li class="nav-item" >
                        <a href="{{ route('tipos_prueba.index') }}" class="nav-link" id="nav_test_type">
                            <i class="fas fa-notes-medical nav-icon text-info"></i>
                            <p>Tipos de prueba</p>
                        </a>
                    </li>
                    <li class="nav-item" >
                        <a href="{{ route('estados_riesgo.index') }}" class="nav-link" id="nav_rstatus">
                            <i class="fas fa-file-signature nav-icon text-success"></i>
                            <p>Estados de Riesgo</p>
                        </a>
                    </li>
                    <li class="nav-item" >
                        <a href="{{ route('tipos_vacuna.index') }}" class="nav-link" id="nav_rstatus">
                            <i class="fas fa-biohazard nav-icon text-success"></i>
                            <p>Tipos de vacuna</p>
                        </a>
                    </li>
                @endif

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
