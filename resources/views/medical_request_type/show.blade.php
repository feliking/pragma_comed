@extends('layouts.app_menu')

@section('content')
@include('flash::message')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Tipos</h1>
                </div>
                
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            {!! Form::open(['route' => ['tipo.destroy', $medical_request_type->id], 'method' => 'delete', 'id' => 'frm-role']) !!}
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Activar o desactivar Prioridad</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row align-items-cente">
                                <div class="col-11 offset-md-1">
                                <h3>¿Está seguro de cambiar el estado del tipo: <i>{{ $medical_request_type->name }}</i>, de
                                @if($medical_request_type->deleted_at == null)
                                Activo a <b>Inactivo</b>
                                @else
                                Inactivo a <b>Activo</b>
                                @endif
                                ?</h3>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <div class="form-group">
                                <div class="col-sm-10 col-xs-12 col-sm-offset-6 text-right">
                                    <button type="submit" class="btn btn-danger">Si</button>
                                    <a href="{{ route('tipo.index') }}" class="btn btn-default">No</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('li#nav_config').addClass('menu-open');
        $('li#nav_config').children().addClass('active');
        $('a#nav_tipo').addClass('active');
    });
</script>
@endsection
