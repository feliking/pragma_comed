<div class="col-md-8 offset-md-3">
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} wrapper-name">
        {!! Form::label('name', '* Nombre', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
    
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
    
        </div>
    </div>
    
    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }} wrapper-description">
        {!! Form::label('description', 'Descripción', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('description', null, ['class' => 'form-control']) !!}
    
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
    
        </div>
    </div>
    <div class="form-group{{ $errors->has('medical_request_priority_id') ? ' has-error' : '' }} wrapper-medical_request_priority_id">
        {!! Form::label('medical_request_priority_id', '* Prioridad', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::select('medical_request_priority_id', isset($medical_request_priorities) ? $medical_request_priorities : [], isset($medical_request_type) ? $medical_request_type->medical_request_priority_id : null, ['class' => 'form-control select2', 'placeholder' => 'Seleccione una prioridad']) !!}
    
            <span class="help-block">
                <strong>{{ $errors->first('medical_request_priority_id') }}</strong>
            </span>
        
        </div>
    </div>
</div>