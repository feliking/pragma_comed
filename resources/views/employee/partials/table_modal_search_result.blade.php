@if(count($employees) > 0)
    <table id="medical_requests_table" class="table table-striped table-hover">
        <thead>
            <tr>
                <th>Empleado</th>
                <th>Empresa</th>
                <th>CI</th>
                <th>Op</th>
            </tr>
        </thead>
        <tbody>
        @foreach($employees as $employee)
            <tr>
                <td>
                    {{$employee->full_name}}
                </td>
                <td>
                    {{$employee->enterprise->nombre}}
                </td>
                <td>
                    {{$employee->full_ci}}
                </td>
                <td>
                    <button class="btn btn-primary" onclick="seleccionarEmpleadoModal({{$employee->sol_empresa_id}},{{$employee->id}})">
                        <i class="fa fa-check"></i> Seleccionar
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    {{--<div class="alert alert-warning alert-dismissible">--}}
    <div class="alert alert-warning">
        <h5><i class="icon fas fa-info"></i> Alerta!</h5>
        <p>No se encontro ningun empleado con los criterios brindados.</p>
        <p>CI: <b>{{$ci}}</b> <b>{{$ci_expedition}}</b></p>
    </div>
@endif
