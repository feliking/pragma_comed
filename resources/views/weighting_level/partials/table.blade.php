<table id="example2" class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Op.</th>
        <th>Nombre</th>
        <th>Descripción</th>
        <th>Ponderación Mínima</th>
        <th>Ponderación Máxima</th>
        <th>Evaluación</th>
        <th>Ultimo Nivel</th>
        <th>Estado</th>
    </tr>
    </thead>

    <tbody>
    @foreach($weighting_levels as $weighting_level)
        <tr>
            <td>
                <div class="btn-group" role="group">
                    <a href="{{ route('niveles_ponderacion.edit', $weighting_level->id) }}" class="btn btn-sm btn-default" title="Editar">
                        <i class="fa fa-edit"></i>
                        <span class="sr-only">Editar</span>
                    </a>
                    <a href="{{ route('niveles_ponderacion.show', $weighting_level->id) }}" class="btn btn-sm btn-default" title="{{($weighting_level->deleted_at == null)?'Desactivar':'Activar'}}">
                        <i class="fa fa-{{($weighting_level->deleted_at == null)?'times':'check'}}"></i>
                        <span class="sr-only">{{($weighting_level->deleted_at == null)?'Desactivar':'Activar'}}</span>
                    </a>
                </div>
            </td>
            <td>
                <span class="badge" style="background-color: {{$weighting_level->color}}">{{$weighting_level->name}}</span>

            </td>
            <td>
                {{$weighting_level->description}}
            </td>
            <td>
                {{$weighting_level->minimum}}
            </td>
            <td>
                {{$weighting_level->maximum}}
            </td>
            <td>
                @if ($weighting_level->evaluation==0)
                    NINGUNO
                @else
                @if ($weighting_level->evaluation==1)
                    DIARIO
                @endif
                @if ($weighting_level->evaluation==2)
                    INTERDIARIO
                @endif
                @if ($weighting_level->evaluation>2)
                    CADA {{$weighting_level->evaluation}} DÍAS
                @endif
                    
                @endif
            </td>
            <td>
                @if($weighting_level->last_level_flag)
                    <span class="badge badge-danger">SI</span>
                @else
                    <span class="badge badge-success">NO</span>
                @endif
            </td>
            <td>
                @if($weighting_level->deleted_at == null)
                <span class="badge badge-success">Activo</span>
                @elseif($weighting_level->deleted_at != null)
                <span class="badge badge-danger">Inactivo</span>
                @endif
            </td>

        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Op.</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Ponderación Mínima</th>
            <th>Ponderación Máxima</th>
            <th>Evaluación</th>
            <th>Ultimo Nivel</th>
            <th>Estado</th>
        </tr>
    </tfoot>
</table>
