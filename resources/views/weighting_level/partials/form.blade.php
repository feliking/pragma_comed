<div class="col-md-8 offset-md-3">
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} wrapper-name">
        {!! Form::label('name', '* Nombre del Nivel', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('name', null, ['class' => 'form-control'.( $errors->has('name') ? ' is-invalid' : '' )]) !!}

            @error('name')
            <span class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
            @enderror

        </div>
    </div>

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }} wrapper-description">
        {!! Form::label('description', 'Descripción', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('description', null, ['class' => 'form-control '.( $errors->has('description') ? ' is-invalid' : '' )]) !!}

            @error('description')
            <span class="invalid-feedback">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
            @enderror

        </div>
    </div>

    <div class="form-group wrapper-color">
        {!! Form::label('description', '* Color', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}

        <div class="input-group col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('color', isset($weighting_level)?$weighting_level->color:null, ['class' => 'form-control'.( $errors->has('color') ? ' is-invalid' : '' )]) !!}

            <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-square"></i></span>
            </div>
        </div>
        <!-- /.input group -->
    </div>

    <div class="form-group{{ $errors->has('minimum') ? ' has-error' : '' }} wrapper-minimum">
        {!! Form::label('minimum', '* Ponderación Mínima', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('minimum', isset($weighting_level)?$weighting_level->minimum:null, ['class' => 'form-control'.( $errors->has('minimum') ? ' is-invalid' : '' )]) !!}

            @error('minimum')
            <span class="invalid-feedback">
                <strong>{{ $errors->first('minimum') }}</strong>
            </span>
            @enderror

        </div>
    </div>
    <div class="form-group{{ $errors->has('maximum') ? ' has-error' : '' }} wrapper-maximum">
        {!! Form::label('maximum', '* Ponderación Máxima', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('maximum', isset($weighting_level)?$weighting_level->maximum:null, ['class' => 'form-control  '.( $errors->has('maximum') ? ' is-invalid' : '' )]) !!}

            @error('maximum')
            <span class="invalid-feedback">
                <strong>{{ $errors->first('maximum') }}</strong>
            </span>
            @enderror

        </div>
    </div>

    <div class="form-group{{ $errors->has('evaluation') ? ' has-error' : '' }} wrapper-evaluation">
        {!! Form::label('evaluation', '* Evaluación (Cant. en días)', ['class' => 'control-label col-md-6 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('evaluation', isset($weighting_level)?$weighting_level->evaluation:null, ['class' => 'form-control  '.( $errors->has('evaluation') ? ' is-invalid' : '' )]) !!}

            @error('evaluation')
            <span class="invalid-feedback">
                <strong>{{ $errors->first('evaluation') }}</strong>
            </span>
            @enderror

        </div>
    </div>

    <div class="form-group col-md-12 col-sm-12">
        <label for="radio_c"> ¿Desea registrar este nivel como último?</label>
    </div>
    <div class="form-group col-md-12 col-sm-12">
        <input type="radio" name="radio_c" id="radio_c1" value="true"  required="required" {{isset($weighting_level)&&$weighting_level->last_level_flag==1?'checked':''}}>
        <label for="radio_c1">Sí</label>
        <input type="radio" name="radio_c" id="radio_c2" value="false"  required="required" {{isset($weighting_level)&&$weighting_level->last_level_flag==1?'':'checked'}}>
        <label for="radio_c2">No</label>
    </div>
</div>
<div id="alert_min" class="alert alert-danger mb-12 text-center" role="alert" style="display: none;">
    Debe ingresar una ponderación mínima mayor {{-- a la última ponderación máxima --}}
</div>
<div id="alert_max" class="alert alert-danger mb-12 text-center" role="alert" style="display: none;">
    Debe ingresar una ponderación máxima mayor {{-- a la última ponderación máxima --}}
</div>
<div id="alert_flag" class="alert alert-warning mb-12 text-center" role="alert" style="display: none;">
    Ya definió un último nivel de ponderación, no podrá crear otro (debe editar el último nivel creado para crear más niveles)
</div>
<label for="table_weight">Ponderaciones Registradas</label>
  <div class="form-row" style="text-align: center;">
    <div id="cont111" style="margin:0 auto;"></div>
    <div class="table-responsive">
        <table class="table table-hover" id="table_weight" style="{{(count($weighting_levels)==0)?'display: none;':''}}">
            <thead>
            <tr>
                <th>Nivel</th>
                <th>Descripción</th>
                <th>Ponderación Mínima</th>
                <th>Ponderación Máxima</th>
                <th>Evaluación</th>
                <th>Último Nivel</th>
            </tr>
            </thead>
            <tbody id="ac">
                @foreach ($weighting_levels as $weighting_level)
                    <tr>
                        <td>{{$weighting_level->name}}</td>
                        <td>{{$weighting_level->description}}</td>
                        <td>{{$weighting_level->minimum}}</td>
                        <td>{{$weighting_level->maximum}}</td>
                        <td>
                            @if ($weighting_level->evaluation==0)
                                NINGUNO
                            @else
                            @if ($weighting_level->evaluation==1)
                                DIARIO
                            @endif
                            @if ($weighting_level->evaluation==2)
                                INTERDIARIO
                            @endif
                            @if ($weighting_level->evaluation>2)
                                CADA {{$weighting_level->evaluation}} DÍAS
                            @endif
                                
                            @endif
                        </td>
                        <td>
                            @if ($weighting_level->last_level_flag)
                                <span class="badge badge-danger">SÍ</span>
                            @else
                                <span class="badge badge-success">NO</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @if (count($weighting_levels)==0)
            <div id="alert_weight" class="alert alert-warning mb-12" role="alert">
                No hay ponderaciones registradas
            </div>
        @endif

    </div>
</div>
