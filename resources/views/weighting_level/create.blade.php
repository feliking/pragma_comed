@extends('layouts.app_menu')
@section('style')
    <!-- Bootstrap Color Picker -->
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Creación de Niveles de Ponderación</h1>
                </div>

            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            {!! Form::open(['route' => 'niveles_ponderacion.store', 'method' => 'post', 'id' => 'frm-weigth_id']) !!}
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Crear Nivel de Ponderación</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @include('weighting_level.partials.form')
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <div class="form-group">
                                <div class="col-xs-12 text-right">
                                    <a href="{{ route('niveles_ponderacion.index') }}" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Volver</a>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Registrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script src="{{ asset('/js/weighting_level/general.js') }}"></script>

<script>
    //$('#minimum').inputmask({alias: 'numeric', allowMinus: false, digits: 2, max: 999.99});
    //$('#maximum').inputmask({alias: 'numeric', allowMinus: false, digits: 2, max: 999.99});
    $("#minimum").mask("ZZ", { translation: { 'Z': { pattern: /^([0-9]+)$/ } } });
    $("#maximum").mask("ZZ", { translation: { 'Z': { pattern: /^([1-9]+)$/ } } });
    $("#evaluation").mask("ZZ", { translation: { 'Z': { pattern: /^([0-9]+)$/ } } });
    $(document).ready(function() {
        $('a#nav_lvl').addClass('active');

        $('input[name=color]').colorpicker();

        $('input[name=color]').on('colorpickerChange', function(event) {
            $('input[name=color] .fa-square').css('color', event.color.toString());
        });
    });
    $('input[type=radio][name^=radio_c]').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });
    $('[name=minimum]').change(function (event) {
        var min=$(this).val();
        var max=$('#maximum').val();
        var radio=$('#radio_c1').is(':checked');
        //console.log(parseFloat(min),max,radio);
        if(min!=null && min!='' && min!=undefined){
            @if(count($weighting_levels)>0)
                /* @foreach ($weighting_levels as $weighting_level)

                @endforeach */
                var m_bd = {{$weighting_levels->last()->maximum}};
                //console.log(m_bd, parseFloat(m_bd)>parseFloat(min));
                if(parseFloat(m_bd)>=parseFloat(min) || parseFloat(min)>=parseFloat(max)){
                    $('#minimum').val('');
                    $('#alert_max').hide();
                    $('#alert_min').show();
                }else{
                    $('#alert_max').hide();
                    $('#alert_min').hide();
                }
                /* $('#presentation_types tbody tr').each(function (index, obj) {

                }); */
            @endif
        }
    });
    $('[name=maximum]').change(function (event) {
        var max=$(this).val();
        var min=$('#minimum').val();
        var radio=$('#radio_c1').is(':checked');
        if(min!=null && min!='' && min!=undefined){
            @if(count($weighting_levels)>0)
                var m_bd = {{$weighting_levels->last()->maximum}};
                //console.log(m_bd, parseFloat(m_bd)>parseFloat(min), parseFloat(max)<=parseFloat(min));
                if(parseFloat(m_bd)>parseFloat(max) || parseFloat(max)<=parseFloat(min)){
                    $('#maximum').val('');
                    $('#alert_min').hide();
                    $('#alert_max').show();
                }else{
                    $('#alert_max').hide();
                    $('#alert_min').hide();
                }
            @endif
        }
    });
    @if(count($weighting_levels)>0)
    var sw_bd = {{$weighting_levels->last()->last_level_flag}};
    var min=$('#minimum').val({{$weighting_levels->last()->maximum+1}});
    if(sw_bd==1){
        $('#alert_flag').show();
        $('#name').attr('disabled',true);
        $('#description').attr('disabled',true);
        $('input[name=color]').attr('disabled',true);
        $('#maximum').attr('disabled',true);
        $('#minimum').attr('disabled',true);
        $('#radio_c1').attr('disabled',true);
        $('#radio_c2').attr('disabled',true);
        $('#evaluation').attr('disabled',true);
    }
    @endif
</script>
@endsection
