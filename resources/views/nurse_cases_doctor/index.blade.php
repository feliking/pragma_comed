@extends('layouts.app_menu')

@section('styles')

@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Casos - Doctores</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Datos de casos - Doctores</h3>

                            <div class="card-tools">

                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">


                            @include('nurse_cases_doctor.partials.table')
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
        </div>
    </section>
    @include('layouts.components.modals.modal_history')
    @include('layouts.components.modals.modal_history_timeline')
@endsection

@section('script')
    <script>
        const showHistory = @json(route('administracion_solicitudes.mostrar_historial.showHistory','_id'));
        const showHistoryTimeline = @json(route('buscar_codigo.mostrarTimeline'));
        //HORA PARA EL NOMBRE, PQ SEGURO ME PEDIRAN ESO
        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();
        var hour = d.getHours();
        var minutes = d.getMinutes();
        var seconds = d.getSeconds();

        var output = d.getFullYear() + '/' +
            ((''+month).length<2 ? '0' : '') + month + '/' +
            ((''+day).length<2 ? '0' : '') + day + "_" +hour+minutes+seconds;
        $(document).ready(function() {
            $('a#nav_docs_cases').addClass('active');
        });
        $(function () {
            $('#doctor_cases_table').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                "pageLength": 5,
                "order": [[1, 'asc']],
                "aoColumnDefs": [{ "bSortable": false, "aTargets": [0] }],
                "language": {
                    "decimal": ",",
                    "thousands": ".",
                    "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "infoPostFix": "",
                    "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "loadingRecords": "Cargando...",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "paginate": {
                        "first": "Primero",
                        "last": "Último",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "searchPlaceholder": "Término de búsqueda",
                    "zeroRecords": "No se encontraron resultados",
                    "emptyTable": "Ningún dato disponible en esta tabla",
                    "aria": {
                        "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                },
                // "dom": 'Blrtp', 'Qfrtip',
                // "dom": 'Qfrtip',
                "dom": 'Bfrtilp',
                buttons: [
                    {"extend": 'excelHtml5',
                        "text":'<i class="fa fa-file-excel"></i> Exportar a Excel',
                        "className": 'btn btn-success',"title": "CasosDoctores_"+output,
                        exportOptions: {
                            columns: [ 2, 3, 4, 5, 6, 7, 8]
                        }
                    },
                    {"extend": 'copyHtml5',
                        "text":'<i class="fa fa-copy"></i> Copiar',
                        "className": 'btn btn-info',
                        exportOptions: {
                            columns: [ 2, 3, 4, 5, 6, 7, 8 ]
                        }
                    },
                ],
            });
        });
    </script>
    <script src="{{asset('js/components/button_historial.js')}}"></script>
    <script src="{{asset('js/components/button_historial_timeline.js')}}"></script>
@endsection
