<table id="doctor_cases_table" class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Op.</th>
            <th>Estado</th>
            <th>Resp. Seguimiento</th>
            <th>Cód. de Solicitud</th>
            <th>Riesgo</th>
            <th>Nombre Completo</th>
            <th>CI</th>
            <th>Empresa</th>
            <th>Nro de Teléfono</th>
        </tr>
    </thead>

    <tbody>
    @foreach($medical_requests as $medical_request)
        <tr id="tr_medical_consultation_{{$medical_request->id}}">
            <td>
                <div class="btn-group">
                    @include('layouts.components.buttons.button_historial_timeline',[
                        'medical_request_code' => $medical_request->code
                    ])
                    @include('layouts.components.buttons.button_historial',[
                    'medical_request_id' => $medical_request->id])
                </div>
            </td>
            <td>
                <span class="badge badge-info" >
                    {{ $medical_request->medical_requests_state->name }}
                </span>
            </td>
            <td>
                {{ $medical_request->medic!=null?$medical_request->medic->name:'' }}
            </td>
            <td>
                {{$medical_request->code}}
            </td>
            <td>
                {{$medical_request->risk_type->name}}
            </td>
            <td>
                @if($medical_request->employee!=null)
                    <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
                    {{$medical_request->employee->full_name}}
                @else
                    @if($medical_request->history_valid_status)
                        <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
                    @else
                        <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
                    @endif
                    {{$medical_request->full_name}}
                @endif
            </td>
            <td>
                {{$medical_request->full_ci}}
            </td>
            <td>
                @if($medical_request->employee!=null)
                    <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
                    {{$medical_request->employee->enterprise->nombre}}
                @else
                    @if($medical_request->history_valid_status)
                        <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
                    @else
                        <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
                    @endif
                    {{$medical_request->enterprise}}
                @endif
            </td>
            <td>
                @if($medical_request->whatsapp_flag)
                    <div class="badge badge-success">
                        <i class="fab fa-whatsapp"></i>
                    </div>
                @endif
                {{$medical_request->phone_number}}
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Op.</th>
            <th>Estado</th>
            <th>Resp. Seguimiento</th>
            <th>Cód. de Solicitud</th>
            <th>Riesgo</th>
            <th>Nombre Completo</th>
            <th>CI</th>
            <th>Empresa</th>
            <th>Nro de Teléfono</th>
        </tr>
    </tfoot>
</table>
