@extends('layouts.app_menu')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edición de Usuarios</h1>
                </div>

            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            {!! Form::model($user, ['route' => ['usuario.update', $user->id], 'method' => 'put', 'id' => 'frm-usuario']) !!}
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Editar Usuario</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @include('user.partials.form')
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <div class="form-group">
                                <div class="col-xs-12 text-right">
                                    <a href="{{ route('usuario.index') }}" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Volver</a>
                                    <button type="submit" class="btn btn-warning"><i class="fa fa-check"></i> Registrar cambios</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script>
    $("#username").mask("ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ", { translation: { 'Z': { pattern: /[a-zA-Z0-9]/, optional: true } } });
    $(document).ready(function() {
        $('li#nav_system').addClass('menu-open');
        $('li#nav_system').children().addClass('active');
        $('a#nav_users').addClass('active');
    });
    $('.select2').select2({
        theme: 'bootstrap4',
        placeholder: 'SELECCIONE UNA OPCIÓN'
    });
    /* $('select#role_id').val(idRole);
    $('select#role_id').change(); */
    var idrole = {{$user->role_id}};
    if (idrole!=null && idrole!=undefined && idrole!='') {
        if(idrole==3 || idrole==4){
            $('div#offices').removeAttr('hidden');
            if (idrole==3) {
                $('div#nurse_staff').removeAttr('hidden');
                $('div#doctor_staff').attr('hidden', true);
                var personal_selected={{$a}};

            }
            if (idrole==4) {
                $('div#doctor_staff').removeAttr('hidden');
                $('div#nurse_staff').attr('hidden', true);
                var personal_selected={{$a}};

            }
            var select_office = {{$user->rrhh_office_id}}//$('#rrhh_office_id:selected').val();
            var role = $('#role_id').val();
            var url = '{{ route('usuario.getmedics') }}';
            //console.log(select_office, role, personal_selected, {{$a}});
            data = {};
            data.select_office = select_office;
            data.role = role;
            $.ajax({
                url: url,
                headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                method: 'POST',
                dataType: 'JSON',
                data: data,
                beforeSend: function(e){
                    $('select[name^=nurse_staff_id]').empty();
                    $('select[name^=doctor_staff_id]').empty();
                }
            }).done(function (response){
                if(role==3){
                    if (response['nurses']!=null) {
                        var array_nurses = [];
                        $.each(response['nurses'], function(key, value){
                            //array_nurses.push(value['id']);
                            $('select[name^=nurse_staff_id]').append($('<option>', {value:value['id'], text:value['name']}));
                        });
                        $.each(personal_selected, function(key, value){
                            //console.log(value);
                            array_nurses.push(value);
                        });
                        $('select[name^=nurse_staff_id]').select2({
                            theme: 'bootstrap4',
                            placeholder: 'SELECCIONE UNA OPCIÓN'
                        }).val(personal_selected).trigger('change');

                        //$('select#nurse_staff_id').val({id:24}).trigger('change');
                    }
                }
                if(role==4){
                    if (response['doctors']!=null) {
                        $.each(response['doctors'], function(key, value){
                            $('select[name^=doctor_staff_id]').append($('<option>', {value:value['id'], text:value['name']}));
                        });
                        $('select[name^=doctor_staff_id]').select2({
                            theme: 'bootstrap4',
                            placeholder: 'SELECCIONE UNA OPCIÓN'
                        }).val(personal_selected).trigger('change');
                        //$('select#doctor_staff_id').val(personal_selected).trigger('change');
                    }
                }

            }).fail(function (response){
                console.log(response);
            });
        }else{
            $('div#offices').attr('hidden', true);
        }
    }
    $('select#role_id').on('change', function(event){
        var idRole = $(this).val();

        if (idRole!='' && idRole!=undefined) {
            $('select#rrhh_office_id').val('').trigger('change');
            $('select[name^=nurse_staff_id]').empty();
            $('select[name^=doctor_staff_id]').empty();
            //console.log(idRole);
            if (idRole==3 || idRole==4) {
                $('div#offices').removeAttr('hidden');
                if (idRole==3) {
                    $('div#nurse_staff').removeAttr('hidden');
                    $('div#doctor_staff').attr('hidden', true);
                    $('select#doctor_staff_id').val({}).trigger('change');
                }
                if (idRole==4) {
                    $('div#doctor_staff').removeAttr('hidden');
                    $('div#nurse_staff').attr('hidden', true);
                    $('select#nurse_staff_id').val({}).trigger('change');
                }
            }else{
                $('div#offices').attr('hidden', true);
                $('div#nurse_staff').attr('hidden', true);
                $('div#doctor_staff').attr('hidden', true);
                $('select#doctor_staff_id').val({}).trigger('change');
                $('select#nurse_staff_id').val({}).trigger('change');
            }
        }else{
            $('div#offices').attr('hidden', true);
            $('div#doctor_staff').attr('hidden', true);
            $('div#nurse_staff').attr('hidden', true);
        }
    });
    $('select#rrhh_office_id').on('change', function(event){
        var select_office = $(this).val();
        var role = $('#role_id').val();
        var url = '{{ route('usuario.getmedics') }}';
        //console.log(select_office, role);

        data = {};
        data.select_office = select_office;
        data.role = role;
        if (select_office != '' && select_office != undefined) {
            $.ajax({
                url: url,
                headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                method: 'POST',
                dataType: 'JSON',
                data: data,
                beforeSend: function(e){
                    $('select[name^=nurse_staff_id]').empty();
                    $('select[name^=doctor_staff_id]').empty();
                }
            }).done(function (response){
                if(role==3){
                    if (response['nurses']!=null) {
                        $.each(response['nurses'], function(key, value){
                            $('select[name^=nurse_staff_id]').append($('<option>', {value:value['id'], text:value['name']}));
                        });
                    }
                }
                if(role==4){
                    if (response['doctors']!=null) {
                        $.each(response['doctors'], function(key, value){
                            $('select[name^=doctor_staff_id]').append($('<option>', {value:value['id'], text:value['name']}));
                        });
                    }
                }

            }).fail(function (response){
                console.log(response);
            });
        }
    });
</script>
@endsection
