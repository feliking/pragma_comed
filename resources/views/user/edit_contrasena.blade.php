@extends('layouts.app_menu')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Usuarios</h1>
                </div>
                
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            {!! Form::model($usuario, ['route' => ['usuario.update_contrasena', $usuario->id], 'method' => 'put', 'id' => 'frm-usuario']) !!}
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Cambiar contraseña de usuario</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @include('user.partials.form_contrasena')
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <div class="form-group">
                                <div class="col-xs-12 text-right">
                                    <a href="{{ route('administracion_solicitudes.index') }}" class="btn btn-default">
                                        <i class="fa fa-btn fa-angle-double-left"></i>Volver
                                    </a>
                                    <button type="submit" class="btn btn-warning"><i class="fa fa-save"></i> Registrar cambios</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        //$('li#nav_config').addClass('menu-open');
        //$('li#nav_config').children().addClass('active');
        //$('a#nav_prioridades').addClass('active');
    });
</script>

@endsection