<div class="col-md-8 offset-md-3">
    <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}" id="wrapper-employee_id">
        {!! Form::label('employee_id', '* Empleado', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::select('employee_id', /* [] */$empleados, null, ['class' => 'form-control select2', 'placeholder' => 'Seleccione un empleado', '']) !!}
    
            <span class="help-block">
                <strong>{{ $errors->first('employee_id') }}</strong>
            </span>
    
        </div>
    </div>
    
    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}" id="wrapper-username">
        {!! Form::label('username', '* Nombre de Usuario', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('username', null, ['class' => 'form-control']) !!}
    
            <span class="help-block">
                <strong>{{ $errors->first('username') }}</strong>
            </span>
    
        </div>
    </div>
    
    <div class="form-group{{ $errors->has('role_id') ? ' has-error' : '' }}" id="wrapper-role_id">
        {!! Form::label('role_id', '* Rol', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::select('role_id', $roles, null, ['class' => 'form-control select2', 'placeholder' => 'Seleccione un rol']) !!}
    
            <span class="help-block">
                <strong>{{ $errors->first('role_id') }}</strong>
            </span>
    
        </div>
    </div>

    <div id="offices" hidden>
    <div class="form-group{{ $errors->has('rrhh_office_id') ? ' has-error' : '' }} wrapper-rrhh_office_id " id="rrhh_office_id">
        {!! Form::label('rrhh_office_id', 'Oficina del Usuario', ['class' => 'control-label col-md-4 col-sm-4 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::select('rrhh_office_id', $offices, null, ['class' => 'form-control select2', 'placeholder' => 'Seleccione una oficina']) !!}
            <span class="help-block">
                <strong>{{ $errors->first('rrhh_office_id') }}</strong>
            </span>
        </div>
    </div>
    </div>
    <div id="doctor_staff" hidden>
        <div class="form-group{{ $errors->has('doctor_staff_id') ? ' has-error' : '' }} wrapper-doctor_staff_id " id="doctor_staff_id">
            {!! Form::label('doctor_staff_id', 'Vincular con Doctor', ['class' => 'control-label col-md-4 col-sm-4 col-xs-12']) !!}
            <div class="col-md-9 col-sm-9 col-xs-12">
                {!! Form::select('doctor_staff_id[]', [], isset($user) ? $user->medical_users()->pluck('doctor_id')->toArray():null, ['class' => 'form-control select2', 'multiple'=>'multiple']) !!}
                <span class="help-block">
                    <strong>{{ $errors->first('doctor_staff_id') }}</strong>
                </span>
            </div>
        </div>
    </div>
    <div id="nurse_staff" hidden>
        <div class="form-group{{ $errors->has('nurse_staff_id') ? ' has-error' : '' }} wrapper-nurse_staff_id " id="nurse_staff_id">
            {!! Form::label('nurse_staff_id', 'Vincular con Enfermera', ['class' => 'control-label col-md-4 col-sm-4 col-xs-12']) !!}
            <div class="col-md-9 col-sm-9 col-xs-12">
                {!! Form::select('nurse_staff_id[]', [], isset($user) ? $user->user_medical()->pluck('nurse_id')->toArray():null, ['class' => 'form-control select2', 'multiple'=>'multiple']) !!}
                <span class="help-block">
                    <strong>{{ $errors->first('nurse_staff_id') }}</strong>
                </span>
            </div>
        </div>
    </div>
    
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}" id="wrapper-password">
        {!! Form::label('password', '* Contraseña', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::password('password', ['class' => 'form-control']) !!}
    
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
    
        </div>
    </div>
    
    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}" id="wrapper-password_confirmation">
        {!! Form::label('password_confirmation', '* Confirmar contraseña', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
    
            <span class="help-block">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
    
        </div>
    </div>
</div>