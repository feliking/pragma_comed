<div class="col-md-8 offset-md-3">
    {!! Form::hidden('name', $usuario->name) !!}
    {!! Form::hidden('email', $usuario->email) !!}
    {!! Form::hidden('rol_id', $usuario->role_id) !!}
    {!! Form::hidden('rrhh_empleado_id', $usuario->rrhh_empleado_id) !!}
    <div class="form-group{{ $errors->has('password_actual') ? ' has-error' : '' }}" id="wrapper-password_actual">
        {!! Form::label('password_actual', 'Contraseña actual', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::password('password_actual', ['class' => 'form-control']) !!}
    
            <span class="help-block">
                <strong>{{ $errors->first('password_actual') }}</strong>
            </span>
    
        </div>
    </div>
    
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}" id="wrapper-password">
        {!! Form::label('password', 'Contraseña nueva', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::password('password', ['class' => 'form-control']) !!}
    
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
    
        </div>
    </div>
    
    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}" id="wrapper-password_confirmation">
        {!! Form::label('password_confirmation', 'Confirmar contraseña nueva', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
    
            <span class="help-block">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
    
        </div>
    </div>
</div>