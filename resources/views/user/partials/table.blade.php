<table id="example2" class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Op.</th>
        <th>Nombre de Usuario</th>
        <th>Email</th>
        <th>Paterno</th>
        <th>Materno</th>
        <th>Nombres</th>
        <th>Rol</th>
        <th>Estado</th>
        
    </tr>
    </thead>

    <tbody>
    @foreach($users as $usuario)
        <tr>
            <td>
                <div class="btn-group" role="group">
                    <a href="{{ route('usuario.edit', $usuario->id) }}" class="btn btn-sm btn-default" title="Editar">
                        <i class="fa fa-edit"></i>
                        <span class="sr-only">Editar</span>
                    </a>
                    <a href="{{ route('usuario.show', $usuario->id) }}" class="btn btn-sm btn-default" title="{{($usuario->deleted_at == null)?'Desactivar':'Activar'}}">
                        <i class="fa fa-{{($usuario->deleted_at == null)?'times':'check'}}"></i>
                        <span class="sr-only">{{($usuario->deleted_at == null)?'Desactivar':'Activar'}}</span>
                    </a>
                </div>
            </td>
            <td>{{ $usuario->username }}</td>
            <td>{{ $usuario->email }}</td>
            <td>
                @if($usuario->employee != null)
                {{ $usuario->employee->apellido_1 }}
                @endif
            </td>
            <td>
                @if($usuario->employee != null)
                {{ $usuario->employee->apellido_2 }}
                @endif
            </td>
            <td>
                @if($usuario->employee != null)
                {{ $usuario->employee->nombres }}
                @endif
            </td>
            <td>
                @if ($usuario->role->id == 1)
                    <span class="label label-primary"> {{$usuario->role->name}} </span>
                @else
                    <span class="label label-success">{{$usuario->role->name}}</span>
                @endif
            </td>
            <td>
                @if($usuario->deleted_at == null)
                <span class="badge badge-success">Activo</span>
                @elseif($usuario->deleted_at != null)
                <span class="badge badge-danger">Inactivo</span>
                @endif
            </td>
            
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Op.</th>
            <th>Nombre de Usuario</th>
            <th>Email</th>
            <th>Paterno</th>
            <th>Materno</th>
            <th>Nombres</th>
            <th>Rol</th>
            <th>Estado</th>
        </tr>
    </tfoot>
</table>
