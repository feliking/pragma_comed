@extends('layouts.app_menu')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edición de Criterios de Enfermedad</h1>
                </div>

            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            {!! Form::model($disease_criteria, ['route' => ['criterios_enfermedad.update', $disease_criteria->id], 'method' => 'put', 'id' => 'frm-diseas_id']) !!}
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Editar Criterio de Enfermedad</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @include('disease_criteria.partials.form')
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <div class="form-group">
                                <div class="col-xs-12 text-right">
                                    <a href="{{ route('criterios_enfermedad.index') }}" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Volver</a>
                                    <button type="submit" class="btn btn-warning"><i class="fa fa-check"></i> Registrar cambios</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script src="{{ asset('/js/disease_criteria/general.js') }}"></script>
    <script>
        $('#weighing').inputmask({alias: 'numeric', allowMinus: false, digits: 2, max: 999.99});
        $(document).ready(function() {
            $('a#nav_disease').addClass('active');
        });
        $('.select2').select2({
            theme: 'bootstrap4',
            placeholder: 'SELECCIONE UNA OPCIÓN'
        });
    </script>
@endsection
