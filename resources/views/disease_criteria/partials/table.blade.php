<table id="example2" class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Op.</th>
        <th>Síntoma</th>
        <th>Peso</th>
        <th>Tipo de Riesgo</th>
        <th>Estado</th>
    </tr>
    </thead>

    <tbody>
    @foreach($disease_criterias as $disease_criteria)
        <tr>
            <td>
                <div class="btn-group" role="group">
                    <a href="{{ route('criterios_enfermedad.edit', $disease_criteria->id) }}" class="btn btn-sm btn-default" title="Editar"
                        style="{{ ($disease_criteria->system_flag)? 'pointer-events: none; cursor: default;' : ''}}">
                        <i class="fa fa-edit"></i>
                        <span class="sr-only">Editar</span>
                    </a>
                    <a href="{{ route('criterios_enfermedad.show', $disease_criteria->id) }}" class="btn btn-sm btn-default" title="{{($disease_criteria->deleted_at == null)?'Desactivar':'Activar'}}">
                        <i class="fa fa-{{($disease_criteria->deleted_at == null)?'times':'check'}}"></i>
                        <span class="sr-only">{{($disease_criteria->deleted_at == null)?'Desactivar':'Activar'}}</span>
                    </a>
                </div>
            </td>
            <td>
                {{$disease_criteria->syntom}}
            </td>
            <td>
                {{$disease_criteria->weighing}}
            </td>
            <td>
                {{$disease_criteria->risk_type->name}}
            </td>
            <td>
                @if($disease_criteria->deleted_at == null)
                <span class="badge badge-success">Activo</span>
                @elseif($disease_criteria->deleted_at != null)
                <span class="badge badge-danger">Inactivo</span>
                @endif
            </td>
            
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Op.</th>
            <th>Síntoma</th>
            <th>Peso</th>
            <th>Tipo de Riesgo</th>
            <th>Estado</th>
        </tr>
    </tfoot>
</table>
