<div class="col-md-8 offset-md-3">
    <div class="form-group{{ $errors->has('syntom') ? ' has-error' : '' }} wrapper-syntom">
        {!! Form::label('syntom', '* Síntoma', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('syntom', null, ['class' => 'form-control'.( $errors->has('syntom') ? ' is-invalid' : '' )]) !!}
    
            <span class="help-block">
                <strong>{{ $errors->first('syntom') }}</strong>
            </span>
    
        </div>
    </div>
    
    <div class="form-group{{ $errors->has('weighing') ? ' has-error' : '' }} wrapper-weighing">
        {!! Form::label('weighing', '* Peso', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('weighing', null, ['class' => 'form-control'.( $errors->has('weighing') ? ' is-invalid' : '' )]) !!}
    
            <span class="help-block">
                <strong>{{ $errors->first('weighing') }}</strong>
            </span>
    
        </div>
    </div>
    <div class="form-group{{ $errors->has('risk_type_id') ? ' has-error' : '' }} wrapper-risk_type_id " id="risk_type_id">
        {!! Form::label('risk_type_id', '* Tipo de Riesgo', ['class' => 'control-label col-md-4 col-sm-4 col-xs-12']) !!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::select('risk_type_id', $risk_types, null, ['class' => 'form-control select2'.( $errors->has('risk_type_id') ? ' is-invalid' : '' ), 'placeholder' => 'Seleccione una criterio']) !!}
            <span class="help-block">
                <strong>{{ $errors->first('risk_type_id') }}</strong>
            </span>
        </div>
    </div>
</div>