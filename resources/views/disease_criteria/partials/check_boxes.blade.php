@if(count($disaeses) > 0)
    <div class="row">
        @foreach($disaeses as $disaese)
        <div class="form-group col-md-3 col-sm-6">
            <div class="icheck-primary">
                <input name="disease_criteria[]" class="form-check-input" type="checkbox" value="{{$disaese->id}}" data-weighing="{{$disaese->weighing}}" id="disaese_{{$disaese->id}}" onclick="disaeseChange(this, {{$disaese->id}})">
                <label for="disaese_{{$disaese->id}}" style="font-weight: lighter;">
                    {{ $disaese->syntom }} ({{$disaese->weighing}})
                </label>
            </div>
        </div>
        @endforeach
    </div>
    <div class="form-row">
        <p><b>Ponderación:</b> <span id="span_puntaje">0</span> <span id="span_weigthing" class="badge"></span> <span id="span_edad" class="badge badge-info"></span> <span id="span_risk_type" class="badge badge-info"></span></p>
    </div>

    <script>
        var obj_weighting_levels = @json($weighting_levels);
    </script>
@else
    <div class="alert alert-info" role="alert">
        <h4 class="alert-heading"><i class="fa fa-info"></i>Sin Datos</h4>
        <p>No existen datos de criterios ligados al riesgo seleccionado.</p>
    </div>
@endif
