@extends('layouts.app_menu')

@section('style')
    <style>
        input[type="text"], textarea {
            text-transform: uppercase;
        }

        .append-ci
        {
            width: 120px;
        }
        .form-group
        {
            margin-bottom: 0px;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Registro de consulta médica</h1>
                </div>

            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            {!! Form::open(['route' => 'solicitudes_medicas.store', 'method' => 'post', 'id' => 'frm-reqsol_id', 'enctype' => 'multipart/form-data']) !!}
            <div class="row">
                <div class="col-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">DATOS DEL PACIENTE</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @include('medical_request.partials.form_call_center')
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <div class="form-group">
                                <div class="col-xs-12 text-right">
                                    <a href="{{ route('administracion_seguimientos.index') }}" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Volver</a>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Registrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            {!! Form::close() !!}
        </div>
    </section>

    <!-- MODALS -->
    @include('complementary_exams.modals.modal_complementary_exam')
@endsection

@section('script')
<script src="{{ asset('/js/medical_request_call_center/general.js') }}"></script>
<script>
    var url_search_by_nurse = '{{ route('enfermera.search_by_nurse') }}';

    $(document).ready(function() {
        $('a#nav_administracion_seguimientos').addClass('active');
    });
</script>
@endsection
