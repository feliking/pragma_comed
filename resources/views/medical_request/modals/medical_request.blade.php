<!-- Modal -->
<div class="modal fade" id="modal_medical_request" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    El empleado tiene una solicitud registrada
                </h5>
            </div>
            <div class="modal-body">

                @include('medical_request.partials.table_medical_request')

                @if (Auth::user() && \Illuminate\Support\Facades\Auth::user()->role_id!=7)
                <!-- MÉDICOS -->
                <label class="col-md-12">Médico que atenderá el caso</label>

                <div class="form-group col-md-12">
                    <div class="input-group mb-3">
                        <select name="user_id" value="{{ old('user_id') }}" class="form-control @error('user_id') is-invalid @enderror">
                            <option></option>
                            @foreach($medical_users as $medical_user)
                                <option value="{{ $medical_user->id }}" {{ old('user_id')?'selected':'' }} {{ (Auth::user() != null && $medical_user->id == Auth::user()->id) ? 'selected':'' }}>
                                    {{ $medical_user->name }}
                                </option>
                            @endforeach
                        </select>

                        @error('user_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
