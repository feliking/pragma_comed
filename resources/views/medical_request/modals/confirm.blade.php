<!-- Modal -->
<div class="modal fade" id="modal_confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    ¿Está seguro de enviar la siguiente información?
                </h5>
            </div>
            <div class="modal-body">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <th style="width: 25%;">Nombre completo</th>
                            <th style="width: 25%;">Cédula de identidad</th>
                            <th style="width: 25%;">Género</th>
                            <th style="width: 25%;">Fecha de nacimiento</th>
                        </tr>
                        <tr>
                            <td id="table-full_name"></td>
                            <td id="table-full_ci"></td>
                            <td id="table-gender"></td>
                            <td id="table-birthday"></td>
                        </tr>
                        <tr>
                            <th>Empresa</th>
                            <th>Código de empleado</th>
                            <th colspan="2">Seguro médico</th>
                        </tr>
                        <tr>
                            <td id="table-enterprise"></td>
                            <td id="table-employee_code"></td>
                            <td id="table-health_insurance" colspan="2"></td>
                        </tr>
                        <tr>
                            <th>Número de teléfono</th>
                            <th>¿Tiene whatsapp?</th>
                            <th colspan="2">Correo eletrónico</th>
                        </tr>
                        <tr>
                            <td id="table-phone_number"></td>
                            <td id="table-whatsapp_flag"></td>
                            <td id="table-email" colspan="2"></td>
                        </tr>
                        <tr>
                            <th>Número corporativo</th>
                            <th>Contacto de referencia</th>
                            <th colspan="2">Número de referencia</th>
                        </tr>
                        <tr>
                            <td id="table-short_phone_number"></td>
                            <td id="table-ref_name"></td>
                            <td id="table-ref_phone_number" colspan="2"></td>
                        </tr>
                        <tr>
                            <th>Motivo de solicitud</th>
                            <th colspan="3">Comentario u Observación</th>
                        </tr>
                        <tr>
                            <td id="table-medical_request_type"></td>
                            <td id="table-comment" colspan="3"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No, revisaré mis datos</button>
                <button type="button" class="btn btn-primary" onclick="$('form#form-request').unbind().submit();">Si, he verificado la información</button>
            </div>
        </div>
    </div>
</div>
