<!-- Modal -->
<div class="modal fade" id="modal_appointment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    El empleado tiene una cita programada
                </h5>
            </div>
            <div class="modal-body">

                @include('medical_request.partials.table_medical_request')

                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <th style="width: 50%;">Fecha de la cita</th>
                            <th style="width: 50%;">Tipo de atención</th>
                        </tr>
                        <tr>
                            <td id="table-next_attention_date"></td>
                            <td id="table-appointment_type"></td>
                        </tr>
                        <tr>
                            <th colspan="2">Observación</th>
                        </tr>
                        <tr>
                            <td id="table-comment"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
