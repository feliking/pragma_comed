@extends('layouts.app_menu')

@section('styles')
    <style>
        .dataTables_filter {
            display: none !important;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Importar datos iniciales</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title"></h3>
                            <div class="card-tools">
                                <p id="demo"></p>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            {{--<div class="mailbox-controls">
                                <!-- Check all button -->
                                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="far fa-square"></i>
                                </button>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                    <button id="btnAssingEmployee" type="button" data-toggle="modal" data-target="#modal_employee_assign" class="btn btn-default btn-sm disable_if_no_check">
                                        <i class="fas fa-share"></i>
                                    </button>
                                    <button id="btnAssingMedic" type="button" data-toggle="modal" data-target="#modal_medic_assign" class="btn btn-default btn-sm disable_if_no_check">
                                        <i class="fas fa-file-medical"></i>
                                    </button>
                                </div>
                                <!-- /.btn-group -->
                                <button type="button" class="btn btn-default btn-sm">
                                    <i class="fas fa-sync-alt"></i>
                                </button>
                            </div>--}}
                            <form action="{{ route('importar_datos.update') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                            <div class="form-group">
                                {!! Form::label('file', 'Archivo', ['class' => 'control-label col-md-12 col-sm-12 col-xs-12']) !!}
                                <div class="input-group">
                                    <div class="custom-file">
                                        {!! Form::file('file', ['class' => 'control-label col-md-12 col-sm-12 col-xs-12']) !!}
                                        {!! Form::label('file', 'Escoge un archivo', ['class' => 'custom-file-label']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="social-auth-links text-center">
                                <button type="submit" class="btn btn-block btn-primary">
                                    <i class="fa fa-file-excel mr-2"></i>
                                    Subir Archivo
                                </button>
                            </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
        </div>
    </section>

    <!--MODALS-->

@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('a#nav_import').addClass('active');
        });
        /*RUTAS DEL SISTEMA*/
        $(function () {
            bsCustomFileInput.init();
        });

    </script>
    <script src="{{ asset('js/complementary_exam/modal_complementary_exam.js') }}"></script>
    <!-- bs-custom-file-input -->
    <script src="{{ asset('adminlte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
@endsection
