{{-- <div class="content-wrapper"> --}}
    <section class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                <h1>Historial de Consulta Médica</h1>
            </div>
        </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            @include('medical_consultations.partials.time_line_employee')
        </div>
        <!-- /.timeline -->
    </section>
{{-- </div> --}}
