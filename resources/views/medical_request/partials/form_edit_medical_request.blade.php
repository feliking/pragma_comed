<div class="row">
    <label class="col-md-12">¿Quién será atendido?</label>
    <!-- NOMBRE DE PERSONA -->
    <div class="form-group col-md-6 col-sm-12">
        <div class="input-group mb-3">
            <input name="full_name" type="text" value="{{ old('full_name') }}" class="form-control @error('full_name') is-invalid @enderror" placeholder="{{ __('* Nombre Completo') }}"/>
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-user"></span>
                </div>
            </div>
            @error('full_name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <!-- CARNET -->
    <div class="form-group col-md-6 col-sm-12">
        <div class="input-group mb-3">
            <input name="ci" type="text" value="{{ old('ci') }}" class="form-control @if ($errors->has('ci') || $errors->has('ci_expedition')) is-invalid @endif" placeholder="{{ __('* Cédula') }}" data-inputmask='"mask": "999999[9][9][-9A]"' data-greedy="false" data-mask/>
            <div class="input-group-append append-ci">
                <select name="ci_expedition" class="select2">
                    <option value="LP">LP</option>
                    <option value="CB">CB</option>
                    <option value="SC">SC</option>
                    <option value="CH">CH</option>
                    <option value="OR">OR</option>
                    <option value="PT">PT</option>
                    <option value="TJ">TJ</option>
                    <option value="BE">BE</option>
                    <option value="PD">PD</option>
                </select>
                @yield('search_by_ci')
            </div>

            @if ($errors->has('ci') || $errors->has('ci_expedition'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('ci') ? : $errors->first('ci_expedition') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <!-- GÉNERO -->
    <div class="form-group col-md-6 col-sm-12">
        <div class="input-group mb-3">
            <select name="gender" value="{{ old('gender') }}" class="form-control @error('gender') is-invalid @enderror select2">
                <option></option>
                <option value="Femenino">Femenino</option>
                <option value="Masculino">Masculino</option>
            </select>

            <div class="input-group-append">
                <div class="input-group-text">
                    <i class="fas fa-restroom"></i>
                </div>
            </div>

            @error('gender')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <!-- FECHA DE NACIMIENTO -->
    <div class="form-group col-md-6 col sm 12">
        <div class="input-group mb-3 date" id="birthday_field" data-target-input="nearest">
            <input type="text" id="birthday" name="birthday" class="form-control @error('birthday') is-invalid @enderror datetimepicker-input" data-target="#birthday_field" placeholder="* Fecha de nacimiento" data-inputmask='"mask": "99/99/9999"' data-greedy="false" data-mask/>
            <div class="input-group-append" data-target="#birthday_field" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="fas fa-birthday-cake"></i></div>
            </div>

            @error('birthday')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>

<div class="row">
    <label class="col-md-12">¿A que empresa pertenece?</label>

    <!-- EMPRESA -->
    <div class="form-group col-md-6 col-sm-12">
        <div class="input-group mb-3">
            <input name="enterprise" type="text" value="{{ old('enterprise') }}" class="form-control @error('enterprise') is-invalid @enderror" placeholder="{{ __('* Empresa') }}">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-building"></span>
                </div>
            </div>

            @error('enterprise')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <!-- CODIGO DE EMPLEADO -->
    <div class="form-group col-md-6 col-sm-12">
        <div class="input-group mb-3">
            <input name="employee_code" type="text" value="{{ old('employee_code') }}" class="form-control @error('employee_code') is-invalid @enderror" placeholder="{{ __('Código de Empleado') }}">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-barcode"></span>
                </div>
            </div>

            @error('employee_code')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <!-- SEGURO SOCIAL -->
    <div class="form-group col-md-6 col-sm-12">
        <div class="input-group mb-3">
            <select name="health_insurance_id" value="{{ old('health_insurance_id') }}" class="form-control @error('health_insurance_id') is-invalid @enderror select2">
                <option></option>
                @foreach($health_insurances as $health_insurance)
                    <option value="{{ $health_insurance->id }}" {{ old('health_insurance_id')?'selected':'' }}>
                        {{ $health_insurance->nombre }}
                    </option>
                @endforeach
            </select>

            <div class="input-group-append">
                <div class="input-group-text">
                    <i class="fas fa-clinic-medical"></i>
                </div>
            </div>

            @error('health_insurance_id')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>

<div class="row">
    <label class="col-md-12">¿Cómo nos contactamos con usted?</label>

    <!-- TELEFONO DEL EMPLEADO -->
    <div class="form-group col-md-6 col-sm-12">
        <div class="input-group mb-3">
            <input name="phone_number" type="text" value="{{ old('phone_number') }}" class="form-control @error('phone_number') is-invalid @enderror" placeholder="{{ __('* Número de Teléfono') }}" data-inputmask='"mask": "9999[9999]"' data-mask>
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-phone"></span>
                </div>
            </div>

            @error('phone_number')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>


    <!-- WHATSAPP -->
    <div class="form-group col-md-6 col-sm-12">
        <div class="input-group mb-3">
            <select name="whatsapp_flag" value="{{ old('whatsapp_flag') }}" class="form-control @error('whatsapp_flag') is-invalid @enderror select2">
                <option></option>
                <option value="1">Con whatsapp (mi número tiene esta aplicación)</option>
                <option value="0">Sin whatsapp (mi número no tiene esta aplicación)</option>
            </select>

            <div class="input-group-append">
                <div class="input-group-text">
                    <i class="fab fa-whatsapp"></i>
                </div>
            </div>

            @error('whatsapp_flag')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="form-group col-md-6 col-sm-12">
        <div class="input-group mb-3">
            <input name="short_phone_number" type="text" value="{{ old('short_phone_number') }}" class="form-control @error('short_phone_number') is-invalid @enderror" placeholder="{{ __('Número Corporativo (Corto)') }}" data-inputmask='"mask": "[9999]"' data-mask>
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-mobile-alt"></span>
                </div>
            </div>

            @error('short_phone_number')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <!-- CORREO ELECTRÓNICO -->
    <div class="form-group col-md-6 col-sm-12">
        <div class="input-group mb-3">
            <input name="email" type="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" placeholder="{{ __('Correo electrónico') }}">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                </div>
            </div>

            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="form-group col-md-6 col-sm-12">
        <div class="input-group mb-3">
            <input name="reference_full_name" type="text" value="{{ old('reference_full_name') }}" class="form-control @error('reference_full_name') /is-invalid @enderror" placeholder="{{ __('Nombre del contacto de referencia') }}" {{-- data-inputmask='"mask": "aaaaaaaaaa"' data-mask --}}/>
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-user"></span>
                </div>
            </div>
            @error('reference_full_name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="form-group col-md-6 col-sm-12">
        <div class="input-group mb-3">
            <input name="reference_phone_number" type="text" value="{{ old('reference_phone_number') }}" class="form-control @error('reference_phone_number') is-invalid @enderror" placeholder="{{ __('Número del contaco de referencia') }}" data-inputmask='"mask": "[99999999]"' data-mask>
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-phone-alt"></span>
                </div>
            </div>

            @error('reference_phone_number')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>

<div class="row">
    <label class="col-md-12">Motivo de solicitud</label>

    <!-- TIPO DE CONSULTA -->

    <div class="form-group col-md-12">
        <div class="input-group mb-3">
            <select name="risk_type_id" value="{{ old('risk_type_id') }}" class="form-control @error('risk_type_id') is-invalid @enderror">
                <option></option>
                @foreach($risk_types as $risk_type)
                    <option value="{{ $risk_type->id }}" {{ old('risk_type_id')?'selected':'' }}>
                        {{ $risk_type->name }}
                    </option>
                @endforeach
            </select>

            @error('risk_type_id')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <!-- OBSERVACIÓN O COMENTARIO -->
    <div class="form-group col-md-12">
        <div class="input-group mb-3">
            <textarea name="comment" rows="3" value="{{ old('comment') }}" class="form-control @error('comment') is-invalid @enderror" placeholder="{{ __('Comentario u Observación') }}"></textarea>

            @error('comment')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>