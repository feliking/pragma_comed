<div class="form-row">
    <label for="asigna">¿Programar cita médica? &nbsp;</label>
    <input type="checkbox" name="asigna" id="asigna" value="1">
</div>
<div id="dates" hidden>
    <div class="form-row">
        <div class="form-group col-md-6 col-sm-12{{ $errors->has('user_id') ? ' has-error' : '' }} wrapper-user_id " id="user_id">
            {!! Form::label('user_id', '* Médico que atenderá el caso', ['class' => 'control-label col-md-12 col-sm-4 col-xs-12 ']) !!}
            <div class="col-md-12 col-sm-9 col-xs-12 input-group mb-3">
                {!! Form::select('user_id', $medical_users, null, ['class' => 'form-control  select2', 'placeholder' => 'Seleccione un médico', 'required'=>'required']) !!}
                @error('user_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        {{-- @include('layouts.components.modals.partials.form_appointment') --}}
        <div class="form-group col-md-6 col-sm-12{{ $errors->has('next_attention_date') ? ' has-error' : '' }} wrapper-next_attention_date">
            <label>{{ __('*Fecha de proxima atención:') }}</label>
            <div class="col-md-12 col-sm-9 col-xs-12 input-group mb-3">
                <input name="next_attention_date" type="text" class="form-control @if ($errors->has('username') || $errors->has('email')) is-invalid @endif"
                        value="@if(isset($appointment)){{$appointment->next_attention_date->format('d/m/Y H:i A')}}@else{{ old('next_attention_date') ? : old('next_attention_date') }}@endif"
                        placeholder="{{ __('Introduzca la fecha para la proxima atención') }}" required>
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                </div>

                <span class="invalid-feedback" role="alert">
                    @error('next_attention_date')
                        <strong>{{ $message }}</strong>
                    @enderror
                </span>
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6 col-sm-12 {{ $errors->has('appointment_type_id') ? ' has-error' : '' }} wrapper-appointment_type_id">
            <label>{{ __('*Tipo de atención:') }}</label>
            <div class="col-md-12 col-sm-9 col-xs-12 input-group mb-3">
                <select name="appointment_type_id" value="" class="form-control @error('appointment_type_id') is-invalid @enderror select2" required>
                    <option></option>
                    @foreach($appointment_types as $appointment_type)
                        <option
                            value="{{ $appointment_type->id }}" @if(isset($appointment)){{ ($appointment->appointment_type_id==$appointment_type->id)?'selected':'' }}@else{{ (old('appointment_type_id') == $appointment_type->id)?'selected':'' }}@endif>
                            {{ $appointment_type->name }}
                        </option>
                    @endforeach
                </select>

                <span class="invalid-feedback" role="alert">
                @error('appointment_type_id')
                    <strong>{{ $message }}</strong>
                @enderror
                </span>
            </div>
        </div>
        <div id="div_zoom" class="form-group col-md-6 col-sm-12 variable_text {{ $errors->has('variable_zoom') ? ' has-error' : '' }} wrapper-variable_zoom" @if(isset($appointment)){{ ($appointment->appointment_type_id==2)?'':'hidden' }}@else hidden @endif>
            <label>{{ __('*Ingrese la dirección de la reunión Zoom:') }}</label>
            <div class="col-md-12 col-sm-9 col-xs-12 input-group mb-3">
                <input name="variable_zoom" type="text"
                       value="@if(isset($appointment)){{ ($appointment->appointment_type_id==2)?$appointment->variable_text:'' }}@else{{ old('variable_zoom') }}@endif"
                       class="form-control @error('variable_zoom') is-invalid @enderror" placeholder="{{ __('INGRESE LA URL') }}">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-arrow-alt-circle-right"></span>
                    </div>
                </div>

                <span class="invalid-feedback" role="alert">
                @error('variable_zoom')
                    <strong>{{ $message }}</strong>
                @enderror
                </span>
            </div>
        </div>

        <!-- WHATSAPP -->
        <div id="div_whatsapp" class="form-group col-md-6 col-sm-12 variable_text {{ $errors->has('variable_whatsapp') ? ' has-error' : '' }} wrapper-variable_whatsapp" @if(isset($appointment)){{ ($appointment->appointment_type_id==3)?'':'hidden' }}@else hidden @endif>
            <label>{{ __('*Ingrese su número de WhatsApp') }}</label>
            <div class="col-md-12 col-sm-9 col-xs-12 input-group mb-3">
                <input name="variable_whatsapp" type="text"
                       value="@if(isset($appointment)){{ ($appointment->appointment_type_id==3)?$appointment->variable_text:'' }}@else{{ old('variable_whatsapp') }}@endif"
                       class="form-control @error('variable_whatsapp') is-invalid @enderror" placeholder="{{ __('* Número de Teléfono') }}"
                       data-inputmask='"mask": "9999[9999]"' data-mask>
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fab fa-whatsapp"></span>
                    </div>
                </div>

                <span class="invalid-feedback" role="alert">
                @error('variable_whatsapp')
                    <strong>{{ $message }}</strong>
                @enderror
                </span>
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <label>{{ __('Comentario u Observación') }}</label>
            <div class="input-group mb-3">
                <textarea name="comment" rows="2" class="form-control @error('comment') is-invalid @enderror" placeholder="{{ __('Comentario u Observación') }}">@if(isset($appointment)){{ $appointment->comment }}@else{{ old('comment') }}@endif</textarea>

                <span class="invalid-feedback" role="alert">
                @error('comment')
                    <strong>{{ $message }}</strong>
                @enderror
                </span>
            </div>
        </div>
    </div>
</div>
