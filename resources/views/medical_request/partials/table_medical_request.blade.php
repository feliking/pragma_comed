<table class="table table-striped">
    <tbody>
        <tr>
            <th style="width: 25%;">Nombre completo</th>
            <th style="width: 25%;">Cédula de identidad</th>
            <th style="width: 25%;">Género</th>
            <th style="width: 25%;">Fecha de nacimiento</th>
        </tr>
        <tr>
            <td id="med_req-full_name"></td>
            <td id="med_req-full_ci"></td>
            <td id="med_req-gender"></td>
            <td id="med_req-birthday"></td>
        </tr>
        <tr>
            <th>Empresa</th>
            <th>Código de empleado</th>
            <th colspan="2">Seguro médico</th>
        </tr>
        <tr>
            <td id="med_req-enterprise"></td>
            <td id="med_req-employee_code"></td>
            <td id="med_req-health_insurance" colspan="2"></td>
        </tr>
        <tr>
            <th>Número de teléfono</th>
            <th>¿Tiene whatsapp?</th>
            <th colspan="2">Correo eletrónico</th>
        </tr>
        <tr>
            <td id="med_req-phone_number"></td>
            <td id="med_req-whatsapp_flag"></td>
            <td id="med_req-email" colspan="2"></td>
        </tr>
        <tr>
            <th>Motivo de solicitud</th>
            <th colspan="3">Comentario u Observación</th>
        </tr>
        <tr>
            <td id="med_req-medical_request_type"></td>
            <td id="med_req-comment" colspan="3"></td>
        </tr>
    </tbody>
</table>
