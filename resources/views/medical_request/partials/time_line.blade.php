<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-12">
            <h1>Historial de Consulta Médica</h1>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">

    <!-- Timelime example  -->
    <div class="row">
        <div class="col-md-12">
        <!-- The time line -->
            @include('medical_consultations.partials.time_line')

        </div>
        <!-- /.col -->
    </div>
    </div>
    <!-- /.timeline -->
</section>
