@extends('layouts.app_simple')

@section('title', 'Registro')

@section('style')
<style>
    .register-box
    {
        /* width: 900px; */
        width: 90%;
    }
    .append-ci
    {
        width: 70px;
    }
    .form-group
    {
        margin-bottom: 0px;
    }
    #modal_confirm .table
    {
        font-size: 12px;
    }
    #modal_medical_request .table
    {
        font-size: 12px;
    }
</style>
@endsection

@section('content')

<div class="register-box">
    <div class="login-logo">
        <a href="#">
            <b>F</b>ormulario de <b>S</b>olicitud para atención <b>M</b>édica
        </a>
    </div>

    <div class="card bg-gradient-primary border-primary col-md-8 offset-md-2 col-sm-12">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Los campos marcados con asterisco (*) son obligatorios.</p>

            <form id="form-request" method="post" action="{{ route('solicitudes_medicas.store') }}" autocomplete="off">
                <!-- CROSS Site Request Forgery Protection -->
                @csrf

                @include('medical_request.partials.form')

                <div class="social-auth-links text-center">
                    <button type="submit" class="btn btn-block btn-primary">
                        <i class="fas fa-hospital-user mr-2"></i>
                        Registrar Solicitud
                    </button>
                </div>
            </form>

            <div class="row">
                <a href="{{ route('busca.solicitudes_medicas') }}" class="col-md-6 col-sm-12">
                    <i class="fas fa-barcode"></i>
                    Ya tengo un codigo de atención médica
                </a>
                <a href="/login" class="col-md-6 col-sm-12 text-right">
                    <i class="fas fa-user-md"></i>
                    Personal de Salud autorizado
                </a>
            </div>
        </div>
        <!-- /.form-box -->
    </div><!-- /.card -->

</div>

    <!-- Modals -->
    @include('medical_request.modals.confirm')
    @include('medical_request.modals.appointment')
    @include('medical_request.modals.medical_request')

@endsection

@section('script')
    @parent
    <script>
        @if(Auth::user()!=null && Auth::user()->role_id==3)
            $('select[name^=user_id] option:not(:selected)').attr('disabled', true);
            $('select[name^=user_id] option:selected').attr('selected', 'selected');
        @endif
        //Initialize Select2 Elements
        $('select[name="ci_expedition"]').select2({
            theme: 'bootstrap4'
        });

        $('select[name="whatsapp_flag"]').select2({
            theme: 'bootstrap4',
            placeholder: '* ¿Su número cuenta con Whatsapp?'
        });

        $('select[name="gender"]').select2({
            theme: 'bootstrap4',
            placeholder: '* Seleccione su género'
        });

        $('select[name="user_id"]').select2({
            theme: 'bootstrap4'
        });

        $('select[name="risk_type_id"]').select2({
            theme: 'bootstrap4',
            placeholder: '* Seleccione el motivo de su consulta'
        });

        $('select[name="health_insurance_id"]').select2({
            theme: 'bootstrap4',
            placeholder: '* Seleccione su seguro médico'
        });

        @if (Auth::user())
        // Buscar Solicitud
        function search_medical_request ()
        {
            $.ajax({
                url: '{{ route('medical_request.search') }}',
                headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                method: 'POST',
                dataType: 'JSON',
                data: 'ci=' + $('input[name="ci"]').val() + '&ci_expedition=' + $('select[name="ci_expedition"]').val(),
                beforeSend: function(e){

                }
            }).done(function (response){
                if (response['medical_request'])
                {
                    if (response.medical_request.medical_request_status_id == 4)
                    {
                        $('input[name="full_name"]').val(response.medical_request.full_name);
                        $('select[name="gender"]').val(response.medical_request.gender).trigger('change');
                        var birthday = moment(response.medical_request.birthday);
                        $('input[name="birthday"]').val(birthday.format('DD/MM/YYYY'));
                        $('input[name="enterprise"]').val(response.medical_request.enterprise);
                        $('input[name="employee_code"]').val(response.medical_request.employee_code);
                        $('select[name="health_insurance_id"]').val(response.medical_request.health_insurance_id).trigger('change');
                        $('input[name="phone_number"]').val(response.medical_request.phone_number);
                        $('select[name="whatsapp_flag"]').val(response.medical_request.whatsapp_flag).trigger('change');
                        $('input[name="email"]').val(response.medical_request.email);

                        toastr.success(
                            '', 'Persona encontrada, los datos se copiaron automáticamente.'
                        );
                    }
                    else if (response.medical_request.medical_request_status_id == 2)
                    {
                        var birthday = moment(response.medical_request.birthday);

                        $('td#med_req-full_name').html(response.medical_request.full_name);
                        $('td#med_req-full_ci').html(response.medical_request.ci + ' ' + response.medical_request.ci_expedition);
                        $('td#med_req-gender').html(response.medical_request.gender);
                        $('td#med_req-birthday').html(birthday.format('DD/MM/YYYY'));
                        $('td#med_req-enterprise').html(response.medical_request.enterprise);
                        $('td#med_req-employee_code').html(response.medical_request.employee_code);
                        $('td#med_req-health_insurance').html(response.medical_request.health_insurance.nombre);
                        $('td#med_req-phone_number').html(response.medical_request.phone_number);

                        if (response.medical_request.whatsapp_flag == 1)
                        {
                            $('td#med_req-whatsapp_flag').html('Con whatsapp (mi número tiene esta aplicación)');
                        }
                        else
                        {
                            $('td#med_req-whatsapp_flag').html('Sin whatsapp (mi número no tiene esta aplicación)');
                        }

                        $('td#med_req-email').html(response.medical_request.email);
                        $('td#med_req-medical_request_type').html(response.medical_request.risk_type.name);
                        $('td#med_req-comment').html(response.medical_request.comment);

                        $('div#modal_medical_request').modal('show');
                    }
                    else if (response.appointment != null)
                    {
                        var last_appointment = moment(response.appointment.next_attention_date);

                        if (last_appointment > moment())
                        {
                            $('td#table-next_attention_date').html(last_appointment.format('DD/MM/YYYY H:m'));
                            switch (response.appointment.appointment_type_id)
                            {
                                case 1:
                                    $('td#table-appointment_type').html('PRESENCIAL');
                                    break;
                                case 2:
                                    $('td#table-appointment_type').html('ZOOM');
                                    break;
                                case 3:
                                    $('td#table-appointment_type').html('WHATSAPP');
                                    break;
                            }

                            $('td#table-comment').html(response.appointment.comment);

                            $('div#modal_appointment').modal('show');
                        }
                    }
                    else
                    {
                        toastr.warning(
                            '', 'La persona ya cuenta con una atención en curso.'
                        );
                    }
                }
                else
                {
                    toastr.warning(
                            '', 'Debe generar una nueva solicitud para la persona.'
                        );
                }
            }).fail(function (response){
                console.log(response);
            });
        }
        @endif

        // Validation
        $(document).ready(function () {
            //Date range picker
            $('#birthday_field').datetimepicker({
                viewMode: 'years',
                format: 'DD/MM/YYYY',
                locale: 'es',
                useCurrent: false,
                maxDate: moment(),
                minDate: moment().subtract(100, 'y')
            });

            // Modal Confirm
            $('#modal_confirm').on('hidden.bs.modal', function (e) {
                $('form#form-request button[type="submit"]')
                    .attr('disabled', false)
                    .html('<i class="fas fa-hospital-user mr-2"></i>Registrar Solicitud');
            });

            $.validator.setDefaults({
                submitHandler: function (form) {
                    $('form#form-request button[type="submit"]')
                        .attr('disabled', true)
                        .html("Enviando la solicitud, un momento por favor...");

                    try
                    {
                        $('#modal_confirm .modal-body td#table-full_name').html($('input[name="full_name"]').val().toUpperCase());
                        $('#modal_confirm .modal-body td#table-full_ci').html($('input[name="ci"]').val().toUpperCase() + ' ' + $('select[name="ci_expedition"] option:selected').text().toUpperCase());
                        $('#modal_confirm .modal-body td#table-gender').html($('select[name="gender"] option:selected').text().toUpperCase());
                        $('#modal_confirm .modal-body td#table-birthday').html($('input[name="birthday"]').val().toUpperCase());

                        $('#modal_confirm .modal-body td#table-enterprise').html($('input[name="enterprise"]').val().toUpperCase());
                        $('#modal_confirm .modal-body td#table-employee_code').html($('input[name="employee_code"]').val().toUpperCase());
                        $('#modal_confirm .modal-body td#table-health_insurance').html($('select[name="health_insurance_id"] option:selected').text().toUpperCase());

                        $('#modal_confirm .modal-body td#table-phone_number').html($('input[name="phone_number"]').val().toUpperCase());
                        $('#modal_confirm .modal-body td#table-whatsapp_flag').html($('select[name="whatsapp_flag"] option:selected').text().toUpperCase());
                        $('#modal_confirm .modal-body td#table-email').html($('input[name="email"]').val());

                        $('#modal_confirm .modal-body td#table-short_phone_number').html($('input[name="short_phone_number"]').val().toUpperCase());
                        $('#modal_confirm .modal-body td#table-ref_name').html($('input[name="reference_full_name"]').val().toUpperCase());
                        $('#modal_confirm .modal-body td#table-ref_phone_number').html($('input[name="reference_phone_number"]').val());

                        $('#modal_confirm .modal-body td#table-medical_request_type').html($('select[name="risk_type_id"] option:selected').text().toUpperCase());
                        $('#modal_confirm .modal-body td#table-comment').html($('textarea[name="comment"]').val().toUpperCase());

                        $('#modal_confirm').modal('show');
                    }
                    catch (e)
                    {
                        console.error(e);
                    }

                    //$(form).unbind().submit();
                }
            });

            $('form#form-request').validate({
                rules: {
                    ci: {
                        required: true,
                        minlength: 6,
                        maxlength: 11,
                    },
                    ci_expedition: {
                        required: true,
                    },
                    gender: {
                        required: true
                    },
                    birthday: {
                        required: true,
                    },
                    enterprise: {
                        required: true,
                        minlength: 3,
                        maxlength: 190,
                    },
                    health_insurance_id: {
                        required: true
                    },
                    email: {
                        email: true,
                    },
                    employee_code: {
                        minlength: 1,
                        maxlength: 5,
                    },
                    health_insurance_id: {
                        required: true
                    },
                    full_name: {
                        required: true,
                        minlength: 5,
                        maxlength: 190,
                    },
                    phone_number: {
                        required: true,
                        minlength: 4,
                        maxlength: 8,
                    },
                    reference_full_name: {
                        minlength: 5,
                        maxlength: 190,
                    },
                    reference_phone_number: {
                        minlength: 7,
                        maxlength: 8,
                    },
                    short_phone_number: {
                        minlength: 4,
                        maxlength: 4,
                    },
                    whatsapp_flag: {
                        required: true,
                    },
                    comment: {
                        maxlength: 190,
                    },
                    risk_type_id: {
                        required: true,
                    },
                    @if (\Auth::user())
                    user_id:{
                        required: true,
                    }
                    @endif
                },
                messages: {
                    ci: {
                        required: 'El campo cédula de indentidad es obligatorio.',
                        minlength: 'El campo cédula de indentidad debe contener al menos 7 caracteres.',
                        maxlength: 'El campo cédula de indentidad no debe contener más de 11 caracteres.',
                    },
                    ci_expedition: {
                        required: 'El campo expedición de la cédula de identidad es obligatorio.',
                    },
                    gender: {
                        required: 'El campo género es obligatorio.'
                    },
                    birthday: {
                        required: 'El campo fecha de nacimiento es obligatorio.',
                    },
                    enterprise: {
                        required: 'El campo empresa es obligatorio.',
                        minlength: 'El campo empresa debe contener al menos 3 caracteres.',
                        maxlength: 'El campo empresa no debe contener más de 190 caracteres.',
                    },
                    health_insurance_id: {
                        required: 'El campo seguro médico es obligatorio.'
                    },
                    email: {
                        email: 'El campo correo electrónico debe ser una dirección de correo válida.',
                    },
                    employee_code: {
                        minlength: 'El campo código de empleado debe contener al menos 1 caracteres.',
                        maxlength: 'El campo código de empleado no debe contener más de 5 caracteres.',
                    },
                    health_insurance_id: {
                        required: 'El campo seguro médico es obligatorio.'
                    },
                    full_name: {
                        required: 'El campo nombre completo es obligatorio.',
                        minlength: 'El campo nombre completo debe contener al menos 5 caracteres.',
                        maxlength: 'El campo nombre completo no debe contener más de 190 caracteres.',
                    },
                    phone_number: {
                        required: 'El campo número de teléfono es obligatorio.',
                        minlength: 'El campo número de teléfono debe contener al menos 4 caracteres.',
                        maxlength: 'El campo número de teléfono no debe contener más de 8 caracteres.',
                    },
                    reference_full_name: {
                        minlength: 'El campo nombre completo del contacto debe contener al menos 5 caracteres.',
                        maxlength: 'El campo nombre completo del contacto no debe contener más de 190 caracteres.',
                    },
                    reference_phone_number: {
                        minlength: 'El campo número de teléfono del contacto debe contener al menos 7 caracteres.',
                        maxlength: 'El campo número de teléfono del contacto no debe contener más de 8 caracteres.',
                    },
                    short_phone_number: {
                        minlength: 'El campo número corporativo debe contener al menos 4 caracteres.',
                        maxlength: 'El campo número corporativo no debe contener más de 4 caracteres.',
                    },
                    whatsapp_flag: {
                        required: 'El campo whatsapp es obligatorio.',
                    },
                    comment: {
                        maxlength: 'El campo comentario u observación no debe contener más de 190 caracteres.',
                    },
                    risk_type_id: {
                        required: 'El campo motivo de solicitud es obligatorio.',
                    },
                    @if (\Auth::user())
                    user_id:{
                        required: 'El campo médico que atenderá el caso es obligatorio.',
                    }
                    @endif
                },
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });
    </script>
@endsection
