@extends('layouts.app_simple')

@section('styles')

@endsection

@section('content')
    <div class="container h-100">
        <div class="register-logo">
            <a href="#"><b>Solicitud Médica</b></a>
        </div>

        <div class="row h-100 justify-content-center align-items-center">

            <div class="col-md-12">
                <!-- Widget: user widget style 1 -->
                <div class="card card-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-info">
                        <h3 class="widget-user-username">Solicitud Médica</h3>
                        {{-- <h5 class="widget-user-desc">Su código es: <b>{{$medical_request->code}}</b></h5> --}}
                    </div>
                    <div class="widget-user-image">
                        <img class="img-circle elevation-2" src="{{ asset('img/healt_icon.png') }}" alt="User Avatar">
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-sm-8 offset-md-2">
                                <div class="description-block">
                                    {{-- <h5 class="description-header">3,200</h5>
                                    <span class="description-text">SALES sadsaf dsfadsf asfdafds  d</span> --}}
                                    <div>
                                        <label for="radio_c"> Tipo de Búsqueda</label>
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12">
                                        <input type="radio" name="radio_c" id="radio_c1" value="1"  required="required" checked>
                                        <label for="radio_c1">Por Código</label>
                                        <input type="radio" name="radio_c" id="radio_c2" value="2"  required="required">
                                        <label for="radio_c2">Por C.I.</label>
                                    </div>
                                    <div id="div_search_code" class="input-group input-group-md">
                                        <label for="search_cod">Introduce tu código: </label>
                                        <input type="text" class="form-control" placeholder="Buscar..." required id="search_cod" name="search_cod">
                                        <span class="input-group-append">
                                            <button type="button" class="btn btn-info btn-flat" onclick="buscar();" id="btn_busca_code">
                                                <i class="fa fa-btn fa-search"></i>Buscar
                                            </button>
                                        </span>
                                    </div>
                                    {{-- <div id="div_search_ci" class="invisible"> --}}
                                        <div id="div_search_ci" class="input-group input-group-md invisible">
                                            <label for="search_ci">Introduce tu CI: </label>
                                            <input type="text" class="form-control" placeholder="Buscar..."
                                            required id="search_ci" name="search_ci"
                                            data-inputmask='"mask": "9999999[9][-9A]"' data-greedy="false" data-mask>
                                            <div class="input-group-append append-ci">
                                                <select name="ci_expedition" class="select2" style="width: 20px;">
                                                    <option value="LP">LP</option>
                                                    <option value="CB">CB</option>
                                                    <option value="SC">SC</option>
                                                    <option value="CH">CH</option>
                                                    <option value="OR">OR</option>
                                                    <option value="PT">PT</option>
                                                    <option value="TJ">TJ</option>
                                                    <option value="BE">BE</option>
                                                    <option value="PD">PD</option>
                                                </select>
                                            </div>
                                            <div class="input-group-append">
                                                <button type="button" class="btn btn-info btn-flat" onclick="buscar1();" id="btn_busca_code">
                                                    <i class="fa fa-btn fa-search"></i>Buscar
                                                </button>
                                            </div>
                                        </div>
                                    {{-- </div> --}}
                                </div>
                                <!-- /.description-block -->
                            </div>
                        </div>
                        <!-- /.row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="description-block">
                                    <div id="div-container-linea" class="">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.widget-user -->
            </div>
        </div>
    </div>
@endsection
@include('layouts.components.modals.modal_files')
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script>
    const showHistoryFiles = @json(route('mostrar_archivos.showFiles','_id'));
    $(document).ready(function() {
        $("#search_cod").mask("ZZZZZZ", { translation: { 'Z': { pattern: /^([a-zA-Z0-9]+)$/ } } });
        //$("#search_ci").mask("ZZZZZZZZ", { translation: { 'Z': { pattern: /^([0-9])*$/} } });
    });
    $('input[type=radio][name^=radio_c]').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });
    $('select[name="ci_expedition"]').select2({
        theme: 'bootstrap4'
    });
    $('input[type=radio][name^=radio_c]').on('ifChecked', function(event){
        //console.log($(this).val());
        $('div#div-container-linea').html('');
        switch ($(this).val()) {
            case "1":
                //search_type = 0;
                //removeAttr( 'style' );
                $('div#div_search_code').removeClass('invisible');
                $('div#div_search_ci').addClass('invisible');
                $('div#div_search_ci').empty();
                $('div#div_search_code').empty();
                $('div#div_search_code').append('<label for="search_cod">Introduce tu código: </label><input type="text" class="form-control" placeholder="Buscar..." required id="search_cod" name="search_cod"><span class="input-group-append"><button type="button" class="btn btn-info btn-flat" onclick="buscar();" id="btn_busca_code"><i class="fa fa-btn fa-search"></i> Buscar</button></span>');
                break;
            case "2":
                //search_type = 1;
                $('div#div_search_code').addClass('invisible');
                $('div#div_search_ci').removeClass('invisible');
                $('div#div_search_code').empty();
                $('div#div_search_ci').empty();
                $('div#div_search_ci').append('<label for="search_ci">Introduce tu CI: </label><input type="text" class="form-control" placeholder="Buscar..." required id="search_ci" name="search_ci"'+
                '>'+
                '<div class="input-group-append append-ci">'+
                '<select name="ci_expedition" class="select2" style="width: 80px;">'+
                '<option value="LP">LP</option><option value="CB">CB</option><option value="SC">SC</option><option value="CH">CH</option>'+
                '<option value="OR">OR</option><option value="PT">PT</option><option value="TJ">TJ</option><option value="BE">BE</option><option value="PD">PD</option>'+
                '</select></div><span class="input-group-append"><button type="button" class="btn btn-info btn-flat" onclick="buscar1();" id="btn_busca_code"><i class="fa fa-btn fa-search"></i> Buscar</button></span>');
                break;
        }
        $("#search_cod").mask("ZZZZZZ", { translation: { 'Z': { pattern: /^([a-zA-Z0-9]+)$/ } } });
        //$('#search_ci').inputmask({alias: 'numeric', allowMinus: false, digits: 2, max: 999.99});9999999[9][-9A]
        $("#search_ci").mask("ZZZZZZZZ", { translation: { 'Z': { pattern: /^([0-9-9A])*$/} } });
        $('.select2').select2({
            theme: 'bootstrap4',
            placeholder: 'SELECCIONE UNA OPCIÓN'
        });
    });
    function buscar(){
        $('div#div-container-linea').html('');
        var medical_code = $('input[name="search_cod"]').val();
        var url = '{{ route('buscar_codigo.buscarcodigo') }}';

        data = {};
        data.code = medical_code;
        if(medical_code!= null && medical_code !=''){
            $.ajax({
                url: url,
                headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                method: 'POST',
                dataType: 'JSON',
                data: data,
                beforeSend: function(e){

                }
            }).done(function (response){

                if (response) {
                    $('div#div-container-linea').html(response);

                    if(response['error']) {
                        $('div#div-container-linea').html('');
                        toastr.error(
                            response['error'], '¡Error!'
                        );
                    }else{

                        toastr.success(
                            '', '¡Código correcto!'
                        );
                    }
                }else{

                }
                $('input[name="search_cod"]').val('');
            }).fail(function (response){
                console.log(response);
            });
        }else{
            $('div#div-container-linea').html('');
            toastr.error(
                'No ingresó ningún código', '¡Error!'
            );
        }
    }
    function buscar1(){
        $('div#div-container-linea').html('');
        var person_ci = $('input[name="search_ci"]').val();
        var ci_expedition = $('select[name="ci_expedition"] option:selected').val();
        var url = '{{ route('buscar_ci.buscarci') }}';

        data = {};
        data.ci = person_ci;
        data.ci_expedition = ci_expedition;
        if(person_ci!= null && person_ci !=''){
            $.ajax({
                url: url,
                headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                method: 'POST',
                dataType: 'JSON',
                data: data,
                beforeSend: function(e){

                }
            }).done(function (response){

                if (response) {
                    $('div#div-container-linea').html(response);

                    if(response['error']) {
                        $('div#div-container-linea').html('');
                        toastr.error(
                            response['error'], '¡Error!'
                        );
                    }else{

                        toastr.success(
                            '', '¡Código correcto!'
                        );
                    }
                }else{

                }
                $('input[name="search_ci"]').val('');
            }).fail(function (response){
                console.log(response);
            });
        }else{
            $('div#div-container-linea').html('');
            toastr.error(
                'No ingresó ningún ci', '¡Error!'
            );
        }
    }
</script>
<script src="{{asset('js/components/button_files.js')}}"></script>

@endsection
