@extends('layouts.pdf')

@section('style')
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ public_path('adminlte/dist/css/adminlte.min.css') }}">
    <style>
        .row{
            font-size: 13px;
        }
    </style>
    <style type="text/css" rel="stylesheet">
        thead { display: table-header-group }
        tfoot { display: table-row-group }
        tr { page-break-inside: avoid }
    </style>
    <style type="text/css" rel="stylesheet">

        #watermark {
            position: fixed;

            /**
                Set a position in the page for your image
                This should center it vertically
            **/
            bottom:   10cm;
            left:     5.5cm;

            /** Change image dimensions**/
            width:    8cm;
            height:   8cm;

            /** Your watermark should be behind every content**/
            z-index:  -1000;
        }

        *{
            color: #171717;

        }
        div .div-heigth-2{
            line-height: 2px;
        }
        div .div-heigth-3{
            line-height: 3px;
        }
        thead th {
            padding-top: 1px !important;
            padding-bottom: 0px !important;
            padding-left: 3px !important;
            padding-right: 3px !important;
        }
        thead > tr > th > p{
            margin-top: 5px !important;
            margin-bottom: 5px !important;
        }
        tbody tr {
            font-size: 0.8em;
            padding-left: 0px !important;
            margin-bottom: 2px !important;
        }
        tbody > tr > td {
            padding-left: 3px !important;
            padding-right: 3px !important;
        }
        tbody > tr > td > p{
            line-height: 2px;
            margin-top: 0px !important;
            margin-bottom: 0px !important;
        }
        .borderless td {
            border: none;
        }

        table.border tr th,
            /*table.border tr td{*/
            /*    border: 1px solid black;*/
            /*    padding: 2px;*/
            /*}*/
        table.border tr th.align_right,
        table.border tr td.align_right{
            text-align: right;
        }

        table.border td { border:1px solid black; }

        table.border tr td {
            vertical-align: top;
        }
    </style>
@endsection

@section('content')
    <div>
        <div class="row justify-content-end">
            <h3 style="text-align: center">REPORTE DE ATENCIÓN MÉDICA</h3>
        </div>
        <div class="row">
            <p style="text-align: right"><strong>CÓDIGO DE SOLICITUD:</strong> {{ $medical_request->code }}</p>
        </div>
        <div class="row">
            <p style="text-align: right" class="font-small-3"><strong>SOLICITUD EN FECHA:</strong> {{ $medical_request->created_at->format('d/m/Y H:i:s') }}</p>
        </div>

    </div>

    <div class="row">
        <table class="border" style="border: 1px solid black !important;width: 100%;page-break-inside:auto !important;table-layout: fixed;">

            <tbody>


            <tr style="border: solid 1px black !important;">
                <td style="overflow: hidden;" colspan="6" class="text-center">
                    {{ $medical_request->full_name }}
                </td>
            </tr>
            <tr style="border-top: solid 1px black !important;background-color: rgba(185,185,185,0.55); font-size: 8px ">
                <td colspan="6" class="text-center">
                    <strong>Nombre Completo</strong>
                </td>
            </tr>
            <tr style="border: solid 1px black !important;">
                <td style="overflow: hidden;" colspan="2" class="text-center">
                    {{ $medical_request->full_ci }}
                </td>
                <td colspan="2" class="text-center">
                    {{ $medical_request->gender }}
                </td>
                <td colspan="2" class="text-center">
                    {{  $medical_request->birthday->format('d/m/Y') }}
                </td>
            </tr>
            <tr style="border-top: solid 1px black !important;background-color: rgba(185,185,185,0.55); font-size: 8px ">
                <td colspan="2" class="text-center">
                    <strong>Cédula de Identidad</strong>
                </td>
                <td colspan="2" class="text-center">
                    <strong>Género</strong>
                </td>
                <td colspan="2" class="text-center">
                    <strong>Fecha de nacimiento</strong>
                </td>
            </tr>


            <tr style="border: solid 1px black !important;">
                <td colspan="2" class="text-center">
                    @if($medical_request->employee!=null)
                        <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
                        {{$medical_request->employee->enterprise->nombre}}
                    @else
                        @if($medical_request->history_valid_status)
                            <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
                        @else
                            <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
                        @endif
                        {{$medical_request->enterprise}}
                    @endif
                </td>
                <td colspan="2" class="text-center">
                    {{ $medical_request->employee_code }}
                </td>
                <td colspan="2" class="text-center">
                    @if($medical_request->healthInsurance != null)
                        {{ $medical_request->healthInsurance->nombre }}
                    @else
                        S/D
                    @endif
                </td>
            </tr>
            <tr style="border-top: solid 1px black !important;background-color: rgba(185,185,185,0.55); font-size: 8px ">
                <td colspan="2" class="text-center">
                    <strong>Empresa</strong>
                </td>
                <td colspan="2" class="text-center">
                    <strong>Código de empleado</strong>
                </td>
                <td colspan="2" class="text-center">
                    <strong>Seguro Médico</strong>
                </td>
            </tr>

            <tr style="border: solid 1px black !important;">
                <td colspan="2" class="text-center">
                    {{ $medical_request->phone_number }}
                </td>
                <td colspan="2" class="text-center">
                    @if ($medical_request->whatsapp_flag)
                        Con whatsapp (mi número tiene esta aplicación)
                    @else
                        Sin whatsapp (mi número no tiene esta aplicación)
                    @endif
                </td>
                <td colspan="2" class="text-center">
                    @if($medical_request->email != null)
                        {{ $medical_request->email }}
                    @else
                        S/D
                    @endif
                </td>
            </tr>
            <tr style="border-top: solid 1px black !important;background-color: rgba(185,185,185,0.55); font-size: 8px ">
                <td colspan="2" class="text-center">
                    <strong>Nro. de Teléfono</strong>
                </td>
                <td colspan="2" class="text-center">
                    <strong>¿Tiene whatsapp?</strong>
                </td>
                <td colspan="2" class="text-center">
                    <strong>Correo eletrónico</strong>
                </td>
            </tr>

            <tr style="border: solid 1px black !important;">
                <td colspan="2" class="text-center">
                    {{ $medical_request->riskType->name }}
                </td>
                <td colspan="4" class="text-center">
                    {{ $medical_request->comment }}
                </td>
            </tr>
            <tr style="border-top: solid 1px black !important;background-color: rgba(185,185,185,0.55); font-size: 8px ">
                <td colspan="2" class="text-center">
                    <strong>Motivod e Consulta</strong>
                </td>
                <td colspan="4" class="text-center">
                    <strong>Comentario u Observación</strong>
                </td>
            </tr>

            </tbody>
        </table>
    </div>

@endsection
