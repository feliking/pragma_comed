@extends('layouts.app_menu')

@section('style')
    <style>
        input[type="text"], textarea {
            text-transform: uppercase;
        }

        .append-ci
        {
            width: 120px;
        }
        .form-group
        {
            margin-bottom: 0px;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edición de datos de la consulta médica</h1>
                </div>

            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            {!! Form::model($medical_request, ['route' => ['solicitudes_medicas.update', $medical_request->id], 'method' => 'put', 'id' => 'frm-medical_request']) !!}
            <div class="row">
                <div class="col-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">DATOS DEL PACIENTE</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @include('medical_request.partials.form_edit_medical_request')
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <div class="form-group">
                                <div class="col-xs-12 text-right">
                                    @if (\Illuminate\Support\Facades\Auth::user()->role_id==4)
                                    <a href="{{ route('enfermera.index') }}" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Volver</a>
                                    @else
                                    @if (\Illuminate\Support\Facades\Auth::user()->role_id==7||\Illuminate\Support\Facades\Auth::user()->role_id==10 ||\Illuminate\Support\Facades\Auth::user()->role_id==11)
                                    <a href="{{ route('administracion_seguimientos.index') }}" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Volver</a>
                                    @else
                                    <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Volver</a>
                                    @endif
                                    @endif
                                    <button type="submit" class="btn btn-warning"><i class="fa fa-check"></i> Registrar cambios</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            {!! Form::close() !!}
        </div>
    </section>

@endsection

@section('script')
<script src="{{ asset('/js/medical_request_call_center/general.js') }}"></script>
<script>
    var url_search_by_nurse = '{{ route('enfermera.search_by_nurse') }}';

    $(document).ready(function() {
        $('a#nav_administracion_seguimientos').addClass('active');
        $('a#nav_enfermera').addClass('active');
    });
    @if(isset($medical_request))
    $('select[name=ci_expedition]').val('{{$medical_request->ci_expedition}}').trigger('change');
    $('select[name=gender]').val('{{$medical_request->gender}}').trigger('change');
    @if($medical_request->employee_id!=null)
        $('input[name^=full_name]').attr('readonly', true);
        $('input[name^=ci]').attr('readonly', true);
        $('input[name^=enterprise]').attr('readonly', true);
        //$('select[name=ci_expedition]').attr('disabled', true);
        $('select[name^=ci_expedition] option:not(:selected)').attr('disabled', true);
        $('select[name^=ci_expedition] option:selected').attr('selected', 'selected');
        $('select[name^=gender] option:not(:selected)').attr('disabled', true);
        $('select[name^=gender] option:selected').attr('selected', 'selected');
    @endif
    $('input[name^=full_name]').val('{{($medical_request->employee_id!=null)?$medical_request->employee->full_name:$medical_request->full_name}}');
    $('input[name^=ci]').val('{{$medical_request->ci}}');
    $('input[name^=birthday]').val('{{$medical_request->birthday->format('d/m/y')}}');
    $('input[name^=enterprise]').val('{{$medical_request->enterprise}}');
    $('input[name^=employee_code]').val('{{$medical_request->employee_code}}');
    $('select[name=health_insurance_id]').val('{{$medical_request->health_insurance_id}}').trigger('change');
    $('input[name^=phone_number]').val('{{$medical_request->phone_number}}');
    $('select[name=whatsapp_flag]').val('{{$medical_request->whatsapp_flag}}').trigger('change');
    $('input[name^=short_phone_number]').val('{{$medical_request->short_phone_number}}');
    $('input[name^=email]').val('{{$medical_request->email}}');
    $('input[name^=reference_full_name]').val('{{$medical_request->reference_full_name}}');
    $('input[name^=reference_phone_number]').val('{{$medical_request->reference_phone_number}}');
    $('select[name=risk_type_id]').val('{{$medical_request->risk_type_id}}').trigger('change');
    $('textarea[name^=comment]').val('{{$medical_request->comment}}');
    @endif
</script>
@endsection
