<table id="example2" class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Op.</th>
        <th>Nombre</th>
        <th>Descripción</th>
        <th>Estado</th>
    </tr>
    </thead>

    <tbody>
    @foreach($treatment_stages as $treatment_stage)
        <tr>
            <td>
                <div class="btn-group" role="group">
                    <a href="{{ route('tratamiento_etapas.edit', $treatment_stage->id) }}" class="btn btn-sm btn-default" title="Editar">
                        <i class="fa fa-edit"></i>
                        <span class="sr-only">Editar</span>
                    </a>
                    <a href="{{ route('tratamiento_etapas.show', $treatment_stage->id) }}" class="btn btn-sm btn-default" title="{{($treatment_stage->deleted_at == null)?'Desactivar':'Activar'}}">
                        <i class="fa fa-{{($treatment_stage->deleted_at == null)?'times':'check'}}"></i>
                        <span class="sr-only">{{($treatment_stage->deleted_at == null)?'Desactivar':'Activar'}}</span>
                    </a>
                </div>
            </td>
            <td>
                {{$treatment_stage->name}}
            </td>
            <td>
                {{$treatment_stage->description}}
            </td>
            <td>
                @if($treatment_stage->deleted_at == null)
                <span class="badge badge-success">Activo</span>
                @elseif($treatment_stage->deleted_at != null)
                <span class="badge badge-danger">Inactivo</span>
                @endif
            </td>

        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Op.</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Estado</th>
        </tr>
    </tfoot>
</table>
