<td>
    <div class="btn-group">
        @include('layouts.components.buttons.button_historial',[
            'medical_request_id' => $medical_request->id
        ])
        @if($medical_request->employee_id != null)
            <button type="button" class="btn btn-sm btn-warning" onclick="abrirModalReasiginarSolicitud({{ $medical_request->id }}, this)">
                <i class="fa fa-user"></i>
            </button>
        @endif
    </div>
</td>
<td data-code>
    {{$medical_request->code}}
</td>
<td data-full_name>
    {{ $medical_request->full_name }}
</td>
<td data-full_ci>
    <input type="hidden" data-ci value="{{ $medical_request->ci }}">
    <input type="hidden" data-del value="{{ $medical_request->deleted_at }}">
    <input type="hidden" data-ci_expedition value="{{ $medical_request->ci_expedition }}">
    <input type="hidden" data-medical_request_status_id value="{{ $medical_request->medical_request_status->name }}">
    <input type="hidden" data-user value="{{ $medical_request->medic->name }}">
    {{ $medical_request->full_ci }}
</td>
<td data-enterprise>
    @if($medical_request->employee!=null)
        <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
        {{$medical_request->employee->enterprise->nombre}}
    @else
        @if($medical_request->history_valid_status)
            <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
        @else
            <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
        @endif
        {{$medical_request->enterprise}}
    @endif
</td>
<td data-email>
    @if($medical_request->email!=null)
        {{ $medical_request->email }}
    @else
        <span class="badge badge-warning">
            S/N
        </span>
    @endif
</td>
<td>
    {{ $medical_request->employee_code }}
</td>
<td data-full_phone>
    <input type="hidden" id="data-phone" value="{{$medical_request->phone_number}}">
    <input type="hidden" id="data-phone-whatsapp_flag" value="{{$medical_request->whatsapp_flag}}">
    @if($medical_request->whatsapp_flag)
        <div class="badge badge-success">
            <i class="fab fa-whatsapp"></i>
        </div>
    @endif
    {{ $medical_request->phone_number }}
</td>
<td>
    @if($medical_request->employee!=null)
        {{ $medical_request->employee->full_name }}
    @else
        <span class="badge badge-warning">
            S/A
        </span>
    @endif
</td>
