<!-- Modal -->
<div class="modal fade" id="modal_employee_reassign" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('Registro de Empleado') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <label><i class="fa fa-barcode"></i><b> Codigo de Solicitud</b></label>
                            <p id="p_modal_code"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fa fa-user"></i><b> Nombre de empleado</b></label>
                            <p id="p_modal_fullname"></p>
                        </div>
                        <div class="form-group col-md-4">
                            <label><i class="fas fa-sort-numeric-down"></i><b> CI</b></label>
                            <p id="p_modal_full_ci"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fa fa-building"></i><b> Empresa</b></label>
                            <p id="p_modal_enterprise"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fa fa-phone"></i><b> Telefono</b></label>
                            <p id="p_modal_phone"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fa fa-envelope"></i><b> Email</b></label>
                            <p id="p_modal_email"></p>
                        </div>
                        <div class="col-md-4 offset-md-2">
                            <label><i class="fas fa-stethoscope"></i><b> Médico asignado</b></label>
                            <p id="p_modal_doctor"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fas fa-minus-square"></i><b> Estado</b></label>
                            <p id="p_modal_status"></p>
                        </div>
                    </div>
                </div>
                <hr>
                <div  class="form-group col-md-10 col-sm-10 offset-md-2">
                    <div class="form-group{{ $errors->has('doctor_id') ? ' has-error' : '' }} wrapper-doctor_id">
                        {!! Form::label('doctor_id', '* Reasignar a Médico', ['class' => 'control-label col-md-5 col-sm-4 col-xs-12']) !!}
                        <div class="col-md-10 col-sm-8 col-xs-12">
                            {!! Form::select('doctor_id', $medical_users, null, ['class' => 'form-control select2', 'placeholder' => 'Seleccione un médico']) !!}

                            <span class="help-block">
                                <strong>{{ $errors->first('doctor_id') }}</strong>
                            </span>

                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                </button>
                <button id="btnReagsinarSolicitud" class="btn btn-info" >
                    <i class="fa fa-file-medical"></i> Reasignar solicitud médica
                </button>

            </div>
        </div>
    </div>
</div>
