@extends('layouts.app_menu')

@section('styles')
    <style>
        .dataTables_filter {
            display: none !important;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Listado de Solicitudes a Reasignar</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Datos de consultas a Reasignar</h3>

                            <div class="card-tools">
                                <div class="input-group input-group-sm">
                                    <input id="search_data" type="text" class="form-control" placeholder="Buscar">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @include('medical_request_reassignment.partials.table')
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
        </div>
    </section>

    <!--MODALS-->
    @include('medical_request_reassignment.partials.reassignment_modal')
    @include('layouts.components.modals.modal_history')
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('a#reassignment').addClass('active');
        });
        /*RUTAS DEL SISTEMA*/
        const search = @json(route('empleados.busqueda_empresa.getEmployeeFromEnterprise','_id'));
        const searchCi = @json(route('empleados.busqueda_ci.getEmployeeFromCI',['ci'=>'_ci','ci_expedition'=>'_ci_exp']));
        const assingToEmp = @json(route('consultas_medicas_pendientes.asignar_empleado.assignToEmployee'));
        const assing = @json(route('consultas_medicas_pendientes.asignar.assign'));
        const employyeNotFound = @json(route('consultas_medicas_pendientes.eliminar_solicitud.deleteRequest'));
        const showHistory = @json(route('administracion_solicitudes.mostrar_historial.showHistory','_id'));
        $(function () {
            $('.select2').select2({
                theme: 'bootstrap4',
                placeholder: 'SELECCIONE UNA OPCIÓN'
            });
        });
        var global_medical_request_id = null;
function abrirModalReasiginarSolicitud(medical_request_id, object) {
    $('#modal_employee_reassign').modal('show');
    var tr = $(object).parent().parent().parent();

    var code = $(tr).find('[data-code]').text().trim();
    var fullname = $(tr).find('[data-full_name]').text().trim();
    var full_ci = $(tr).find('[data-full_ci]').text().trim();
    var enterprise = $(tr).find('[data-enterprise]').text().trim();
    var ci = $(tr).find('[data-ci]').val().trim();
    var ci_expedition = $(tr).find('[data-ci_expedition]').val().trim();
    var phone = $(tr).find('#data-phone').val();
    var whatsapp_flag = $(tr).find('#data-phone-whatsapp_flag').val();
    var email = $(tr).find('[data-email]').text().trim();
    var status = $(tr).find('[data-medical_request_status_id]').val();
    var doctor = $(tr).find('[data-user]').val();

    $('p#p_modal_code').text(code);
    $('input#txt_modal_full_name').val(fullname);
    $('p#p_modal_fullname').text(fullname);
    $('p#p_modal_full_ci').text(full_ci);
    $('p#p_modal_enterprise').text(enterprise);
    $('#enterprise').val(enterprise);
    $('p#p_modal_doctor').text(doctor);
    $('p#p_modal_status').text(status);
    $('input#ci').val(ci);
    $('select#ci_expedition').val(ci_expedition).trigger('change');
    $('p#p_modal_phone').empty();
    if (whatsapp_flag) {
        var whatsapp_flag_div = '<div class="badge badge-success">'+
                            '<i class="fab fa-whatsapp"></i>'+
                            '</div>';
        $('p#p_modal_phone').append(whatsapp_flag_div + ' ' + phone);
    } else {
        $('p#p_modal_phone').text(phone);
    }
    $('p#p_modal_email').text(email);

    document.getElementById('btnReagsinarSolicitud').setAttribute("onClick","reasignar("+medical_request_id+")");
}
function reasignar(id){
    var select_doc = $('#doctor_id').val();
    var url = '{{ route('solicitudes_reasignar_post.reassignmentRequestPost') }}';
    data = {};
    data.id = id;
    data.select_doc = select_doc;
    if (select_doc != '' && select_doc != undefined) {
        $.ajax({
                url: url,
                headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                method: 'POST',
                dataType: 'JSON',
                data: data,
                beforeSend: function(e){

                }
            }).done(function (response){
                if (response) {
                    if(response['error']) {

                        toastr.error(
                            response['error'], '¡Error!'
                        );

                    }else{
                        $('#modal_employee_reassign').modal('hide');
                        toastr.success(
                            response['message'], '¡Reasignación correcta!'
                        );
                        location.reload();
                    }
                }else{

                }
                $('input[name="search_ci"]').val('');
            }).fail(function (response){
                console.log(response);
            });
    }else{
        toastr.error(
            'No escogió al médico para la reasignación', '¡Error!'
        );
    }

}
    </script>
    {{-- <script src="{{asset('js/pending_request/index.js')}}"></script> --}}
    <script src="{{asset('js/components/button_historial.js')}}"></script>
@endsection
