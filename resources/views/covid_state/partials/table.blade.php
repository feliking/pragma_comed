<table id="example2" class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Op.</th>
        <th>Nombre</th>
        <th>Descripción</th>
        <th>Estado</th>
    </tr>
    </thead>

    <tbody>
    @foreach($cvd_states as $cvd_state)
        <tr>
            <td>
                <div class="btn-group" role="group">
                    <a href="{{ route('covid_estados.edit', $cvd_state->id) }}" class="btn btn-sm btn-default" title="Editar">
                        <i class="fa fa-edit"></i>
                        <span class="sr-only">Editar</span>
                    </a>
                    <a href="{{ route('covid_estados.show', $cvd_state->id) }}" class="btn btn-sm btn-default" title="{{($cvd_state->deleted_at == null)?'Desactivar':'Activar'}}">
                        <i class="fa fa-{{($cvd_state->deleted_at == null)?'times':'check'}}"></i>
                        <span class="sr-only">{{($cvd_state->deleted_at == null)?'Desactivar':'Activar'}}</span>
                    </a>
                </div>
            </td>
            <td>
                {{$cvd_state->name}}
            </td>
            <td>
                {{$cvd_state->description}}
            </td>
            <td>
                @if($cvd_state->deleted_at == null)
                <span class="badge badge-success">Activo</span>
                @elseif($cvd_state->deleted_at != null)
                <span class="badge badge-danger">Inactivo</span>
                @endif
            </td>

        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Op.</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Estado</th>
        </tr>
    </tfoot>
</table>
