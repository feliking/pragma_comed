@extends('layouts.app_menu')
@section('style')
    <style>
        input[type="text"], textarea {
            text-transform: uppercase;
        }
    </style>
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edición de Estados de Enfermedad</h1>
                </div>
                
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            {!! Form::model($cvd_state, ['route' => ['covid_estados.update', $cvd_state->id], 'method' => 'put', 'id'=> 'frm-cvd_id']) !!}
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Editar Prioridad</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @include('covid_state.partials.form')
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <div class="form-group">
                                <div class="col-xs-12 text-right">
                                    <a href="{{ route('covid_estados.index') }}" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Volver</a>
                                    <button type="submit" class="btn btn-warning"><i class="fa fa-check"></i> Registrar cambios</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@endsection

@section('script')
<script src="{{ asset('/js/cvd_state/general.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('a#nav_cvd').addClass('active');
        });
    </script>
@endsection