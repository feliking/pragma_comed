<input type="hidden" name="medical_request_id" id="medical_request_id" value="{{ $medical_request->id }}">
<div class="modal-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <label><i class="fa fa-barcode"></i><b> Codigo de Solicitud</b></label>
                <p id="p_modal_code">{{ $medical_request->code }}</p>
            </div>
            <div class="col-md-4">
                <label><i class="fa fa-user"></i><b> Nombre de empleado</b></label>
                <p id="p_modal_fullname">
                    @if($medical_request->employee!=null)
                        <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
                        {{$medical_request->employee->full_name}}
                    @else
                        @if($medical_request->history_valid_status)
                            <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
                        @else
                            <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
                        @endif
                        {{$medical_request->full_name}}
                    @endif
                </p>
            </div>
            <div class="col-md-4">
                <label><i class="fa fa-sort-numeric-desc"></i><b> CI</b></label>
                <p id="p_modal_full_ci">{{ $medical_request->full_ci }}</p>
            </div>
            <div class="col-md-4">
                <label><i class="fa fa-building"></i><b> Empresa</b></label>
                <p id="p_modal_enterprise">
                    @if($medical_request->employee!=null)
                        <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
                        {{$medical_request->employee->enterprise->nombre}}
                    @else
                        @if($medical_request->history_valid_status)
                            <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
                        @else
                            <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
                        @endif
                        {{$medical_request->enterprise}}
                    @endif
                </p>
            </div>
            <div class="col-md-4">
                <label><i class="fa fa-phone"></i><b> Telefono</b></label>
                <p id="p_modal_full_ci">
                    @if ($medical_request->whatsapp_flag)
                        <div class="badge badge-success">
                            <i class="fab fa-whatsapp"></i>
                        </div>
                    @endif
                    {{ $medical_request->phone_number }}
                </p>
            </div>
            <div class="col-md-4">
                <label><i class="fa fa-envelope"></i><b> Email</b></label>
                <p id="p_modal_email">{{ $medical_request->email }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label><i class="fa fa-edit"></i><b> Motivo de solicitud</b></label>
                <p id="p_modal_type">{{ $medical_request->medical_requests_type->name }}</p>
            </div>
            <div class="col-md-8">
                <label><i class="fa fa-comment"></i><b> Comentario u Observacion</b></label>
                <p id="p_modal_comment">{{ $medical_request->comment }}</p>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4 col-sm-12">
            <div class="form-row">
                <div class="form-group col-md-12 col-sm-12">
                    <label>{{ __('*Fecha de atención:') }}</label>
                    <div class="input-group">
                        <input type="text" class="form-control " id="fecha_consulta" name="fecha_consulta" value="{{ $fecha_actual->format('d/m/Y') }}">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-12" id="wrapper-status_id">
                    <label for="status_id" class="control-label col-12">{{ __('*Estado de la consulta') }}</label>
                    <div class="form-group">
                        <select id="medical_status_id" required value="{{ old('medical_status_id') }}" class="form-control select2 @error('medical_status_id') is-invalid @enderror" style="width: 100%;" name="medical_status_id">
                            <option></option>
                            @foreach($medical_request_status as $status)
                                <option value="{{$status->id}}">{{$status->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <span class="help-block">
                        <strong></strong>
                    </span>
                </div>
            </div>
            <div class="form-row">
                <label for="signos_vitales" class="control-label col-12">{{ __('Signos vitales') }}</label>
                <div class="col-sm-12 col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group-text">
                            <span class="">FC</span>
                        </div>
                        <input name="fc" id="fc" type="text" value="" class="form-control text-right" placeholder="">
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group-text">
                            <span class="">FR</span>
                        </div>
                        <input name="fr" id="fr" type="text" value="" class="form-control text-right" placeholder="">
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group-text">
                            <span class="">T°</span>
                        </div>
                        <input name="t" id="t" type="text" value="" class="form-control text-right" placeholder="">
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group-text">
                            <span class="">pa</span>
                        </div>
                        <input name="pa" id="pa" type="text" value="" class="form-control text-right" placeholder="">
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group-text">
                            <span class="">SaO2</span>
                        </div>
                        <input name="sao2" id="sao2" type="text" value="" class="form-control text-right" placeholder="">
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group-text">
                            <span class="">otros</span>
                        </div>
                        <input name="other" id="other" type="text" value="" class="form-control text-right" placeholder="">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="current_history" class="control-label col-12 my-0">{{ __('*Historia de la enfermedad actual') }}</label>
                    <div class="input-group mb-0">
                        <textarea name="current_history"  id="current_history" rows="3" value="" class="form-control placeholder="{{ __('Historia de la enfermedad actual') }}" required></textarea>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="physical_exam" class="control-label col-12 my-0">{{ __('*Examen fisico') }}</label>
                    <div class="input-group mb-0">
                        <textarea name="physical_exam"  id="physical_exam" rows="3" value="" class="form-control placeholder="{{ __('Examen fisico') }}" required></textarea>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="diagnostico" class="control-label col-12 my-0">{{ __('*Diagnostico') }}</label>
                    <div class="input-group mb-0">
                        <textarea name="diagnostico"  id="diagnostico" rows="3" value="" class="form-control" placeholder="{{ __('Diagnostico') }}" required></textarea>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="tratamiento" class="control-label col-12 my-0">{{ __('*Tratamiento') }}</label>
                    <div class="input-group mb-0">
                        <textarea name="tratamiento"  id="tratamiento" rows="3" value="" class="form-control" placeholder="{{ __('Tratamiento') }}" required></textarea>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-12">
                    <label class="my-0">{{ __('*Fecha próxima atención:') }}</label>
                    <div class="input-group mb-0">
                        <input type="text" class="form-control " name="fecha_proxima_consulta" id="fecha_proxima_consulta" value="">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-sm-12" style="transform: traslate(0,0); height: 500px;overflow: auto;padding: 10px">
            @if ($cantidad == 0)
                <div class="alert alert-light" role="alert">
                    Sin registros
                </div>
            @else
                @if ($historial_total)
                    @include('medical_request.partials.time_line_employee', ['medical_request' => $medical_requests ])
                @else
                    @include('medical_request.partials.time_line', ['history' => $historial_requests ])
                @endif

            @endif
        </div>
    </div>
</div>
<!-- date-range-picker -->
<script src="{{ asset('adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script>
    $(document).ready(function() {
        $('[data-mask]').inputmask();

        $('.select2').select2({
            theme: 'bootstrap4',
            placeholder: 'SELECCIONE UNA OPCIÓN'
        });

        $('#fecha_proxima_consulta').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            "autoApply": true,
            "linkedCalendars": false,
            "showCustomRangeLabel": false,
            "drops": "up",
            "minDate": "{{ $fecha_actual->format('d/m/Y') }}",
            autoUpdateInput: false,
            timePicker: true,
            timePickerIncrement: 15,
            locale: {
                format: 'DD/MM/YYYY hh:mm A'
            }
        }, function(start, end, label) {
            console.log('New date range selected: ' + start.format('DD/MM/YYYY hh:mm A') + ' to ' + end.format('DD/MM/YYYY hh:mm A') + ' (predefined range: ' + label + ')');
            this.element.val(start.format('DD/MM/YYYY hh:mm A'));
        });

        $('#fecha_consulta').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            "autoApply": false,
            "linkedCalendars": false,
            "showCustomRangeLabel": false,
            "drops": "auto",
            "maxDate": "{{ $fecha_actual->format('d/m/Y') }}",
            locale: {
                format: 'DD/MM/YYYY'
            }
        }, function(start, end, label) {
          console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
        });
        $('#hora_siguiente_consulta').datetimepicker({
            format: 'LT'
        })
    });

    $('#medical_status_id').change(function (e) {
        var id_status = $(this).val();
        if (id_status != null && id_status == 4) {
            $('#fecha_proxima_consulta').attr('disabled', 'disabled');
            $('#tratamiento').removeAttr('required');
            $('#diagnostico').removeAttr('required');
        }
    });


</script>
