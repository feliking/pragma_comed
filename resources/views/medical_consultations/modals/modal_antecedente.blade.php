<!-- Modal -->
<div class="modal fade" id="modal_antecedente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tituloModalAntecedente">{{ __('Registro de entecedente') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['id' => 'frm_antecedente']) !!}
            <input type="hidden" name="modal_antecedent_type_id" id="modal_antecedent_type_id" value="">
            <input type="hidden" name="modal_employee_id" id="modal_employee_id" value="{{ $medical_request->employee_id }}">
            <input type="hidden" name="modal_medical_request_id" id="modal_medical_request_id" value="{{ $medical_request->id }}">
            <input type="hidden" name="modal_antecedent_id" id="modal_antecedent_id" value="">
            <div id="form-row">
                <div class="form-group col-12">
                    <label for="" class="control-label col-12" id="tipo_antecedente"></label>
                </div>
                <div class="form-group col-12">
                    <label for="descripcion_antecedente" class="control-label col-12 my-0 text-uppercase">Descripción</label>
                    <div class="input-group mb-0">
                        <textarea name="descripcion_antecedente"  id="descripcion_antecedente" rows="3" value="" class="form-control" placeholder="Descripción" ></textarea>
                        <span class="invalid-feedback" role="alert">
                        </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                </button>
                <button type="button" class="btn btn-success" onclick="registrar_antecedente()"  id="btnModalRegistrar">
                    <i class="fa fa-save"></i> Registrar
                </button>
                <button type="button" class="btn btn-warning" onclick="editar_atecedente()" hidden id="btnModalModificar">
                    <i class="fa fa-edit" ></i> Modificar
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
