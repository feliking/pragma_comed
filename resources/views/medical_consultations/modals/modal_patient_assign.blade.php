<!-- Modal -->
<div class="modal fade" id="modal_patient_assign" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('asignación') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['id' => 'frm_asignacion']) !!}
            <input type="hidden" name="medical_request_id" id="medical_request_id" value="">
            <div id="frm_content">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                </button>
                <button type="button" class="btn btn-success" onclick="registrar_asignacion()">
                    <i class="fa fa-save"></i> Asignar
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
