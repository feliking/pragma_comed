<!-- Modal -->
<div class="modal fade" id="modal_resumen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('Registro de atención') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['id' => 'frm_atencion_modal', 'enctype' => 'multipart/form-data']) !!}
            <input type="hidden" name="medical_request_id" id="medical_request_id" value="">
            <div class="modal-body">
                <div class="form-row">
                    <div class="col-12">
                        <strong>Seleccione las partes que desea imprimir</strong>
                    </div>
                </div>
                <div id="content_resumen" style="transform: traslate(0,0); height: 540px;overflow: auto;padding: 10px">
                    @include('medical_consultations.partials.form_resumen')
                </div>
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                </button>
                <button type="button" class="btn btn-success" onclick="registrar_atencion(1)">
                    <i class="fa fa-save"></i> Registrar
                </button>
                <button type="button" class="btn btn-primary" onclick="registrar_atencion(2)">
                    <i class="fa fa-print"></i> Registrar e imprimir
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
