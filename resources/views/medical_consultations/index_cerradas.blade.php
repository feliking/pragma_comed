@extends('layouts.app_menu')

@section('styles')
    <style>
        .dataTables_filter {
            display: none !important;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Consultas realizadas</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Listado de consultas</h3>

                            <div class="card-tools">
                                <div class="input-group input-group-sm">
                                    <input id="search_data" type="text" class="form-control" placeholder="Buscar">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @include('medical_consultations.partials.table')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--MODALS-->
    @include('layouts.components.modals.modal_history')
    @include('layouts.components.modals.modal_history_timeline')
@endsection

@section('script')
    <script>
        const showHistory = @json(route('administracion_solicitudes.mostrar_historial.showHistory','_id'));
        const showHistoryTimeline = @json(route('buscar_codigo.mostrarTimeline'));

        $(document).ready(function() {
            $('a#nav_close_requests').addClass('active');
        });
    </script>
    <script src="{{asset('js/medical_consultations/index_cerradas.js')}}"></script>
    <script src="{{asset('js/components/button_historial.js')}}"></script>
    <script src="{{asset('js/components/button_historial_timeline.js')}}"></script>

@endsection
