@extends('layouts.app_menu')

@section('styles')
    <style>
        .dataTables_filter {
            display: none !important;
        }

        div.dt-buttons {
            float: right !important;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Mis consultas</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Datos de consultas médicas</h3>

                            <div class="card-tools">
                                {{-- <div class="input-group input-group-sm">
                                    <input id="search_data" type="text" class="form-control" placeholder="Buscar">
                                </div> --}}
                                
                            </div>
                        </div>
                        
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content" id="active_tab">
                                <div class="tab-pane fade show active" id="" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                                    <div class="row">
                                        <div class="col-md-12" id="div_buscador">
                                            @include('layouts.components.cards.search_my_consultation')
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- @include('medical_consultations.partials.table') --}}
                            <div id="wrapper_table">
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
        </div>
    </section>

    <!--MODALS-->
    @include('medical_consultations.modals.modal_patient_assign')
    @include('layouts.components.modals.modal_assign_appointment')
    @include('layouts.components.modals.modal_history')
    @include('layouts.components.modals.modal_remit')

    @include('layouts.components.modals.modal_history_timeline')
@endsection

@section('script')
    <script src="{{asset('js/components/search.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('a#mis_consultas').addClass('active');
            getTableRender("{{ route('consultas_medicas.index') }}");
        });
        const assingToAten = @json(route('consultas_medicas.store'));
        const getAttenHist = @json(route('consultas_medicas.getAttentionHistorial', '_ID'));
        const showHistory = @json(route('administracion_solicitudes.mostrar_historial.showHistory','_id'));
        const showHistoryTimeline = @json(route('buscar_codigo.mostrarTimeline'));
        const consultasIndex = @json(route('consultas_medicas.index'));
        const forSearchProjects = @json(route('proyectos.para_buscador.getProjectsFromEnterpriseSearch','_id'));
        /*FUNCIONES*/
        function getValues() {
            var medical_request_id_search = $('#medical_request_id_search option:selected').val();
            var enterprise_id_search = $('#enterprise_id_search option:selected').val();
            var project_id_search = $('#project_id_search option:selected').val();

            var url = consultasIndex;
            getTableRender(url,{
                'medical_request_id_search': medical_request_id_search,
                'enterprise_id_search': enterprise_id_search,
                'project_id_search': project_id_search
            });
        }
        function clearValues() {
            $('#search_data').val('');
            $('select[name=medical_request_id_search]').val(0).trigger('change');
            $('select[name=enterprise_id_search]').val(0).trigger('change');
            $('select[name=project_id_search]').val(0).trigger('change');
            $('#init_date').val('');
            $('#end_date').val('');
            getValues();
        }
        function CleanDates(){
            $('#init_date').val('');
            $('#end_date').val('');
            getValues();
        }
    </script>

    <script src="{{asset('js/medical_consultations/index.js')}}"></script>
    <script src="{{asset('js/components/button_historial.js')}}"></script>
    <script src="{{asset('js/components/button_historial_timeline.js')}}"></script>
    
@endsection
