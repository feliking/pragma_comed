<div class="col-md-6">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">ANTECEDENTES</h3>
        </div>
        <div class="card-body" style="transform: traslate(0,0); height: 354px;overflow: auto;padding: 10px">
            <div id="accordion">
                @php
                    $abrir = 0;
                @endphp
                @foreach ($antescedent_types as $antescedent_type)
                    @php
                        $abrir++;

                        $mis_antecedentes = $antecendents->where('antecendent_type_id', $antescedent_type->id);

                    @endphp
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4 class="card-title ">
                                <a class="d-block " data-toggle="collapse" href="#collapse_{{ $antescedent_type->id }}">
                                    {{ $antescedent_type->name }}  <span title="3 New Messages" class="badge badge-light" id="cantidad_antecedente_{{ $antescedent_type->id }}">{{ count ($mis_antecedentes) }}</span>
                                </a>
                            </h4>
                            <div class="card-tools">
                                <ul class="nav nav-pills ml-auto">
                                    <button type="button" class="btn btn-sm btn-default" onclick="añadirAntecedente({{ $antescedent_type->id }}, '{{ $antescedent_type->name }}')"><i class="fa fa-plus"> Adicionar</i></button>
                                </ul>
                            </div>
                        </div>
                        <div id="collapse_{{ $antescedent_type->id }}" class="collapse {{ ($abrir == 1) ? 'show':'' }}" data-parent="#accordion">
                            <div class="card-body" id="antescedente_{{ $antescedent_type->id }}">

                                <table class="table table-hover">
                                    @if (count($mis_antecedentes) > 0 )
                                        @foreach ($mis_antecedentes as $mi_antecedente)
                                        <tr>
                                            <td data-description>{{ $mi_antecedente->description }}</td>
                                            <td class="text-right py-0 align-middle">
                                                <div class="btn-group btn-group-sm">
                                                    <button type="button" class="btn btn-warning" onclick="editarAntecedente({{ $antescedent_type->id }}, '{{ $antescedent_type->name }}', {{ $mi_antecedente->id }}, this)"><i class="fas fa-edit"></i></button>
                                                    <button type="button" class="btn btn-danger" onclick="eliminarAntecedente({{ $mi_antecedente->id }}, this)"><i class="fas fa-trash"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <div class="alert alert-info alert-dismissible">
                                            <h5><i class="icon fas fa-info"></i> ¡Sin antecedentes!</h5>
                                        </div>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
