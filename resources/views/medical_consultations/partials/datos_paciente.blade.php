<div class="form-row">
    <div class="col-md-4 col-sm-6  col-xs-6">
        <label><i class="fa fa-barcode"></i><b> Codigo de Solicitud</b></label>
        <p id="p_modal_code">{{ $medical_request->code }}</p>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-6">
        <label><i class="fa fa-user"></i><b> Nombre de empleado</b></label>
        <p id="p_modal_fullname">
            @if($medical_request->employee!=null)
                <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
                {{$medical_request->employee->full_name}}
            @else
                @if($medical_request->history_valid_status)
                    <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
                @else
                    <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
                @endif
                {{$medical_request->full_name}}
            @endif
        </p>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-6">
        <label><i class="fas fa-sort-numeric-down"></i><b> CI</b></label>
        <p id="p_modal_full_ci">{{ $medical_request->full_ci }}</p>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-6">
        <label><i class="fa fa-building"></i><b> Empresa</b></label>
        <p id="p_modal_enterprise">
            @if($medical_request->employee!=null)
                <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
                {{$medical_request->employee->enterprise->nombre}}
            @else
                @if($medical_request->history_valid_status)
                    <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
                @else
                    <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
                @endif
                {{$medical_request->enterprise}}
            @endif
        </p>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-6">
        <label><i class="fa fa-phone"></i><b> Telefono</b></label>
        @if ($medical_request->whatsapp_flag)
            <p>
                <span class="badge badge-success">
                    <i class="fab fa-whatsapp"></i>
                </span>
                {{ $medical_request->phone_number }}
            </p>
        @else
            <p>
                {{ $medical_request->phone_number }}
            </p>
        @endif
    </div>
    <div class="col-md-4 col-sm-6 col-xs-6">
        <label><i class="fa fa-envelope"></i><b> Email</b></label>
        <p id="p_modal_email">{{ $medical_request->email }}</p>
    </div>
</div>
<div class="form-row">
</div>
<div class="form-row">
    <div class="col-md-4 col-sm-6">
        <label><i class="fas fa-mobile-alt"></i><b> Corporativo</b></label>
        <p>
            {{ $medical_request->short_phone_number }}
        </p>
    </div>
    <div class="col-md-4 col-sm-6">
        <label><i class="fa fa-calendar"></i><b> Edad</b></label>
        <p id="p_modal_born_date">
            @if($medical_request->employee!=null)
                <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
                {{$medical_request->employee->edad}}
            @else
                @if($medical_request->history_valid_status)
                    <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
                @else
                    <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
                @endif
                {{ $medical_request->edad }}
            @endif
            años
        </p>
    </div>
    <div class="col-md-4 col-sm-6">
        <label><i class="fa fa-restroom"></i><b> Sexo</b></label>
        <p id="p_modal_email">{{ $medical_request->gender }}</p>
    </div>

</div>
<div class="form-row">
    <div class="col-md-4 col-sm-6">
        <label><i class="fa fa-id-card"></i><b> Seguro</b></label>
         {{-- <p id="p_modal_email">{{ $medical_request->healthInsurance->nombre }}</p> --}}
    </div>
    <div class="col-md-4 col-sm-6">
        <label><i class="fa fa-user"></i><b> Contacto de referencia</b></label>
         <p id="p_modal_email">{{ $medical_request->reference_full_name }}</p>
    </div>
    <div class="col-md-4 col-sm-6">
        <label><i class="fa fa-phone"></i><b> Teléfono de referencia</b></label>
         <p id="p_modal_email">{{ $medical_request->reference_phone_number }}</p>
    </div>
</div>
<div class="form-row">
    <div class="col-md-4">
        <label><i class="fa fa-edit"></i><b> Motivo de solicitud</b></label>
        <p id="p_modal_type">{{ $medical_request->riskType->name }}</p>
    </div>
    <div class="col-md-8">
        <label><i class="fa fa-comment"></i><b> Comentario u Observacion</b></label>
        <p id="p_modal_comment">{{ $medical_request->comment }}</p>
    </div>
</div>
