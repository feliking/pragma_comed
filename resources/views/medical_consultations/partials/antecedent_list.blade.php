<table class="table table-hover">
    @if (count($antecendents) > 0 )
        @foreach ($antecendents as $mi_antecedente)
        <tr>
            <td data-description>{{ $mi_antecedente->description }}</td>
            <td class="text-right py-0 align-middle">
                <div class="btn-group btn-group-sm">
                    <button type="button" class="btn btn-warning" onclick="editarAntecedente({{ $mi_antecedente->antecendent_type_id }}, '{{ $mi_antecedente->antecedent_type->name }}', {{ $mi_antecedente->id }}, this)">
                        <i class="fas fa-edit"></i>
                    </button>
                    <button type="button" class="btn btn-danger" onclick="eliminarAntecedente({{ $mi_antecedente->id }}, this)">
                        <i class="fas fa-trash"></i>
                    </button>
                </div>
            </td>
        </tr>
        @endforeach
    @else
        <div class="alert alert-info alert-dismissible">
            <h5><i class="icon fas fa-info"></i> ¡Sin antecedentes!</h5>
        </div>
    @endif
</table>
