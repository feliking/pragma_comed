@php
    $last_appointment = $medical_request
        ->appointments
        ->sortBy('id')
        ->last();
@endphp

<td>
    <div class="btn-group">
        @include('layouts.components.buttons.button_historial_timeline',[
                        'medical_request_code' => $medical_request->code
                    ])
        @if ($medical_request->medical_request_status_id == 2 || $medical_request->medical_request_status_id == 15)
            {{-- @if (!isset($asignar)) --}}
                <a class="btn btn-sm btn-{{ ($medical_request->medical_request_status_id == 2) ? 'success' : 'warning' }}" href="{{ route('consultas_medicas.edit', $medical_request->id) }}" role="button"><i class="fas fa-bookmark"></i></a>
            {{-- @endif --}}
            @include('layouts.components.buttons.button_remit',[
                        'medical_request_id' => $medical_request->id
                    ])
        @endif
        @include('layouts.components.buttons.button_historial',[
            'medical_request_id' => $medical_request->id
        ])
      </div>
</td>
<td>
    <span class="badge badge-{{ ($medical_request->medical_request_status_id == 2) ? 'info' : 'warning' }}" >
        {{ $medical_request->medical_requests_state->name }}
    </span>
</td>
<td>
    @if($last_appointment!=null)
        @switch($last_appointment->appointment_type_id)
            @case(1)
                <span class="badge badge-warning" data-toggle="tooltip" data-placement="top" title="La cita será presencial">
                    <i class="fa fa-walking"></i>
                </span>
                @break
            @case(2)
                <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="La cita de zoom se hara a traves del siguiente link: {{$last_appointment->variable_text}}">
                    <i class="fa fa-video"></i>
                </span>
                @break
            @case(3)
                <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="El número para la llamada es: {{$last_appointment->variable_text}}">
                    <i class="fab fa-whatsapp"></i>
                </span>
                @break
            @default
                @break
        @endswitch
        <span data-attention_date="{{$medical_request->consulta_proxima->format('Y/m/d')}}"></span>
        {{$medical_request->consulta_proxima->format('d/m/Y H:i A')}}
    @endif
</td>
<td>
    @if($medical_request->last_tracing_day!='S/S')
        {{$medical_request->last_tracing_day}}
    @else
        <span class="badge badge-warning">
            S/S
        </span>
    @endif
</td>
<td>
    {{$medical_request->code}}
</td>
<td>
    @if($medical_request->employee!=null)
        <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
        {{$medical_request->employee->full_name}}
    @else
        @if($medical_request->history_valid_status)
            <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
        @else
            <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
        @endif
        {{$medical_request->full_name}}
    @endif
</td>
<td>
    {{$medical_request->full_ci}}
</td>
<td>
    {{$medical_request->risk_type->name}}
</td>
<td>
    @if($medical_request->employee!=null)
        <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
        {{$medical_request->employee->enterprise->nombre}}
    @else
        @if($medical_request->history_valid_status)
            <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
        @else
            <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
        @endif
        {{$medical_request->enterprise}}
    @endif
    <input type="hidden" id="empresa_name_{{ $medical_request->id }}" value="{{ $medical_request->enterprise }}">
</td>
<td>
    @if($medical_request->email!=null)
        {{$medical_request->email}}
    @else
        <span class="badge badge-warning">
            S/N
        </span>
    @endif
</td>
<td>
    <input type="hidden" id="employee_code_{{ $medical_request->id }}" value="{{ $medical_request->employee_code }}">
    {{$medical_request->employee_code}}
</td>
<td>
    @if($medical_request->whatsapp_flag)
        <div class="badge badge-success">
            <i class="fab fa-whatsapp"></i>
        </div>
    @endif
    {{$medical_request->phone_number}}
</td>
