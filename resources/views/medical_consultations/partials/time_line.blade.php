<div class="timeline timeline-inverse">
    @php
        $cantidad_atenciones = 0;
    @endphp
    @foreach ($history as $histo)
    @php
        $cantidad_atenciones++;
    @endphp
    @if ($histo->medical_request_status_id != 15)
    <div class="time-label" style="text-align:left;">
        <span class="bg-red" >
            @if ($histo->medical_request_status->id == 8 || $histo->medical_request_status->id == 9 || $histo->medical_request_status->id == 10 || $histo->medical_request_status->id == 11)
                {{ $histo->attention->attention_date->format('d/m/Y') }}
            @else
                {{ $histo->created_at->format('d/m/Y') }}
            @endif
        </span>
    </div>

    <div>
        <i class="fas fa-user bg-blue"></i>
        <div class="timeline-item">
            <span class="time"><i class="fas fa-clock"></i> {{ $histo->created_at->format('H:i') }}</span>
            <h3 class="timeline-header" style="text-align:left;">
                <a href="#">{{ isset($histo->user) ? $histo->user->name: 'Sistema' }}:</a> <span class="text-success">{{ $histo->medical_request_status->name }}</span>
            </h3>

            <div class="timeline-body" style="text-align:left;">
                {{ $histo->comment }}
                @if ($histo->medical_request_status->id == 8 || $histo->medical_request_status->id == 9 || $histo->medical_request_status->id == 10 || $histo->medical_request_status->id == 11)
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1">
                                    <i class="fas fa-heartbeat"></i>
                                </span>

                                <div class="info-box-content">
                                    <span class="info-box-text"> FC</span>
                                    <span class="info-box-number">
                                        {{ $histo->attention->fc }}<small></small>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1">
                                    <i class="fas fa-lungs"></i>
                                </span>

                                <div class="info-box-content">
                                    <span class="info-box-text">FR</span>
                                    <span class="info-box-number">
                                        {{ $histo->attention->fr }}<small></small>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1">
                                    <i class="fas fa-thermometer-three-quarters"></i>
                                </span>

                                <div class="info-box-content">
                                    <span class="info-box-text">T</span>
                                    <span class="info-box-number">
                                        {{ $histo->attention->t }}<small>ºC</small>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1">
                                    <i class="fas fa-pump-medical"></i>
                                </span>

                                <div class="info-box-content">
                                    <span class="info-box-text">PA</span>
                                    <span class="info-box-number">
                                        {{ $histo->attention->pa }}<small></small>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1">
                                    <i class="fas fa-head-side-cough"></i>
                                </span>

                                <div class="info-box-content">
                                    <span class="info-box-text">SaO2</span>
                                    <span class="info-box-number">
                                        {{ $histo->attention->sao2 }}<small>%</small>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1">
                                    <i class="fas fa-stethoscope"></i>
                                </span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Otros</span>
                                    <span class="info-box-number">
                                        {{ $histo->attention->otros }}<small></small>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-5 col-sm-3">
                            <div class="nav flex-column nav-tabs h-100" id="v-pills-tab-{{ $histo->id }}" role="tablist" aria-orientation="vertical">
                                @if(\Illuminate\Support\Facades\Auth::user()->role_id == 3)
                                <a class="nav-link {{\Illuminate\Support\Facades\Auth::user()->role_id == 3?'active':''}}" id="v-pills-home-tab-{{ $histo->id }}" data-toggle="pill" href="#v-pills-home-{{ $histo->id }}" role="tab" aria-controls="v-pills-home-{{ $histo->id }}" aria-selected="{{\Illuminate\Support\Facades\Auth::user()->role_id == 3?'true':'false'}}">Historia de la enfermedad</a>
                                <a class="nav-link" id="v-pills-profile-tab-{{ $histo->id }}" data-toggle="pill" href="#v-pills-profile-{{ $histo->id }}" role="tab" aria-controls="v-pills-profile-{{ $histo->id }}" aria-selected="false">Examen físico</a>
                                <a class="nav-link" id="v-pills-messages-tab-{{ $histo->id }}" data-toggle="pill" href="#v-pills-messages-{{ $histo->id }}" role="tab" aria-controls="v-pills-messages-{{ $histo->id }}" aria-selected="false">Diagnostico</a>
                                @endif
                                <a class="nav-link {{\Illuminate\Support\Facades\Auth::user()->role_id == 3?'':'active'}}" id="v-pills-settings-tab-{{ $histo->id }}" data-toggle="pill" href="#v-pills-settings-{{ $histo->id }}" role="tab" aria-controls="v-pills-settings-{{ $histo->id }}" aria-selected="{{\Illuminate\Support\Facades\Auth::user()->role_id == 3?'false':'true'}}">Tratamiento</a>
                                <a class="nav-link" id="v-pills-obs-tab-{{ $histo->id }}" data-toggle="pill" href="#v-pills-obs-{{ $histo->id }}" role="tab" aria-controls="v-pills-obs-{{ $histo->id }}" aria-selected="false">Observacion</a>
                            </div>
                        </div>
                        <div class="col-9">
                            <div class="tab-content" id="v-pills-tabContent">
                                @if(\Illuminate\Support\Facades\Auth::user()->role_id == 3)
                                <div class="tab-pane fade {{\Illuminate\Support\Facades\Auth::user()->role_id == 3?'show active':''}}" id="v-pills-home-{{ $histo->id }}" role="tabpanel" aria-labelledby="v-pills-home-tab-{{ $histo->id }}">
                                    {{ $histo->attention->current_history }}
                                </div>
                                <div class="tab-pane fade" id="v-pills-profile-{{ $histo->id }}" role="tabpanel" aria-labelledby="v-pills-profile-tab-{{ $histo->id }}">
                                    {{ $histo->attention->physical_exam }}
                                </div>
                                <div class="tab-pane fade" id="v-pills-messages-{{ $histo->id }}" role="tabpanel" aria-labelledby="v-pills-messages-tab-{{ $histo->id }}">
                                    {{ $histo->attention->diagnosis }}
                                </div>
                                @endif
                                <div class="tab-pane fade {{\Illuminate\Support\Facades\Auth::user()->role_id == 3?'':'show active'}}" id="v-pills-settings-{{ $histo->id }}" role="tabpanel" aria-labelledby="v-pills-settings-tab-{{ $histo->id }}">
                                    {{ $histo->attention->treatment }}
                                </div>
                                <div class="tab-pane fade" id="v-pills-obs-{{ $histo->id }}" role="tabpanel" aria-labelledby="v-pills-obs-tab-{{ $histo->id }}">
                                    {{ $histo->tracing->comment }}
                                </div>
                            </div>
                          </div>
                    </div>
                @endif

                <!-- SEGUIMIENTO -->
                @if ($histo->tracing != null)
                    <hr>
                    <div class="row">
                        <div class="col-5 col-sm-3">
                            <div class="nav flex-column nav-tabs h-100" id="v-pills-tab-s-{{ $histo->tracing->id }}" role="tablist" aria-orientation="vertical">
                                <a class="nav-link active" id="v-pills-situation-tab-{{ $histo->tracing->id }}" data-toggle="pill" href="#v-pills-situation-{{ $histo->tracing->id }}" role="tab" aria-controls="v-pills-situation-{{ $histo->tracing->id }}" aria-selected="true">
                                    Situación
                                </a>
                                @if ($histo->tracing->riskState != null)
                                <a class="nav-link" id="v-pills-risk-tab-{{ $histo->tracing->id }}" data-toggle="pill" href="#v-pills-risk-{{ $histo->tracing->id }}" role="tab" aria-controls="v-pills-risk-{{ $histo->tracing->id }}" aria-selected="false">
                                    Riesgo
                                </a>
                                @endif
                                @if (count($histo->tracing->complementaryExams) > 0)
                                <a class="nav-link" id="v-pills-exam-tab-{{ $histo->tracing->id }}" data-toggle="pill" href="#v-pills-exam-{{ $histo->tracing->id }}" role="tab" aria-controls="v-pills-exam-{{ $histo->tracing->id }}" aria-selected="false">
                                    Exámenes complementarios
                                </a>
                                @endif
                            </div>
                        </div>

                        <div class="col-9">
                            <div class="tab-content" id="v-pills-tab-sContent">
                                <div class="tab-pane fade show active" id="v-pills-situation-{{ $histo->tracing->id }}" role="tabpanel" aria-labelledby="v-pills-situation-tab-{{ $histo->tracing->id }}">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <b>Etapa de tratamiento</b><br>
                                            {{ $histo->tracing->treatmentStage->name }}
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <b>Tipo de riesgo</b><br>
                                            {{ $histo->tracing->riskType->name }}
                                        </div>
                                    </div>
                                </div>
                                @if ($histo->tracing->riskState != null)
                                <div class="tab-pane fade" id="v-pills-risk-{{ $histo->tracing->id }}" role="tabpanel" aria-labelledby="v-pills-risk-tab-{{ $histo->tracing->id }}">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <b>Estado de riesgo</b><br>
                                            {{ $histo->tracing->riskState->name }}
                                        </div>
                                    </div>
                                    @if (count($histo->tracing->diseaseCriterias) > 0)
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <b>Síntomas</b><br>
                                        </div>
                                    </div>
                                    <div class="row">
                                        @php
                                         $total = 0;
                                        @endphp
                                        @foreach ($histo->tracing->diseaseCriterias as $disease_criteria)
                                            @php
                                                $total = $total + $disease_criteria->weighing;
                                            @endphp
                                        <div class="col-md-4 col-sm-6">
                                            <i class="far fa-check-square"></i> {{ $disease_criteria->syntom }}
                                        </div>
                                        @endforeach
                                    </div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12">
                                                @php
                                                    $riesgo = \App\WeightingLevel::where('last_level_flag','1')->first();

                                                    if($total < $riesgo->minimum)
                                                        $riesgo = \App\WeightingLevel::where('minimum', '<=', $total)
                                                                ->where('maximum', '>=', $total)
                                                                ->first();
                                                @endphp
                                                <b>Ponderación: </b> {{$total}} <span class="badge" style="background-color: {{$riesgo->color}}">{{$riesgo->name}}</span><br>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                @endif
                                @if (count($histo->tracing->complementaryExams) > 0)
                                <div class="tab-pane fade" id="v-pills-exam-{{ $histo->tracing->id }}" role="tabpanel" aria-labelledby="v-pills-exam-tab-{{ $histo->tracing->id }}">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Lab.</th>
                                                        <th>Tipo</th>
                                                        <th>Costo (Bs)</th>
                                                        <th>Nro. Fact.</th>
                                                        <th>Doc.</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($histo->tracing->complementaryExams as $complementary_exam)
                                                    <tr>
                                                        <td>
                                                            {{ $complementary_exam->laboratory->name }}
                                                        </td>
                                                        <td>
                                                            @if($complementary_exam->testType != null)
                                                            {{ $complementary_exam->testType->name }}
                                                            @else
                                                                S/R
                                                            @endif
                                                        </td>
                                                        <td>
                                                            {{ $complementary_exam->cost }}
                                                            @if ($complementary_exam->enterprise_flag)
                                                            <i class="fas fa-university"></i>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            {{ $complementary_exam->bill_number }}
                                                        </td>
                                                        <td>
                                                        @if (count($complementary_exam->files) > 0)

                                                            <div class="btn-group" role="group">
                                                                <a href="{{ route('image.downloadFile', $complementary_exam->id) }}" class="btn btn-sm btn-default" title="Descargar Archivo">
                                                                    <i class="fa fa-download"></i>
                                                                </a>
                                                                @if($complementary_exam->files->first()->mime == 'application/pdf' || substr($complementary_exam->files->first()->mime, 0, strlen("image/")) === "image/")
                                                                <button title="Ver Archivo" class="btn btn-sm btn-info" onclick="mostrarArchivos({{$complementary_exam->id}})">
                                                                    <i class="fa fa-eye"></i>
                                                                </button>
                                                                @endif
                                                            </div>

                                                        @else
                                                            <span class="badge badge-warning"> Sin Archivos</span>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                          </div>
                    </div>
                @endif
                <!-- SEGUIMIENTO -->

            </div>
            <div class="timeline-footer">
                @if ($cantidad_atenciones == 1 && $histo->medical_request_status->id != 4)
                <div class="row">
                    <dt class="col-sm-4">Próxima consulta</dt>
                    <dd class="col-sm-8">
                        <p>
                            @if ($fecha_proxima_atencion != null)
                                {{ $fecha_proxima_atencion->next_attention_date->format('d/m/Y H:i A') }}
                            @else
                                Sin fecha
                            @endif
                        </p>
                    </dd>
                </div>
                @endif
            </div>
        </div>
    </div>
    @endif
    @endforeach
    <div>
        <i class="fas fa-clock bg-gray"></i>
    </div>
</div>
<script>

</script>
{{--<script src="{{asset('js/medical_consultations/index_cerradas.js')}}"></script>--}}
<script src="{{asset('js/components/button_files.js')}}"></script>
