<div class="row">
    {{-- <div class="card-deck mb-3"> --}}
        <div class="col-6">
            @include('medical_request.partials.div_card_pacient_data')
        </div>
        @include('medical_consultations.partials.antecedents')
    {{-- </div> --}}
</div>

<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">HISTORIAL CLÍNICO</h3>
            </div>
            <div class="card-body" style="transform: traslate(0,0); height: 1150px;overflow: auto;padding: 10px">
                <div class="col-12" style="">
                    @if ($cantidad == 0)
                        <div class="alert alert-light" role="alert">
                            Sin registros
                        </div>
                    @else
                        @if ($historial_total)
                            @include('medical_consultations.partials.time_line_employee', ['medical_request' => $medical_requests ])
                        @else
                            @include('medical_consultations.partials.time_line', ['history' => $historial_requests ])
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-sm-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">FORMULARIO DE ATENCIÓN</h3>
            </div>
            {!! Form::model($medical_request, [
                'id'           => 'frm_atencion',
                'route'        => ['consultas_medicas.update', $medical_request->id],
                'method'       => 'put',
                'autocomplete' => "off",
                'files'        => true
            ]) !!}

            <div class="card-body">
                @if (!is_null($mi_mje))
                    <div class="alert alert-warning alert-dismissible">
                        <h5><i class="icon fas fa-exclamation-triangle"></i> Advertencia</h5>
                        <p>El paciente se encuentra en <strong>{{ $mi_mje }}</strong></p>
                    </div>
                @endif
                <input type="hidden" name="presave" value="0" />
                <div class="form-row">
                    <div class="form-group col-md-6 col-sm-12 pb-0 mb-0">
                        <label>{{ __('*Fecha de atención:') }}</label>
                        <div class="input-group">
                            <input type="text" class="form-control @error('fecha_consulta') is-invalid @enderror" id="fecha_consulta" name="fecha_consulta" value="{{ old('fecha_consulta') }}">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                        </div>
                        @if ($diferencia_fecha)
                            <div class="alert alert-info py-0 mb-0">
                                <i class="icon fas fa-info"></i>Proxima atencion <strong>{{ $medical_request->consulta_proxima->format('d/m/Y') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="form-group col-md-6 col-sm-12" id="wrapper-status_id">
                        <label for="status_id" class="control-label col-12">{{ __('*Estado de la consulta') }}</label>
                        <div class="form-group">
                            <select id="medical_status_id" required class="form-control select2 @error('medical_status_id') is-invalid @enderror" style="width: 100%;" name="medical_status_id">
                                <option></option>
                                @foreach($medical_request_status as $status)
                                    @if ($medical_request->medical_request_status_id == 15)
                                    <option value="{{$status->id}}" 
                                        {{-- @if ($medical_request->last_appointment->appointment_type_id==2&&$status->id==11)
                                        selected='selected'
                                        @else
                                        @if ($medical_request->last_appointment->appointment_type_id==3&&$status->id==10)
                                        selected='selected'
                                        @else
                                        @if ($medical_request->last_appointment->appointment_type_id==1&&$status->id==9)
                                            selected='selected'
                                        @else
                                            @if ($status->id==9)
                                            selected='selected'
                                            @endif
                                        @endif
                                            
                                        @endif
                                            
                                        @endif --}}
                                        {{-- @switch($medical_request->last_appointment->appointment_type_id)
                                        @case(1 && $status->id==9)
                                            selected='selected'
                                            @break
                                        @case(2 && $status->id==11)
                                            selected='selected'
                                            @break
                                        @case(3 && $status->id==10)
                                            selected='selected'
                                            @break
                                        @case(4 && $status->id==9)
                                            selected='selected'
                                            @break
                                        @default
                                        @if ($status->id==9)
                                        selected='selected'
                                        @endif
                                            
                                        @endswitch --}}
                                        >
                                        {{$status->name}}
                                    </option>
                                    
                                    @else
                                    <option value="{{$status->id}}" {{($status->id==9) ? 'selected':'' }}>{{$status->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <span class="help-block">
                            <strong></strong>
                        </span>
                        @if ($errors->has('medical_status_id'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('medical_status_id') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="d-flex bd-highlight">
                    <div>
                        <label for="signos_vitales" class="control-label">{{ __('Signos vitales') }}</label>
                    </div>
                    <div class="ml-auto">
                        <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-info btn-sm" onclick="RecargaSignosV({{ $medical_request->id }})">
                                <i class="fas fa-redo-alt"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-row" id="signos_vitales">
                    @include('medical_consultations.partials.form_signos_vitales')
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6 col-sm-12">
                        <label for="current_history" class="control-label col-12 my-0">{{ __('Historia de la enfermedad actual') }}</label>
                        <div class="input-group mb-0">
                            <textarea name="current_history"  id="current_history" rows="3" value="{{ old('current_history') }}" class="form-control @error('current_history') is-invalid @enderror" placeholder="{{ __('Historia de la enfermedad actual') }}" >{{ ($medical_request->medical_request_status_id == 15) ? $attention->current_history : '' }}</textarea>
                            @if ($errors->has('current_history'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('current_history') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-md-6 col-sm-12">
                        <label for="physical_exam" class="control-label col-12 my-0">{{ __('Examen fisico') }}</label>
                        <div class="input-group mb-0">
                            <textarea name="physical_exam"  id="physical_exam" rows="3" value="" class="form-control" placeholder="{{ __('Examen fisico') }}" >{{ ($medical_request->medical_request_status_id == 15) ? $attention->physical_exam : '' }}</textarea>
                            @if ($errors->has('physical_exam'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('physical_exam') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6 col-sm-12">
                        <label for="diagnostico" class="control-label col-12 my-0">{{ __('Diagnostico') }}</label>
                        <div class="input-group mb-0">
                            <textarea name="diagnostico"  id="diagnostico" rows="3" value="" class="form-control" placeholder="{{ __('Diagnostico') }}" >{{ ($medical_request->medical_request_status_id == 15) ? $attention->diagnosis : '' }}</textarea>
                            @if ($errors->has('diagnostico'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('diagnostico') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-md-6 col-sm-12">
                        <label for="tratamiento" class="control-label col-12 my-0">{{ __('Tratamiento') }}</label>
                        <div class="input-group mb-0">
                            <textarea name="tratamiento" id="tratamiento" rows="3" value="" class="form-control" placeholder="{{ __('Tratamiento') }}" >{{ ($medical_request->medical_request_status_id == 15) ? $attention->treatment : '' }}</textarea>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('tratamiento') }}</strong>
                            </span>
                        </div>
                    </div>
                </div>


                <div class="card card-info card-outline">
                    <div class="card-header">
                        <h3 class="card-title"><b>DATOS DE SEGUIMIENTO</b></h3>
                    </div>
                    <div class="card-body">
                        @include('tracing.partials.form')
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="form-group">
                    <div class="col-xs-12 text-right">
                        <a class="btn btn-secondary" href="{{ route('consultas_medicas.index') }}" role="button"><i class="fas fa-arrow-left"></i> Volver</a>

                        <button type="button" onclick="printReceta('{{$medical_request->id}}')" class="btn btn-primary">
                            <i class="fa fa-file-pdf"></i> Imprimir receta
                        </button>

                        <button id="btn_presave" type="button" class="btn btn-info">
                            <i class="fa fa-save"></i> Pre Registrar
                        </button>

                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-save"></i> Registrar
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

