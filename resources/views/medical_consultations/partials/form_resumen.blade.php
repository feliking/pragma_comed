<div class="form-row">
    <div class="form-group col-md-6 col-sm-12">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" value="" id="chk_fecha_consulta" name="chk_fecha_consulta">
            <label class="form-check-label" for="chk_fecha_consulta">
                {{ __('Fecha de atención:') }}
            </label>
        </div>
        <div class="input-group">
            <input type="text" class="form-control" id="modal_fecha_consulta" name="modal_fecha_consulta" name="modal_fecha_consulta" value="" readonly="true">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
            </div>
        </div>
    </div>
    <div class="form-group col-md-6 col-sm-12">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" value="" id="chk_estado_consulta" name="chk_estado_consulta">
            <label class="form-check-label" for="chk_estado_consulta">
                {{ __('Estado de la consulta:') }}
            </label>
        </div>
        <div class="form-group">
            <select id="modal_medical_status_id" required class="form-control select2" style="width: 100%;" name="modal_medical_status_id" disabled="true">
                <option></option>
                @foreach($medical_request_status as $status)
                    <option value="{{$status->id}}">{{$status->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="form-row">
    <div class="col-sm-12 col-md-4">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" value="" id="chk_fc" name="chk_fc">
            <label class="form-check-label" for="chk_fc">
                FC
            </label>
        </div>
        <div class="form-group">
            <input name="modal_fc" id="modal_fc" type="text" value="" class="form-control text-right" placeholder="" data-inputmask-regex="^[0-9]{1,3}(\.\d{1,2})?$" readonly="">
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" value="" id="chk_fr" name="chk_fr">
            <label class="form-check-label" for="chk_fr">
                FR
            </label>
        </div>
        <div class="form-group">
            <input name="modal_fr" id="modal_fr" type="text" value="" class="form-control text-right" placeholder="" data-inputmask-regex="^[0-9]{1,2}(\.\d{1,2})?$" readonly="true">
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" value="" id="chk_t" name="chk_t">
            <label class="form-check-label" for="chk_t">
                Tº
            </label>
        </div>
        <div class="form-group">
            <input name="modal_t" id="modal_t" type="text" value="" class="form-control text-right" placeholder="" data-inputmask-regex="^[0-9]{1,2}(\.\d{1,2})?$" readonly="true">
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" value="" id="chk_pa" name="chk_pa">
            <label class="form-check-label" for="chk_pa">
                PA
            </label>
        </div>
        <div class="form-group">
            <input name="moda_pa" id="moda_pa" type="text" value="" class="form-control text-right" placeholder="" data-inputmask-regex="^[0-9]{1,3}(\/\d{1,3})?$" readonly="true">
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" value="" id="chk_sao2" name="chk_sao2">
            <label class="form-check-label" for="chk_sao2">
                SaO2
            </label>
        </div>
        <div class="form-group">
            <input name="modal_sao2" id="modal_sao2" type="text" value="" class="form-control text-right " placeholder="" data-inputmask-regex="^[0-9]{1,3}?$" max="99" maxlength="2" readonly="true">
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" value="" id="chk_otros" name="chk_otros" >
            <label class="form-check-label" for="chk_otros">
                Otros
            </label>
        </div>
        <div class="form-group">
            <input name="modal_other" id="modal_other" type="text" value="" class="form-control text-right" placeholder="" maxlength="7" readonly="true">
        </div>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6 col-sm-12">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" value="" id="chk_current_history" name="chk_current_history">
            <label class="form-check-label" for="chk_current_history">
                Historia de la enfermedad actual
            </label>
        </div>
        <div class="input-group mb-0">
            <textarea name="modal_current_history"  id="modal_current_history" rows="3" value="" class="form-control" placeholder="" readonly="true"></textarea>
        </div>
    </div>
    <div class="form-group col-md-6 col-sm-12">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" value="" id="chk_physical_exam" name="chk_physical_exam">
            <label class="form-check-label" for="chk_physical_exam">
                Examen fisico
            </label>
        </div>
        <div class="input-group mb-0">
            <textarea name="modal_physical_exam"  id="modal_physical_exam" rows="3" value="" class="form-control" placeholder="" readonly="true"></textarea>
        </div>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6 col-sm-12">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" value="" id="chk_diagnostico" name="chk_diagnostico">
            <label class="form-check-label" for="chk_diagnostico">
                Diagnostico
            </label>
        </div>
        <div class="input-group mb-0">
            <textarea name="modal_diagnostico"  id="modal_diagnostico" rows="3" value="" class="form-control" placeholder="" readonly="true"></textarea>
        </div>
    </div>
    <div class="form-group col-md-6 col-sm-12">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" value="" id="chk_tratamiento" name="chk_tratamiento">
            <label class="form-check-label" for="chk_tratamiento">
                Tratamiento
            </label>
        </div>
        <div class="input-group mb-0">
            <textarea name="modal_tratamiento"  id="modal_tratamiento" rows="3" value="" class="form-control" placeholder="" readonly="true"></textarea>
        </div>
    </div>
</div>
