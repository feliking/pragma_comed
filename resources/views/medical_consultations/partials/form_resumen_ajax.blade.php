<div class="form-row">
    <div class="form-group col-md-6 col-sm-12">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" value="" id="chk_fecha_consulta" name="chk_fecha_consulta" checked="">
            <label class="form-check-label" for="chk_fecha_consulta">
                {{ __('Fecha de atención:') }}
            </label>
        </div>
        <div class="input-group">
            <input type="text" class="form-control" id="modal_fecha_consulta" name="modal_fecha_consulta" name="modal_fecha_consulta" value="{{ $datos->fecha_consulta }}" disabled="true">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
            </div>
        </div>
    </div>
    <div class="form-group col-md-6 col-sm-12">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" value="" id="chk_estado_consulta" name="chk_estado_consulta">
            <label class="form-check-label" for="chk_estado_consulta">
                {{ __('Estado de la consulta:') }}
            </label>
        </div>
        <div class="form-group">
            <select id="modal_medical_status_id" required class="form-control select2" style="width: 100%;" name="modal_medical_status_id" disabled="true">
                <option></option>
                @foreach($medical_request_status as $status)
                @if ($status->id == $datos->medical_status_id)
                    <option  selected="true" value="{{$status->id}}">{{$status->name}}</option>
                @else
                    <option value="{{$status->id}}">{{$status->name}}</option>
                @endif
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="form-row">
    <div class="col-sm-12 col-md-12">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" value="" id="chk_signos_vitales" name="chk_signos_vitales" @if (is_null($datos->fc) && is_null($datos->fr) && is_null($datos->t) && is_null($datos->pa) && is_null($datos->sao2) && is_null($datos->other))
                disabled
            @endif>
            <label class="form-check-label" for="chk_signos_vitales">
                Signos Vitales
            </label>
        </div>
    </div>
</div>
<div class="form-row">
    <div class="col-sm-12 col-md-2">
        <label class="form-check-label" for="chk_fc">
            FC
        </label>
        <div class="form-group">
            <input name="modal_fc" id="modal_fc" type="text" value="{{ $datos->fc }}" class="form-control text-right" placeholder="" data-inputmask-regex="^[0-9]{1,3}(\.\d{1,2})?$" disabled="">
        </div>
    </div>
    <div class="col-sm-12 col-md-2">
        <label class="form-check-label" for="">
            FR
        </label>
        <div class="form-group">
            <input name="modal_fr" id="modal_fr" type="text" value="{{ $datos->fr }}" class="form-control text-right" placeholder="" data-inputmask-regex="^[0-9]{1,2}(\.\d{1,2})?$" disabled="true">
        </div>
    </div>
    <div class="col-sm-12 col-md-2">
        <label class="form-check-label" for="">
            Tº
        </label>
        <div class="form-group">
            <input name="modal_t" id="modal_t" type="text" value="{{ $datos->t }}" class="form-control text-right" placeholder="" data-inputmask-regex="^[0-9]{1,2}(\.\d{1,2})?$" disabled>
        </div>
    </div>
    <div class="col-sm-12 col-md-2">

        <label class="form-check-label" for="">
            PA
        </label>

        <div class="form-group">
            <input name="moda_pa" id="moda_pa" type="text" value="{{ $datos->pa }}" class="form-control text-right" placeholder="" data-inputmask-regex="^[0-9]{1,3}(\/\d{1,3})?$" disabled="true">
        </div>
    </div>
    <div class="col-sm-12 col-md-2">
        <label class="form-check-label" for="chk_sao2">
            SaO2
        </label>

        <div class="form-group">
            <input name="modal_sao2" id="modal_sao2" type="text" value="{{ $datos->sao2 }}" class="form-control text-right " placeholder="" data-inputmask-regex="^[0-9]{1,3}?$" max="99" maxlength="2" disabled="true">
        </div>
    </div>
    <div class="col-sm-12 col-md-2">
        <label class="form-check-label" for="chk_otros">
            Otros
        </label>
        <div class="form-group">
            <input name="modal_other" id="modal_other" type="text" value="{{ $datos->other }}" class="form-control text-right" placeholder="" maxlength="7" disabled>
        </div>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6 col-sm-12">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" value="" id="chk_current_history" name="chk_current_history" {{ is_null($datos->current_history) ? 'disabled' : '' }}>
            <label class="form-check-label" for="chk_current_history">
                Historia de la enfermedad actual
            </label>
        </div>
        <div class="input-group mb-0">
            <textarea name="modal_current_history"  id="modal_current_history" rows="3" value="" class="form-control" placeholder="" disabled="">{{ $datos->current_history }}</textarea>
        </div>
    </div>
    <div class="form-group col-md-6 col-sm-12">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" value="" id="chk_physical_exam" name="chk_physical_exam" {{ is_null($datos->physical_exam) ? 'disabled' : '' }}>
            <label class="form-check-label" for="chk_physical_exam">
                Examen fisico
            </label>
        </div>
        <div class="input-group mb-0">
            <textarea name="modal_physical_exam"  id="modal_physical_exam" rows="3" value="" class="form-control" placeholder="" disabled="">{{ $datos->physical_exam }}</textarea>
        </div>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6 col-sm-12">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" value="" id="chk_diagnostico" name="chk_diagnostico" {{ is_null($datos->diagnostico) ? 'disabled' : '' }}>
            <label class="form-check-label" for="chk_diagnostico">
                Diagnostico
            </label>
        </div>
        <div class="input-group mb-0">
            <textarea name="modal_diagnostico"  id="modal_diagnostico" rows="3" value="" class="form-control" placeholder="" disabled>{{ $datos->diagnostico }}</textarea>
        </div>
    </div>
    <div class="form-group col-md-6 col-sm-12">
        <div class="icheck-primary">
            <input class="form-check-input" type="checkbox" value="" id="chk_tratamiento" name="chk_tratamiento"  {{ is_null($datos->tratamiento) ? 'disabled' : ' checked="true"' }}>
            <label class="form-check-label" for="chk_tratamiento">
                Tratamiento
            </label>
        </div>
        <div class="input-group mb-0">
            <textarea name="modal_tratamiento"  id="modal_tratamiento" rows="3" value="" class="form-control" placeholder="" disabled>{{ $datos->tratamiento }}</textarea>
        </div>
    </div>
</div>
<div class="card card-info card-outline">
    <div class="card-header">
        <h3 class="card-title"><b>DATOS DE SEGUIMIENTO</b></h3>
    </div>
    <div class="card-body">
        <div class="form-row">
            <div class="form-group col-md-12">
                <div class="icheck-primary">
                    <input class="form-check-input" type="checkbox" value="" id="chk_comment_tracing" name="chk_comment_tracing" {{ is_null($datos->comment_tracing) ? 'disabled' : '' }}>
                    <label class="form-check-label" for="chk_comment_tracing">
                        Comentario u Observación
                    </label>
                </div>
                <div class="input-group mb-0">
                    <textarea name="modal_comment_tracing" rows="3" class="form-control @error('comment_tracing') is-invalid @enderror" placeholder="{{ __('Comentario u Observación') }}" disabled="">{{ $datos->comment_tracing }}</textarea>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12 col-sm-12">
                <div class="icheck-primary">
                    <input class="form-check-input" type="checkbox" value="" id="chk_treatment_stage_id" name="chk_treatment_stage_id" {{ is_null($datos->treatment_stage_id) ? 'disabled' : '' }}>
                    <label class="form-check-label" for="chk_treatment_stage_id">
                        Etapa de Tratamiento
                    </label>
                </div>
                <div class="input-group mb-0">
                    {!! Form::select('modal_treatment_stage_id', $treatment_stages, $datos->treatment_stage_id, ['class' => 'form-control select2', 'style'=>'with: 100%;', 'disabled' => 'true']) !!}
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6 col-sm-12">
                <div class="icheck-primary">
                    <input class="form-check-input" type="checkbox" value="" id="chk_risk_type_id" name="chk_risk_type_id" {{ is_null($datos->risk_type_id) ? 'disabled' : '' }}>
                    <label class="form-check-label" for="chk_risk_type_id">
                        Tipos de Riesgo
                    </label>
                </div>
                <div class="input-group mb-0">
                    {!! Form::select('risk_type_id', $risk_types, $datos->risk_type_id, ['class' => 'form-control select2', 'style'=>'with: 100%;', 'disabled' => 'true']) !!}
                </div>
            </div>
            <div class="form-group col-md-6 col-sm-12">
                <div class="icheck-primary">
                    <input class="form-check-input" type="checkbox" value="" id="chk_risk_state_id" name="chk_risk_state_id" {{ is_null($datos->risk_state_id) ? 'disabled' : '' }}>
                    <label class="form-check-label" for="chk_risk_state_id">
                        Estado de Riesgo
                    </label>
                </div>
                <div class="input-group mb-0">
                    {!! Form::select('modal_risk_state_id', $estados_riesgos, $datos->risk_state_id, ['class' => 'form-control select2', 'style'=>'with: 100%;', 'disabled' => 'true']) !!}
                </div>
            </div>
        </div>
        <div id="div_disaese_criteria">
            <div class="icheck-primary">
                <input class="form-check-input" type="checkbox" value="" id="chk_disease_criteria" name="chk_disease_criteria" {{ (isset($datos->disease_criteria) ) ? '' : 'disabled' }}>
                <label class="form-check-label" for="chk_disease_criteria">
                    Criterios
                </label>
            </div>
            <div class="input-group mb-0">
                @if (isset($datos->disease_criteria))
                    @if(count($disaeses) > 0)
                        <div class="form-row">
                            @foreach($disaeses as $disaese)
                            <div class="form-group col-md-3 col-sm-6">
                                <div class="icheck-primary">
                                    <input name="modal_disease_criteria[]" class="form-check-input" type="checkbox" value="{{$disaese->id}}" id="modal_disaese_{{$disaese->id}}" disabled="" {{ (in_array($disaese->id, $datos->disease_criteria)) ? 'checked' :'' }}>
                                    <label for="modal_disaese_{{$disaese->id}}">
                                        {{ $disaese->syntom }} ({{$disaese->weighing}})
                                    </label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="form-row">
                            <p><b>Ponderación:</b> <span id="modal_span_puntaje">{{ $disease_points }}</span> <span id="_modal_span_weigthing" class="badge"></span></p>
                        </div>
                    @else
                        <div class="alert alert-info" role="alert">
                            <h4 class="alert-heading"><i class="fa fa-info"></i>Sin Datos</h4>
                            <p>No existen datos de criterios ligados al riesgo seleccionado.</p>
                        </div>
                    @endif
                @else
                    @if(count($disaeses) > 0)
                        <div class="form-row">
                            @foreach($disaeses as $disaese)
                            <div class="form-group col-md-3 col-sm-6">
                                <div class="icheck-primary">
                                    <input name="modal_disease_criteria[]" class="form-check-input" type="checkbox" value="{{$disaese->id}}" id="modal_disaese_{{$disaese->id}}" disabled="">
                                    <label for="modal_disaese_{{$disaese->id}}">
                                        {{ $disaese->syntom }} ({{$disaese->weighing}})
                                    </label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="form-row">
                            <p><b>Ponderación:</b> <span id="modal_span_puntaje">{{ $disease_points }}</span> <span id="_modal_span_weigthing" class="badge"></span></p>
                        </div>
                    @else
                        <div class="alert alert-info" role="alert">
                            <h4 class="alert-heading"><i class="fa fa-info"></i>Sin Datos</h4>
                            <p>No existen datos de criterios ligados al riesgo seleccionado.</p>
                        </div>
                    @endif
                @endif
            </div>
        </div>
        <div class="form-row">
            <div class="icheck-primary">
                <input class="form-check-input" type="checkbox" value="" id="chk_laboratory" name="chk_laboratory" {{ isset($datos->laboratories) ? '':'disabled' }}>
                <label class="form-check-label" for="chk_laboratory" >
                    Laboratorio
                </label>
            </div>
            <div class="table-responsive">
                @if (isset($datos->laboratories))
                    <table class="table table-hover" id="modal_table_complementary_exams">
                        <thead>
                        <tr>
                            <th>Laboratorio</th>
                            <th>Tipo de Prueba</th>
                            <th>Costo</th>
                            <th>Número de Factura</th>
                            <th>¿La Empresa Pagó?</th>
                            <th>Archivo</th>
                        </tr>
                        </thead>
                        <tbody id="modal_tbody_complementary_exams">
                            @for ($i = 0; $i < count($datos->laboratories) ; $i++)
                                <tr>
                                    <td>
                                        @php
                                            $mi_laboratorio = $laboratorios->firstWhere('id',$datos->laboratories[$i]);
                                            $tipo_prueba = $aux_test_types->firstWhere('id', $datos->test_types[$i]);
                                        @endphp
                                        {{ $mi_laboratorio->name }}
                                    </td>
                                    <td>
                                        {{ $tipo_prueba->name }}
                                    </td>
                                    <td>
                                        {{ $datos->costs[$i] }}
                                    </td>
                                    <td>
                                        {{ $datos->bill_numbers[$i] }}
                                    </td>
                                    <td>
                                        @if ($datos->enterprise_pay_flags[$i] == 'true')
                                            <span class="badge badge-success">SI</span>
                                        @else
                                            <span class="badge badge-danger">NO</span>
                                        @endif
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            @endfor
                        </tbody>
                    </table>
                @else
                    <div id="modal_alert_table_complementary" class="alert alert-info mb-12" role="alert">
                        <h4 class="alert-heading"><i class="fa fa-info"></i> Sin Registro</h4>
                        <p>No hay pruebas de laboratorio registradas.</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
