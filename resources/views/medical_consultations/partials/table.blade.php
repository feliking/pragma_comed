<table id="medical_consultations_table" class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Op.</th>
            <th>Estado</th>
            <th>Sug. Prox. F.</th>
            <th>Días Aislamiento</th>
            <th>Cód. de Solicitud</th>
            <th>Nombre Completo</th>
            <th>CI</th>
            <th>Riesgo</th>
            <th>Empresa</th>
            <th>Email</th>
            <th>Cód. de Empleado</th>
            <th>Nro de Teléfono</th>
        </tr>
    </thead>

    <tbody>
    @foreach($medical_requests as $medical_request)
        <tr id="tr_medical_consultation_{{$medical_request->id}}">
            @include('medical_consultations.partials.tr_content',['medical_request'=>$medical_request])
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Op.</th>
            <th>Estado</th>
            <th>Sug. Prox. F.</th>
            <th>Días Aislamiento</th>
            <th>Cód. de Solicitud</th>
            <th>Empleado Asignado</th>
            <th>CI</th>
            <th>Riesgo</th>
            <th>Empresa</th>
            <th>Email</th>
            <th>Cód. de Empleado</th>
            <th>Nro de Teléfono</th>
        </tr>
    </tfoot>
</table>
<script>
    var sw = false;
    $(function() {
        var oTable_adm = $('#medical_consultations_table').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
           /*  "search": {
                "regex": true,
                "caseInsensitive": false,
            }, */
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            "columnDefs": [
                { "targets": 0, "bSortable": false },
                { "type": "html", "targets": 1 },
                { "type": "date", "targets": 2 },
                { "type": "html-num", "targets": 3 }
            ],
            //ESTA OPCION DE DOM ES PARA ESCOGER ELEMENTOS QUE APARECEN POR DEFECTO EN EL DATA TABLE
            "dom": 'lrtp',
            "pageLength": 10,
            "order": [[2, 'desc']],
            "aoColumnDefs": [{ "bSortable": false, "aTargets": [0] }],
            "language": {
                "decimal": ",",
                "thousands": ".",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoPostFix": "",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                "lengthMenu": "Mostrar _MENU_ registros",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "processing": "Procesando...",
                "search": "Buscar:",
                "searchPlaceholder": "Término de búsqueda",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla",
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },
        
            }
        });
        $('input#search_data').keyup(function () {
            oTable_adm.search($(this).val()).draw();
        });
        /* minDateFilter = "";
        maxDateFilter = "";
        $.fn.dataTableExt.afnFiltering.push(
        function(oSettings, aData, iDataIndex) {
            if (typeof aData._date == 'undefined') {
            aData._date = new Date(aData[0]).getTime();
            }

            if (minDateFilter && !isNaN(minDateFilter)) {
            if (aData._date < minDateFilter) {
                return false;
            }
            }

            if (maxDateFilter && !isNaN(maxDateFilter)) {
            if (aData._date > maxDateFilter) {
                return false;
            }
            }

            return true;
            }
        ); */
        /* $(document).ready(function() {
            $("#Date_search").val("");
        }); */

        /* $("#Date_search").daterangepicker({
        "locale": {
            "format": "YYYY-MM-DD",
            "separator": " to ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "Custom",
            "weekLabel": "W",
            "daysOfWeek": [
            "Su",
            "Mo",
            "Tu",
            "We",
            "Th",
            "Fr",
            "Sa"
            ],
            "monthNames": [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
            ],
            "firstDay": 1
        },
        "opens": "center",
        }, function(start, end, label) {
        maxDateFilter = end;
        minDateFilter = start;
        oTable_adm.draw();  
        }); */
        $('.datetimepicker-input').change(function() {
            
        });
        
        $('#init_date_field').datetimepicker({
            format: 'DD/MM/YYYY',
            /* buttons: {showClear: true},
            icons: {
                clear: 'fa fa-trash',
                close: 'fa fa-times'
            }, */
            locale: 'es',
            useCurrent: false,
            maxDate: moment(),
            //endDate: null
        });

        $('#end_date_field').datetimepicker({
            format: 'DD/MM/YYYY',
            locale: 'es',
            /* buttons: {showClear: true}, */
            useCurrent: false,
            //maxDate: moment(),
            //endDate: null
        });
        
        $('#clear').click(function() {
            $("#init_date_field").data("DateTimePicker").date(null);
        })
        $("#init_date_field").on("change.datetimepicker", function (e) {
            /* if(){ */
                
            /* } */
            //console.log(e.date._d,moment(e.date._d).format('DD/MM/YYYY') );
            if(e.date!=undefined){
                $('#end_date_field').datetimepicker('minDate', e.date);
                min = moment(e.date._d).format('YYYY/MM/DD') || 0;
                sw = true;
                oTable_adm.draw();
            }else{
                //$('#init_date_field').datetimepicker('clear');
                //$('#init_date_field').datetimepicker('destroy');
                var now = moment();
                //$("div#init_date_field").data("DateTimePicker").date(now);
                //$('#end_date_field').datetimepicker({'value': null});
                /* e._dates[0] = e.getMoment();
                e._viewDate = e.getMoment().locale(this._options.locale).clone(); */
                /* $('#init_date_field').datetimepicker("clear");
                $('#init_date_field').datetimepicker("destroy");
                $('#init_date_field').datetimepicker({ useCurrent: false }); */
                /* $('#init_date_field').datetimepicker({
                    format: 'DD/MM/YYYY',
                    locale: 'es',
                    useCurrent: false,
                    maxDate: moment(),
                }); */
            }
            
            //console.log($('#end_date_field').datetimepicker('minDate', e.date));
            
        });
        $("#end_date_field").on("change.datetimepicker", function (e) {
            
            if(e.date!=undefined){
                $('#init_date_field').datetimepicker('maxDate', e.date);
                max = moment(e.date._d).format('YYYY/MM/DD') || 0;
                sw = true;
                oTable_adm.draw();
            }else{
                //$('#end_date_field').datetimepicker('clear');
                //$('#end_date_field').datetimepicker('destroy');
                var now = moment();
                //$("#end_date_field").data("DateTimePicker").date(now);
                //$('#end_date_field').datetimepicker({'value': null});
                /* this._dates[0] = this.getMoment();
                this._viewDate = this.getMoment().locale(this._options.locale).clone(); */
                /* $('#end_date_field').data("DateTimePicker").clear();
                $('#end_date_field').datetimepicker("destroy");
                $('#end_date_field').datetimepicker({ useCurrent: false }); */
                /* $('#end_date_field').datetimepicker({
                    format: 'DD/MM/YYYY',
                    locale: 'es',
                    useCurrent: false,
                    maxDate: moment(),
                }); */
            }
            
        });
    });
    var min = '';
    var max = '';
    $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });

    // Set up your table
    /* table = $('#my-table').DataTable({
        paging: false,
        info: false
    }); */

    // Extend dataTables search
    $.fn.dataTableExt.afnFiltering.push(
        function(settings, data, dataIndex) {
            /* var min = $('#init_date').val();
            var max = $('#end_date').val(); */
            //console.log(min,max, settings, data, dataIndex);
            
            var createdAt = $($(data[2])[2]).data('attention_date') || 0; // Our date column in the table
            //console.log(createdAt, createdAt!=0, min);
            //console.log(data[2].substring(205,215));
            //console.log(createdAt);
            /* console.log(data._date);
            if (typeof data._date == 'undefined') {
                data._date = new Date(data[0]).getTime();
            }
            console.log(data._date); */
            /* var startDate   = moment(min, "DD/MM/YYYY");
            var endDate     = moment(max, "DD/MM/YYYY");
            var diffDate = moment(createdAt, "DD/MM/YYYY"); */
            //console.log(moment(createdAt).isSameOrAfter(min), moment(createdAt).isSameOrBefore(max));
            if(min != ""){
                //mins=moment(min).format("YYYY/MM/DD");
                //console.log(createdAt >= min, min/* , mins */, moment(createdAt).isSameOrAfter(min), createdAt);
            }
            
            if ((min != "" && max != "") && (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max)) && createdAt!=0) {
                return true;
            }else{
                //console.log('',sw);
                if ((min == "" && max == ""&& createdAt!=0) &&sw) {
                    return false;
                }
                if ((min != "" && max == "" && createdAt!=0) && moment(createdAt).isSameOrAfter(min)) {
                    //console.log('asdffff');
                    return true;
                }
                if ((min == "" && max != "" && createdAt!=0) && moment(createdAt).isSameOrBefore(max)) {
                    return true;
                }
            }
            if ((min != "" || max != "") && createdAt==0) {
                //console.log('dasddasdkasbhd');
                return true;
            }
            if ((min == "" || max == "" || createdAt==0) && !sw) {
                //console.log('ads', sw);
                return true;
            }
            /* if (
              (min == "" || max == "") ||
              (diffDate.isBetween(startDate, endDate))


            ) {  return true;  }
            return false; */
        }
    );

    // Re-draw the table when the a date range filter changes
    

    //$('#my-table_filter').hide();
</script>