<div class="col-sm-12 col-md-4">
    <div class="input-group mb-3">
        <div class="input-group-text">
            <span class="">FC</span>
        </div>
        <input name="fc" id="fc" type="text" value="{{ isset($attention_vital_sign->fc) ?  $attention_vital_sign->fc : ''}}" class="form-control text-right @error('fc') is-invalid @enderror" placeholder="" data-inputmask-regex="^[0-9]{1,3}(\.\d{1,2})?$">
    </div>
    @if ($errors->has('fc'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('fc') }}</strong>
    </span>
    @endif
</div>
<div class="col-sm-12 col-md-4">
    <div class="input-group mb-3">
        <div class="input-group-text">
            <span class="">FR</span>
        </div>
        <input name="fr" id="fr" type="text" value="{{ isset($attention_vital_sign->fr) ?  $attention_vital_sign->fr : ''}}" class="form-control text-right @error('fr') is-invalid @enderror" placeholder="" data-inputmask-regex="^[0-9]{1,2}(\.\d{1,2})?$">
    </div>
    @if ($errors->has('fr'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('fr') }}</strong>
    </span>
    @endif
</div>
<div class="col-sm-12 col-md-4">
    <div class="input-group mb-3">
        <div class="input-group-text">
            <span class="">T°</span>
        </div>
        <input name="t" id="t" type="text" value="{{ isset($attention_vital_sign->t) ? $attention_vital_sign->t:''}}" class="form-control text-right @error('t') is-invalid @enderror" placeholder="" data-inputmask-regex="^[0-9]{1,2}(\.\d{1,2})?$">
    </div>
    @if ($errors->has('t'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('t') }}</strong>
    </span>
    @endif
</div>
<div class="col-sm-12 col-md-4">
    <div class="input-group mb-3">
        <div class="input-group-text">
            <span class="">PA</span>
        </div>
        <input name="pa" id="pa" type="text" value="{{ isset($attention_vital_sign->pa) ? $attention_vital_sign->pa:''}}" class="form-control text-right @error('pa') is-invalid @enderror" placeholder="" data-inputmask-regex="^[0-9]{1,3}(\/\d{1,3})?$">
    </div>
    @if ($errors->has('pa'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('pa') }}</strong>
    </span>
    @endif
</div>
<div class="col-sm-12 col-md-4">
    <div class="input-group mb-3">
        <div class="input-group-text">
            <span class="">SaO2</span>
        </div>
        <input name="sao2" id="sao2" type="text" value="{{ isset($attention_vital_sign->sao2) ? $attention_vital_sign->sao2:''}}" class="form-control text-right @error('sao2') is-invalid @enderror" placeholder="" data-inputmask-regex="^[0-9]{1,3}?$" max="99" maxlength="2">
    </div>
    @if ($errors->has('sao2'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('sao2') }}</strong>
    </span>
    @endif
</div>
<div class="col-sm-12 col-md-4">
    <div class="input-group mb-3">
        <div class="input-group-text">
            <span class="">otros</span>
        </div>
        <input name="other" id="other" type="text" value="{{ isset($attention_vital_sign->otros) ? $attention_vital_sign->otros:''}}" class="form-control text-right @error('other') is-invalid @enderror" placeholder="" maxlength="7">
    </div>
    @if ($errors->has('other'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('other') }}</strong>
    </span>
    @endif
</div>
