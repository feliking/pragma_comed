@extends('layouts.app_menu')

@section('styles')
    <style>
        .dataTables_filter {
            display: none !important;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Registro de atención</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            @include('medical_consultations.partials.form_attention')
        </div>
    </section>
    {{-- modal de antecedentes --}}
    @include('medical_consultations.modals.modal_antecedente')
    @include('medical_consultations.modals.modal_resumen')
    @include('complementary_exams.modals.modal_complementary_exam')
    @include('layouts.components.modals.modal_files')
@endsection

@section('script')
    @parent
    <script src="{{ asset('adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('js/medical_consultations/edit.js') }}"></script>
    <script src="{{ asset('/js/jquery.mask.min.js') }}"></script>
    <script>
        const assingAecenden = @json(route('antecedentes.store'));
        const editAntecenden = @json(route('antecedentes.update',['_ID']));
        const deleteAntecenden = @json(route('antecedentes.destroy',['_ID']));
        const getResumen = @json(route('consultas_medicas.get_resumen'));
        const showHistoryFiles = @json(route('mostrar_archivos.showFiles','_id'));
        const getSignosVitales = @json(route('consultas_medicas.get_signos_vitales','_ID'));
        const printRecetaRoute = @json(route('consultas_medicas.imprimir_receta'));

        $(document).ready(function() {
            $('a#mis_consultas').addClass('active');
        });
        $(document).ready(function() {
            $('[data-mask]').inputmask();

            $('#fecha_consulta').daterangepicker({
                "singleDatePicker": true,
                "showDropdowns": true,
                "autoApply": false,
                "linkedCalendars": false,
                "showCustomRangeLabel": false,
                "drops": "auto",
                "maxDate": "{{ $fecha_actual->format('d/m/Y') }}",
                locale: {
                    format: 'DD/MM/YYYY',
                    applyLabel: "Aceptar",
                    cancelLabel: "Cancelar",
                    fromLabel: "Desde",
                    toLabel: "Hasta",
                    customRangeLabel: "Custom",
                    daysOfWeek: [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    monthNames: [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Septiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                }
            }, function(start, end, label) {
              console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
            });
            $('#hora_siguiente_consulta').datetimepicker({
                format: 'LT'
            });

            $('.select2').select2({
                theme: 'bootstrap4',
                placeholder: 'SELECCIONE UNA OPCIÓN',
                width: 'auto'
            });

            $('select[name=risk_type_id]').select2({
                theme: 'bootstrap4',
                placeholder: 'SELECCIONE UNA OPCIÓN',
                width: 'auto'
            }).val('').trigger('change');

            // Pre grabado
            @if ($medical_request->medical_request_status_id == 15)

            $('input[name=fecha_consulta]').val('{{ $attention->attention_date->format('d/m/Y') }}');
            $('select[name=medical_status_id]').val('{{ $historical_request->medical_request_status_id }}').trigger('change');
            $('input[name=fc]').val('{{ $attention->fc }}');
            $('input[name=fr]').val('{{ $attention->fr }}');
            $('input[name=t]').val('{{ $attention->t }}');
            $('input[name=pa]').val('{{ $attention->pa }}');
            $('input[name=sao2]').val('{{ $attention->sao2 }}');
            $('input[name=other]').val('{{ $attention->otros }}');

            @if ($medical_request->last_appointment->appointment_type_id==2)
                //console.log('zo');
                $('select[name=medical_status_id]').val(11).trigger('change');
            @else
                @if ($medical_request->last_appointment->appointment_type_id==3)
                //console.log('wa');
                    $('select[name=medical_status_id]').val(10).trigger('change');
                @else
                    @if ($medical_request->last_appointment->appointment_type_id==1)
                    //console.log('pr');
                        $('select[name=medical_status_id]').val(9).trigger('change');
                    @else
                    //console.log('def');
                        $('select[name=medical_status_id]').val(9).trigger('change');
                    @endif
                @endif
            @endif
            @if ($tracing->medical_attention_flag == true)
            $('input[name=medical_attention_flag]').prop('checked', true);
            @endif
            @endif
        });

        // $('#medical_status_id').change(function (e) {
        //     var id_status = $(this).val();
        //     if (id_status != null && id_status == 4) {
        //         $('#fecha_proxima_consulta').attr('disabled', 'disabled');
        //         disableAttentionOptions();
        //     }else {
        //         enableAttentionOptions();
        //     }
        // });

        $(function () {
            $(":input").inputmask();
        });

        function RecargaSignosV (medical_request_id) {
            var url = getSignosVitales.replace('_ID',medical_request_id);
            $.ajax({
                url: url,
                method: 'get',
                beforeSend: function(e){
                    // Loading
                    // blockCard('#form_content');
                }
            })
            .done(function(response) {
                $('#signos_vitales').empty();
                $('#signos_vitales').append(response.frm_signos);
                $(function () {
                    $(":input").inputmask();
                });
            })
            .fail(function(response) {
                toastr.error(
                    response,
                    '¡Error!'
                );
                if(response.responseJSON['errors']){
                    $.each(response.responseJSON['errors'], function(index, value){
                        $('[name="'+ index +'"]').addClass('is-invalid');
                        $('[name="'+ index +'"]').siblings('span.invalid-feedback').html('<strong>'+value+'</strong>');
                    });
                }
            });
        }

    </script>
    <script src="{{asset('js/components/button_files.js')}}"></script>
@endsection

