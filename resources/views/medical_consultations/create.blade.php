@extends('layouts.app_menu')

@section('styles')
    <style>
        .dataTables_filter {
            display: none !important;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Registro de atención</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Datos de consultas médicas</h3>

                            <div class="card-tools">
                                <div class="input-group input-group-sm">
                                    <input id="search_data" type="text" class="form-control" placeholder="Buscar">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @include('medical_consultations.partials.table')
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
        </div>
    </section>

    <!--MODALS-->
    @include('medical_consultations.modals.modal_patient_assign')
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('a#mis_consultas').addClass('active');
        });
        const assingToAten = @json(route('consultas_medicas.store'));
        const getAttenHist = @json(route('consultas_medicas.getAttentionHistorial', '_ID'));
    </script>
    <script src="{{asset('js/medical_consultations/index.js')}}"></script>
@endsection
