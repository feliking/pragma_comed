@extends('layouts.pdf')

@section('style')
    <!-- Theme style -->

    <style>
        .row{
            font-size: 15px;
        }

    </style>
    @php
        $logo = asset('/img/slogo.png');
        if($medical_request->employee != null)
            $logo = "https://solicitudes.pragmainvest.com.bo/".$medical_request->employee->enterprise->logo;

    @endphp
    <style type="text/css" rel="stylesheet">
        thead { display: table-header-group }
        tfoot { display: table-row-group }
        tr { page-break-inside: avoid }
    </style>
    <style type="text/css" rel="stylesheet">

        #watermark {
            position: fixed;

            /**
                Set a position in the page for your image
                This should center it vertically
            **/
            bottom:   10cm;
            left:     5.5cm;

            /** Change image dimensions**/
            width:    8cm;
            height:   8cm;

            /** Your watermark should be behind every content**/
            z-index:  -1000;
        }

        *{
            color: #171717;

        }
        div .div-heigth-2{
            line-height: 2px;
        }
        div .div-heigth-3{
            line-height: 3px;
        }
        thead th {
            padding-top: 1px !important;
            padding-bottom: 0px !important;
            padding-left: 3px !important;
            padding-right: 3px !important;
        }
        thead > tr > th > p{
            margin-top: 5px !important;
            margin-bottom: 5px !important;
        }
        tbody tr {
            font-size: 0.8em;
            padding-left: 0px !important;
            margin-bottom: 2px !important;
        }
        tbody > tr > td {
            padding-left: 3px !important;
            padding-right: 3px !important;
        }
        tbody > tr > td > p{
            line-height: 2px;
            margin-top: 0px !important;
            margin-bottom: 0px !important;
        }
        .borderless td {
            border: none;
        }

        table.border tr th,
        table.border tr th.align_right,
        table.border tr td.align_right{
            text-align: right;
        }

        table.border td { border:1px solid black; }

        table.border tr td {
            vertical-align: top;
        }

        #gradient{
            background: rgb(19,78,97);
            background: linear-gradient(90deg, rgba(19,78,97,1) 0%, rgba(23,162,184,1) 47%);
        }

        .watermark{
            border: none !important;
            color: #134e61 !important;
        }

        .watermark::after {
            content: "";
            background-image: url({{$logo}});
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: 0px 150px;
            background-size: cover !important;
            opacity: 0.1;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            position: absolute;
            z-index: -1;
        }

        footer {
            position: fixed;
            bottom: -60px;
            left: 0px;
            right: 0px;
            height: 50px;

            /** Extra personal styles **/
            color: rgba(0, 0, 0, 0.48);
            font-size: 10px;
            text-align: center;
            line-height: 35px;
        }
    </style>
@endsection

@section('content')
    @php
        $user = \App\User::find(\Illuminate\Support\Facades\Auth::id());
    @endphp
    @if($user->office != null)
        <footer>
            DIRECCIÓN: {{$user->office->direccion}}
        </footer>
    @endif
    <div class="row">
        <table style="width: 100%">
            <tbody >
            <tr>
                <td rowspan="2" style="width: 20%;text-align: center; vertical-align: middle; border-bottom: solid 3px #134e61 !important">

                    <img style="margin-top: 5px; margin-bottom: 1px; max-height: 50px; align-content: center" class="img-responsive" src="{{$logo}}" alt="Logo empresa">
                </td>
                <td style="vertical-align: bottom; width: 80%; text-align: right !important; ">
                    <p style="color: #134e61;font-size: 15px; font-weight: bold">RECETA MÉDICA</p>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top; width: 80%; text-align: right !important;color: #17A2B8;font-size: 15px;border-bottom: solid 3px #17A2B8 !important">
                    CÓDIGO DE SOLICITUD: <b style="color: #17A2B8;font-size: 15px">{{ $medical_request->code }} </b>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="row">
        <table style="width: 100%;page-break-inside:auto !important;table-layout: fixed;">
            <thead>
            <tr style="vertical-align: center !important;">
                <th class="text-left" colspan="6">
                    <p style="color: #134e61;margin-left: 10px; font-size: 15px; font-weight: bold">DATOS DE PACIENTE</p>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr style="background-color: #134e61; font-size: 8px; color: white !important; ">
                <td colspan="6" class="text-center">
                    <strong>NOMBRE COMPLETO</strong>
                </td>
                <td colspan="3" class="text-center">
                    <strong>EDAD</strong>
                </td>
                <td colspan="3" class="text-center">
                    <strong>FECHA DE ATENCIÓN</strong>
                </td>
            </tr>

            <tr>
                <td style="overflow: hidden;margin-top: 5px !important" colspan="6" class="text-center">
                    {{ $medical_request->full_name }}
                </td>
                <td style="overflow: hidden;margin-top: 5px !important" colspan="3" class="text-center">
                    @if($medical_request->employee!=null)
                        {{$medical_request->employee->edad}}
                    @else
                        {{ $medical_request->edad }}
                    @endif
                    años
                </td>
                <td style="overflow: hidden;margin-top: 5px !important" colspan="3" class="text-center">
                    {{ $request->date }}
                </td>
            </tr>

            </tbody>
        </table>
    </div>
    <div class="row">

        <table style="width: 100%;page-break-inside:auto !important;table-layout: fixed; height: 500px;padding: 10px !important;">

            <tbody>
                <tr>
                    <td style="border-top: 3px solid #134e61 !important;border-right: 3px solid #134e61 !important;border-left: 3px solid #134e61 !important;height: 90% !important;" class="watermark" colspan="6">
                    </td>
                </tr>
                <tr>
                    <td style="border-top: none !important;border-right: none !important;border-left: 3px solid #134e61 !important;border-bottom: 3px solid #134e61 !important" colspan="3"></td>
                    <td style="font-size: 8px; color: #134e61 !important;border-top: 2px solid #134e61 !important;border-right: 3px solid #134e61 !important;border-left: none !important;border-bottom: 3px solid #134e61 !important" class="text-center" colspan="3">
                        <strong>FIRMA DE MÉDICO</strong>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    {{--    <div class="row">--}}
{{--        <table style="page-break-inside:auto !important;width: 100%;table-layout: fixed;border: 3px solid #134e61 !important;height: 60px ">--}}

{{--            <tbody>--}}
{{--            <tr>--}}
{{--                <td style="vertical-align: center !important;border: none !important"></td>--}}
{{--                <td class="text-left">--}}

{{--                </td>--}}
{{--            </tr>--}}
{{--            <tr>--}}
{{--                <td style="vertical-align: center !important;border: none !important"></td>--}}
{{--                <td style="background-color: #134e61; font-size: 8px; color: white !important; " class="text-center">--}}
{{--                    <strong>FIRMA DE MÉDICO</strong>--}}
{{--                </td>--}}
{{--            </tr>--}}
{{--            <tr>--}}
{{--                <td style="border: none !important"></td>--}}
{{--                <td style="border: 1px solid #134e61 !important;background-color: rgba(22,164,185,0.15);font-weight: bold; color: #134e61 !important; " class="text-center">--}}
{{--                    {{$historical_request->tracing->user->employee->full_name}}<br>--}}
{{--                    {{$historical_request->tracing->user->employee->full_ci}}--}}
{{--                </td>--}}
{{--            </tr>--}}
{{--            </tbody>--}}
{{--        </table>--}}
{{--    </div>--}}
@endsection
