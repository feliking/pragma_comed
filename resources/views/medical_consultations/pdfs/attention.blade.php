@extends('layouts.pdf')

@section('style')
    <!-- Theme style -->

    <style>
        .row{
            font-size: 18px;
        }

    </style>
    <style type="text/css" rel="stylesheet">
        thead { display: table-header-group }
        tfoot { display: table-row-group }
        tr { page-break-inside: avoid }
    </style>
    <style type="text/css" rel="stylesheet">

        #watermark {
            position: fixed;

            /**
                Set a position in the page for your image
                This should center it vertically
            **/
            bottom:   10cm;
            left:     5.5cm;

            /** Change image dimensions**/
            width:    8cm;
            height:   8cm;

            /** Your watermark should be behind every content**/
            z-index:  -1000;
        }

        *{
            color: #171717;

        }
        div .div-heigth-2{
            line-height: 2px;
        }
        div .div-heigth-3{
            line-height: 3px;
        }
        thead th {
            padding-top: 1px !important;
            padding-bottom: 0px !important;
            padding-left: 3px !important;
            padding-right: 3px !important;
        }
        thead > tr > th > p{
            margin-top: 5px !important;
            margin-bottom: 5px !important;
        }
        tbody tr {
            font-size: 0.8em;
            padding-left: 0px !important;
            margin-bottom: 2px !important;
        }
        tbody > tr > td {
            padding-left: 3px !important;
            padding-right: 3px !important;
        }
        tbody > tr > td > p{
            line-height: 2px;
            margin-top: 0px !important;
            margin-bottom: 0px !important;
        }
        .borderless td {
            border: none;
        }

        table.border tr th,
        table.border tr th.align_right,
        table.border tr td.align_right{
            text-align: right;
        }

        table.border td { border:1px solid black; }

        table.border tr td {
            vertical-align: top;
        }

        #gradient{
            background: rgb(19,78,97);
            background: linear-gradient(90deg, rgba(19,78,97,1) 0%, rgba(23,162,184,1) 47%);
        }
    </style>
@endsection

@section('content')

<div class="row">
    <table style="width: 100%">
        <tbody >
        <tr>
            <td rowspan="2" style="width: 20%;text-align: center; vertical-align: middle; border-bottom: solid 10px #134e61 !important">
                @php
                    $logo = asset('/img/logo.png');
                    if($medical_request->employee != null)
                        $logo = "https://solicitudes.pragmainvest.com.bo/".$medical_request->employee->enterprise->logo;

                @endphp
                <img style="margin-top: 10px; margin-bottom: 10px; max-height: 80px; align-content: center" class="img-responsive" src="{{$logo}}" alt="Logo empresa">
            </td>
            <td style="vertical-align: bottom; width: 80%; text-align: right !important; ">
                <p style="color: #134e61;font-size: 30px; font-weight: bold">REPORTE DE ATENCIÓN MÉDICA</p>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; width: 80%; text-align: right !important;color: #17A2B8;font-size: 20px;border-bottom: solid 10px #17A2B8 !important">
                CÓDIGO DE SOLICITUD: <b style="color: #17A2B8;font-size: 20px">{{ $medical_request->code }} </b>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<br>
<br>
<div class="row">
    <table class="borderless" style="width: 100%;page-break-inside:auto !important;table-layout: fixed;">
        <thead>
        <tr style="vertical-align: center !important;">
            <th style="border-bottom: 3px solid #134e61 !important;" class="text-left" colspan="6">
                <p style="color: #134e61;margin-left: 10px; font-size: 20px; font-weight: bold">DATOS DE PACIENTE</p>
            </th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td style="overflow: hidden;margin-top: 5px !important" colspan="3" class="text-center">
                    {{ $medical_request->full_name }}
                </td>
                <td style="overflow: hidden;margin-top: 5px !important" colspan="3" class="text-center">
                   {{ $medical_request->full_ci }}
                </td>
            </tr>
            <tr style="border-top: solid 1px black !important;background-color: rgba(22,164,185,0.15); font-size: 8px; color: #134e61 !important; ">
                <td colspan="3" class="text-center">
                    <strong>NOMBRE COMPLETO</strong>
                </td>
                <td colspan="3" class="text-center">
                    <strong>CÉDULA DE IDENTIDAD</strong>
                </td>
            </tr>
            <tr style="border: solid 1px black !important;">
                <td colspan="2" class="text-center">
                    {{ $medical_request->edad }} años
                </td>
                <td colspan="2" class="text-center">
                    {{ $medical_request->gender }}
                </td>
                <td colspan="2" class="text-center">
                    @if($medical_request->employee!=null)
                        <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
                        {{$medical_request->employee->enterprise->nombre}}
                    @else
                        @if($medical_request->history_valid_status)
                            <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
                        @else
                            <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
                        @endif
                        {{$medical_request->enterprise}}
                    @endif
                </td>
            </tr>
            <tr style="border-top: solid 1px black !important;background-color: rgba(22,164,185,0.15); font-size: 8px; color: #134e61 !important; ">
                <td colspan="2" class="text-center">
                    <strong>EDAD</strong>
                </td>
                <td colspan="2" class="text-center">
                    <strong>SEXO</strong>
                </td>
                <td colspan="2" class="text-center">
                    <strong>EMPRESA</strong>
                </td>
            </tr>
            <tr style="border: solid 1px black !important;">
                <td colspan="2" class="text-center">
                    @if($medical_request->healthInsurance != null)
                        {{ $medical_request->healthInsurance->nombre }}
                    @else
                        S/D
                    @endif
                </td>
                <td colspan="2" class="text-center">
                    {{ $medical_request->phone_number }}
                </td>
                <td colspan="2" class="text-center">
                    {{ $medical_request->email }}
                </td>
            </tr>
            <tr style="border-top: solid 1px black !important;background-color: rgba(22,164,185,0.15); font-size: 8px; color: #134e61 !important; ">
                <td colspan="2" class="text-center">
                    <strong>SEGURO SOCIAL</strong>
                </td>
                <td colspan="2" class="text-center">
                    <strong>TELÉFONO</strong>
                </td>
                <td colspan="2" class="text-center">
                    <strong>CORREO ELECTRÓNICO</strong>
                </td>
            </tr>

        </tbody>
    </table>
</div>
<br>
<br>
<div class="row">

    @php
        $maxcols = 6;
       $chk_maxcols = 0;


    @endphp
    <table class="borderless" style="width: 100%;page-break-inside:auto !important;table-layout: fixed;">
        <thead>
        <tr style="vertical-align: center !important;">
            <th style="border-bottom: 3px solid #134e61 !important;" class="text-left" colspan="6">
                <p style="color: #134e61;margin-left: 10px; font-size: 20px; font-weight: bold">DATOS DE ATENCIÓN</p>
            </th>
        </tr>
        </thead>
        <tbody>
        @if($request->has('chk_fecha_consulta') || $request->has('chk_estado_consulta'))

            <tr>
            @php
            $cols_1 = $maxcols;
                if($request->has('chk_fecha_consulta') && $request->has('chk_estado_consulta'))
                    $cols_1 = $maxcols/2;
            @endphp

            @if($request->has('chk_fecha_consulta'))
                <td style="overflow: hidden;margin-top: 5px !important" colspan="{{$cols_1}}" class="text-center">
                    {{ $attention->attention_date->format('d/m/Y') }}
                </td>
            @endif
            @if($request->has('chk_estado_consulta'))
                <td style="overflow: hidden;margin-top: 5px !important" colspan="{{$cols_1}}" class="text-center">
                    {{ $historical_request->medical_request_status->name }}
                </td>
            @endif
            </tr>

            <tr style="border-top: solid 1px black !important;background-color: rgba(22,164,185,0.15); font-size: 8px; color: #134e61 !important; ">
                @if($request->has('chk_fecha_consulta'))
                <td colspan="{{$cols_1}}" class="text-center">
                    <strong>FECHA DE ATENCIÓN</strong>
                </td>
                @endif
                @if($request->has('chk_estado_consulta'))
                <td colspan="{{$cols_1}}" class="text-center">
                    <strong>ESTADO DE CONSULTA</strong>
                </td>
                @endif
            </tr>
        @endif

        @if($request->has('chk_signos_vitales'))
        <tr style="border: solid 1px black !important;">
            <td class="text-center">
                {{ $attention->fc }}
            </td>
            <td class="text-center">
                {{ $attention->fr }}
            </td>
            <td class="text-center">
                {{ $attention->t }}
            </td>
            <td class="text-center">
                {{ $attention->pa }}
            </td>
            <td class="text-center">
                {{ $attention->sao2 }}
            </td>
            <td class="text-center">
                {{ $attention->otros }}
            </td>
        </tr>
        <tr style="border-top: solid 1px black !important;background-color: rgba(22,164,185,0.15); font-size: 8px; color: #134e61 !important; ">
            <td class="text-center">
                FC
            </td>
            <td class="text-center">
                FR
            </td>
            <td class="text-center">
                T°
            </td>
            <td class="text-center">
                PA
            </td>
            <td class="text-center">
                SaO2
            </td>
            <td class="text-center">
                OTROS
            </td>
        </tr>
        @endif

        @if($request->has('chk_current_history') || $request->has('chk_physical_exam'))
            <tr style="border: solid 1px black !important;">
                @php
                    $cols_2 = $maxcols;
                        if($request->has('chk_current_history') && $request->has('chk_physical_exam'))
                            $cols_2 = $maxcols/2;
                @endphp

                @if($request->has('chk_current_history'))
                <td style="overflow: hidden;" colspan="{{$cols_2}}" class="text-center">
                    {{ $attention->current_history }}
                </td>
                @endif
                @if($request->has('chk_physical_exam'))
                <td style="overflow: hidden;" colspan="{{$cols_2}}" class="text-center">
                    {{ $attention->physical_exam }}
                </td>
                @endif
            </tr>
            <tr style="border-top: solid 1px black !important;background-color: rgba(22,164,185,0.15); font-size: 8px; color: #134e61 !important; ">
                @if($request->has('chk_current_history'))
                <td style="overflow: hidden;" colspan="{{$cols_2}}" class="text-center">
                    <strong>HISTORIA DE LA ENFERMEDAD ACTUAL</strong>
                </td>
                @endif
                @if($request->has('chk_physical_exam'))
                <td style="overflow: hidden;" colspan="{{$cols_2}}" class="text-center">
                    <strong>EXAMEN FÍSICO</strong>
                </td>
                @endif
            </tr>
        @endif

        @if($request->has('chk_diagnostico'))
            <tr style="border: solid 1px black !important;">

                <td style="overflow: hidden;" colspan="{{$maxcols}}" class="text-center">
                    {{ $attention->diagnosis }}
                </td>
        </tr>
            <tr style="background-color: rgba(22,164,185,0.15); font-size: 8px; color: #134e61 !important; ">
            <td  colspan="{{$maxcols}}" class="text-center">
                <strong>DIAGNÓSTICO</strong>
            </td>
        </tr>
        @endif

        @if($request->has('chk_comment_tracing') || $request->has('chk_work_situation_id') || $request->has('chk_treatment_stage_id') || $request->has('chk_risk_type_id') || $request->has('chk_risk_state_id') || $request->has('chk_disease_criteria') )
        <tr style="background-color: rgba(21,109,130,0.46); font-size: 15px; color: #134e61 !important; ">
            <td colspan="{{$maxcols}}">
                <strong style="margin-left: 10px !important;">SEGUIMIENTO</strong>
            </td>
        </tr>

        @if($request->has('chk_comment_tracing'))
        <tr style="border: solid 1px black !important;">
            <td style="overflow: hidden;" colspan="{{$maxcols}}" class="text-center">
                {{ $historical_request->tracing->comment }}
            </td>
        </tr>
        <tr style="background-color: rgba(22,164,185,0.15); font-size: 8px; color: #134e61 !important; ">
            <td  colspan="{{$maxcols}}" class="text-center">
                <strong>COMENTARIO U OBSERVACIÓN</strong>
            </td>
        </tr>
        @endif

        @if($request->has('chk_work_situation_id') || $request->has('chk_treatment_stage_id'))
        <tr style="border: solid 1px black !important;">
            @php
                $cols_4 = $maxcols;
                    if($request->has('chk_work_situation_id') && $request->has('chk_treatment_stage_id'))
                        $cols_4 = $maxcols/2;
            @endphp

            @if($request->has('chk_treatment_stage_id'))
            <td style="overflow: hidden;" colspan="{{$cols_4}}" class="text-center">
                {{ $historical_request->tracing->treatmentStage->name }}
            </td>
            @endif
        </tr>
        <tr style="background-color: rgba(22,164,185,0.15); font-size: 8px; color: #134e61 !important; ">
            @if($request->has('chk_work_situation_id'))
            <td  colspan="{{$cols_4}}" class="text-center">
                <strong>SITUACIÓN LABORAL</strong>
            </td>
            @endif
                @if($request->has('chk_treatment_stage_id'))
            <td  colspan="{{$cols_4}}" class="text-center">
                <strong>ETAPA DE TRATAMIENTO</strong>
            </td>
                @endif
        </tr>
        @endif
        @if($request->has('chk_risk_type_id') || $request->has('chk_risk_state_id'))

        <tr style="border: solid 1px black !important;">
            @php
                $cols_5 = $maxcols;
                    if($request->has('chk_risk_type_id') && $request->has('chk_risk_state_id'))
                        $cols_5 = $maxcols/2;
            @endphp

            @if($request->has('chk_risk_type_id'))
            <td style="overflow: hidden;" colspan="{{$cols_5}}" class="text-center">
                {{ $historical_request->tracing->riskType->name }}
            </td>
            @endif
            @if($request->has('chk_risk_state_id'))
            <td style="overflow: hidden;" colspan="{{$cols_5}}" class="text-center">
                {{ $historical_request->tracing->riskState->name }}
            </td>
            @endif
        </tr>
        <tr style="background-color: rgba(22,164,185,0.15); font-size: 8px; color: #134e61 !important; ">
            @if($request->has('chk_risk_type_id'))
            <td  colspan="{{$cols_5}}" class="text-center">
                <strong>TIPO DE RIESGO</strong>
            </td>
            @endif
            @if($request->has('chk_risk_state_id'))
            <td  colspan="{{$cols_5}}" class="text-center">
                <strong>ESTADO DE RIESGO</strong>
            </td>
            @endif
        </tr>
        @endif

        @if($request->has('chk_disease_criteria'))
        <tr style="border: solid 1px black !important;">
            <td style="overflow: hidden;" colspan="{{$maxcols}}" class="text-center">
                @php
                    $total = 0;
                @endphp

                @foreach($historical_request->tracing->diseaseCriterias as $criteria)
                    • {{$criteria->syntom}}
                    @php
                        $total = $total + $criteria->weighing;
                    @endphp
                @endforeach
                <br>
                <strong> PONDERACIÓN: </strong> {{$total}}
            </td>
        </tr>
        <tr style="background-color: rgba(22,164,185,0.15); font-size: 8px; color: #134e61 !important; ">
            <td  colspan="{{$maxcols}}" class="text-center">
                <strong>CRITERIOS</strong>
            </td>
        </tr>
        @endif

        @endif
        @if($request->has('chk_laboratory'))
            <tr style="background-color: rgba(21,109,130,0.46); font-size: 15px; color: #134e61 !important; ">
                <td colspan="{{$maxcols}}">
                    <strong style="margin-left: 10px !important;">LABORATORIO</strong>
                </td>
            </tr>

            @foreach($historical_request->tracing->complementaryExams as $exam)
                <tr style="background-color: rgba(22,164,185,0.15); font-size: 12px !important; color: #134e61 !important; ">
                    <td colspan="{{$maxcols}}">
                        <strong>EXAMEN # {{$loop->index + 1}}</strong>
                    </td>
                </tr>

                <tr style="border: solid 1px black !important;">
                    <td style="overflow: hidden;" class="text-center">
                        {{ $exam->laboratory->name }}
                    </td>
                    <td style="overflow: hidden;" colspan="2" class="text-center">
                        {{ $exam->testType->name }}
                    </td>
                    <td style="overflow: hidden;" class="text-center">
                        {{ $exam->bill_number}}
                    </td>
                    <td style="overflow: hidden;" colspan="2" class="text-center">
                        {{ $exam->cost }} Bs. <strong>(@if($exam->enterprise_flag == 0) NO @endif PAGÓ LA EMPRESA) </strong>
                    </td>
                </tr>
                <tr style="background-color: rgba(22,164,185,0.15); font-size: 8px; color: #134e61 !important; ">
                    <td  class="text-center">
                        <strong>LABORATORIO</strong>
                    </td>
                    <td  class="text-center" colspan="2">
                        <strong>TIPO DE PRUEBA</strong>
                    </td>
                    <td  class="text-center">
                        <strong># DE FACTURA</strong>
                    </td>
                    <td  class="text-center" colspan="2">
                        <strong>COSTO</strong>
                    </td>
                </tr>

            @endforeach
        @endif
        </tbody>
    </table>
</div>

<br>
<br>
@if($request->has('chk_tratamiento'))
<div class="row">

    @php
        $maxcols = 6;
       $chk_maxcols = 0;
    @endphp
    <table class="borderless" style="width: 100%;page-break-inside:auto !important;table-layout: fixed;">
        <thead>
        <tr style="vertical-align: center !important;">
            <th style="border-bottom: 3px solid #134e61 !important;" class="text-left" colspan="6">
                <p style="color: #134e61;margin-left: 10px; font-size: 20px; font-weight: bold">TRATAMIENTO</p>
            </th>
        </tr>
        </thead>
        <tbody>

            <tr style="border: solid 1px black !important;">

                    <td style="overflow: hidden;" colspan="{{$maxcols}}" class="text-center">
                        {{ $attention->treatment }}
                    </td>
            </tr>

            <tr style="background-color: rgba(22,164,185,0.15); font-size: 8px; color: #134e61 !important; ">
                <td style="overflow: hidden;" colspan="{{$maxcols}}" class="text-center">
                    <strong>DETALLE DE TRATAMIENTO REALIZADA POR EL MÉDICO</strong>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<br>
<br>
@endif
<br>
<div class="row">
    <table style="page-break-inside:auto !important;width: 100%;table-layout: fixed;">

        <tbody>
            <tr>
                <td style="vertical-align: center !important;border: none !important"></td>
                <td style="border: 1px solid #134e61 !important;height: 80px" class="text-left">

                </td>
            </tr>
            <tr>
                <td style="vertical-align: center !important;border: none !important"></td>
                <td style="border: 1px solid #134e61 !important;background-color: #134e61; font-size: 8px; color: white !important; " class="text-center">
                    <strong>FIRMA DE MÉDICO</strong>
                </td>
            </tr>
            <tr>
                <td style="border: none !important"></td>
                <td style="border: 1px solid #134e61 !important;background-color: rgba(22,164,185,0.15);font-weight: bold; color: #134e61 !important; " class="text-center">
                    {{$historical_request->tracing->user->employee->full_name}}<br>
                    {{$historical_request->tracing->user->employee->full_ci}}
                </td>
            </tr>
        </tbody>
    </table>
</div>
@endsection
