<table id="example2" class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Op.</th>
        <th>Nombre</th>
        <th>Descripción</th>
        <th>Estado</th>
    </tr>
    </thead>

    <tbody>
    @foreach($medical_request_priorities as $medical_request_priority)
        <tr>
            <td>
                <div class="btn-group" role="group">
                    <a href="{{ route('prioridades.edit', $medical_request_priority->id) }}" class="btn btn-sm btn-default" title="Editar"
                        style="{{ ($medical_request_priority->system_flag==1)? 'pointer-events: none; cursor: default;' : ''}}">
                        <i class="fa fa-edit"></i>
                        <span class="sr-only">Editar</span>
                    </a>
                    <a href="{{ route('prioridades.show', $medical_request_priority->id) }}" class="btn btn-sm btn-default" title="{{($medical_request_priority->deleted_at == null)?'Desactivar':'Activar'}}">
                        <i class="fa fa-{{($medical_request_priority->deleted_at == null)?'times':'check'}}"></i>
                        <span class="sr-only">{{($medical_request_priority->deleted_at == null)?'Desactivar':'Activar'}}</span>
                    </a>
                </div>
            </td>
            <td>
                {{$medical_request_priority->name}}
            </td>
            <td>
                {{$medical_request_priority->description}}
            </td>
            <td>
                @if($medical_request_priority->deleted_at == null)
                <span class="badge badge-success">Activo</span>
                @elseif($medical_request_priority->deleted_at != null)
                <span class="badge badge-danger">Inactivo</span>
                @endif
            </td>

        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Op.</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Estado</th>
        </tr>
    </tfoot>
</table>
