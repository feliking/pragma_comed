@extends('layouts.app_simple')

@section('title', 'Iniciar sesión')

@section('style')
<style>
    .login-box
    {
        width: auto;
    }
</style>
@endsection

@section('content')
<div class="login-box">
    <div class="login-logo">
        <a href="#">
            <b>S</b>istema de <b>C</b>onsultas <b>M</b>édicas
        </a>
    </div>

    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Ingresa tus credenciales para empezar</p>

            <form id="form-login" action="{{ route('login') }}" method="post">
                @csrf
                <div class="input-group mb-3">
                    <input id="login" type="text" class="form-control @if ($errors->has('username') || $errors->has('email')) is-invalid @endif" name="login" value="{{ old('username') ? : old('email') }}" placeholder="Nombre de usuario" required autocomplete="login" autofocus>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                    @if ($errors->has('username') || $errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('username') ? : $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="input-group mb-3">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Contraseña" required autocomplete="current-password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="row">
                    <div class="col-6">
                        <div class="icheck-primary">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="remember">
                                Recuerdame
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-6">
                        <button type="submit" class="btn btn-primary btn-block">Iniciar</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        <!-- /.login-card-body -->
    </div>

</div>
@endsection

@section('script')
<script>
    // Validation
    $(document).ready(function () {
        $.validator.setDefaults({
            submitHandler: function (form) {
                $('form#form-login button[type="submit"]')
                    .attr('disabled', true)
                    .html("Iniciando...");
                $(form).unbind().submit();
            }
        });

        $('form#form-login').validate({
            rules: {
                login: {
                    required: true,
                },
                password: {
                    required: true,
                },
            },
            messages: {
                login: {
                    required: "Favor ingrese el nombre de usuario",
                },
                password: {
                    required: "Favor ingrese su contraseña",
                },
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    });
</script>
@endsection
