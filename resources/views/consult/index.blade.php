@extends('layouts.app_simple')

@section('style')
<style>
    .register-box
    {
        width: auto;
    }
</style>
@endsection

@section('content')

<div class="register-box">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-md-12">
            <!-- Widget: user widget style 1 -->
            <div class="card card-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-info">
                    <h2 class="widget-user-desc">Código: <b>{{ $medical_request->code }}</b></h2>
                </div>
                <div class="widget-user-image">
                    <img class="img-circle elevation-2" src="{{ asset('img/healt_icon.png') }}" alt="User Avatar">
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>Paciente</th>
                                        <td class="text-right">{{ $medical_request->full_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Cédula</th>
                                        <td class="text-right">{{ $medical_request->full_ci }}</td>
                                    </tr>
                                    <tr>
                                        <th>Empresa</th>
                                        <td class="text-right">
                                            {{$medical_request->enterprise}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="description-block">
                                <h5 class="description-header">¿Para que me sirve este código?</h5>
                                <span class="description-text">
                                    Este código es muy <b>importante</b> pues le sirve para <br/>realizar el seguimiento correspondiente a su solicitud médica.
                                </span>
                            </div>
                            <div class="description-block">
                                <a href="{{ route('busca.solicitudes_medicas') }}">Haga click aqui</a>
                            </div>
                            <!-- /.description-block -->
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="description-block">
                                <h5 class="description-header">Descargar Solicitud Médica</h5>
                                <span class="description-text">
                                    Hemos generado un documento PDF que muestra la información <br> de su solicitud médica
                                </span>
                            </div>
                            <div class="description-block">
                                <a href="{{ route('medical_request.print', $medical_request->code) }}">Descargar solicitud en formato PDF</a>
                            </div>
                            <!-- /.description-block -->
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="description-block">
                                <h5 class="description-header">¡ATENCIÓN!</h5>
                                <span class="description-text">
                                    Nuestro personal médico se comunicará en breve con usted, favor <br/>tenga paciencia y mantenga todas las medidas de bioseguridad.
                                </span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="description-block">
                                <a href="{{ url()->previous() }}" class="btn btn-dark">
                                    <i class="fa fa-arrow-left"></i> Volver
                                </a>
                            </div>
                            <!-- /.description-block -->
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
            </div>
            <!-- /.widget-user -->
        </div>
    </div>
</div>
@endsection
