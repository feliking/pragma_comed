<input type="hidden" name="parametro_asignacion" id="parametro_asignacion" value="{{ $parametro_seguimiento->value }}">
<input type="hidden" name="parametro_cita" id="parametro_cita" value="{{ $parametro_cita->value }}">
{{-- <input type="hidden" name="hora_entrada" id="hora_entrada" value="{{ $parametro_hora_entrada->value }}"> --}}
<input type="hidden" name="hora_salida" id="hora_salida" value="{{ $parametro_hora_salida->value }}">
<div class="modal-body">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body" style="transform: traslate(0,0); height: 200px;overflow: auto;padding: 10px">
                @include('tracing_administrations.partials.lista_paciente')
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <hr>
        <div class="form-group col-12">
            <input type="radio" name="radio_option" id="radio_seguir" value="seguimiento"  required="required" checked>
            <label for="radio_seguir">Asignar a seguimiento</label>
            <input type="radio" name="radio_option" id="radio_cita_medica" value="cita"  required="required">
            <label for="radio_cita_medica">Asignar cita médica</label>
        </div>
        <div class="form-group col-6">
            <label for="">*Hora inicial</label>
            <div class="input-group date" id="hora_entrada" data-target-input="nearest">
                <input type="text" name="hora_entrada" class="form-control datetimepicker-input" placeholder="Hora inicial" data-target="#hora_entrada" value="{{ $parametro_hora_entrada->value }}" />
                <div class="input-group-append" data-target="#hora_entrada" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-clock"></i></div>
                </div>
            </div>
        </div>
    </div>
    {{--DATOS DE SEGUIMIENTO--}}
    <div class="form-row" id="div_seguimiento">
        <div class="col-md-6 col-sm-12">
            <h4>Datos de Atención</h4>
            <hr>
            <div class="form-row">
                <div class="col-12">
                    <label for="personal" class="control-label col-12">{{ __('*Personal seguimiento') }}</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <select name="personal_id" id="personal_id" required class="form-control select2" style="width: 100%;">
                                <option></option>
                                @foreach($users_seguimiento as $user)
                                    <option value="{{$user->id}}">{{$user->full_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            @include('layouts.components.modals.partials.form_appointment_tracing_list')
        </div>

        <div class="col-md-6 col-sm-12">
            <h4>Citas Programadas</h4>
            <hr>
            <div class="form-row" id="div_doctor_agenda_atencion" style="transform: traslate(0,0); height: 395px;overflow: auto;padding: 10px" >
            </div>
        </div>
    </div>

    {{--CITA MÉDICA--}}

    <div class="form-row" id="div_cita_medica" hidden>
        <div class="col-md-6 col-sm-12">
            <h4>Datos de Atención</h4>
            <hr>
            <div class="form-row">
                <div class="col-12">
                    <label for="medico" class="control-label col-12">{{ __('*Medico') }}</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <select name="medico_id" id="medico_id" required class="form-control select2" style="width: 100%;">
                                <option></option>
                                @foreach($users_medico as $user)
                                    <option value="{{$user->id}}">{{$user->full_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            @include('layouts.components.modals.partials.form_appointment_list')
        </div>
        <div class="col-md-6 col-sm-12">
            <h4>Citas Programadas</h4>
            <hr>
            <div class="form-row" id="div_doctor_agenda" style="transform: traslate(0,0); height: 200px;overflow: auto;padding: 10px">

            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/components/appointment.js') }}"></script>
<script src="{{ asset('js/components/appointment_tracing.js') }}"></script>
<script>
    $(document).ready(function() {
        $('select[name=appointment_type_id]').select2({
            theme: 'bootstrap4',
            width: 'auto',
            placeholder: '* Seleccione un tipo de atención'
        });

        $('select[name=appointment_type_tracing_id]').select2({
            theme: 'bootstrap4',
            width: 'auto',
            placeholder: '* Seleccione un tipo de atención'
        });

        $('select[name=personal_id]').select2({
            theme: 'bootstrap4',
            placeholder: '* Seleccione a una persona'
        });

        $('select[name=medico_id]').select2({
            theme: 'bootstrap4',
            placeholder: '* Seleccione un médico'
        });

        $('[data-mask]').inputmask({
            removeMaskOnSubmit: true,
        });

        $('#next_attention_date').datetimepicker({
            format: 'DD/MM/YYYY',
        });

        $('#next_attention_date_tracing').datetimepicker({
            format: 'DD/MM/YYYY',
        });

        $('#next_attention_time').datetimepicker({
            format: 'LT'
        });

        $('#next_attention_time_tracing').datetimepicker({
            format: 'LT'
        });

        $('#hora_entrada').datetimepicker({
            format: 'LT'
        });
    });
    $('input[type=radio][name^=radio_option]').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });

    $('input[type=radio][name^=radio_option]').on('ifChecked', function(event){
        //$('select[name=appointment_type_tracing_id]').val('').trigger('change');
        switch ($(this).val()) {
            case "seguimiento":
                $('div#div_cita_medica').attr('hidden', 'hidden');
                $('div#div_seguimiento').removeAttr('hidden');
                $('#btnCita').attr({
                    disabled: 'disabled',
                    hidden: 'hidden'
                });
                $('#btnSeguimiento').removeAttr('disabled');
                $('#btnSeguimiento').removeAttr('hidden');
                $('select#appointment_type_tracing_id').val(4).trigger('change');
                $('#div_seguimiento').find('input').removeAttr('diabled');
                $('#div_cita_medica').find('input').attr('disable', 'disabled');
                break;
            case "cita":
                $('div#div_cita_medica').removeAttr('hidden');
                $('div#div_seguimiento').attr('hidden', 'hidden');
                $('#btnSeguimiento').attr({
                    disabled: 'disabled',
                    hidden: 'hidden'
                });
                $('select[name=appointment_type_id]').val(1).trigger('change');
                $('#btnCita').removeAttr('disabled');
                $('#btnCita').removeAttr('hidden');
                break;
        }
    });

    var fecha_atencion = null;
    $("#next_attention_date").on("change.datetimepicker", ({date, oldDate}) => {
        fecha_atencion = date;
        searchAppointments();
    });

    var fecha_atencion_tracing = null;
    $("#next_attention_date_tracing").on("change.datetimepicker", ({date, oldDate}) => {
        fecha_atencion_tracing = date;
        searchAppointmentSeguim();
    });

    $('#hora_entrada').on("change.datetimepicker", ({date, oldDate}) => {
        searchAppointmentSeguim();
    });

    function searchAppointments() {
        var selected_op = $('select#medico_id option:selected').val();

        if(selected_op!=null && selected_op!="" && fecha_atencion!=null){
            var url = citasAppointment;
            var token = $("meta[name=csrf-token]").attr("content");

            $.ajax({
                url: url,
                method: 'post',
                headers: {'X-CSRF-TOKEN': token},
                data: {
                    user_id: selected_op,
                    next_attention_date: fecha_atencion.format('YYYY-MM-DD')
                },
                beforeSend: function(e){
                    var div_alert = '<div class="alert alert-warning col-12">'+
                                        '<h5><i class="icon fa fa-spin fa-spinner"></i> ¡Cargando!</h5>'+
                                        '<p>Cargando datos de la cita, espere porfavor...</p>'+
                                    '</div>';
                    $('div#div_doctor_agenda').empty();
                    $('div#div_doctor_agenda').append(div_alert);
                }
            })
            .done(function(response) {
                switch (response['type']) {
                    case "correct":
                        $('div#div_doctor_agenda').empty();
                        $('div#div_doctor_agenda').append(response['view']);
                        asignar_fecha_probable(response.array_time, 2);
                        break;
                    case "error":
                        toastr.error(
                            response['msj'],
                            '¡Error!'
                        );
                        var div_alert = '<div class="alert alert-danger col-12">'+
                            '<h5><i class="icon fa fa-spin fa-spinner"></i> ¡Error!</h5>'+
                            '<p>Existió algun error a la hora de cargar los datos de cita</p>'+
                            '</div>';
                        $('div#div_doctor_agenda').empty();
                        $('div#div_doctor_agenda').append(div_alert);
                        break;
                }
            })
            .fail(function() {
                var div_alert = '<div class="alert alert-danger col-12">'+
                    '<h5><i class="icon fa fa-spin fa-spinner"></i> ¡Error!</h5>'+
                    '<p>Existió algun error a la hora de cargar los datos de cita</p>'+
                    '</div>';
                $('div#div_doctor_agenda').empty();
                $('div#div_doctor_agenda').append(div_alert);
            });
        }
    }

    function searchAppointmentSeguim(){
        var selected_op = $('select#personal_id option:selected').val();

        if(selected_op!=null && selected_op!="" && fecha_atencion_tracing!=null){
            var url = citasAppointment;
            var token = $("meta[name=csrf-token]").attr("content");

            $.ajax({
                url: url,
                method: 'post',
                headers: {'X-CSRF-TOKEN': token},
                data: {
                    user_id: selected_op,
                    next_attention_date: fecha_atencion_tracing.format('YYYY-MM-DD')
                },
                beforeSend: function(e){
                    var div_alert = '<div class="alert alert-warning col-12">'+
                        '<h5><i class="icon fa fa-spin fa-spinner"></i> ¡Cargando!</h5>'+
                        '<p>Cargando datos de la cita, espere porfavor...</p>'+
                        '</div>';
                    $('div#div_doctor_agenda_atencion').empty();
                    $('div#div_doctor_agenda_atencion').append(div_alert);
                }
            })
            .done(function(response) {
                switch (response['type']) {
                    case "correct":
                        $('div#div_doctor_agenda_atencion').empty();
                        $('div#div_doctor_agenda_atencion').append(response['view']);
                        asignar_fecha_probable(response.array_time, 1);
                        break;
                    case "error":
                        toastr.error(
                            response['msj'],
                            '¡Error!'
                        );
                        var div_alert = '<div class="alert alert-danger col-12">'+
                            '<h5><i class="icon fa fa-spin fa-spinner"></i> ¡Error!</h5>'+
                            '<p>Existió algun error a la hora de cargar los datos de cita</p>'+
                            '</div>';
                        $('div#div_doctor_agenda').empty();
                        $('div#div_doctor_agenda').append(div_alert);
                        break;
                }
            })
            .fail(function() {
                var div_alert = '<div class="alert alert-danger col-12">'+
                    '<h5><i class="icon fa fa-spin fa-spinner"></i> ¡Error!</h5>'+
                    '<p>Existió algun error a la hora de cargar los datos de cita</p>'+
                    '</div>';
                $('div#div_doctor_agenda').empty();
                $('div#div_doctor_agenda').append(div_alert);
            });
        }
    }

    $('select#medico_id').change(function (e) {
        searchAppointments();
        var selected_op = $('select#medico_id option:selected').val();
        if(selected_op!=null && selected_op!=""){
            searchAppointments();
        }
    });

    $('select#personal_id').change(function (e) {
        searchAppointmentSeguim();
        var selected_op = $('select#personal_id option:selected').val();
        if(selected_op!=null && selected_op!=""){
            searchAppointmentSeguim();
        }
    });

    function asignar_fecha_probable(horas_programadas, tipo) {
        var hora_inicio = $('input[name=hora_entrada]').val();
        hora_inicio = moment(hora_inicio, 'HH:mm').format('HH:mm');
        var hora_salida = $('#hora_salida').val();
        if(tipo == 1){
            var seguimiento = '00:'+$('#parametro_asignacion').val()+':00';
            $.each($('.data-horario'), function (index, value) {
                var hora_asignacion = moment(hora_inicio, 'HH:mm').format('HH:mm');
                var hora_final = moment(hora_inicio,'HH:mm').add(moment.duration(seguimiento)).format('HH:mm');
                for (var i = 0; i < horas_programadas.length; i++) {
                    if(hora_inicio <= horas_programadas[i] && hora_final > horas_programadas[i]){
                        hora_asignacion = moment(horas_programadas[i], 'HH:mm').add(moment.duration(seguimiento)).format('HH:mm');
                        hora_final = moment(hora_asignacion,'HH:mm').add(moment.duration(seguimiento)).format('HH:mm');
                    }
                }
                if(hora_asignacion <= hora_salida){
                    // console.log('hora_asignacion '+hora_asignacion);
                    $(this).find('p').text(hora_asignacion);
                    $(this).find('input[type=hidden]').val(hora_asignacion);
                }else{
                    $(this).find('p').text('Sin Asignar');
                    $(this).find('input[type=hidden]').attr('disabled', 'true');
                    $(this).parent().find('input[name="medical_request_id[]"]').attr('disabled', 'true');
                    $(this).parent().addClass('table-danger')
                }
                hora_inicio = moment(hora_asignacion, 'HH:mm').add(moment.duration(seguimiento)).format('HH:mm');
            });
        }else{
            var cita = '00:'+$('#parametro_cita').val()+':00';
            $.each($('.data-horario'), function (index, value) {
                var hora_asignacion = moment(hora_inicio, 'HH:mm').format('HH:mm');
                var hora_final = moment(hora_inicio,'HH:mm').add(moment.duration(cita)).format('HH:mm');
                for (var i = 0; i < horas_programadas.length; i++) {
                    if(hora_inicio <= horas_programadas[i] && hora_final >= horas_programadas[i]){
                        hora_asignacion = moment(horas_programadas[i], 'HH:mm').add(moment.duration(cita)).format('HH:mm');
                        hora_final = moment(hora_asignacion,'HH:mm').add(moment.duration(seguimiento)).format('HH:mm');
                    }
                }
                if(hora_asignacion <= hora_salida){
                    // console.log('hora_asignacion '+hora_asignacion);
                    $(this).find('p').text(hora_asignacion);
                    $(this).find('input[type=hidden]').val(hora_asignacion);
                }else{
                    $(this).find('p').text('Sin Asignar');
                    $(this).find('input[type=hidden]').attr('disabled', 'true');
                    $(this).parent().find('input[name="medical_request_id[]"]').attr('disabled', 'true');
                    $(this).parent().addClass('table-danger')
                }
                hora_inicio = moment(hora_asignacion, 'HH:mm').add(moment.duration(cita)).format('HH:mm');
            });
        }
    }
</script>
