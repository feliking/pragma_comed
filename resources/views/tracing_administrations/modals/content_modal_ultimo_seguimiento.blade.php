<input type="hidden" name="medical_request_id" id="medical_request_id" value="{{ $medical_request->id }}">
<div class="modal-body">
    <div class="container-fluid">
        @include('medical_consultations.partials.datos_paciente')
    </div>
    <hr>
    {{--DATOS DE SEGUIMIENTO--}}
    <div class="card card-info card-outline">
        <div class="card-header">
            <h3 class="card-title"><b>DATOS DE ÚLTIMO SEGUIMIENTO</b></h3>
        </div>
        <div class="card-body">
            @include('tracing.partials.form_administration')
        </div>
    </div>
</div>
<script></script>
