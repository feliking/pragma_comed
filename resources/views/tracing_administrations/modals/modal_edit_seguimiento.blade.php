<!-- Modal -->
<div class="modal fade" id="modal_edit_seguimiento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('Ultimo Seguimiento') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['id' => 'frm_last_seguimiento', 'files' => true]) !!}
            <div id="frm_content_last_seguimiento">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                </button>
                <button type="button" class="btn btn-warning" onclick="registrar_seguimiento_modificado()">
                    <i class="fa fa-edit"></i> Modificar
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
