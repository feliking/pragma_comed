<!-- Modal -->
<div class="modal fade" id="modal_close_case" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    {{ __('Cierre') }}
                </h5>
            </div>
            <div class="modal-body">
                <div class="container-fluid" id="datos_paciente">
                    <div class="row">
                        <div class="col-md-4">
                            <label><i class="fa fa-barcode"></i><b> Codigo de Solicitud</b></label>
                            <p id="p_modal_close_code"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fa fa-user"></i><b> Nombre de empleado</b></label>
                            <p id="p_modal_close_fullname"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fa fa-sort-numeric-desc"></i><b> CI</b></label>
                            <p id="p_modal_close_full_ci"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fa fa-building"></i><b> Empresa</b></label>
                            <p id="p_modal_close_enterprise"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fa fa-phone"></i><b> Telefono</b></label>
                            <p id="p_modal_close_phone"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fa fa-envelope"></i><b> Email</b></label>
                            <p id="p_modal_close_email"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label><i class="fa fa-edit"></i><b> Motivo de solicitud</b></label>
                            <p id="p_modal_close_type"></p>
                        </div>
                        <div class="col-md-8">
                            <label><i class="fa fa-comment"></i><b> Comentario u Observacion</b></label>
                            <p id="p_modal_close_comment"></p>
                        </div>
                    </div>
                </div>
                <hr>
                <h3><span class="badge badge-warning"><i class="fa fa-times-circle"></i> <b>¿Desea Cerrar el Caso?</b></span></h3>
                <p>Al presionar el botón de cerrar, confirma que el caso ya fue resuelto. Puede Dejar un comentario u observación de este a continuación:</p>
                <div class="form-group col-md-12">
                    <label>{{ __('Comentario u Observación') }}</label>
                    <div class="input-group mb-3">
                        <textarea name="close_comment" rows="3" class="form-control @error('comment') is-invalid @enderror" placeholder="{{ __('Comentario u Observación') }}">@if(isset($appointment)){{ $appointment->comment }}@else{{ old('comment') }}@endif</textarea>

                        <span class="invalid-feedback" role="alert">
                        @error('close_comment')
                            <strong>{{ $message }}</strong>
                        @enderror
                        </span>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button id="btnCerrarModalAsignacion" type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-close"></i> No, revisar la información!
                </button>
                <button id="btnCierreCaso" class="btn btn-warning">
                    <i class="fa fa-times-circle"></i> De acuerdo, cerrar caso!
                </button>
            </div>
        </div>
    </div>
</div>
