<input type="hidden" name="medical_request_id" id="medical_request_id" value="{{ $medical_request->id }}">
<div class="modal-body">
    <div class="container-fluid">
        @include('medical_consultations.partials.datos_paciente')
        @include('layouts.components.modals.partials.table_modal_tracings')
    </div>
    <hr>
    <div class="row">
        <hr>
        <div class="form-group col-md-12 col-sm-12">
            <input type="radio" name="radio_option" id="radio_seguir" value="seguimiento"  required="required" checked>
            <label for="radio_seguir">Asignar a seguimiento</label>
            <input type="radio" name="radio_option" id="radio_cita_medica" value="cita"  required="required">
            <label for="radio_cita_medica">Asignar cita médica</label>
        </div>
    </div>
    {{--DATOS DE SEGUIMIENTO--}}
    <div class="form-row" id="div_seguimiento">
        <div class="col-md-6 col-sm-12">
            <h4>Datos de Atención</h4>
            <hr>
            <div class="form-row">
            <div class="col-12">
                <label for="personal" class="control-label col-12">{{ __('*Personal seguimiento') }}</label>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <select name="personal_id" id="personal_id" required class="form-control select2" style="width: 100%;">
                            <option></option>
                            @foreach($users_seguimiento as $user)
                                <option value="{{$user->id}}">{{$user->full_name_rol}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

        </div>
            @include('layouts.components.modals.partials.form_appointment_tracing')
        </div>

        <div class="col-md-6 col-sm-12">
            <h4>Citas Programadas</h4>
            <hr>
            <div class="form-row" id="div_doctor_agenda_atencion">

            </div>
        </div>
    </div>

    {{--CITA MÉDICA--}}

    <div class="form-row" id="div_cita_medica" hidden>
        <div class="col-md-6 col-sm-12">
            <h4>Datos de Atención</h4>
            <hr>
            <div class="form-row">
                <div class="col-12">
                    <label for="medico" class="control-label col-12">{{ __('*Medico') }}</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <select name="medico_id" id="medico_id" required class="form-control select2" style="width: 100%;">
                                <option></option>
                                @foreach($users_medico as $user)
                                    <option value="{{$user->id}}">{{$user->full_name_rol}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            @include('layouts.components.modals.partials.form_appointment')
        </div>
        <div class="col-md-6 col-sm-12">
            <h4>Citas Programadas</h4>
            <hr>
            <div class="form-row" id="div_doctor_agenda">

            </div>
        </div>
    </div>
</div>
<!-- date-range-picker -->
{{--<script src="{{ asset('adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>--}}
<script src="{{ asset('js/components/appointment.js') }}"></script>
<script src="{{ asset('js/components/appointment_tracing.js') }}"></script>
<script>
    $(document).ready(function() {
        $('select[name=appointment_type_id]').select2({
            theme: 'bootstrap4',
            width: 'auto',
            placeholder: '* Seleccione un tipo de atención'
        });

        $('select[name=appointment_type_tracing_id]').select2({
            theme: 'bootstrap4',
            width: 'auto',
            placeholder: '* Seleccione un tipo de atención'
        });

        $('select[name=personal_id]').select2({
            theme: 'bootstrap4',
            placeholder: '* Seleccione a una persona'
        });

        $('select[name=medico_id]').select2({
            theme: 'bootstrap4',
            placeholder: '* Seleccione un médico'
        });

        $('[data-mask]').inputmask({
            removeMaskOnSubmit: true,
        });

        $('#next_attention_date').datetimepicker({
            format: 'DD/MM/YYYY',
        });

        $('#next_attention_date_tracing').datetimepicker({
            format: 'DD/MM/YYYY',
        });

        $('#next_attention_time').datetimepicker({
            format: 'LT'
        });

        $('#next_attention_time_tracing').datetimepicker({
            format: 'LT'
        });
    });
    $('input[type=radio][name^=radio_option]').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });

    $('input[type=radio][name^=radio_option]').on('ifChecked', function(event){
        //$('select[name=appointment_type_tracing_id]').val('').trigger('change');

        switch ($(this).val()) {
            case "seguimiento":
                $('div#div_cita_medica').attr('hidden', 'hidden');
                $('div#div_seguimiento').removeAttr('hidden');
                $('#btnCita').attr({
                    disabled: 'disabled',
                    hidden: 'hidden'
                });
                $('#btnSeguimiento').removeAttr('disabled');
                $('#btnSeguimiento').removeAttr('hidden');
                $('select#appointment_type_tracing_id').val(4).trigger('change');
                $('#div_seguimiento').find('input').removeAttr('diabled');
                $('#div_cita_medica').find('input').attr('disable', 'disabled');
                break;
            case "cita":
                $('div#div_cita_medica').removeAttr('hidden');
                $('div#div_seguimiento').attr('hidden', 'hidden');
                $('#btnSeguimiento').attr({
                    disabled: 'disabled',
                    hidden: 'hidden'
                });
                $('select[name=appointment_type_id]').val(1).trigger('change');
                $('#btnCita').removeAttr('disabled');
                $('#btnCita').removeAttr('hidden');
                break;
        }

    });

    var fecha_atencion = null;
    $("#next_attention_date").on("change.datetimepicker", ({date, oldDate}) => {
        fecha_atencion = date;
        searchAppointments();
    });

    var fecha_atencion_tracing = null;
    $("#next_attention_date_tracing").on("change.datetimepicker", ({date, oldDate}) => {
        fecha_atencion_tracing = date;
        searchAppointmentSeguim();
    });

    function searchAppointments() {
        var selected_op = $('select#medico_id option:selected').val();

        if(selected_op!=null && selected_op!="" && fecha_atencion!=null){
            var url = citasAppointment;
            var token = $("meta[name=csrf-token]").attr("content");

            $.ajax({
                url: url,
                method: 'post',
                headers: {'X-CSRF-TOKEN': token},
                data: {
                    user_id: selected_op,
                    next_attention_date: fecha_atencion.format('YYYY-MM-DD')
                },
                beforeSend: function(e){
                    var div_alert = '<div class="alert alert-warning col-12">'+
                                        '<h5><i class="icon fa fa-spin fa-spinner"></i> ¡Cargando!</h5>'+
                                        '<p>Cargando datos de la cita, espere porfavor...</p>'+
                                    '</div>';
                    $('div#div_doctor_agenda').empty();
                    $('div#div_doctor_agenda').append(div_alert);
                }
            })
            .done(function(response) {
                switch (response['type']) {
                    case "correct":
                        $('div#div_doctor_agenda').empty();
                        $('div#div_doctor_agenda').append(response['view']);
                        break;
                    case "error":
                        toastr.error(
                            response['msj'],
                            '¡Error!'
                        );
                        var div_alert = '<div class="alert alert-danger col-12">'+
                            '<h5><i class="icon fa fa-spin fa-spinner"></i> ¡Error!</h5>'+
                            '<p>Existió algun error a la hora de cargar los datos de cita</p>'+
                            '</div>';
                        $('div#div_doctor_agenda').empty();
                        $('div#div_doctor_agenda').append(div_alert);
                        break;
                }
            })
            .fail(function() {
                var div_alert = '<div class="alert alert-danger col-12">'+
                    '<h5><i class="icon fa fa-spin fa-spinner"></i> ¡Error!</h5>'+
                    '<p>Existió algun error a la hora de cargar los datos de cita</p>'+
                    '</div>';
                $('div#div_doctor_agenda').empty();
                $('div#div_doctor_agenda').append(div_alert);
            });
        }
    }

    function searchAppointmentSeguim(){
        var selected_op = $('select#personal_id option:selected').val();

        if(selected_op!=null && selected_op!="" && fecha_atencion_tracing!=null){
            var url = citasAppointment;
            var token = $("meta[name=csrf-token]").attr("content");

            $.ajax({
                url: url,
                method: 'post',
                headers: {'X-CSRF-TOKEN': token},
                data: {
                    user_id: selected_op,
                    next_attention_date: fecha_atencion_tracing.format('YYYY-MM-DD')
                },
                beforeSend: function(e){
                    var div_alert = '<div class="alert alert-warning col-12">'+
                        '<h5><i class="icon fa fa-spin fa-spinner"></i> ¡Cargando!</h5>'+
                        '<p>Cargando datos de la cita, espere porfavor...</p>'+
                        '</div>';
                    $('div#div_doctor_agenda_atencion').empty();
                    $('div#div_doctor_agenda_atencion').append(div_alert);
                }
            })
            .done(function(response) {
                switch (response['type']) {
                    case "correct":
                        $('div#div_doctor_agenda_atencion').empty();
                        $('div#div_doctor_agenda_atencion').append(response['view']);
                        break;
                    case "error":
                        toastr.error(
                            response['msj'],
                            '¡Error!'
                        );
                        var div_alert = '<div class="alert alert-danger col-12">'+
                            '<h5><i class="icon fa fa-spin fa-spinner"></i> ¡Error!</h5>'+
                            '<p>Existió algun error a la hora de cargar los datos de cita</p>'+
                            '</div>';
                        $('div#div_doctor_agenda').empty();
                        $('div#div_doctor_agenda').append(div_alert);
                        break;
                }
            })
            .fail(function() {
                var div_alert = '<div class="alert alert-danger col-12">'+
                    '<h5><i class="icon fa fa-spin fa-spinner"></i> ¡Error!</h5>'+
                    '<p>Existió algun error a la hora de cargar los datos de cita</p>'+
                    '</div>';
                $('div#div_doctor_agenda').empty();
                $('div#div_doctor_agenda').append(div_alert);
            });
        }
    }

    $('select#medico_id').change(function (e) {
        searchAppointments();
        var selected_op = $('select#medico_id option:selected').val();
        if(selected_op!=null && selected_op!=""){
            searchAppointments();
        }
    });

    $('select#personal_id').change(function (e) {
        searchAppointmentSeguim();
        var selected_op = $('select#personal_id option:selected').val();
        if(selected_op!=null && selected_op!=""){
            searchAppointmentSeguim();
        }
    });
</script>
