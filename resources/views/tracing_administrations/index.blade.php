@extends('layouts.app_menu')

@section('styles')
    <style>
        .dataTables_filter {
            display: none !important;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Listado de Casos</h1>
                </div>

            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                @include('tracing_administrations.partials.tab')
            </div>
        </div>
    </section>
    <!--MODALS-->
    @include('tracing_administrations.modals.modal_patient_assign')
    @include('tracing_administrations.modals.modal_edit_seguimiento')
    @include('tracing_administrations.modals.modal_employee_assign')
    @include('layouts.components.modals.modal_history')
    @include('tracing_administrations.modals.modal_close')
    @include('layouts.components.modals.modal_covid_file')
    @include('layouts.components.modals.modal_history_timeline')
    @include('layouts.components.modals.modal_tracings')
    @include('layouts.components.modals.modal_files')
    @include('complementary_exams.modals.modal_complementary_exam')
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('a#nav_administracion_seguimientos').addClass('active');

            $('#enterprise_id').select2({
                theme: 'bootstrap4',
                width: 'auto',
                placeholder: 'Selecione una empresa'
            });
            $('#ci_expedition').select2({
                theme: 'bootstrap4',
                width: 'auto',
                placeholder: 'Selecione expedicion'
            });
            $('select[name=employee_id]').select2({
                theme: 'bootstrap4',
                width: 'auto',
                placeholder: 'Selecionde un empleado'
            });

            getTableRender("{{ route('administracion_seguimientos.index') }}");
        });
        /*RUTAS DEL SISTEMA*/
        const patientAsign = @json(route('administracion_seguimientos.get_patient', '_ID'));
        const patientAsignStore = @json(route('administracion_seguimientos.store'));
        const patientAsignMedicStore = @json(route('administracion_seguimientos.storeMedic'));
        const seguimientoIndex = @json(route('administracion_seguimientos.index'));
        const forSearchProjects = @json(route('proyectos.para_buscador.getProjectsFromEnterpriseSearch','_id'));

        const editSeguimiento = @json(route('administracion_seguimientos.get_ultimo_seguimiento', '_ID'));
        const storeSeguimiento = @json(route('administracion_seguimientos.storeSeguimiento'));

        const search = @json(route('empleados.busqueda_empresa.getEmployeeFromEnterprise','_id'));
        const searchCi = @json(route('empleados.busqueda_ci.getEmployeeFromCI',['ci'=>'_ci','ci_expedition'=>'_ci_exp']));
        const assingToEmp = @json(route('administracion_seguimientos.asignar_empleado.assignToEmploye'));
        const assingToMed = @json(route('administracion_seguimientos.asignar_medico.assignToMedic'));
        const assingToMe = @json(route('administracion_seguimientos.asignarme.assignToMe'));
        const employyeNotFound = @json(route('administracion_seguimientos.no_encontrado.employeeNotFound'));
        const verifyStatePre = @json(route('administracion_seguimientos.verificar.verifyStatePreSol'));
        const closeStatePre = @json(route('administracion_seguimientos.cerrar_pre.closeStatePreSol'));
        const closeStateAuto = @json(route('administracion_seguimientos.cerrar_pre.closeChangeStateAuto'));
        const updateRegisters = @json(route('administracion_seguimientos.actualizar_tabla.updateRegisters'));
        const showHistory = @json(route('administracion_solicitudes.mostrar_historial.showHistory','_id'));
        const closeCase = @json(route('administracion_seguimientos.cerrar_caso.closeCase','_id'));
        const citasAppointment = @json(route('citas.get_appointments.getAppointmentsFromUserDate'));
        const showHistoryTimeline = @json(route('buscar_codigo.mostrarTimeline'));
        const showHistoryFiles = @json(route('mostrar_archivos.showFiles','_id'));
        const chkGetAsign = @json(route('administracion_seguimientos.get_medical_request'));

        /*FUNCIONES*/
        function getValues() {
            $('#search_data').val('');

            var tab_value = $('#id_nav_tab').find('a.active').attr('data-value');
            var medical_request_id_search = $('#medical_request_id_search option:selected').val();
            var enterprise_id_search = $('#enterprise_id_search option:selected').val();
            var project_id_search = $('#project_id_search option:selected').val();
            /*var option_date_search = $('#option_date_search option:selected').val();
            var start_date = $('#start_date_search').data('datetimepicker').date();

            console.log(start_date);*/

            var url = seguimientoIndex;
            getTableRender(url,{
                'tab_value': tab_value,
                'medical_request_id_search': medical_request_id_search,
                'enterprise_id_search': enterprise_id_search,
                'project_id_search': project_id_search,
                // 'option_date_search': option_date_search,
                // 'start_date': start_date,
            });
        }
    </script>
    <script src="{{asset('js/tracing_administration/index.js')}}"></script>
    <script src="{{asset('js/components/button_historial.js')}}"></script>
    <script src="{{asset('js/components/button_historial_timeline.js')}}"></script>
    <script src="{{asset('js/components/search.js')}}"></script>
@endsection
