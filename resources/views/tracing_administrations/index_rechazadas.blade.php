@extends('layouts.app_menu')

@section('styles')
    <style>
        .dataTables_filter {
            display: none !important;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Listado de Casos rechazados</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Consultas rechazadas</h3>

                            <div class="card-tools">
                                <p id="demo"></p>

                                <div class="input-group input-group-sm">
                                    <input id="search_data" type="text" class="form-control" placeholder="Buscar">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">

                            @include('tracing_administrations.partials.table_rechazadas')
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
        </div>
    </section>

    <!--MODALS-->
    @include('layouts.components.modals.modal_history')

@endsection

@section('script')
    <script>
        const showHistory = @json(route('administracion_solicitudes.mostrar_historial.showHistory','_id'));

        $(document).ready(function() {
            $('a#nav_rechazadas').addClass('active');

            $('.select2').select2({
                theme: 'bootstrap4',
                width: 'auto'
            });
            var oTable = $('#medical_requests_administration').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                "pageLength": 5,
                "order": [[3, 'desc']],
                "dom": 'lrtp',
                "aoColumnDefs": [{ "bSortable": false, "aTargets": [0] }],
                "language": {
                    "decimal": ",",
                    "thousands": ".",
                    "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "infoPostFix": "",
                    "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "loadingRecords": "Cargando...",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "paginate": {
                        "first": "Primero",
                        "last": "Último",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "searchPlaceholder": "Término de búsqueda",
                    "zeroRecords": "No se encontraron resultados",
                    "emptyTable": "Ningún dato disponible en esta tabla",
                    "aria": {
                        "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                }
            });
            $('input#search_data').keyup(function () {
                oTable.search($(this).val()).draw();
            });
        });
        /*RUTAS DEL SISTEMA*/
    </script>
    {{-- <script src="{{asset('js/tracing_administration/index_rechazadas.js')}}"></script> --}}
    <script src="{{asset('js/components/button_historial.js')}}"></script>

@endsection
