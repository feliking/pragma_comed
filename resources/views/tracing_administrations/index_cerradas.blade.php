@extends('layouts.app_menu')

@section('styles')
    <style>
        .dataTables_filter {
            display: none !important;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Consultas Cerradas</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Listado de consultas</h3>

                            <div class="card-tools">
                                <div class="input-group input-group-sm">
                                    <input id="search_data" type="text" class="form-control" placeholder="Buscar">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @include('tracing_administrations.partials.table_cerradas')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--MODALS-->
    @include('layouts.components.modals.modal_history')
    @include('layouts.components.modals.modal_history_timeline')
    @include('layouts.components.modals.modal_tracings')
    @include('layouts.components.modals.modal_files')
@endsection

@section('script')
    <script>
        const showHistory = @json(route('administracion_solicitudes.mostrar_historial.showHistory','_id'));
        const showHistoryTimeline = @json(route('buscar_codigo.mostrarTimeline'));
        const showHistoryFiles = @json(route('mostrar_archivos.showFiles','_id'));
        $(document).ready(function() {
            $('a#nav_close_requests').addClass('active');
        });
        $(function () {
            var oTable = $('#medical_requests_administration').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                "pageLength": 5,
                "order": [[10, 'desc']],
                "dom": 'lrtp',
                "aoColumnDefs": [{ "bSortable": false, "aTargets": [0] }],
                "language": {
                    "decimal": ",",
                    "thousands": ".",
                    "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "infoPostFix": "",
                    "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "loadingRecords": "Cargando...",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "paginate": {
                        "first": "Primero",
                        "last": "Último",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "searchPlaceholder": "Término de búsqueda",
                    "zeroRecords": "No se encontraron resultados",
                    "emptyTable": "Ningún dato disponible en esta tabla",
                    "aria": {
                        "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                }
            });
            $('input#search_data').keyup(function () {
                oTable.search($(this).val()).draw();
            });

        });
    </script>
    <script src="{{asset('js/medical_consultations/index_cerradas.js')}}"></script>
    <script src="{{asset('js/components/button_historial.js')}}"></script>
    <script src="{{asset('js/components/button_historial_timeline.js')}}"></script>

@endsection
