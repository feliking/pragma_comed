@if (\Illuminate\Support\Facades\Auth::user()->role_id==7
    || \Illuminate\Support\Facades\Auth::user()->role_id==10 || \Illuminate\Support\Facades\Auth::user()->role_id==11)
    <div class="mailbox-controls">
        <div class="btn-group">
            <button type="button" class="btn btn-dark btn-sm" id="btn_asignar" data-toggle="tooltip" data-placement="top" title="Asignar a seguimiento o consulta" {{ ($tab == 'pendiente') ? 'hidden' : '' }}>
                <i class="fa fa-file-medical"> Asignación en lote</i>
            </button>
        </div>
    </div>
@endif
<form id="frm_check_asignacion" action="">
<table id="medical_requests_administration" class="table table-striped table-hover">
    <thead>
    <tr>
        @if (\Illuminate\Support\Facades\Auth::user()->role_id==7 || \Illuminate\Support\Facades\Auth::user()->role_id==10 || \Illuminate\Support\Facades\Auth::user()->role_id==11)<th></th>@endif
        <th>Op.</th>
        <th>Estado</th>
        <th>Ult. Consulta</th>
        <th>Sug. Prox. F.</th>
        <th>Días Aislamiento</th>
        <th>Riesgo</th>
        <th>Cód. de Solicitud</th>
        <th>Nombre Completo</th>
        <th>CI</th>
        <th>Empresa</th>
        <th>Proyecto</th>
        <th>Cód. de Empleado</th>
        <th>Nro de Teléfono</th>
        <th>Pers. Asig.</th>
    </tr>
    </thead>

    <tbody>

    @foreach($medical_requests as $medical_request)
        @php
          $class_tr = "";
            if($medical_request->last_tracing!=null){
                if($medical_request->last_tracing->medical_attention_flag){
                    $class_tr = "table-danger";
                }
            }
        @endphp

        <tr class="{{ $class_tr }}" id="tr_medical_consultation_{{$medical_request->id}}">
            @include('tracing_administrations.partials.tr_tracing_content')
        </tr>
    @endforeach

    </tbody>
    <tfoot>
    <tr>
        @if (\Illuminate\Support\Facades\Auth::user()->role_id==7 || \Illuminate\Support\Facades\Auth::user()->role_id==10 || \Illuminate\Support\Facades\Auth::user()->role_id==11)<th></th>@endif
        <th>Op.</th>
        <th>Estado</th>
        <th>Ult. Consulta</th>
        <th>Sug. Prox. F.</th>
        <th>Días Aislamiento</th>
        <th>Riesgo</th>
        <th>Cód. de Solicitud</th>
        <th>Nombre Completo</th>
        <th>CI</th>
        <th>Empresa</th>
        <th>Proyecto</th>
        <th>Cód. de Empleado</th>
        <th>Nro de Teléfono</th>
        <th>Pers. Asig.</th>
    </tr>
    </tfoot>
</table>
</form>

<script>
    //HORA PARA EL NOMBRE, PQ SEGURO ME PEDIRAN ESO
    var d = new Date();

    var month = d.getMonth()+1;
    var day = d.getDate();
    var hour = d.getHours();
    var minutes = d.getMinutes();
    var seconds = d.getSeconds();

    var output = d.getFullYear() + '/' +
        ((''+month).length<2 ? '0' : '') + month + '/' +
        ((''+day).length<2 ? '0' : '') + day + "_" +hour+minutes+seconds;
    $(document).ready(function() {
        var oTable_adm = $('#medical_requests_administration').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            "columnDefs": [
                { "targets": 0, "bSortable": false },
                { "type": "html", "targets": 1 },
                { "type": "date", "targets": 2 },
                { "type": "date", "targets": 3 },
                { "type": "html-num", "targets": 4 }
            ],
            //ESTA OPCION DE DOM ES PARA ESCOGER ELEMENTOS QUE APARECEN POR DEFECTO EN EL DATA TABLE
            "dom": 'Blrtp',
            buttons: [
                {"extend": 'excelHtml5',
                    "text":'<i class="fa fa-file-excel"></i> Exportar a Excel',
                    "className": 'btn btn-success',"title": "CasosActivos_"+output,
                    exportOptions: {
                        columns: [ 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 ]
                    }
                },
                {"extend": 'copyHtml5',
                    "text":'<i class="fa fa-copy"></i> Copiar',
                    "className": 'btn btn-info',
                    exportOptions: {
                        columns: [ 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 ]
                    }
                },
            ],
            "pageLength": 10,
            "order": [[3, 'desc']],
            "aoColumnDefs": [{ "bSortable": false, "aTargets": [0] }],
            "language": {
                "decimal": ",",
                "thousands": ".",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoPostFix": "",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                "lengthMenu": "Mostrar _MENU_ registros",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "processing": "Procesando...",
                "search": "Buscar:",
                "searchPlaceholder": "Término de búsqueda",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla",
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },

            }
        });
        $('input#search_data').keyup(function () {
            oTable_adm.search($(this).val()).draw();
        });

    });

    $('#btn_asignar').click(function(event) {
        var data = $('#frm_check_asignacion').serialize();
        console.log('dta', data);
        var token = $("meta[name=csrf-token]").attr("content");
        var url = chkGetAsign;
        console.log(url);
        $.ajax({
            url: url,
            method: 'post',
            headers: {'X-CSRF-TOKEN': token},
            data: data,
            beforeSend: function(e){
                // Loading
                // blockCard('#form_content');
            }
        })
        .done(function(response) {
            $('#frm_content').empty();
            $('#frm_content').append(response.content);
            var apoitment = $('#radio_seguir').is(':checked');
            var apoitment_med = $('#radio_cita_medica').is(':checked');
            if (apoitment) {
                $('select#appointment_type_tracing_id').val(4).trigger('change');
            }
            if (apoitment_med) {
                $('select[name=appointment_type_id]').val(1).trigger('change');
            }
            $('#modal_patient_assign').modal({
                show: true
            });

        })
        .fail(function() {
            Swal.fire(
                'Error',
                'Debe seleccionar al menos un elemento',
                'danger'
            );
        });
    });
</script>
