<table id="medical_requests_administration" class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Op.</th>
        <th>Estado</th>
        <th>Cód. de Solicitud</th>
        <th>Nombre Completo</th>
        <th>CI</th>
        <th>Empresa</th>
        <th>Email</th>
        <th>Cód. de Empleado</th>
        <th>Nro de Teléfono</th>
        {{--            <th>Empleado Asignado</th>--}}
        <th>Personal Asignado</th>
        <th>Fecha de Cierre</th>
        <th>Personal de Cierre</th>
    </tr>
    </thead>

    <tbody>
    @foreach($medical_requests as $medical_request)
        <tr id="tr_medical_consultation_{{$medical_request->id}}">
            @include('tracing_administrations.partials.tr_cerradas_content')
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th>Op.</th>
        <th>Estado</th>
        <th>Cód. de Solicitud</th>
        <th>Nombre Completo</th>
        <th>CI</th>
        <th>Empresa</th>
        <th>Email</th>
        <th>Cód. de Empleado</th>
        <th>Nro de Teléfono</th>
        {{--            <th>Empleado Asignado</th>--}}
        <th>Personal Asignado</th>
        <th>Fecha de Cierre</th>
        <th>Personal de Cierre</th>
    </tr>
    </tfoot>
</table>




