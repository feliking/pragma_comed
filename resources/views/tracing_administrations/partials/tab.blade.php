<div class="col-12">
    <div class="card card-primary card-outline card-tabs">
        <div class="card-header p-0 pt-1 border-bottom-0">
            <ul class="nav nav-tabs" id="id_nav_tab" role="tablist">
                <li class="nav-item" style="float: left;">
                    <a class="nav-link" id="active_tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="false" data-value='solicitado'>
                        Casos Nuevos <span id="tab_cant_nuevos" class="badge badge-dark">{{$cant_tabs[0]}}</span>
                    </a>
                </li>
                <li class="nav-item" style="float: left;">
                    <a class="nav-link active" id="active_tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="true" data-value='seguimiento'>
                        Casos Activos <span id="tab_cant_seguimientos" class="badge badge-dark">{{$cant_tabs[1]}}</span>
                    </a>
                </li>
                @if (\Illuminate\Support\Facades\Auth::user()->role_id==1 || \Illuminate\Support\Facades\Auth::user()->role_id!=8 || \Illuminate\Support\Facades\Auth::user()->role_id!=10)
                <li class="nav-item" style="float: left;">
                    <a class="nav-link" id="active_tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false" data-value='pendiente'>
                        Casos por Validar <span id="tab_cant_pendientes" class="badge badge-dark">{{$cant_tabs[2]}}</span>
                    </a>
                </li>
                @endif
                <li class="nav-item" style="float: left;">
                    <a class="nav-link" id="active_tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false" data-value='cerrados'>
                        Casos Cerrados <span id="tab_cant_casos_cerrados" class="badge badge-dark">{{$cant_tabs[3]}}</span>
                    </a>
                </li>
                <li class="nav-item" style="float: left;">
                    <a class="nav-link" id="active_tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false" data-value='rechazados'>
                        Casos Rechazados <span id="tab_cant_casos_rechazados" class="badge badge-dark">{{$cant_tabs[4]}}</span>
                    </a>
                </li>
                @if (\Illuminate\Support\Facades\Auth::user()->role_id==1
                    || \Illuminate\Support\Facades\Auth::user()->role_id==7
                    || \Illuminate\Support\Facades\Auth::user()->role_id==8
                    || \Illuminate\Support\Facades\Auth::user()->role_id==10
                    || \Illuminate\Support\Facades\Auth::user()->role_id==11)
                    <li role="" class="ml-auto pull-right" style="float: right;">
                        <a href="{{ route('call_center.solicitudes_medica') }}" type="button" class="btn btn-primary">
                            <i class="fa fa-btn fa-plus"></i> <b>Nueva Solicitud Médica</b>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content" id="active_tab">
                <div class="tab-pane fade show active" id="" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                    <div class="row">
                        <div class="col-md-12" id="div_buscador">
                            @include('layouts.components.cards.search_tracing_administration')
                        </div>
                    </div>
                    <div id="wrapper_table">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
