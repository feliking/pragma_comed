@if (\Illuminate\Support\Facades\Auth::user()->role_id==7
    || \Illuminate\Support\Facades\Auth::user()->role_id==10
    || \Illuminate\Support\Facades\Auth::user()->role_id==11)
    @include('layouts.components.buttons.button_opcion')
@endif
<form id="frm_check_asignacion" action="">
<table id="medical_requests_administration" class="table table-striped table-hover">
    <thead>
        <tr>
            @if (\Illuminate\Support\Facades\Auth::user()->role_id==7 || \Illuminate\Support\Facades\Auth::user()->role_id==10 || \Illuminate\Support\Facades\Auth::user()->role_id==11)<th></th>@endif
            <th>Op.</th>
            <th>Estado</th>
            <th>Cód. de Solicitud</th>
            <th>Nombre Completo</th>
            <th>CI</th>
            <th>Empresa</th>
            <th>Proyecto</th>
            <th>Email</th>
            <th>Cód. de Empleado</th>
            <th>Nro de Teléfono</th>
            <th>Personal Asignado</th>
        </tr>
    </thead>

    <tbody>
    @foreach($medical_requests as $medical_request)
        <tr id="tr_medical_consultation_{{$medical_request->id}}">
            @include('tracing_administrations.partials.tr_content')
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            @if (\Illuminate\Support\Facades\Auth::user()->role_id==7 || \Illuminate\Support\Facades\Auth::user()->role_id==10 || \Illuminate\Support\Facades\Auth::user()->role_id==11)<th></th>@endif
            <th>Op.</th>
            <th>Estado</th>
            <th>Cód. de Solicitud</th>
            <th>Nombre Completo</th>
            <th>CI</th>
            <th>Empresa</th>
            <th>Proyecto</th>
            <th>Email</th>
            <th>Cód. de Empleado</th>
            <th>Nro de Teléfono</th>
            <th>Personal Asignado</th>
        </tr>
    </tfoot>
</table>
</form>
<script>
    $(document).ready(function() {
        //HORA PARA EL NOMBRE, PQ SEGURO ME PEDIRAN ESO
        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();
        var hour = d.getHours();
        var minutes = d.getMinutes();
        var seconds = d.getSeconds();

        var output = d.getFullYear() + '/' +
            ((''+month).length<2 ? '0' : '') + month + '/' +
            ((''+day).length<2 ? '0' : '') + day + "_" +hour+minutes+seconds;
        //DATATBLES
        var oTable_adm = $('#medical_requests_administration').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            //ESTA OPCION DE DOM ES PARA ESCOGER ELEMENTOS QUE APARECEN POR DEFECTO EN EL DATA TABLE
            "dom": 'Blrtp',
            buttons: [
                {"extend": 'excelHtml5',
                    "text":'<i class="fa fa-file-excel"></i> Exportar a Excel',
                    "className": 'btn btn-success',"title": "CasosNuevos_"+output,

                },
                {"extend": 'copyHtml5',
                    "text":'<i class="fa fa-copy"></i> Copiar',
                    "className": 'btn btn-info',
                },
            ],
            "order": [[3, 'desc']],
            "aoColumnDefs": [{ "bSortable": false, "aTargets": [0] }],
            "language": {
                "decimal": ",",
                "thousands": ".",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoPostFix": "",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                "lengthMenu": "Mostrar _MENU_ registros",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "processing": "Procesando...",
                "search": "Buscar:",
                "searchPlaceholder": "Término de búsqueda",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla",
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },
            },
            "pageLength": 10
        });
        $('input#search_data').keyup(function () {
            oTable_adm.search($(this).val()).draw();
        });

    });

    $('#btn_asignar_list').click(function(event) {
        var data = $('#frm_check_asignacion').serialize();
        var token = $("meta[name=csrf-token]").attr("content");
        var url = chkGetAsign;
        $.ajax({
            url: url,
            method: 'post',
            headers: {'X-CSRF-TOKEN': token},
            data: data,
            beforeSend: function(e){
                // Loading
                // blockCard('#form_content');
            }
        })
        .done(function(response) {
            $('#frm_content').empty();
            $('#frm_content').append(response.content);
            var apoitment = $('#radio_seguir').is(':checked');
            var apoitment_med = $('#radio_cita_medica').is(':checked');
            if (apoitment) {
                $('select#appointment_type_tracing_id').val(4).trigger('change');
            }
            if (apoitment_med) {
                $('select[name=appointment_type_id]').val(1).trigger('change');
            }
            $('#modal_patient_assign').modal({
                show: true
            });

        })
        .fail(function() {
            Swal.fire(
                'Error',
                'Debe seleccionar al menos un elemento',
                'danger'
            );
        });
    });
</script>
