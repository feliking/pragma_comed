@if (\Illuminate\Support\Facades\Auth::user()->role_id==7
|| \Illuminate\Support\Facades\Auth::user()->role_id==10 || \Illuminate\Support\Facades\Auth::user()->role_id==11)
    <td>
        @if (($medical_request->medical_request_status_id == 1
               || $medical_request->medical_request_status_id == 5
               || $medical_request->medical_request_status_id == 12
               || $medical_request->medical_request_status_id == 16
               || $medical_request->medical_request_status_id == 17
               ))
            <div class="icheck-primary">
                <input type="checkbox" value="{{ $medical_request->id }}" id="check_{{ $medical_request->id }}" name="check_historical[]">
                <label for="check_{{ $medical_request->id }}"></label>
            </div>
            @else
        @endif
    </td>
@endif
<td>
    <div class="btn-group">
        @if (\Illuminate\Support\Facades\Auth::user()->role_id==4||\Illuminate\Support\Facades\Auth::user()->role_id==7|| \Illuminate\Support\Facades\Auth::user()->role_id==10|| \Illuminate\Support\Facades\Auth::user()->role_id==1 || \Illuminate\Support\Facades\Auth::user()->role_id==11)
        <a href="{{ route('call_center.edicion_solicitudes_medica', $medical_request->id) }}" class="btn btn-sm btn-default" data-toggle="tooltip" data-html="true"  data-placement="top" title="Editar Información del Paciente">
            <i class="fa fa-edit"></i>
            <span class="sr-only">Editar</span>
        </a>
        @endif
        @include('layouts.components.buttons.button_historial_timeline',[
                    'medical_request_code' => $medical_request->code
                ])
        @include('layouts.components.buttons.button_historial',[
                'medical_request_id' => $medical_request->id
            ])

        @if (\Illuminate\Support\Facades\Auth::user()->role_id==1
            || \Illuminate\Support\Facades\Auth::user()->role_id==7
            || \Illuminate\Support\Facades\Auth::user()->role_id==8
            || \Illuminate\Support\Facades\Auth::user()->role_id==10
            || \Illuminate\Support\Facades\Auth::user()->role_id==11)

            @if ($medical_request->employee_id == null && ($medical_request->medical_request_status_id == 1 || $medical_request->medical_request_status_id == 6))
                <button type="button" class="btn btn-sm btn-warning" onclick="abrirModalRegistrarEmpleado({{$medical_request->id}}, this)">
                    <i class="fa fa-user"></i>
                </button>
            @endif

            @if((\Illuminate\Support\Facades\Auth::user()->role_id==7 || \Illuminate\Support\Facades\Auth::user()->role_id==11) && (($medical_request->employee_id != null && ($medical_request->medical_request_status_id == 1
                                                        || $medical_request->medical_request_status_id == 6
                                                        || $medical_request->medical_request_status_id == 12
                                                        || $medical_request->medical_request_status_id == 16
                                                        || $medical_request->medical_request_status_id == 17))
                or ($medical_request->employee_id == null && ($medical_request->medical_request_status_id == 5
                                                        || $medical_request->medical_request_status_id == 12
                                                        || $medical_request->medical_request_status_id == 16
                                                        || $medical_request->medical_request_status_id == 17)))
                or (\Illuminate\Support\Facades\Auth::user()->role_id==10 && ($medical_request->medical_request_status_id == 1
                                                        || $medical_request->medical_request_status_id == 2
                                                        || $medical_request->medical_request_status_id == 5
                                                        || $medical_request->medical_request_status_id == 6
                                                        || $medical_request->medical_request_status_id == 12
                                                        || $medical_request->medical_request_status_id == 13
                                                        || $medical_request->medical_request_status_id == 14
                                                        || $medical_request->medical_request_status_id == 15
                                                        || $medical_request->medical_request_status_id == 16
                                                        || $medical_request->medical_request_status_id == 17)
                ))

                    <button type="button" class="btn btn-sm btn-dark" onclick="abrirModalAsignacion({{ $medical_request->id }})">
                        <i class="fa fa-file-medical"></i>
                    </button>

                    <button type="button" class="btn btn-sm btn-info" onclick="abrirModalUltimoSeguimiento({{ $medical_request->id }})" data-toggle="tooltip" data-placement="top" title="Ver ultimo seguimiento">
                        <i class="fa fa-adjust"></i>
                    </button>
            @endif

            @if(((\Illuminate\Support\Facades\Auth::user()->role_id==7 || \Illuminate\Support\Facades\Auth::user()->role_id==11) &&
                $medical_request->medical_request_status_id == 12)
                or
                \Illuminate\Support\Facades\Auth::user()->role_id==10 && ($medical_request->medical_request_status_id == 1
                                                        || $medical_request->medical_request_status_id == 2
                                                        || $medical_request->medical_request_status_id == 5
                                                        || $medical_request->medical_request_status_id == 6
                                                        || $medical_request->medical_request_status_id == 12
                                                        || $medical_request->medical_request_status_id == 13
                                                        || $medical_request->medical_request_status_id == 14
                                                        || $medical_request->medical_request_status_id == 15
                                                        || $medical_request->medical_request_status_id == 16
                                                        || $medical_request->medical_request_status_id == 17))
                <button type="button" class="btn btn-sm btn-danger" onclick="cerrarCaso({{$medical_request}}, this)">
                    <i class="fa fa-times-circle"></i>
                </button>
            @endif

            @if(\Illuminate\Support\Facades\Auth::user()->role_id==10 &&
                ($medical_request->medical_request_status_id != 3
                && $medical_request->medical_request_status_id != 4
                && $medical_request->medical_request_status_id != 7
                ))
                <a href="{{route('seguimientos.edit', $medical_request->id)}}" type="button" class="btn btn-sm btn-info" data-toggle="tooltip" data-html="true"  data-placement="top" title="Registrar seguimiento de solicitud <em>{{$medical_request->code}}</em>">
                    <i class="fa fa-arrow-alt-circle-right"></i>
                </a>
            @endif

            @include('layouts.components.buttons.button_tracings',[
                        'medical_request_id' => $medical_request->id
                    ])
        @endif
    </div>
</td>
<td>
    @php
        $today = Carbon\Carbon::now();
        $badge_status = "badge-info";
        switch ($medical_request->medical_request_status->id){
            case 12:
            $badge_status = "bg-orange";
            break;
            case 13:
            $badge_status = "badge-info";
            break;
            case 2:
            $badge_status = "badge-primary";
            break;
            case 14:
            $badge_status = "badge-danger";
            break;
            case 16:
            $badge_status = "badge-warning";
            break;
        }
         $badge_date = "badge-warning";
        if($medical_request->next_suggest_date != null){
            $date_proxima = Carbon\Carbon::parse($medical_request->next_suggest_date);
            if($today->greaterThan($date_proxima))
                $badge_date = "badge-danger";
            if($date_proxima->greaterThan($today))
                $badge_date = "badge-success";
        }
    @endphp
    <p>
    <span class="badge {{$badge_status}}">
        {{$medical_request->medical_request_status->name}}
    </span>
    @if($medical_request->medical_request_status_id==14)
        <span class="badge badge-danger">{{ $medical_request->reassign_count }}</span>
    @endif
    </p>
</td>
<td>
    @php
        $last_appointment = $medical_request
            ->appointments
            ->sortBy('id')
            ->last();
    @endphp

    @if($last_appointment!=null)
        @switch($last_appointment->appointment_type_id)
            @case(1)
            <span class="badge badge-warning" data-toggle="tooltip" data-placement="top" title="La cita será presencial">
                    <i class="fa fa-walking"></i>
                </span>
            @break
            @case(2)
            <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="La cita de zoom se hara a traves del siguiente link: {{$last_appointment->variable_text}}">
                    <i class="fa fa-video"></i>
                </span>
            @break
            @case(3)
            <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="El número para la llamada es: {{$last_appointment->variable_text}}">
                    <i class="fab fa-whatsapp"></i>
                </span>
            @break
            @default
            @break
        @endswitch
        {{$medical_request->last_appointment->next_attention_date->format('d/m/Y H:i A')}}
{{--        {{$medical_request->consulta_proxima->format('d/m/Y H:i A')}}--}}
        {{-- {{($medical_request->last_appointment_penul!=null)?$medical_request->last_appointment_penul->next_attention_date->format('d/m/Y H:i A'):''}} --}}
    @endif
</td>
<td>
    @if($medical_request->next_suggest_date!=null)
        <span class="badge {{$badge_date}}">
            {{ $medical_request->next_suggest_date->format('d/m/Y') }}
        </span>
    @else
        <span class="badge badge-warning">S/F</span>
    @endif
</td>
<td>
    @if($medical_request->last_tracing_day!='S/S')
        {{$medical_request->last_tracing_day}}
    @else
        <span class="badge badge-warning">
            S/S
        </span>
    @endif
</td>
<td>
    @if($medical_request->last_tracing!=null)
        <span class="badge" style="background-color: {{$medical_request->last_tracing->historical_weighting_level->color}}">
            {{$medical_request->last_tracing->historical_weighting_level->name}}
        </span>
        @include('layouts.components.buttons.button_covid_file',[
            'tracing_id' => $medical_request->last_tracing->id
        ])
    @else
        <span class="badge badge-warning">S/A</span>
    @endif
</td>
<td data-code>
    {{$medical_request->code}}
</td>
<td data-full_name>
    @if($medical_request->employee!=null)
        <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
        {{$medical_request->employee->full_name}}
    @else
        @if($medical_request->history_valid_status)
            <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
        @else
            <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
        @endif
        {{$medical_request->full_name}}
    @endif
</td>
<td data-full_ci>
    <input type="hidden" data-ci value="{{$medical_request->ci}}">
    <input type="hidden" data-ci_expedition value="{{$medical_request->ci_expedition}}">
    {{$medical_request->full_ci}}
</td>
<td data-enterprise>
    @if($medical_request->employee!=null)
        <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
        {{$medical_request->employee->enterprise->nombre}}
    @else
        @if($medical_request->history_valid_status)
            <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
        @else
            <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
        @endif
        {{$medical_request->enterprise}}
    @endif
</td>
<td>
    @if($medical_request->employee!=null)
        <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
        {{$medical_request->employee->project->nombre}}
    @else
        @if($medical_request->project!=null)
            @if($medical_request->history_valid_status)
                <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
            @else
                <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
            @endif
                {{$medical_request->project->nombre}}
        @else
            <span class="badge badge-warning" data-placement="top" title="Sin proyecto">S/P</span>
        @endif
    @endif
</td>
<td>
    {{$medical_request->employee_code}}
</td>
<td data-phone-all>
    <input type="hidden" id="data-phone" value="{{$medical_request->phone_number}}">
    <input type="hidden" id="data-phone-whatsapp_flag" value="{{$medical_request->whatsapp_flag}}">
    @if($medical_request->whatsapp_flag)
        <div class="badge badge-success" data-whatsapp>
            <i class="fab fa-whatsapp"></i>
        </div>
    @endif
    {{$medical_request->phone_number}}
</td>
<td>
    @if($medical_request->user_id!=null)
        {{$medical_request->medic->full_name}}
    @else
        <span class="badge badge-warning">
            S/A
        </span>
    @endif
</td>
