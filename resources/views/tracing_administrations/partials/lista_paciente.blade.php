<table class="table table-striped table-hover display" id="tbl_list_pacient" style="width:100% !important">
    <thead style="width: 100%">
        <tr>
            <th>Nombre Completo</th>
            <th>CI</th>
            <th>Empresa</th>
            <th>Horario</th>
        </tr>
    </thead>
    <tbody style="transform: traslate(0,0); height: 200px;overflow: auto;padding: 10px">
    @foreach($medical_requests as $medical_request)
        @php
          $class_tr = "";
            if($medical_request->last_tracing!=null){
                if($medical_request->last_tracing->medical_attention_flag){
                    $class_tr = "table-danger";
                }
            }
        @endphp
        <tr>
            <td data-full_name>
                @if($medical_request->employee!=null)
                    <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
                    {{$medical_request->employee->full_name}}
                @else
                    @if($medical_request->history_valid_status)
                        <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
                    @else
                        <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
                    @endif
                    {{$medical_request->full_name}}
                @endif
                <input type="hidden" name="medical_request_id[]" value="{{ $medical_request->id }}" placeholder="">
            </td>
            <td data-full_ci>
                <input type="hidden" data-ci_expedition value="{{$medical_request->ci_expedition}}">
                {{$medical_request->full_ci}}
            </td>
            <td data-enterprise>
                @if($medical_request->employee!=null)
                    <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
                    {{$medical_request->employee->enterprise->nombre}}
                @else
                    @if($medical_request->history_valid_status)
                        <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
                    @else
                        <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
                    @endif
                    {{$medical_request->enterprise}}
                @endif
            </td>
            <td data-horario class="data-horario">
                <p></p>
                <input type="hidden" name="hora_asignada[]"  value="">
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<script>

    // $('#tbl_list_pacient').DataTable( {
    //     "scrollY": "25vh",
    //     "paging": false,
    //     "lengthChange": false,
    //     "searching": false,
    //     "ordering": false,
    //     "info": true,
    //     "autoWidth": true,
    //     "responsive": false,
    //     "dom": 'lrtp',
    // } );

</script>
