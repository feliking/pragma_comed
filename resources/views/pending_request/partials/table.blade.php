<table id="medical_requests_table" class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Op.</th>
            <th>Cód. de Solicitud</th>
            <th>Nombre Completo</th>
            <th>CI</th>
            <th>Empresa</th>
            <th>Email</th>
            <th>Cód. de Empleado</th>
            <th>Nro de Teléfono</th>
        </tr>
    </thead>

    <tbody>
    @foreach($medical_requests as $medical_request)
        <tr id="tr_medical_request_{{ $medical_request->id }}">
            @include('pending_request.partials.tr_content', [
                'medical_request' => $medical_request
            ])
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Op.</th>
            <th>Cód. de Solicitud</th>
            <th>Nombre Completo</th>
            <th>CI</th>
            <th>Empresa</th>
            <th>Email</th>
            <th>Cód. de Empleado</th>
            <th>Nro de Teléfono</th>
        </tr>
    </tfoot>
</table>
