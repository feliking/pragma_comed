<!-- Modal -->
<div class="modal fade" id="modal_employee_assign" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('Registro de Empleado') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <label><i class="fa fa-barcode"></i><b> Codigo de Solicitud</b></label>
                            <p id="p_modal_code"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fa fa-user"></i><b> Nombre de empleado</b></label>
                            <p id="p_modal_fullname"></p>
                        </div>
                        <div class="form-group col-md-4">
                            <label><i class="fa fa-sort-numeric-desc"></i><b> CI</b></label>
                            <p id="p_modal_full_ci"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fa fa-building"></i><b> Empresa</b></label>
                            <p id="p_modal_enterprise"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fa fa-phone"></i><b> Telefono</b></label>
                            <p id="p_modal_phone"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fa fa-envelope"></i><b> Email</b></label>
                            <p id="p_modal_email"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fas fa-mobile-alt"></i><b> Corto</b></label>
                            <p id="p_modal_short"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fa fa-user"></i><b> Contacto de Referencia</b></label>
                            <p id="p_modal_refc"></p>
                        </div>
                        <div class="col-md-4">
                            <label><i class="fas fa-phone-alt"></i><b> Número de Referencia</b></label>
                            <p id="p_modal_refn"></p>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="form-group col-md-12 col-sm-12">
                    <input type="radio" name="radio_c" id="radio_fa2" value="2"  required="required" checked>
                    <label for="radio_fa2">Asignar empleado</label>
                    <input type="radio" name="radio_c" id="radio_fa1" value="1"  required="required">
                    <label for="radio_fa1">Validar empleado</label>
                </div>

                <div id="div_asigna_empleado">
                    <div class="form-group row">
                        <div class="col-md-11 col-sm-12">
                            <div class="input-group mb-3">
                                <input id="ci" name="ci" type="text" value="{{ old('ci') }}" class="form-control @if ($errors->has('ci') || $errors->has('ci_expedition')) is-invalid @endif" placeholder="{{ __('* Cédula') }}" data-inputmask='"mask": "9999999[9][-9A]"' data-greedy="false" data-mask/>
                                <div class="input-group-append append-ci">
                                    <select id="ci_expedition" name="ci_expedition" class="select2">
                                        <option value="LP">LP</option>
                                        <option value="CB">CB</option>
                                        <option value="SC">SC</option>
                                        <option value="CH">CH</option>
                                        <option value="OR">OR</option>
                                        <option value="PT">PT</option>
                                        <option value="TJ">TJ</option>
                                        <option value="BE">BE</option>
                                        <option value="PD">PD</option>
                                    </select>
                                </div>

                                @if ($errors->has('ci') || $errors->has('ci_expedition'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ci') ? : $errors->first('ci_expedition') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-1 col-sm-12">
                            <button class="btn btn-primary" onclick="buscarEmpleadoCI()">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>

                    <div class="form-group wrapper-empresa_id" id="wrapper-empresa_id">
                        <label for="empresa_id" class="control-label col-md-4 col-sm-4 col-xs-12">{{ __('*Empresa') }}</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <select id="enterprise_id" required value="{{ old('enterprise_id') }}" class="form-control select2 @error('enterprise_id') is-invalid @enderror" style="width: 100%;">
                                    <option></option>
                                    @foreach($enterprises as $enterprise)
                                        <option value="{{$enterprise->id}}">{{$enterprise->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <span class="help-block">
                                <strong></strong>
                            </span>
                        </div>
                    </div>

                    <div class="form-group wrapper-rrhh_empleado_id" id="wrapper-rrhh_empleado_id">
                        <label for="rrhh_empleado_id" class="control-label col-md-4 col-sm-4 col-xs-12">{{ __('*Empleado') }}</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <select disabled name="employee_id" required value="{{ old('employee_id') }}" class="form-control select2 @error('employee_id') is-invalid @enderror" style="width: 100%;">
                                    <option></option>
                                </select>
                            </div>
                            <span class="help-block">
                            <strong></strong>
                        </span>

                        </div>
                    </div>
                </div>
                <div id="div_favor_atender" hidden>
                    <div class="form-group row">
                        <div class="form-group col-md-6">
                            <label><i class="fa fa-user"></i><b> Nombre de empleado</b></label>
                            <div class="input-group mb-3">
                                <!--<p id="p_modal_fullname"></p>-->
                                <input id="txt_modal_full_name" name="full_name" type="text" class="form-control" placeholder="{{ __('* Nombre Completo') }}"/>
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-user"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- CARNET -->
                        <div class="form-group col-md-6">
                            <label><i class="fa fa-numeric"></i><b> Cédula</b></label>
                            <div class="input-group mb-3">
                                <input name="ci" id="ci" type="text" value="{{ old('ci') }}" class="form-control @if ($errors->has('ci') || $errors->has('ci_expedition')) is-invalid @endif" placeholder="{{ __('* Cédula') }}" data-inputmask='"mask": "9999999[9][-9A]"' data-greedy="false" data-mask/>
                                <div class="input-group-append append-ci">
                                    <select name="ci_expedition" id="ci_expedition" class="select2">
                                        <option value="LP">LP</option>
                                        <option value="CB">CB</option>
                                        <option value="SC">SC</option>
                                        <option value="CH">CH</option>
                                        <option value="OR">OR</option>
                                        <option value="PT">PT</option>
                                        <option value="TJ">TJ</option>
                                        <option value="BE">BE</option>
                                        <option value="PD">PD</option>
                                    </select>
                                </div>

                                @if ($errors->has('ci') || $errors->has('ci_expedition'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('ci') ? : $errors->first('ci_expedition') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <!-- EMPRESA -->
                        <div class="form-group col-md-6">
                            <label><i class="fa fa-building"></i><b> Empresa</b></label>
                            <div class="input-group mb-3">
                                <input name="enterprise" id="enterprise" type="text" value="{{ old('enterprise') }}" class="form-control @error('enterprise') is-invalid @enderror" placeholder="{{ __('* Empresa') }}">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-building"></span>
                                    </div>
                                </div>
                                @error('enterprise')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                </button>
                <button id="btnEliminarSolicitud" class="btn btn-danger" >
                    <i class="fa fa-minus-circle"></i> Rechazar solicitud médica
                </button>
                <button id="btnFavorAtender" class="btn btn-info" disabled="disabled" hidden>
                    <i class="fa fa-file-medical"></i> Validar Empleado
                </button>
                <button disabled id="btnAsignarEmpleado" type="button" class="btn btn-success">
                    <i class="fas fa-user-tie"></i> Asignar Empleado
                </button>
            </div>
        </div>
    </div>
</div>
