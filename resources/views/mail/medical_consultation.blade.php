@extends('layouts.app_mail')

@section('content')
    <h3>Estimado: {{ $medical_request->full_name }}</h3>

    <span>
        Su Solicitud de Atención Médica con código:
    </span>

    <br/>

    <h3>{{ $medical_request->code }}</h3>

    <span>
        Fue atendida por el medico: <b>{{ $doctor->employee->nombres . ' ' . $doctor->employee->apellido_1 . ' ' . $doctor->employee->apellido_2 }}</b> y registró lo siguiente:
    </span>

    <br/><br/>

    <table>
        <tr>
            <th>Fecha de Atención</th>
            <td>{{ $attention->attention_date->format('d/m/Y H:i') }}</td>
        </tr>
        <tr>
            <th>Tratamiento</th>
            <td>{{ $attention->treatment }}</td>
        </tr>
        <tr>
            <th>Observaciones</th>
            <td>{{ $attention->observation }}</td>
        </tr>
    </table>

    * Favor siga todas las instrucciones del médico y cumpla con el tratamiento respectivo.

    <br/><br/>

    <span>
        Este código puede ser utilizado para realizar seguimiento a su solicitud dando <a href="{{ route('busca.solicitudes_medicas') }}">Clic aquí</a>.
    </span>

    <br/><br/>

    <span>
        Este correo fue enviado automáticamente por nuestro Sistema de Consultas Médicas, favor no responda este correo electrónico.
    </span>
@endsection
