@extends('layouts.app_mail')

@section('content')
    <h3>Estimado: {{ $medical_request->full_name }}</h3>

    <span>
        Su Solicitud de Atención Médica con código:
    </span>

    <br/>

    <h3>{{ $medical_request->code }}</h3>

    <span>
        Fue asignada a una cita <b>{{ $appointment->appointment_type->name }}</b> en fecha: <b>{{ $appointment->next_attention_date->format('d/m/Y') }}</b> a horas: <b>{{ $appointment->next_attention_date->format('H:i') }}</b>
    </span>

    <br/>

    <span>
        <b>Detalle de la cita:</b>
        <br/>
        @switch($appointment->appointment_type_id)
            @case(2)
                El link para la reunión Zoom es el siguiente: <a href="{{ $appointment->variable_text }}">{{ $appointment->variable_text }}</a>
                @break
            @case(3)
                Recibirá una llamada vía Whatsapp del siguiente número: <b>{{ $appointment->variable_text }}</b>
                @break
        @endswitch
        <br/>
        <b>Nota:</b>
        <br/>
        {{ $appointment->comment }}
    </span>

    <br/><br/>

    <span>
        Este código puede ser utilizado para realizar seguimiento a su solicitud dando <a href="{{ route('busca.solicitudes_medicas') }}">Clic aquí</a>.
    </span>

    <br/><br/>

    <span>
        Este correo fue enviado automáticamente por nuestro Sistema de Consultas Médicas, favor no responda este correo electrónico.
    </span>
@endsection
