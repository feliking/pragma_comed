@extends('layouts.app_mail')

@section('content')
    <h3>Estimado: {{ $medical_request->full_name }}</h3>

    <span>
        Su Solicitud de Atención Médica con código:
    </span>

    <br/>

    <h3>{{ $medical_request->code }}</h3>

    <span>
        Fue asignada al medico: <b>{{ $medical_request->medic->employee->nombres . ' ' . $medical_request->medic->employee->apellido_1 . ' ' . $medical_request->medic->employee->apellido_2 }}</b>
    </span>

    <br/><br/>

    <span>
        Este código puede ser utilizado para realizar seguimiento a su solicitud dando <a href="{{ route('busca.solicitudes_medicas') }}">Clic aquí</a>.
    </span>

    <br/><br/>

    <span>
        Nuestro personal médico se comunicará en breve con usted, favor tenga paciencia y mantenga todas las medidas de bioseguridad.
    </span>

    <br/><br/>

    <span>
        Este correo fue enviado automáticamente por nuestro Sistema de Consultas Médicas, favor no responda este correo electrónico.
    </span>
@endsection
