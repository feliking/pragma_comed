@extends('layouts.app_mail')

@section('content')
    <h3>Estimado: {{ $medical_request->full_name }}</h3>

    <span>
        Su Solicitud de Atención Médica fue registrada y se le asignó el siguiente código:
    </span>

    <br/>

    <h3>{{ $medical_request->code }}</h3>

    <span>
        Este código puede ser utilizado para realizar seguimiento a su solicitud dando <a href="{{ route('busca.solicitudes_medicas') }}">Clic aquí</a>.
    </span>

    <br/><br/>

    <span>
        Nuestro personal médico se comunicará en breve con usted, favor tenga paciencia y mantenga todas las medidas de bioseguridad.
    </span>

    <br/><br/>

    <span>
        Este correo fue enviado automáticamente por nuestro Sistema de Consultas Médicas, favor no responda este correo electrónico.
    </span>
@endsection
