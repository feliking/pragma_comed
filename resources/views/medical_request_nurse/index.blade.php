@extends('layouts.app_menu')

@section('styles')
    <style>
        .dataTables_filter {
            display: none !important;
        }

        div.dt-buttons {
            float: right !important;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Mis Pacientes</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Datos de consultas médicas</h3>

                            <div class="card-tools">
                               {{--  <div class="input-group input-group-sm">
                                    <input id="search_data" type="text" class="form-control" placeholder="Buscar">
                                </div> --}}
                                <div class="mailbox-controls ml-auto pull-right" style="float: right;">
                                    <a href="{{ route('enfermera.create') }}" type="button" class="btn btn-primary">
                                        <i class="fa fa-btn fa-plus"></i> <b>Nueva Solicitud Médica</b>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            
                            <div class="tab-content" id="active_tab">
                                <div class="tab-pane fade show active" id="" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                                    <div class="row">
                                        <div class="col-md-12" id="div_buscador">
                                            @include('layouts.components.cards.search_nurse_pacients')
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- @include('medical_request_nurse.partials.table') --}}
                            <div id="wrapper_table">
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
        </div>
    </section>

    <!--MODALS-->
    @include('layouts.components.modals.modal_assign_appointment')
    @include('layouts.components.modals.modal_files')
    @include('layouts.components.modals.modal_history_timeline')
    @include('medical_request_nurse.modals.modal_signos_vitales')
    @include('layouts.components.modals.modal_history')

@endsection

@section('script')
    {{-- <script src="{{ asset('/js/jquery.mask.min.js') }}"></script> --}}
    
    <script>
        const showHistoryTimeline = @json(route('buscar_codigo.mostrarTimeline'));
        const showHistoryFiles = @json(route('mostrar_archivos.showFiles','_id'));
        $(document).ready(function() {
            $('a#nav_enfermera').addClass('active');
            
            getValues();
        });
        const assingToAten = @json(route('consultas_medicas.store'));
        const getAttenHist = @json(route('consultas_medicas.getAttentionHistorial', '_ID'));
        const getSignosVitales = @json(route('enfermera.get_signos_vitales','_ID'));
        const storeVitalSigns = @json(route('enfermera.storeVitalSigns'));
        const editVitalSigns = @json(route('enfermera.editSignosVitales','_ID'));
        const updateVitalSigns = @json(route('enfermera.updateVitalSigns'));
        const showHistory = @json(route('administracion_solicitudes.mostrar_historial.showHistory','_id'));
        const enfermeraIndex = @json(route('enfermera.index'));
        const forSearchProjects = @json(route('proyectos.para_buscador.getProjectsFromEnterpriseSearch','_id'));
        function getValues() {
            var medical_request_id_search = $('#medical_request_id_search option:selected').val();
            var enterprise_id_search = $('#enterprise_id_search option:selected').val();
            var project_id_search = $('#project_id_search option:selected').val();
            var url = enfermeraIndex;
            getTableRender1(url,{
                'medical_request_id_search': medical_request_id_search,
                'enterprise_id_search': enterprise_id_search,
                'project_id_search': project_id_search
            });
        }
        function clearValues() {
            $('#search_data').val('');
            $('select[name=medical_request_id_search]').val(0).trigger('change');
            $('select[name=enterprise_id_search]').val(0).trigger('change');
            $('select[name=project_id_search]').val(0).trigger('change');
            $('#init_date').val('');
            $('#end_date').val('');
            getValues();
        }
        function CleanDates(){
            $('#init_date').val('');
            $('#end_date').val('');
            getValues();
        }
    </script>
    <script src="{{asset('js/components/search.js')}}"></script>
    {{-- <script src="{{asset('js/medical_consultations/index.js')}}"></script> --}}
    <script src="{{asset('js/components/button_historial_timeline.js')}}"></script>
    <script src="{{asset('js/medical_request_nurse/index.js')}}"></script>
    <script src="{{asset('js/components/button_historial.js')}}"></script>

@endsection
