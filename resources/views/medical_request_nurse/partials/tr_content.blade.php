@php
    $last_appointment = $medical_request
        ->appointments
        ->sortBy('id')
        ->last();
@endphp

<td>
    <div class="btn-group">
        @if (\Illuminate\Support\Facades\Auth::user()->role_id==4||\Illuminate\Support\Facades\Auth::user()->role_id==7|| \Illuminate\Support\Facades\Auth::user()->role_id==10 || \Illuminate\Support\Facades\Auth::user()->role_id==11)
        <a href="{{ route('call_center.edicion_solicitudes_medica', $medical_request->id) }}" class="btn btn-sm btn-default" data-toggle="tooltip" data-html="true"  data-placement="top" title="Editar Información del Paciente">
            <i class="fa fa-edit"></i>
            <span class="sr-only">Editar</span>
        </a>
        @endif
        @include('layouts.components.buttons.button_historial_timeline',[
                    'medical_request_code' => $medical_request->code
                ])
        @include('layouts.components.buttons.button_historial',[
                'medical_request_id' => $medical_request->id])
        @if(count($medical_request->attention_vital_sign) > 0 )
            <button type="button" class="btn btn-sm btn-warning" onclick="abrirModalSignosVitales({{ $medical_request->id }}, 2)">
                <i class="fas fa-bookmark"></i>
            </button>
        @else
            <button type="button" class="btn btn-sm btn-success" onclick="abrirModalSignosVitales({{ $medical_request->id }}, 1)">
                <i class="fas fa-bookmark"></i>
            </button>
        @endif
        @if($medical_request->consulta_proxima->diffInMinutes(\Carbon\Carbon::now(),false) <= 0)
            {{--QUIERE DECIR QUE AUN NO SE PASO DE LA FECHA--}}
            @include('layouts.components.buttons.button_assign_appointment',[
                'medical_request_id' => $medical_request->id,
                'pass_date' => false,
                'last_appointment' => $last_appointment
               ])
        @else
            {{--AUN NO SE PASO DE LA FECHA--}}
            @include('layouts.components.buttons.button_assign_appointment',[
                'medical_request_id' => $medical_request->id,
                'pass_date' => true
               ])
        @endif
      </div>
</td>
<td>
    <span class="badge badge-info" >
        {{ $medical_request->medical_requests_state->name }}
    </span>
</td>
<td>
    @if($last_appointment!=null)
    @switch($last_appointment->appointment_type_id)
    @case(1)
    <span class="badge badge-warning" data-toggle="tooltip" data-placement="top" title="La cita será presencial">
        <i class="fa fa-walking"></i>
    </span>
    @break
    @case(2)
    <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="La cita de zoom se hara a traves del siguiente link: {{$last_appointment->variable_text}}">
        <i class="fa fa-video"></i>
    </span>
    @break
    @case(3)
    <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="El número para la llamada es: {{$last_appointment->variable_text}}">
        <i class="fab fa-whatsapp"></i>
    </span>
    @break
    @default
    @break
    @endswitch
    <span data-attention_date="{{$medical_request->consulta_proxima->format('Y/m/d')}}"></span>
    {{$medical_request->consulta_proxima->format('d/m/Y H:i A')}}
    @endif
</td>
<td>
    @if($medical_request->last_tracing_day!='S/S')
        {{$medical_request->last_tracing_day}}
    @else
        <span class="badge badge-warning">
            S/S
        </span>
    @endif
</td>
<td>
    {{ $medical_request->medic->name }}
</td>
<td>
    {{$medical_request->code}}
</td>
<td>
    @if($medical_request->employee!=null)
        <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
        {{$medical_request->employee->full_name}}
    @else
        @if($medical_request->history_valid_status)
            <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
        @else
            <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
        @endif
        {{$medical_request->full_name}}
    @endif
{{--    <input type="hidden" id="full_name_{{ $medical_request->id }}" value="{{$medical_request->full_name}}">--}}
</td>
<td>
    {{$medical_request->full_ci}}
</td>
<td>
    @if($medical_request->employee!=null)
        <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Empleado asignado"><i class="fa fa-user-check"></i></span>
        {{$medical_request->employee->enterprise->nombre}}
    @else
        @if($medical_request->history_valid_status)
            <span class="badge badge-info" data-toggle="tooltip" data-placement="top" title="Empleado validado"><i class="fa fa-user-check"></i></span>
        @else
            <span class="badge badge-danger" data-placement="top" title="Empleado sin asignar"><i class="fa fa-user-times"></i></span>
        @endif
        {{$medical_request->enterprise}}
    @endif
    <input type="hidden" id="empresa_name_{{ $medical_request->id }}" value="{{ $medical_request->enterprise }}">
</td>
<td>
    @if($medical_request->email!=null)
        {{$medical_request->email}}
    @else
        <span class="badge badge-warning">
            S/N
        </span>
    @endif
</td>
<td>
    <input type="hidden" id="employee_code_{{ $medical_request->id }}" value="{{ $medical_request->employee_code }}">
    {{$medical_request->employee_code}}
</td>
<td>
    @if($medical_request->whatsapp_flag)
        <div class="badge badge-success">
            <i class="fab fa-whatsapp"></i>
        </div>
    @endif
    {{$medical_request->phone_number}}
</td>
<td>
    {{ $medical_request->risk_type->name }}
</td>
