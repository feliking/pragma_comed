<table id="medical_consultations_table" class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Op.</th>
            <th>Estado</th>
            <th>Sug. Prox. F.</th>
            <th>Días Aislamiento</th>
            <th>Doctor</th>
            <th>Cód. de Solicitud</th>
            <th>Nombre Completo</th>
            <th>CI</th>
            <th>Empresa</th>
            <th>Email</th>
            <th>Cód. de Empleado</th>
            <th>Nro de Teléfono</th>
            <th>Tipo de Solicitud</th>
        </tr>
    </thead>

    <tbody>
    @foreach($medical_requests as $medical_request)
        <tr id="tr_medical_consultation_{{$medical_request->id}}">
            @include('medical_request_nurse.partials.tr_content',['medical_request'=>$medical_request])
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Op.</th>
            <th>Estado</th>
            <th>Sug. Prox. F.</th>
            <th>Días Aislamiento</th>
            <th>Doctor</th>
            <th>Cód. de Solicitud</th>
            <th>Nombre Completo</th>
            <th>CI</th>
            <th>Empresa</th>
            <th>Email</th>
            <th>Cód. de Empleado</th>
            <th>Nro de Teléfono</th>
            <th>Tipo de Solicitud</th>
        </tr>
    </tfoot>
</table>
<script>
    var sw = false;
    $(function() {
        var oTable_adm = $('#medical_consultations_table').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
           /*  "search": {
                "regex": true,
                "caseInsensitive": false,
            }, */
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            "columnDefs": [
                { "targets": 0, "bSortable": false },
                { "type": "html", "targets": 1 },
                { "type": "date", "targets": 2 },
                { "type": "html-num", "targets": 3 }
            ],
            //ESTA OPCION DE DOM ES PARA ESCOGER ELEMENTOS QUE APARECEN POR DEFECTO EN EL DATA TABLE
            "dom": 'lrtp',
            "pageLength": 10,
            "order": [[2, 'desc']],
            "aoColumnDefs": [{ "bSortable": false, "aTargets": [0] }],
            "language": {
                "decimal": ",",
                "thousands": ".",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoPostFix": "",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                "lengthMenu": "Mostrar _MENU_ registros",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "processing": "Procesando...",
                "search": "Buscar:",
                "searchPlaceholder": "Término de búsqueda",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla",
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },
        
            }
        });
        $('input#search_data').keyup(function () {
            oTable_adm.search($(this).val()).draw();
        });
        $('.datetimepicker-input').change(function() {
            
        });
        
        $('#init_date_field').datetimepicker({
            format: 'DD/MM/YYYY',
            locale: 'es',
            useCurrent: false,
            maxDate: moment(),
        });

        $('#end_date_field').datetimepicker({
            format: 'DD/MM/YYYY',
            locale: 'es',
            useCurrent: false,
        });
        
        $('#clear').click(function() {
            $("#init_date_field").data("DateTimePicker").date(null);
        })
        $("#init_date_field").on("change.datetimepicker", function (e) {
            // /console.log(e.date._d,moment(e.date._d).format('DD/MM/YYYY') );
            if(e.date!=undefined){
                $('#end_date_field').datetimepicker('minDate', e.date);
                min = moment(e.date._d).format('YYYY/MM/DD') || 0;
                sw = true;
                oTable_adm.draw();
            }
            
        });
        $("#end_date_field").on("change.datetimepicker", function (e) {
            
            if(e.date!=undefined){
                $('#init_date_field').datetimepicker('maxDate', e.date);
                max = moment(e.date._d).format('YYYY/MM/DD') || 0;
                sw = true;
                oTable_adm.draw();
            }
        });
    });
    var min = '';
    var max = '';
    $('.input-daterange input').each(function() {
        $(this).datepicker('clearDates');
    });

    // Extend dataTables search
    $.fn.dataTableExt.afnFiltering.push(
        function(settings, data, dataIndex) {
            
            var createdAt = $($(data[2])[2]).data('attention_date') || 0; // Our date column in the table
            if ((min != "" && max != "") && (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max)) && createdAt!=0) {
                return true;
            }else{
                if ((min == "" && max == ""&& createdAt!=0) &&sw) {
                    return false;
                }
                if ((min != "" && max == "" && createdAt!=0) && moment(createdAt).isSameOrAfter(min)) {
                    return true;
                }
                if ((min == "" && max != "" && createdAt!=0) && moment(createdAt).isSameOrBefore(max)) {
                    return true;
                }
            }
            if ((min != "" || max != "") && createdAt==0) {
                return true;
            }
            if ((min == "" || max == "" || createdAt==0) && !sw) {
                return true;
            }
        }
    );
</script>