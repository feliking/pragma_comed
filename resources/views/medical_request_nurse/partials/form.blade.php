@extends('medical_request.partials.form')

@section('search_by_ci')

<button id="btn_search_ci" type="button" onclick="search_by_nurse()" class="btn btn-info">
    <i class="fas fa-search"></i>
</button>

@endsection

@section('other_fields')

    <div class="card card-info card-outline">
        <div class="card-header">
            <h3 class="card-title"><b>DATOS DE ATENCIÓN</b></h3>
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="col-md-6 col-sm-12">
                    <h5>Datos de Atención</h5>
                    <hr>
                    <label>{{ __('*Médico que atenderá el caso: ') }}</label>
                    <!-- MÉDICOS -->
                    <div class="form-group col-md-12 col-sm-12">
                        <div class="input-group mb-3">
                            <select id="user_id" name="user_id" value="{{ old('user_id') }}" class="form-control @error('user_id') is-invalid @enderror">
                                <option></option>
                                @foreach($medical_users as $medical_user)
                                    <option value="{{ $medical_user->id }}" {{ old('user_id')?'selected':'' }} {{ ($medical_user->id == Auth::user()->id) ? 'selected':'' }}>
                                        {{ $medical_user->name }}
                                    </option>
                                @endforeach
                            </select>

                            @error('user_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <label>{{ __('*Fecha de atención: ') }}</label>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="input-group date" id="attention_date" data-target-input="nearest">
                                <input type="text" name="attention_date" value="" class="form-control datetimepicker-input" placeholder="Fecha de atención" data-target="#attention_date" data-inputmask='"mask": "99/99/9999"' data-greedy="false" data-mask/>
                                <div class="input-group-append" data-target="#attention_date" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <label>{{ __('*Hora de atención: ') }}</label>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="input-group date" id="attention_time" data-target-input="nearest">
                                <input type="text" name="attention_time" class="form-control datetimepicker-input" placeholder="Hora de atención" data-target="#attention_time"/>
                                <div class="input-group-append" data-target="#attention_time" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <h5>Citas Programadas</h5>
                    <hr>
                    <div class="form-row" id="div_doctor_agenda_atencion" style="transform: traslate(0,0); height: 200px;overflow: auto;padding: 10px">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
