@extends('layouts.app_menu')

@section('style')
    <style>
        input[type="text"], textarea {
            text-transform: uppercase;
        }

        .append-ci
        {
            width: 120px;
        }
        .form-group
        {
            margin-bottom: 0px;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Registro de consulta médica</h1>
                </div>

            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            {!! Form::open(['route' => 'enfermera.store', 'method' => 'post', 'id' => 'frm-medical_request']) !!}
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">DATOS DEL PACIENTE</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                @include('medical_request_nurse.partials.form')
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <div class="form-group">
                                    <div class="col-xs-12 text-right">
                                        <a href="{{ route('enfermera.index') }}" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Volver</a>
                                        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Registrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@endsection

@section('script')
<script src="{{ asset('/js/medical_request_nurse/general.js') }}"></script>
<script>
    const citasAppointment = @json(route('citas.get_appointments.getAppointmentsFromUserDate'));
    var url_search_by_nurse = '{{ route('enfermera.search_by_nurse') }}';

    $(document).ready(function() {
        $('a#nav_enfermera').addClass('active');

        // $('#attention_date').val('');
        // $('#attention_date').text('');
    });

    var fecha_atencion = null;
    $("#attention_date").on("change.datetimepicker", ({date, oldDate}) => {
        fecha_atencion = date;
        searchAppointments();
    });

    $('select#user_id').change(function (e) {
        searchAppointments();
        var selected_op = $('select#user_id option:selected').val();
        if(selected_op!=null && selected_op!=""){
            searchAppointments();
        }
    });

    function searchAppointments(){
        var selected_op = $('select#user_id option:selected').val();

        console.log(selected_op, fecha_atencion);
        if(selected_op!=null && selected_op!="" && fecha_atencion!=null){
            var url = citasAppointment;
            var token = $("meta[name=csrf-token]").attr("content");

            $.ajax({
                url: url,
                method: 'post',
                headers: {'X-CSRF-TOKEN': token},
                data: {
                    user_id: selected_op,
                    next_attention_date: fecha_atencion.format('YYYY-MM-DD')
                },
                beforeSend: function(e){
                    var div_alert = '<div class="alert alert-warning col-12">'+
                        '<h5><i class="icon fa fa-spin fa-spinner"></i> ¡Cargando!</h5>'+
                        '<p>Cargando datos de la cita, espere porfavor...</p>'+
                        '</div>';
                    $('div#div_doctor_agenda_atencion').empty();
                    $('div#div_doctor_agenda_atencion').append(div_alert);
                }
            })
            .done(function(response) {
                switch (response['type']) {
                    case "correct":
                        $('div#div_doctor_agenda_atencion').empty();
                        $('div#div_doctor_agenda_atencion').append(response['view']);
                        break;
                    case "error":
                        toastr.error(
                            response['msj'],
                            '¡Error!'
                        );
                        var div_alert = '<div class="alert alert-danger col-12">'+
                            '<h5><i class="icon fa fa-spin fa-spinner"></i> ¡Error!</h5>'+
                            '<p>Existió algun error a la hora de cargar los datos de cita</p>'+
                            '</div>';
                        $('div#div_doctor_agenda').empty();
                        $('div#div_doctor_agenda').append(div_alert);
                        break;
                }
            })
            .fail(function() {
                var div_alert = '<div class="alert alert-danger col-12">'+
                    '<h5><i class="icon fa fa-spin fa-spinner"></i> ¡Error!</h5>'+
                    '<p>Existió algun error a la hora de cargar los datos de cita</p>'+
                    '</div>';
                $('div#div_doctor_agenda').empty();
                $('div#div_doctor_agenda').append(div_alert);
            });
        }
    }
</script>
@endsection
