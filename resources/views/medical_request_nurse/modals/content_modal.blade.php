<div class="modal-body">
    <div class="container-fluid">
        @include('medical_consultations.partials.datos_paciente')
    </div>

    <div class="form-row">
        <label for="signos_vitales" class="control-label col-12">{{ __('Signos vitales') }}</label>
        @include('medical_consultations.partials.form_signos_vitales')
    </div>
</div>
