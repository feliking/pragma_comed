<!-- Modal -->
<div class="modal fade" id="modal_signos_vitales" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('Registro Signos Vitales') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['id' => 'frm_signos_vitales', 'autocomplete' => 'off']) !!}
            <input type="hidden" name="medical_request_id" id="medical_request_id" value="">
            <div id="frm_content_signos_vitales">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                </button>
                <button type="button" class="btn btn-success" onclick="registrar_signos_vitales()" id="btn_guardar_signos">
                    <i class="fa fa-save"></i> Registrar
                </button>
                <button type="button" class="btn btn-warning" onclick="editar_signos_vitales()" hidden="true" id="btn_editar_signos">
                    <i class="fa fa-edit"></i> Modificar
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
